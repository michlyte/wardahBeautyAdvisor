package com.gghouse.wardah.wardahba.webservices.model;

import java.util.List;

public class SalesInputProducts {
    private List<ProductHighlight> highlight;
    private List<ProductHighlight> focus;

    public SalesInputProducts(List<ProductHighlight> highlight, List<ProductHighlight> focus) {
        this.highlight = highlight;
        this.focus = focus;
    }

    public List<ProductHighlight> getHighlight() {
        return highlight;
    }

    public void setHighlight(List<ProductHighlight> highlight) {
        this.highlight = highlight;
    }

    public List<ProductHighlight> getFocus() {
        return focus;
    }

    public void setFocus(List<ProductHighlight> focus) {
        this.focus = focus;
    }
}
