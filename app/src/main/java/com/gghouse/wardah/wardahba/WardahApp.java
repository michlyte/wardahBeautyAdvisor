package com.gghouse.wardah.wardahba;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.gghouse.wardah.wardahba.adapters.PicassoImageLoadingService;
import com.github.pwittchen.prefser.library.Prefser;
import com.google.gson.Gson;

import ss.com.bannerslider.Slider;

/**
 * Created by michaelhalim on 5/6/17.
 */

public class WardahApp extends MultiDexApplication {
    private static WardahApp instance;

    private static Context context;
    private static Prefser prefser;
    private static NotificationManager mNotificationManager;
    private static Gson gson;

    public WardahApp() {
        instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        WardahApp.context = getApplicationContext();
        prefser = new Prefser(getApplicationContext());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        gson = new Gson();

        Slider.init(new PicassoImageLoadingService(context));
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_DEFAULT));
        }
    }

    public static WardahApp getInstance() {
        return instance;
    }

    public Context getAppContext() {
        return WardahApp.context;
    }

    public Prefser getPrefser() {
        return prefser;
    }

    public NotificationManager getNotificationManager() {
        return mNotificationManager;
    }

    public Gson getGson() {
        return gson;
    }
}
