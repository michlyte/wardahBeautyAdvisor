package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.TestRecap

class TestRecapResponse : GenericResponse() {
    var data: TestRecap? = null
}
