package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.DCDropdown
import com.gghouse.wardah.wardahba.webservices.model.DCLocationDropdown

open class VisitCreateRequest {
    val userId: Long
    val location: DCLocationDropdown
    val dc: DCDropdown
    val category: String
    val startTime: Long
    val endTime: Long
    val alarmTime: Long
    var alarmTimeStr: String?
    val notes: String

    constructor(userId: Long, location: DCLocationDropdown, dc: DCDropdown, category: String, startTime: Long, endTime: Long, alarmTime: Long, alarmTimeStr: String?, notes: String) {
        this.userId = userId
        this.location = location
        this.dc = dc
        this.category = category
        this.startTime = startTime
        this.endTime = endTime
        this.alarmTime = alarmTime
        this.alarmTimeStr = alarmTimeStr
        this.notes = notes
    }
}