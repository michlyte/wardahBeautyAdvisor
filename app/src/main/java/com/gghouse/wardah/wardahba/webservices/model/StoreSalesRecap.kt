package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem

class StoreSalesRecap : WardahHistoryRecyclerViewItem {
    val amount: Double
    val quantity: Integer
    val buyer: Integer
    val visitor: Integer

    constructor(amount: Double, quantity: Integer, buyer: Integer, visitor: Integer) : super() {
        this.amount = amount
        this.quantity = quantity
        this.buyer = buyer
        this.visitor = visitor
    }
}
