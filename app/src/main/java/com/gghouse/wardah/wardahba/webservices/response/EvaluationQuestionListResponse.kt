package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.EvaluationQuestion

class EvaluationQuestionListResponse : GenericResponse() {
    var data: List<EvaluationQuestion>? = null
}
