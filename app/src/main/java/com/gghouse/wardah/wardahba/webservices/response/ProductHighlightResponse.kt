package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Pagination
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight

/**
 * Created by michael on 3/20/2017.
 */

class ProductHighlightResponse : GenericResponse() {
    var data: List<ProductHighlight>? = null
    var pagination: Pagination? = null
}
