package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.BAHomepageItem

class Homepage {
    var isPersonalSalesExist: Boolean = false
    var personalDailySales: List<BAHomepageItem>? = null
    var storeHomepageSales: StoreHomepageSales? = null

    //Testing Dummy
    var salesRecap: List<BAHomepageItem>? = null
    var salesByCategories: List<BAHomepageItem>? = null
    var totalSalesHarianVsBulanan: List<BAHomepageItem>? = null
    var salesVsTargetVsCompetitor: List<BAHomepageItem>? = null
    var buyerVsVisitor: List<BAHomepageItem>? = null
    var buyingPower: List<BAHomepageItem>? = null
}
