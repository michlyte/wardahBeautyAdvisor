package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class VisitLocation : Serializable {
    val id: Long
    val name: String
    val latitude: String
    val longitude: String

    constructor(id: Long, name: String, latitude: String, longitude: String) {
        this.id = id
        this.name = name
        this.latitude = latitude
        this.longitude = longitude
    }
}