package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.dummy.StoreDummy
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.enumerations.ViewMode
import com.gghouse.wardah.wardahba.enumerations.WardahItemType
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.DailySalesView
import com.gghouse.wardah.wardahba.webservices.model.MonthlySalesView
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight
import com.gghouse.wardah.wardahba.webservices.model.ProductType
import com.gghouse.wardah.wardahba.webservices.request.StoreDailyCreateRequest
import com.gghouse.wardah.wardahba.webservices.request.StoreDailyEditRequest
import com.gghouse.wardah.wardahba.webservices.request.StoreMonthlyCreateRequest
import com.gghouse.wardah.wardahba.webservices.request.StoreMonthlyEditRequest
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class StoreInputActivityViewModel : ViewModel() {

    val dailySalesViewMutableLiveData: MutableLiveData<DailySalesView> by lazy { MutableLiveData<DailySalesView>() }
    val monthlySalesViewMutableLiveData: MutableLiveData<MonthlySalesView> by lazy { MutableLiveData<MonthlySalesView>() }
    val viewModeMutableLiveData: MutableLiveData<ViewMode> by lazy { MutableLiveData<ViewMode>() }
    val typeMutableLiveData: MutableLiveData<WardahItemType> by lazy { MutableLiveData<WardahItemType>() }
    val productHighlightMutableLiveData: MutableLiveData<List<ProductHighlight>> by lazy { MutableLiveData<List<ProductHighlight>>() }
    val productFocusMutableLiveData: MutableLiveData<List<ProductHighlight>> by lazy { MutableLiveData<List<ProductHighlight>>() }
    val categoryMutableLiveData: MutableLiveData<List<ProductType>> by lazy { MutableLiveData<List<ProductType>>() }
    val categoryInputMutableLiveData: MutableLiveData<List<ProductType>> by lazy { MutableLiveData<List<ProductType>>() }
    val numberOfBuyerMutableLiveData: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    val isSubmitSucceedMutableLiveData: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    var date: Date? = null

    fun getData() {
        val userId = SessionUtil.getUserId()
        if (userId == null) {
            SessionUtil.loggingOut()
        } else {
            when (Config.mode) {
                ModeEnum.DUMMY_DEVELOPMENT -> callStoreSalesInputDummyData(userId)
                ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> {
                }
            }//                    callWardahHistoryData(userId, page);
        }
    }

    fun callStoreSalesInputDummyData(userId: Long?) {
        productHighlightMutableLiveData.postValue(StoreDummy.storeSalesProductHighlight)
        productFocusMutableLiveData.postValue(StoreDummy.storeSalesProductFocus)
        //        getCategoryMutableLiveData().postValue(CategoryDummy.categoryList);
        //
        //        List<Category> categoryInputList = new ArrayList<Category>() {{
        //            add(new Category(0L, "Skin care", 0.00, 0));
        //            add(new Category(1L, "Decorative", 0.00, 0));
        //            add(new Category(2L, "Hair Care", 0.00, 0));
        //        }};
        //        getCategoryInputMutableLiveData().postValue(categoryInputList);
    }

    fun submitDailySales(storeDailyCreateRequest: StoreDailyCreateRequest) {
        val callDailySalesCreate = ApiClient.client.apiDailyCreate(storeDailyCreateRequest)
        callDailySalesCreate.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                        else -> {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSucceedMutableLiveData.postValue(false)
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSucceedMutableLiveData.postValue(false)
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun editDailySales(storeDailyEditRequest: StoreDailyEditRequest) {
        val callDailySalesEdit = ApiClient.client.apiDailyEdit(storeDailyEditRequest)
        callDailySalesEdit.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                        else -> {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSucceedMutableLiveData.postValue(false)
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSucceedMutableLiveData.postValue(false)
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun submitMonthlySales(storeMonthlyCreateRequest: StoreMonthlyCreateRequest) {
        val callDailySalesCreate = ApiClient.client.apiMonthlyCreate(storeMonthlyCreateRequest)
        callDailySalesCreate.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                        else -> {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSucceedMutableLiveData.postValue(false)
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSucceedMutableLiveData.postValue(false)
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun editMonthlySales(storeMonthlyEditRequest: StoreMonthlyEditRequest) {
        val callMonthlySalesEdit = ApiClient.client.apiMonthlyEdit(storeMonthlyEditRequest)
        callMonthlySalesEdit.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                        else -> {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSucceedMutableLiveData.postValue(false)
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSucceedMutableLiveData.postValue(false)
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    companion object {

        private val TAG = StoreInputActivityViewModel::class.java.simpleName
    }
}
