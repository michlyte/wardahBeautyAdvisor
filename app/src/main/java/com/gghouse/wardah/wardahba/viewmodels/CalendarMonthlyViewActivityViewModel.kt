package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum
import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.*
import com.gghouse.wardah.wardahba.webservices.response.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class CalendarMonthlyViewActivityViewModel : ViewModel() {

    val selectedDateMutableLiveData: MutableLiveData<Date> by lazy {
        MutableLiveData<Date>()
    }
    val currentMonthMutableLiveData: MutableLiveData<Date> by lazy {
        MutableLiveData<Date>()
    }
    var userType: UserTypeEnum? = null

    // Event
    val calendarEventMutableLiveData: MutableLiveData<List<BPMonthlyEvent>> by lazy {
        MutableLiveData<List<BPMonthlyEvent>>()
    }
    val eventMutableLiveData: MutableLiveData<Event> by lazy {
        MutableLiveData<Event>()
    }

    // Checklist
    val checklistEventMutableLiveData: MutableLiveData<ChecklistEvent> by lazy {
        MutableLiveData<ChecklistEvent>()
    }

    // Attendant
    val attendantButtonMutableLiveData: MutableLiveData<AttendantButton> by lazy {
        MutableLiveData<AttendantButton>()
    }

    // Event Category
    val eventCategoryListMutableLiveData: MutableLiveData<List<EventCategory>> by lazy {
        MutableLiveData<List<EventCategory>>()
    }
    val visitCategoryListMutableLiveData: MutableLiveData<List<VisitCategory>> by lazy {
        MutableLiveData<List<VisitCategory>>()
    }

    // Check In
    val checkInEventMutableLiveData: MutableLiveData<Event> by lazy {
        MutableLiveData<Event>()
    }
    val checkInRadiusBPMutableLiveData: MutableLiveData<CheckInRadius> by lazy {
        MutableLiveData<CheckInRadius>()
    }
    val checkInRadiusFCMutableLiveData: MutableLiveData<CheckInRadius> by lazy {
        MutableLiveData<CheckInRadius>()
    }

    // Visit
    val calendarVisitMutableLiveData: MutableLiveData<List<FCMonthlyVisit>> by lazy {
        MutableLiveData<List<FCMonthlyVisit>>()
    }
    val visitViewMutableLiveData: MutableLiveData<VisitView> by lazy {
        MutableLiveData<VisitView>()
    }

    // Store Information
    val storeInformationMutableLiveData: MutableLiveData<StoreInformation> by lazy {
        MutableLiveData<StoreInformation>()
    }

    fun getData(date: Date) {
        if (SessionUtil.validateLoginSession()) {
            when (Config.mode) {
                ModeEnum.DUMMY_DEVELOPMENT -> selectedDateMutableLiveData.postValue(Date())
                ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> callEventData(SessionUtil.getUserId()!!, date)
            }
        }
    }

    private fun callEventData(userId: Long, date: Date) {
        when (userType) {
            UserTypeEnum.BEAUTY_PROMOTER_LEADER, UserTypeEnum.BEAUTY_PROMOTOR -> {
                val callEventList = ApiClient.client.apiEventListByMonth(userId,
                        DateTimeUtil.getMonthFromDate(date).toString() + "/" +
                                DateTimeUtil.getYearFromDate(date))
                callEventList.enqueue(object : Callback<MonthlyEventListResponse> {
                    override fun onResponse(call: Call<MonthlyEventListResponse>, response: Response<MonthlyEventListResponse>) {
                        if (response.isSuccessful) {
                            val rps = response.body()
                            when (rps!!.code) {
                                Config.CODE_200 -> calendarEventMutableLiveData.postValue(rps.data)
                                else -> {
                                }
                            }
                        } else {
                            ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                        }
                    }

                    override fun onFailure(call: Call<MonthlyEventListResponse>, t: Throwable) {
                        ToastUtil.retrofitFailMessage(TAG, t.message.toString())
                    }
                })
            }
            UserTypeEnum.FIELD_CONTROLLER -> {
                val callVisitList = ApiClient.client.apiVisitListByMonth(userId,
                        DateTimeUtil.getMonthFromDate(date).toString() + "/" +
                                DateTimeUtil.getYearFromDate(date).toString())
                callVisitList.enqueue(object : Callback<MonthlyVisitListResponse> {
                    override fun onResponse(call: Call<MonthlyVisitListResponse>, response: Response<MonthlyVisitListResponse>) {
                        if (response.isSuccessful) {
                            val rps = response.body()
                            when (rps!!.code) {
                                Config.CODE_200 -> calendarVisitMutableLiveData.postValue(rps.data)
                                else -> {
                                }
                            }
                        } else {
                            ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                        }
                    }

                    override fun onFailure(call: Call<MonthlyVisitListResponse>, t: Throwable) {
                        ToastUtil.retrofitFailMessage(TAG, t.message.toString())
                    }
                })
            }
        }
    }

    fun callChecklistEventData(calendarId: Long) {
        val callChecklistEvent = ApiClient.client.apiChecklistEvent(calendarId)
        callChecklistEvent.enqueue(object : Callback<ChecklistEventResponse> {
            override fun onResponse(call: Call<ChecklistEventResponse>, response: Response<ChecklistEventResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> {
                            val checklistEvent = rps.data
                            checklistEvent!!.calendarId = calendarId
                            checklistEventMutableLiveData.postValue(checklistEvent)
                        }
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<ChecklistEventResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callAttendantEventData(calendarId: Long) {
        val callAttendantButton = ApiClient.client.apiAttendantButton(calendarId)
        callAttendantButton.enqueue(object : Callback<AttendantButtonResponse> {
            override fun onResponse(call: Call<AttendantButtonResponse>, response: Response<AttendantButtonResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> {
                            val attendantButton = rps.data
                            attendantButton!!.calendarId = calendarId
                            attendantButtonMutableLiveData.postValue(attendantButton)
                        }
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<AttendantButtonResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callEventView(calendarId: Long, userId: Long) {
        val callEventView = ApiClient.client.apiEventView(calendarId, userId)
        callEventView.enqueue(object : Callback<EventViewResponse> {
            override fun onResponse(call: Call<EventViewResponse>, response: Response<EventViewResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> eventMutableLiveData.postValue(rps.data)
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<EventViewResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callEventCategory(userId: Long) {
        val callEventCategory = ApiClient.client.apiEventCategory(userId)
        callEventCategory.enqueue(object : Callback<EventCategoryResponse> {
            override fun onResponse(call: Call<EventCategoryResponse>, response: Response<EventCategoryResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> eventCategoryListMutableLiveData.postValue(rps.data)
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<EventCategoryResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callVisitCategory(userId: Long) {
        val callVisitCategory = ApiClient.client.apiVisitCategory(userId)
        callVisitCategory.enqueue(object : Callback<VisitCategoryResponse> {
            override fun onResponse(call: Call<VisitCategoryResponse>, response: Response<VisitCategoryResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> visitCategoryListMutableLiveData.postValue(rps.data)
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<VisitCategoryResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callVisitView(calendarId: Long) {
        val callVisitView = ApiClient.client.apiVisitView(calendarId)
        callVisitView.enqueue(object : Callback<VisitViewResponse> {
            override fun onResponse(call: Call<VisitViewResponse>, response: Response<VisitViewResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> visitViewMutableLiveData.postValue(rps.data)
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<VisitViewResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callStoreInformation(storeId: Long) {
        val callStoreInformation = ApiClient.client.apiStoreInformation(storeId)
        callStoreInformation.enqueue(object : Callback<StoreInformationResponse> {
            override fun onResponse(call: Call<StoreInformationResponse>, response: Response<StoreInformationResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> {
                            val storeInformation = rps.data
                            storeInformation!!.storeId = storeId
                            storeInformationMutableLiveData.postValue(storeInformation)
                        }
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<StoreInformationResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callCheckInRadius(userTypeEnum: UserTypeEnum, userId: Long, calendarId: Long) {
        val callCheckInRadius = ApiClient.client.apiCheckInRadius(userId, calendarId)
        callCheckInRadius.enqueue(object : Callback<CheckInRadiusResponse> {
            override fun onResponse(call: Call<CheckInRadiusResponse>, response: Response<CheckInRadiusResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> when (userTypeEnum) {
                            UserTypeEnum.FIELD_CONTROLLER -> {
                                var checkInRadius = rps.data
                                checkInRadius!!.calendarId = calendarId
                                checkInRadiusFCMutableLiveData.postValue(checkInRadius)
                            }
                            UserTypeEnum.BEAUTY_PROMOTOR, UserTypeEnum.BEAUTY_PROMOTER_LEADER -> {
                                var checkInRadius = rps.data
                                checkInRadius!!.calendarId = calendarId
                                checkInRadiusBPMutableLiveData.postValue(checkInRadius)
                            }
                        }
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<CheckInRadiusResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    companion object {

        private val TAG = CalendarMonthlyViewActivityViewModel::class.java.simpleName
    }
}
