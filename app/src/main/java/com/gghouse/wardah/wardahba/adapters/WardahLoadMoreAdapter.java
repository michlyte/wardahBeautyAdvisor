package com.gghouse.wardah.wardahba.adapters;

import android.content.Context;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.interfaces.OnLoadMoreListListener;

public class WardahLoadMoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected OnLoadMoreListListener mOnLoadMoreListListener;
    protected boolean isLoading;
    protected int lastVisibleItem, totalItemCount;

    protected final Context mContext;
    protected final RecyclerView mRecyclerView;

    public WardahLoadMoreAdapter(Context context, RecyclerView recyclerView) {
        mContext = context;
        mRecyclerView = recyclerView;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + Config.Companion.getHISTORY_ITEM_PER_PAGE())) {
                    if (mOnLoadMoreListListener != null) {
                        mOnLoadMoreListListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void removeOnLoadMoreListener() {
        this.mOnLoadMoreListListener = null;
    }

    public void setOnLoadMoreListener(OnLoadMoreListListener mOnLoadMoreListListener) {
        this.mOnLoadMoreListListener = mOnLoadMoreListListener;
    }
}
