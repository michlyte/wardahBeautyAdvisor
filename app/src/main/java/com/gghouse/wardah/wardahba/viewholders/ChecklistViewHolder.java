package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.ChecklistListener;
import com.gghouse.wardah.wardahba.webservices.model.Checklist;

public class ChecklistViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final TextView mTitleTextView;
    private final CheckBox mValueCheckbox;
    private Checklist mItem;

    public ChecklistViewHolder(View view) {
        super(view);

        this.view = view;
        mTitleTextView = view.findViewById(R.id.titleEditText);
        mValueCheckbox = view.findViewById(R.id.valueCheckbox);
    }

    private void setupActionEvent(ChecklistListener checklistListener) {
        mValueCheckbox.setOnCheckedChangeListener((compoundButton, b) -> checklistListener.onCheck(mItem, b));
    }

    public void setData(ViewMode viewMode, Checklist checklist, ChecklistListener checklistListener) {
        mItem = checklist;

        mTitleTextView.setText(mItem.getContent());
        mValueCheckbox.setChecked(mItem.getStatus());

        switch (viewMode) {
            case VIEW:
                mValueCheckbox.setEnabled(false);
                break;
            case INPUT:
            case EDIT:
                setupActionEvent(checklistListener);
                break;
        }
    }
}