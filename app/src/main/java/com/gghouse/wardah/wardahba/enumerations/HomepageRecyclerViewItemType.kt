package com.gghouse.wardah.wardahba.enumerations

enum class HomepageRecyclerViewItemType {
    NORMAL,
    TWO_DOTS,
    THREE_DOTS
}
