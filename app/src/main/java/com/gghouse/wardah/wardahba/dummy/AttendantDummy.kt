package com.gghouse.wardah.wardahba.dummy

import com.gghouse.wardah.wardahba.webservices.model.Attendant
import java.util.*

object AttendantDummy {
    val ATTENDANT_SIMPLE_LIST: ArrayList<Attendant> = object : ArrayList<Attendant>() {
        init {
            add(Attendant(0L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri"))
            add(Attendant(1L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri"))
            add(Attendant(2L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri"))
        }
    }

    val ATTENDANT_EVENT_LIST: List<Attendant> = object : ArrayList<Attendant>() {
        init {
            add(Attendant(0L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri"))
            add(Attendant(1L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri"))
            add(Attendant(2L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri"))
        }
    }
}