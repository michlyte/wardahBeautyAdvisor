package com.gghouse.wardah.wardahba.webservices.model

class BAHomeInputPerProductTypes {
    val amount: Double
    val quantity: Integer
    val productTypes: List<ProductType>

    constructor(amount: Double, quantity: Integer, productTypes: List<ProductType>) {
        this.amount = amount
        this.quantity = quantity
        this.productTypes = productTypes
    }
}