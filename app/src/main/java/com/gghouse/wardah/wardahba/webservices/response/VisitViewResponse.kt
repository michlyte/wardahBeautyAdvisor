package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.VisitView

class VisitViewResponse : GenericResponse() {
    var data: VisitView? = null
}
