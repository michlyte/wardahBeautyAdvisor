package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.enumerations.HomepageRecyclerViewItem
import com.gghouse.wardah.wardahba.enumerations.HomepageRecyclerViewItemType

class StoreHomepageNormalItem(var title: String?, var value: String?) : HomepageRecyclerViewItem(HomepageRecyclerViewItemType.NORMAL)
