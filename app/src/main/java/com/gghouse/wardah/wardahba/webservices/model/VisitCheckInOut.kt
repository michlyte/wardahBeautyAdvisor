package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import java.io.Serializable

class VisitCheckInOut : Serializable {
    val buttonShown: String
    var checkInTime: Long? = null
    var checkOutTime: Long? = null
    var gpsStatus: String? = null
    var fcImageURL: String? = null

    constructor(buttonShown: String) {
        this.buttonShown = buttonShown
    }

    val checkInTimeStr: String
        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkInTime)

    val checkOutTimeStr: String
        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkOutTime)
}