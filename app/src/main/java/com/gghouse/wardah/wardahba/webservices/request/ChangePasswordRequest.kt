package com.gghouse.wardah.wardahba.webservices.request

/**
 * Created by michaelhalim on 6/9/17.
 */

class ChangePasswordRequest(private val userId: Long?, private val oldPassword: String, private val password: String)
