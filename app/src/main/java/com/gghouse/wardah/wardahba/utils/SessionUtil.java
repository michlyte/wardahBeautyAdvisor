package com.gghouse.wardah.wardahba.utils;

import com.gghouse.wardah.wardahba.WardahApp;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.User;
import com.github.pwittchen.prefser.library.Prefser;

/**
 * Created by michaelhalim on 1/25/17.
 */

public abstract class SessionUtil {
    private static final String TAG = SessionUtil.class.getSimpleName();
    private static final String KEY_IS_LOGIN = "KEY_IS_LOGIN";
    private static final String KEY_USER = "KEY_USER";
    private static final String KEY_SALES_NEED_REFRESH = "SP_SALES_NEED_REFRESH";
    private static final String KEY_IP_ADDRESS = "SP_IP_ADDRESS";

    public static boolean isLoggedIn() {
        String methodName = "isLoggedIn";
        LogUtil.INSTANCE.log(LogUtil.INSTANCE.getLOG_BEGIN() + methodName);
        boolean isLoggedIn;
        Prefser prefser = WardahApp.getInstance().getPrefser();
        if (prefser.contains(KEY_IS_LOGIN) && prefser.get(KEY_IS_LOGIN, Boolean.class, false)) {
            isLoggedIn = true;
        } else {
            isLoggedIn = false;
        }
        LogUtil.INSTANCE.log(LogUtil.INSTANCE.getLOG_END() + methodName + " " + isLoggedIn);
        return isLoggedIn;
    }

    public static void loggingIn(User user) {
        String methodName = "loggingIn";
        LogUtil.INSTANCE.log(LogUtil.INSTANCE.getLOG_BEGIN() + methodName);
        Prefser prefser = WardahApp.getInstance().getPrefser();
        if (prefser.contains(KEY_IS_LOGIN) && prefser.get(KEY_IS_LOGIN, Boolean.class, false)) {
            LogUtil.INSTANCE.log(LogUtil.INSTANCE.getLOG_END() + methodName + " : Something wrong you are logged in already.");
        } else {
            prefser.put(KEY_IS_LOGIN, true);
            prefser.put(KEY_USER, user);

            LogUtil.INSTANCE.log(LogUtil.INSTANCE.getLOG_END() + methodName + " success.");
        }
    }

    public static void loggingOut() {
        String methodName = "loggingOut";
        LogUtil.INSTANCE.log(LogUtil.INSTANCE.getLOG_BEGIN() + methodName);
        Prefser prefser = WardahApp.getInstance().getPrefser();
        if (prefser.contains(KEY_IS_LOGIN)) {
            if (prefser.get(KEY_IS_LOGIN, Boolean.class, false)) {
                prefser.remove(KEY_IS_LOGIN);
                prefser.remove(KEY_USER);

                LogUtil.INSTANCE.log(LogUtil.INSTANCE.getLOG_END() + methodName + " success.");
            } else {
                LogUtil.INSTANCE.log(LogUtil.INSTANCE.getLOG_END() + methodName + " : Something wrong you are logged out already.");
            }
        } else {
            LogUtil.INSTANCE.log(methodName + " : Something wrong you do not have log in session.");
        }
    }

    public static boolean validateLoginSession() {
        Long userId = SessionUtil.getUserId();
        if (userId == null) {
            loggingOut();
            return false;
        } else {
            return true;
        }
    }

    public static User getUser() {
        Prefser prefser = WardahApp.getInstance().getPrefser();

        if (prefser.contains(KEY_USER)) {
            return prefser.get(KEY_USER, User.class, new User());
        } else {
            LogUtil.INSTANCE.log("Error: could not find user.");
            return null;
        }
    }

    public static Long getUserId() {
        Prefser prefser = WardahApp.getInstance().getPrefser();

        if (prefser.contains(KEY_USER)) {
            User user = prefser.get(KEY_USER, User.class, new User());
            try {
                return user.getUserId();
            } catch (NullPointerException npe) {
                return null;
            }
        } else {
            LogUtil.INSTANCE.log("Error: could not find user id.");
            return null;
        }
    }

    public static Long getUserLocationId() {
        Prefser prefser = WardahApp.getInstance().getPrefser();

        if (prefser.contains(KEY_USER)) {
            User user = prefser.get(KEY_USER, User.class, new User());
            try {
                return user.getLocationId();
            } catch (NullPointerException npe) {
                return null;
            }
        } else {
            LogUtil.INSTANCE.log("Error: could not find user id.");
            return null;
        }
    }

    public static String getUserLocationName() {
        Prefser prefser = WardahApp.getInstance().getPrefser();

        if (prefser.contains(KEY_USER)) {
            User user = prefser.get(KEY_USER, User.class, new User());
            try {
                return user.getLocation();
            } catch (NullPointerException npe) {
                return null;
            }
        } else {
            LogUtil.INSTANCE.log("Error: could not find user location name.");
            return null;
        }
    }

    public static String getUserType() {
        Prefser prefser = WardahApp.getInstance().getPrefser();

        if (prefser.contains(KEY_USER)) {
            User user = prefser.get(KEY_USER, User.class, new User());
            try {
                return user.getPosition();
            } catch (NullPointerException npe) {
                return null;
            }
        } else {
            LogUtil.INSTANCE.error("Could not find user type");
            return null;
        }
    }

    public static UserTypeEnum getUserTypeEnum() {
        Prefser prefser = WardahApp.getInstance().getPrefser();

        if (prefser.contains(KEY_USER)) {
            User user = prefser.get(KEY_USER, User.class, new User());
            try {
                return UserTypeEnum.Companion.getUserTypeEnum(user.getPosition());
            } catch (NullPointerException npe) {
                return null;
            }
        } else {
            LogUtil.INSTANCE.error("Could not find user type");
            return null;
        }
    }

    public static void saveIPAddress(String ipAddress) {
        Prefser prefser = WardahApp.getInstance().getPrefser();
        prefser.put(KEY_IP_ADDRESS, ipAddress);
        ApiClient.INSTANCE.generateClientWithNewIP(ipAddress);
    }

    public static String loadIPAddress() {
        Prefser prefser = WardahApp.getInstance().getPrefser();
        return prefser.get(KEY_IP_ADDRESS, String.class, Config.Companion.getBASE_URL());
    }

    public static void setSalesNeedRefresh(boolean value) {
        Prefser prefser = WardahApp.getInstance().getPrefser();
        prefser.put(KEY_SALES_NEED_REFRESH, value);
    }

    public static boolean doesSalesNeedRefresh() {
        Prefser prefser = WardahApp.getInstance().getPrefser();
        return prefser.get(KEY_SALES_NEED_REFRESH, Boolean.class, false);
    }
}
