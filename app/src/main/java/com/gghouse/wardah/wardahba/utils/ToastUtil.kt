package com.gghouse.wardah.wardahba.utils

import android.util.Log
import android.widget.Toast

import com.gghouse.wardah.wardahba.WardahApp

/**
 * Created by ecquaria-macmini on 5/26/17.
 */

object ToastUtil {
    fun toastMessage(text: String) {
        Toast.makeText(WardahApp.getInstance().appContext, text, Toast.LENGTH_SHORT).show()
    }

    fun toastMessage(tag: String, text: String) {
        Log.d(tag, text)
        Toast.makeText(WardahApp.getInstance().appContext, text, Toast.LENGTH_SHORT).show()
    }

    fun toastMessage(resId: Int?) {
        Toast.makeText(WardahApp.getInstance().appContext,
                WardahApp.getInstance().appContext.getString(resId!!), Toast.LENGTH_SHORT).show()
    }

    fun toastMessage(tag: String, resId: Int?) {
        Log.d(tag, WardahApp.getInstance().appContext.getString(resId!!))
        Toast.makeText(WardahApp.getInstance().appContext,
                WardahApp.getInstance().appContext.getString(resId), Toast.LENGTH_SHORT).show()
    }

    /**
     * Retrofit
     */

    fun retrofitFailMessage(tag: String, message: String) {
        Toast.makeText(WardahApp.getInstance().appContext,
                message, Toast.LENGTH_SHORT).show()
    }

    fun retrofitFailMessage(tag: String, resId: Int?) {
        Toast.makeText(WardahApp.getInstance().appContext,
                WardahApp.getInstance().appContext.getString(resId!!), Toast.LENGTH_SHORT).show()
    }
}
