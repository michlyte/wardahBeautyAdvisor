package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import com.gghouse.wardah.wardahba.utils.WardahUtil
import java.io.Serializable

class SalesTarget : WardahHistoryRecyclerViewItem, Serializable {
    val salesTargetId: Long
    var userId: Long? = null
    val locationId: Long
    val month: Int
    val year: Int
    val salesTarget: Double
    var editable: Boolean
    val location: String
    val dc: String
    val dcId: Long

    constructor(salesTargetId: Long, userId: Long?, locationId: Long, month: Int, year: Int, salesTarget: Double, editable: Boolean, location: String, dc: String, dcId: Long) : super() {
        this.salesTargetId = salesTargetId
        this.userId = userId
        this.locationId = locationId
        this.month = month
        this.year = year
        this.salesTarget = salesTarget
        this.editable = editable
        this.location = location
        this.dc = dc
        this.dcId = dcId
    }

    val dateStr: String
        get() = DateTimeUtil.sdfMonthYear.format(DateTimeUtil.getDateFromMonthYear(month, year))

    val salesTargetStr: String
        get() = WardahUtil.getAmount(salesTarget)
}