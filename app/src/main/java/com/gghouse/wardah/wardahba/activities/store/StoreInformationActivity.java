package com.gghouse.wardah.wardahba.activities.store;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.adapters.WardahSimpleAdapter;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.viewholders.StoreInformationActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.DailySales;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySales;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;
import com.gghouse.wardah.wardahba.webservices.model.StoreInformation;

import java.util.ArrayList;

public class StoreInformationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = StoreInformationActivity.class.getSimpleName();

    private static final int ACTIVITY_RESULT_OK = 1;

    private StoreInformationActivityViewModel mModel;

    private TextView mDCTextView;
    private TextView mProvinceTextView;
    private TextView mEvaluationPeriodTextView;
    private TextView mTotalBATextView;
    private TextView mEvaluationCompletedTextView;
    private TextView mTargetSalesTextView;
    private TextView mCompetitorTurnoverTextView;
    private TextView mSalesSummaryTextView;
    private WardahSimpleAdapter mSalesAdapter;
    private WardahSimpleAdapter mDailyStoreSalesAdapter;
    private WardahSimpleAdapter mMonthlyStoreSalesAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_information);

        mModel = ViewModelProviders.of(this).get(StoreInformationActivityViewModel.class);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        mDCTextView = findViewById(R.id.dcTextView);
        mProvinceTextView = findViewById(R.id.provinceTextView);
        mEvaluationPeriodTextView = findViewById(R.id.evaluationPeriodTextView);
        mTotalBATextView = findViewById(R.id.totalBATextView);
        mEvaluationCompletedTextView = findViewById(R.id.numOfEvaluationCompletedTextView);
        mTargetSalesTextView = findViewById(R.id.targetSalesTextView);
        mCompetitorTurnoverTextView = findViewById(R.id.competitorTurnoverTextView);
        mSalesSummaryTextView = findViewById(R.id.categorySummaryTextView);
        RecyclerView mSalesRecyclerView = findViewById(R.id.categoryRecyclerView);
        mSalesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSalesAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.CATEGORY, new ArrayList<>());
        mSalesRecyclerView.setAdapter(mSalesAdapter);
        mSalesRecyclerView.setNestedScrollingEnabled(false);

        WardahListener mDailyStoreSalesListener = (item, viewMode) -> {
            DailySales dailySales = (DailySales) item;
            mModel.callDailySalesView(dailySales.getDailyReportId());
        };
        RecyclerView mDailyStoreSalesRecyclerView = findViewById(R.id.storeSalesHarianRecyclerView);
        mDailyStoreSalesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDailyStoreSalesAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.STORE_DAILY_SALES, new ArrayList<>(), mDailyStoreSalesListener);
        mDailyStoreSalesRecyclerView.setAdapter(mDailyStoreSalesAdapter);
        mDailyStoreSalesRecyclerView.setNestedScrollingEnabled(false);
        findViewById(R.id.moreStoreSalesHarianLinearLayout).setOnClickListener(this);

        WardahListener mMonthlyStoreSalesListener = (item, viewMode) -> {
            MonthlySales monthlySales = (MonthlySales) item;
            mModel.callMonthlySalesView(monthlySales.getMonthlyReportId());
        };
        RecyclerView mMonthlyStoreSalesRecyclerView = findViewById(R.id.storeSalesBulananRecyclerView);
        mMonthlyStoreSalesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMonthlyStoreSalesAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.STORE_MONTHLY_SALES, new ArrayList<>(), mMonthlyStoreSalesListener);
        mMonthlyStoreSalesRecyclerView.setAdapter(mMonthlyStoreSalesAdapter);
        mMonthlyStoreSalesRecyclerView.setNestedScrollingEnabled(false);
        findViewById(R.id.moreStoreSalesBulananLinearLayout).setOnClickListener(this);

        try {
            mModel.getStoreInformationMutableLiveData().postValue((StoreInformation) intent.getSerializableExtra(IntentUtil.Companion.getSTORE_INFORMATION()));
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

        mModel.getStoreInformationMutableLiveData().observe(this, storeInformation -> {
            if (storeInformation != null) {
                mModel.setStoreId(storeInformation.getStoreId());

                mDCTextView.setText(storeInformation.getDc());
                mProvinceTextView.setText(storeInformation.getProvince());
                mEvaluationPeriodTextView.setText(storeInformation.getPeriod());
                mTotalBATextView.setText(String.valueOf(storeInformation.getTotalBA()));
                mEvaluationCompletedTextView.setText(String.valueOf(storeInformation.getEvaluationCompleted()));
                mTargetSalesTextView.setText(storeInformation.getTargetSalesStr());
                mCompetitorTurnoverTextView.setText(storeInformation.getCompetitorTurnoverStr());

                Double salesByCategoryAmount = 0.00;
                int salesByCateogryPcs = 0;
                for (ProductType productType : storeInformation.getProductTypes()) {
                    salesByCategoryAmount += productType.getAmount();
                    salesByCateogryPcs += productType.getQuantity();
                }
                mSalesSummaryTextView.setText(WardahUtil.INSTANCE.getStoreSalesAmountAndPcsContent(salesByCategoryAmount, salesByCateogryPcs));
                mSalesAdapter.setData(storeInformation.getProductTypes());
                mSalesAdapter.notifyDataSetChanged();

                mDailyStoreSalesAdapter.setData(storeInformation.getStoreSalesDailyHistory());
                mDailyStoreSalesAdapter.notifyDataSetChanged();

                mMonthlyStoreSalesAdapter.setData(storeInformation.getStoreSalesMonthlyHistory());
                mMonthlyStoreSalesAdapter.notifyDataSetChanged();
            }
        });

        mModel.getDailySalesViewMutableLiveData().observe(this, dailySalesView -> {
            if (dailySalesView != null) {
                Intent iInputDailyStoreSales = new Intent(this, StoreInputActivity.class);
                iInputDailyStoreSales.putExtra(IntentUtil.Companion.getDATA(), dailySalesView);
                iInputDailyStoreSales.putExtra(IntentUtil.Companion.getDATE(), dailySalesView.getInputDate());
                iInputDailyStoreSales.putExtra(IntentUtil.Companion.getTYPE(), WardahItemType.STORE_DAILY_SALES);

                if (dailySalesView.getEditable()) {
                    iInputDailyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                    startActivityForResult(iInputDailyStoreSales, ACTIVITY_RESULT_OK);
                } else {
                    iInputDailyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.VIEW);
                    startActivity(iInputDailyStoreSales);
                }
            }
        });

        mModel.getMonthlySalesViewMutableLiveData().observe(this, monthlySalesView -> {
            if (monthlySalesView != null) {
                Intent iEditMonthlyStoreSales = new Intent(this, StoreInputActivity.class);
                iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getDATA(), monthlySalesView);
                iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getDATE(), DateTimeUtil.INSTANCE.getDateFromMonthYear(monthlySalesView.getMonth(), monthlySalesView.getYear()).getTime());
                iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getTYPE(), WardahItemType.STORE_MONTHLY_SALES);

                if (monthlySalesView.getEditable()) {
                    iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                    startActivityForResult(iEditMonthlyStoreSales, ACTIVITY_RESULT_OK);
                } else {
                    iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.VIEW);
                    startActivity(iEditMonthlyStoreSales);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        // TODO: hardcoded
        switch (view.getId()) {
            case R.id.moreStoreSalesHarianLinearLayout:
                if (mModel.getStoreInformationMutableLiveData().getValue() != null) {
                    Intent iStoreSalesHistory = new Intent(getBaseContext(), StoreSalesHistoryActivity.class);
                    iStoreSalesHistory.putExtra(IntentUtil.Companion.getLOCATION_ID(), mModel.getStoreInformationMutableLiveData().getValue().getLocationId());
                    iStoreSalesHistory.putExtra(IntentUtil.Companion.getFREQUENT(), WardahItemType.STORE_DAILY_SALES);
                    startActivity(iStoreSalesHistory);
                }
                break;
            case R.id.moreStoreSalesBulananLinearLayout:
                if (mModel.getStoreInformationMutableLiveData().getValue() != null) {
                    Intent iStoreSalesHistory = new Intent(getBaseContext(), StoreSalesHistoryActivity.class);
                    iStoreSalesHistory.putExtra(IntentUtil.Companion.getLOCATION_ID(), mModel.getStoreInformationMutableLiveData().getValue().getLocationId());
                    iStoreSalesHistory.putExtra(IntentUtil.Companion.getFREQUENT(), WardahItemType.STORE_MONTHLY_SALES);
                    startActivity(iStoreSalesHistory);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ACTIVITY_RESULT_OK && resultCode == Activity.RESULT_OK) {
            mModel.getData();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
