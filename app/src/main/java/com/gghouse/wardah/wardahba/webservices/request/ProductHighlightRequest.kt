package com.gghouse.wardah.wardahba.webservices.request

class ProductHighlightRequest(var id: Long, var quantity: Integer)