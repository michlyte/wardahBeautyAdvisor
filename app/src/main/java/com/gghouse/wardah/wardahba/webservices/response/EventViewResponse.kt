package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Event

class EventViewResponse : GenericResponse() {
    var data: Event? = null
}
