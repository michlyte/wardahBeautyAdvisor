package com.gghouse.wardah.wardahba.webservices.model

class BPMonthlyEvent {
    val date: Long
    val events: List<EventCalendar>

    constructor(date: Long, events: List<EventCalendar>) {
        this.date = date
        this.events = events
    }
}