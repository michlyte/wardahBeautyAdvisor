package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;
import com.squareup.picasso.Picasso;

public class ProductHighlightStoreDetailHistoryViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final ImageView mImageView;
    private final TextView mTitleTextView;
    private final TextView mPcsTextView;
    private final TextView mAmountTextView;
    public ProductHighlight mItem;

    public ProductHighlightStoreDetailHistoryViewHolder(View view) {
        super(view);

        this.view = view;
        mImageView = view.findViewById(R.id.iv_image);
        mPcsTextView = view.findViewById(R.id.pcsTextView);
        mAmountTextView = view.findViewById(R.id.amountTextView);
        mTitleTextView = view.findViewById(R.id.titleEditText);
    }

    public void setData(ProductHighlight productHighlight) {
        mItem = productHighlight;

        String pcs = mItem.getQuantity() + " pcs";
        String amount = WardahUtil.INSTANCE.getAmount(mItem.getPrice());

        if (mItem.getImgURL() != null && !mItem.getImgURL().isEmpty()) {
            Picasso.get()
                    .load(ImageUtil.INSTANCE.getImageUrl(mItem.getImgURL()))
                    .fit()
                    .centerCrop()
                    .into(mImageView);
        } else {
            Picasso.get()
                    .load(R.drawable.pic_image_not_found)
                    .fit()
                    .centerCrop()
                    .into(mImageView);
        }

        mPcsTextView.setText(pcs);
        mAmountTextView.setText(amount);
        mTitleTextView.setText(mItem.getName());
    }
}