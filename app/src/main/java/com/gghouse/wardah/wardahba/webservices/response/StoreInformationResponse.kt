package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.StoreInformation

class StoreInformationResponse : GenericResponse() {
    var data: StoreInformation? = null
}
