package com.gghouse.wardah.wardahba.webservices.model

class BAHome {
    val userId: Long
    val lastQuestionId: Long
    val userType: String
    val salesOverview: BAHomeSalesOverview
    val location: BAHomeLocation

    constructor(userId: Long, lashtQuestionId: Long, userType: String, salesOverview: BAHomeSalesOverview, location: BAHomeLocation) {
        this.userId = userId
        this.lastQuestionId = lashtQuestionId
        this.userType = userType
        this.salesOverview = salesOverview
        this.location = location
    }
}