package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import java.io.Serializable

class Event : Serializable {
    val viewType: String
    val calendarId: Long
    val title: String
    val category: String
    val startTime: Long
    val endTime: Long
    var alarmTime: Long? = null
    var alarmTimeStr: String? = null
    val notes: String
    val location: EventLocation
    var checkIn: CheckInEvent? = null
    val checklist: List<Checklist>
    var attendant: List<Attendant>? = null
    var categories: List<EventCategory>? = null

    constructor(viewType: String, calendarId: Long, title: String, category: String, startTime: Long, endTime: Long, notes: String, location: EventLocation, checklist: List<Checklist>, attendant: List<Attendant>) {
        this.viewType = viewType
        this.calendarId = calendarId
        this.title = title
        this.category = category
        this.startTime = startTime
        this.endTime = endTime
        this.notes = notes
        this.location = location
        this.checklist = checklist
        this.attendant = attendant
    }

    constructor(viewType: String, calendarId: Long, title: String, category: String, startTime: Long, endTime: Long, notes: String, location: EventLocation, checkIn: CheckInEvent, checklist: List<Checklist>, attendant: List<Attendant>) {
        this.viewType = viewType
        this.calendarId = calendarId
        this.title = title
        this.category = category
        this.startTime = startTime
        this.endTime = endTime
        this.notes = notes
        this.location = location
        this.checkIn = checkIn
        this.checklist = checklist
        this.attendant = attendant
    }

    val startTimeStr: String
        get() = DateTimeUtil.sdfEventStartEndTime.format(startTime)

    val endTimeStr: String
        get() = DateTimeUtil.sdfEventStartEndTime.format(endTime)

    val dateStr: String
        get() = DateTimeUtil.sdfDateMonthYear.format(startTime)

    val timeStr: String
        get() = DateTimeUtil.sdfEventTime.format(startTime) + " - " + DateTimeUtil.sdfEventTime.format(endTime)

    val alarmStr: String
        get() = DateTimeUtil.sdfEventStartEndTime.format(alarmTime)

//    val checklistStr: String
//        get() = numOfChecked.toString() + "/" + numOfChecklist.toString() + " checklist completed"

    val numOfAttendantStr: String
        get() = "${attendant?.size} peserta"

    val checkInStr: String
        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkIn?.checkInTime)

//    val checkOutStr: String
//        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkOut)

//    val targetSalesStr: String
//        get() = WardahUtil.getAmount(targetSales)
//
//    val totalSalesStr: String
//        get() = WardahUtil.getAmount(totalSales)

}