package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.User

/**
 * Created by michael on 3/10/2017.
 */

class LoginResponse : GenericResponse() {
    val data: User? = null
}
