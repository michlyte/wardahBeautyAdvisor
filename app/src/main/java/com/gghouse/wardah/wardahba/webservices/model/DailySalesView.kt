package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class DailySalesView : Serializable {
    var dailyReportId: Long? = null
    val userId: Long
    val inputDate: Long
    val locationId: Long
    val buyer: Integer
    val visitor: Integer
    val buyingPower: BuyingPower
    val editable: Boolean
    val salesOverview: BAHomeSalesOverview
    val highlight: List<ProductHighlight>
    val focus: List<ProductHighlight>

    constructor(userId: Long, inputDate: Long, locationId: Long, buyer: Integer, visitor: Integer, buyingPower: BuyingPower, editable: Boolean, salesOverview: BAHomeSalesOverview, highlight: List<ProductHighlight>, focus: List<ProductHighlight>) {
        this.userId = userId
        this.inputDate = inputDate
        this.locationId = locationId
        this.buyer = buyer
        this.visitor = visitor
        this.buyingPower = buyingPower
        this.editable = editable
        this.salesOverview = salesOverview
        this.highlight = highlight
        this.focus = focus
    }
}