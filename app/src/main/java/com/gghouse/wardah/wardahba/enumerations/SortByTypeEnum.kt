package com.gghouse.wardah.wardahba.enumerations

import com.gghouse.wardah.wardahba.utils.LogUtil

enum class SortByTypeEnum(var title: String?, var value: String?) {
    NAME("Nama", "Name"),
    PRICE("Harga", "Price"),
    QTY("Kuantitas", "Quantity");

    companion object {
        fun getSortByTypeEnum(title: String): SortByTypeEnum {
            for (sortByTypeEnum in values()) {
                if (sortByTypeEnum.title == title) {
                    return sortByTypeEnum
                }
            }
            LogUtil.log("value [$title] is not supported.")
            return NAME
        }
    }
}
