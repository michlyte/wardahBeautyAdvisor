package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.QuestionerLatestDate

class QuestionerLatestDateResponse : GenericResponse() {
    var data: QuestionerLatestDate? = null
}
