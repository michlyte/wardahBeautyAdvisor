package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.VisitCategory

class VisitCategoryResponse : GenericResponse() {
    var data: List<VisitCategory>? = null
}
