package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.Checklist

class ChecklistEventEditRequest {
    val calendarId: Long
    val checklist: List<Checklist>

    constructor(calendarId: Long, checklist: List<Checklist>) {
        this.calendarId = calendarId
        this.checklist = checklist
    }
}