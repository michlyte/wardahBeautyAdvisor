package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.BuyingPower

class StoreDailyEditRequest {
    val dailyReportId: Long
    val buyer: Integer
    val visitor: Integer
    val buyingPower: BuyingPower

    constructor(dailyReportId: Long, buyer: Integer, visitor: Integer, buyingPower: BuyingPower) {
        this.dailyReportId = dailyReportId
        this.buyer = buyer
        this.visitor = visitor
        this.buyingPower = buyingPower
    }
}