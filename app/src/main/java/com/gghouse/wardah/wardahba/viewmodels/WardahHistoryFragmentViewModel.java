package com.gghouse.wardah.wardahba.viewmodels;

import android.os.Handler;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.adapters.WardahHistoryAdapterWithHeader;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.dummy.SalesDummy;
import com.gghouse.wardah.wardahba.dummy.StoreDummy;
import com.gghouse.wardah.wardahba.dummy.TestDummy;
import com.gghouse.wardah.wardahba.enumerations.OrderByEnum;
import com.gghouse.wardah.wardahba.enumerations.SortByTypeEnum;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.models.WardahItem;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.DailySalesView;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySalesView;
import com.gghouse.wardah.wardahba.webservices.model.Pagination;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;
import com.gghouse.wardah.wardahba.webservices.model.Sales;
import com.gghouse.wardah.wardahba.webservices.model.SalesEditProducts;
import com.gghouse.wardah.wardahba.webservices.model.SalesRecap;
import com.gghouse.wardah.wardahba.webservices.model.StoreSales;
import com.gghouse.wardah.wardahba.webservices.model.StoreSalesBulanan;
import com.gghouse.wardah.wardahba.webservices.model.StoreSalesRecap;
import com.gghouse.wardah.wardahba.webservices.model.Test;
import com.gghouse.wardah.wardahba.webservices.response.DailySalesResponse;
import com.gghouse.wardah.wardahba.webservices.response.DailySalesViewResponse;
import com.gghouse.wardah.wardahba.webservices.response.HistoryTestResponse;
import com.gghouse.wardah.wardahba.webservices.response.MonthlySalesResponse;
import com.gghouse.wardah.wardahba.webservices.response.MonthlySalesViewResponse;
import com.gghouse.wardah.wardahba.webservices.response.ProductHighlightResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesEditProductResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesListResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesRecapResponse;
import com.gghouse.wardah.wardahba.webservices.response.StoreSalesRecapResponse;
import com.gghouse.wardah.wardahba.webservices.response.TestRecapResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WardahHistoryFragmentViewModel extends ViewModel {

    private static final String TAG = WardahHistoryFragmentViewModel.class.getSimpleName();

    private MutableLiveData<WardahHistoryRecyclerViewItem> dataHeaderHistoryMutableLiveData;
    private MutableLiveData<WardahItem> wardahItemMutableLiveData;
    private WardahItemType mType;
    private SortByTypeEnum mSortByType = SortByTypeEnum.QTY; // Set QTY as default
    private OrderByEnum mOrderBy = OrderByEnum.DESC; // Set DESC as default
    private int mPage;
    private long mLocationId;
    private long mUserId;

    private MutableLiveData<MonthlySalesView> monthlySalesViewMutableLiveData;

    // Daily Sales
    private MutableLiveData<DailySalesView> dailySalesEditMutableLiveData;

    // Sales
    private MutableLiveData<SalesEditProducts> salesEditProductsMutableLiveData;

    public MutableLiveData<WardahHistoryRecyclerViewItem> getDataHeaderHistoryMutableLiveData() {
        if (dataHeaderHistoryMutableLiveData == null) {
            dataHeaderHistoryMutableLiveData = new MutableLiveData<>();
        }
        return dataHeaderHistoryMutableLiveData;
    }

    public MutableLiveData<WardahItem> getWardahItemMutableLiveData() {
        if (wardahItemMutableLiveData == null) {
            wardahItemMutableLiveData = new MutableLiveData<>();
        }
        return wardahItemMutableLiveData;
    }

    public MutableLiveData<SalesEditProducts> getSalesEditProductsMutableLiveData() {
        if (salesEditProductsMutableLiveData == null) {
            salesEditProductsMutableLiveData = new MutableLiveData<>();
        }
        return salesEditProductsMutableLiveData;
    }

    public MutableLiveData<DailySalesView> getDailySalesEditMutableLiveData() {
        if (dailySalesEditMutableLiveData == null) {
            dailySalesEditMutableLiveData = new MutableLiveData<>();
        }
        return dailySalesEditMutableLiveData;
    }

    public WardahItemType getType() {
        return mType;
    }

    public void setType(WardahItemType type) {
        mType = type;
    }

    public SortByTypeEnum getSortByType() {
        return mSortByType;
    }

    public void setSortByType(SortByTypeEnum sortByType) {
        mSortByType = sortByType;
    }

    public OrderByEnum getOrderBy() {
        return mOrderBy;
    }

    public void setOrderBy(OrderByEnum orderBy) {
        mOrderBy = orderBy;
    }

    public int getPage() {
        return mPage;
    }

    public long getLocationId() {
        return mLocationId;
    }

    public void setLocationId(long mLocationId) {
        this.mLocationId = mLocationId;
    }

    public long getUserId() {
        return mUserId;
    }

    public void setUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public MutableLiveData<MonthlySalesView> getMonthlySalesViewMutableLiveData() {
        if (monthlySalesViewMutableLiveData == null) {
            monthlySalesViewMutableLiveData = new MutableLiveData<>();
        }
        return monthlySalesViewMutableLiveData;
    }

    public void getData(WardahHistoryAdapterWithHeader adapter, int page, Date beginDate, Date endDate) {
        Long userId = SessionUtil.getUserId();
        if (userId == null) {
            SessionUtil.loggingOut();
        } else {
            mPage = page;

            switch (Config.Companion.getMode()) {
                case DUMMY_DEVELOPMENT:
                    callWardahHistoryDummyData(userId, adapter, page);
                    break;
                case DEVELOPMENT:
                case PRODUCTION:
                    callHistoryHeaderData(userId, page, beginDate, endDate);
                    break;
            }
        }
    }

    private void callWardahHistoryDummyData(Long userId,
                                            WardahHistoryAdapterWithHeader adapter, int page) {
        new Handler().postDelayed(() -> {
            if (page == 0) {
                Pagination pagination = new Pagination();
                pagination.setLast(false);
                switch (mType) {
                    case SALES:
                        switch (SessionUtil.getUserTypeEnum()) {
                            case FIELD_CONTROLLER:
                                getWardahItemMutableLiveData().postValue(new WardahItem(SalesDummy.salesNonEditableList, pagination));
                                break;
                            default:
                                getWardahItemMutableLiveData().postValue(new WardahItem(SalesDummy.salesList, pagination));
                                break;
                        }
                        break;
                    case TEST:
                        getWardahItemMutableLiveData().postValue(new WardahItem(TestDummy.INSTANCE.getTestList(), pagination));
                        break;
                    case PRODUCT_HIGHLIGHT:
                        // Sort by
                        switch (mSortByType) {
                            case NAME:
                                getWardahItemMutableLiveData().postValue(new WardahItem(SalesDummy.productHighlightCleanZeroQtyList, pagination));
                                break;
                            case PRICE:
                                getWardahItemMutableLiveData().postValue(new WardahItem(SalesDummy.productHighlightHistorySortByPrice, pagination));
                        }
                        break;
                    case PRODUCT_FOCUS:
                        switch (mSortByType) {
                            case NAME:
                                getWardahItemMutableLiveData().postValue(new WardahItem(SalesDummy.productFocusCleanZeroQtyList, pagination));
                                break;
                            case PRICE:
                                getWardahItemMutableLiveData().postValue(new WardahItem(SalesDummy.productFocusHistorySortByPrice, pagination));
                                break;
                        }
                        break;
                    case STORE_DAILY_SALES:
                        switch (SessionUtil.getUserTypeEnum()) {
                            case FIELD_CONTROLLER:
                                getWardahItemMutableLiveData().postValue(new WardahItem(StoreDummy.storeSalesNonEditableList, pagination));
                                break;
                            default:
                                getWardahItemMutableLiveData().postValue(new WardahItem(StoreDummy.storeSalesList, pagination));
                                break;
                        }
                        break;
                    case STORE_MONTHLY_SALES:
                        switch (SessionUtil.getUserTypeEnum()) {
                            case FIELD_CONTROLLER:
                                getWardahItemMutableLiveData().postValue(new WardahItem(StoreDummy.storeSalesMonthlyNonEditableList, pagination));
                                break;
                            default:
                                getWardahItemMutableLiveData().postValue(new WardahItem(StoreDummy.storeSalesMonthlyList, pagination));
                                break;
                        }
                        break;

                }
            } else {
                /// Remove loading item
                adapter.remove(adapter.getItemCount() - 1);

                List<WardahHistoryRecyclerViewItem> wardahHistoryRecyclerViewItemList = new ArrayList<>();
                //Load data
                int index = adapter.getItemCount();
                int end = index + 20;

                for (long i = index; i < end; i++) {

                    switch (mType) {
                        case SALES:
                            Sales sales = new Sales(
                                    i,
                                    (Double.parseDouble(i + "") + 1000000),
                                    new Date().getTime() - (i * (1000 * 60 * 60 * 24)),
                                    false,
                                    (int) i,
                                    (int) i,
                                    (int) i,
                                    1L);
                            wardahHistoryRecyclerViewItemList.add(sales);
                            break;
                        case TEST:
                            Test test = new Test((int) i, (int) i, new Date().getTime());
                            wardahHistoryRecyclerViewItemList.add(test);
                            break;
                        case PRODUCT_HIGHLIGHT:
                            ProductHighlight productHighlight = new ProductHighlight(i, "Item " + i, "", 10000.00, (int) i);
                            wardahHistoryRecyclerViewItemList.add(productHighlight);
                            break;
                        case PRODUCT_FOCUS:
                            productHighlight = new ProductHighlight(i, "Item " + i, "", 10000.00, (int) i);
                            wardahHistoryRecyclerViewItemList.add(productHighlight);
                            break;
                        case STORE_DAILY_SALES:
                            StoreSales storeSales = new StoreSales(new Date().getTime() - (i * (1000 * 60 * 60 * 24)), i, (i * 100000.00), userId,
                                    i == 0, (int) i, (int) i, (int) i, new ArrayList<>());
                            wardahHistoryRecyclerViewItemList.add(storeSales);
                            break;
                        case STORE_MONTHLY_SALES:
                            storeSales = new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth((int) i).getTime(), i, (i * 100000.00), userId,
                                    i == 0, (int) i, (int) i, (int) i, new ArrayList<>(), 200000000.00, 210000000.00);
                            wardahHistoryRecyclerViewItemList.add(storeSales);
                            break;
                    }
                }

                // Manipulate pagination for dummy data
                Pagination pagination = new Pagination();
                if (mPage == Config.Companion.getMAX_LOAD_MORE_FOR_DUMMY_DATA()) {
                    pagination.setLast(true);
                } else {
                    pagination.setLast(false);
                }

                getWardahItemMutableLiveData().postValue(new WardahItem(wardahHistoryRecyclerViewItemList, pagination));
            }
        }, Config.Companion.getDELAY_LOAD_MORE());
    }

    private void callHistoryHeaderData(Long userId, int page, Date beginDate, Date endDate) {
        switch (mType) {
            case SALES_FC:
                userId = mUserId;
            case SALES:
                Call<SalesRecapResponse> callSalesRecap = ApiClient.INSTANCE.getClient().apiSalesRecap(userId);
                callSalesRecap.enqueue(new Callback<SalesRecapResponse>() {
                    @Override
                    public void onResponse(Call<SalesRecapResponse> call, Response<SalesRecapResponse> response) {
                        if (response.isSuccessful()) {
                            SalesRecapResponse salesRecapResponse = response.body();
                            getDataHeaderHistoryMutableLiveData().setValue(salesRecapResponse.getData());
                        } else {
                            getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesRecapResponse> call, Throwable t) {
                        getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_DAILY_SALES:
            case STORE_MONTHLY_SALES:
                Call<StoreSalesRecapResponse> callStoreSalesRecap = ApiClient.INSTANCE.getClient().apiStoreSalesRecap(mLocationId);
                callStoreSalesRecap.enqueue(new Callback<StoreSalesRecapResponse>() {
                    @Override
                    public void onResponse(Call<StoreSalesRecapResponse> call, Response<StoreSalesRecapResponse> response) {
                        if (response.isSuccessful()) {
                            StoreSalesRecapResponse storeSalesRecapResponse = response.body();
                            getDataHeaderHistoryMutableLiveData().setValue(storeSalesRecapResponse.getData());
                        } else {
                            getDataHeaderHistoryMutableLiveData().setValue(new StoreSalesRecap(0.00, 0, 0, 0));
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<StoreSalesRecapResponse> call, Throwable t) {
                        getDataHeaderHistoryMutableLiveData().setValue(new StoreSalesRecap(0.00, 0, 0, 0));
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_DAILY_PRODUCT_HIGHLIGHT:
            case STORE_MONTHLY_PRODUCT_HIGHLIGHT:
                Call<SalesRecapResponse> callStoreProductHighlightRecap = ApiClient.INSTANCE.getClient().apiStoreProductHighlightRecap(mLocationId);
                callStoreProductHighlightRecap.enqueue(new Callback<SalesRecapResponse>() {
                    @Override
                    public void onResponse(Call<SalesRecapResponse> call, Response<SalesRecapResponse> response) {
                        if (response.isSuccessful()) {
                            SalesRecapResponse salesRecapResponse = response.body();
                            getDataHeaderHistoryMutableLiveData().setValue(salesRecapResponse.getData());
                        } else {
                            getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesRecapResponse> call, Throwable t) {
                        getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_DAILY_PRODUCT_FOCUS:
            case STORE_MONTHLY_PRODUCT_FOCUS:
                Call<SalesRecapResponse> callStoreDailyProductFocusRecap = ApiClient.INSTANCE.getClient().apiStoreProductFocusRecap(mLocationId);
                callStoreDailyProductFocusRecap.enqueue(new Callback<SalesRecapResponse>() {
                    @Override
                    public void onResponse(Call<SalesRecapResponse> call, Response<SalesRecapResponse> response) {
                        if (response.isSuccessful()) {
                            SalesRecapResponse salesRecapResponse = response.body();
                            getDataHeaderHistoryMutableLiveData().setValue(salesRecapResponse.getData());
                        } else {
                            getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesRecapResponse> call, Throwable t) {
                        getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case TEST:
                Call<TestRecapResponse> callTestRecap = ApiClient.INSTANCE.getClient().apiTestRecap(userId);
                callTestRecap.enqueue(new Callback<TestRecapResponse>() {
                    @Override
                    public void onResponse(Call<TestRecapResponse> call, Response<TestRecapResponse> response) {
                        if (response.isSuccessful()) {
                            TestRecapResponse testRecapResponse = response.body();
                            getDataHeaderHistoryMutableLiveData().setValue(testRecapResponse.getData());
                        } else {
                            getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<TestRecapResponse> call, Throwable t) {
                        getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case PRODUCT_HIGHLIGHT_FC:
                userId = mUserId;
            case PRODUCT_HIGHLIGHT:
                Call<SalesRecapResponse> callProductHighlightRecap = ApiClient.INSTANCE.getClient().apiProductHighlightRecap(userId);
                callProductHighlightRecap.enqueue(new Callback<SalesRecapResponse>() {
                    @Override
                    public void onResponse(Call<SalesRecapResponse> call, Response<SalesRecapResponse> response) {
                        if (response.isSuccessful()) {
                            SalesRecapResponse salesRecapResponse = response.body();
                            getDataHeaderHistoryMutableLiveData().setValue(salesRecapResponse.getData());
                        } else {
                            getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesRecapResponse> call, Throwable t) {
                        getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case PRODUCT_FOCUS_FC:
                userId = mUserId;
            case PRODUCT_FOCUS:
                Call<SalesRecapResponse> callProductFocusRecap = ApiClient.INSTANCE.getClient().apiProductFocusRecap(userId);
                callProductFocusRecap.enqueue(new Callback<SalesRecapResponse>() {
                    @Override
                    public void onResponse(Call<SalesRecapResponse> call, Response<SalesRecapResponse> response) {
                        if (response.isSuccessful()) {
                            SalesRecapResponse salesRecapResponse = response.body();
                            getDataHeaderHistoryMutableLiveData().setValue(salesRecapResponse.getData());
                        } else {
                            getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesRecapResponse> call, Throwable t) {
                        getDataHeaderHistoryMutableLiveData().setValue(new SalesRecap(0.00, 0, 0.00, 0));
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
        }
    }

    public void callHistoryData(Long userId, int page, Date beginDate, Date endDate) {
        String beginDateStr = null;
        String endDateStr = null;
        if (beginDate != null && endDate != null) {
            beginDateStr = DateTimeUtil.INSTANCE.getSdfFilter().format(beginDate);
            endDateStr = DateTimeUtil.INSTANCE.getSdfFilter().format(endDate);
        }

        switch (mType) {
            case TEST:
                Call<HistoryTestResponse> callTestList = ApiClient.INSTANCE.getClient().apiTest(userId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), beginDateStr, endDateStr);
                callTestList.enqueue(new Callback<HistoryTestResponse>() {
                    @Override
                    public void onResponse(Call<HistoryTestResponse> call, Response<HistoryTestResponse> response) {
                        if (response.isSuccessful()) {
                            HistoryTestResponse historyTestResponse = response.body();
                            switch (historyTestResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(historyTestResponse.getData(), historyTestResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, historyTestResponse.getStatus());
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<HistoryTestResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case SALES_FC:
                userId = mUserId;
            case SALES:
                Call<SalesListResponse> callSales = ApiClient.INSTANCE.getClient().apiSales(userId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), beginDateStr, endDateStr);
                callSales.enqueue(new Callback<SalesListResponse>() {
                    @Override
                    public void onResponse(Call<SalesListResponse> call, Response<SalesListResponse> response) {
                        if (response.isSuccessful()) {
                            SalesListResponse salesListResponse = response.body();
                            switch (salesListResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(salesListResponse.getData(), salesListResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, salesListResponse.getStatus());
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesListResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case PRODUCT_HIGHLIGHT_FC:
                userId = mUserId;
            case PRODUCT_HIGHLIGHT:
                Call<ProductHighlightResponse> callProductHighlight = ApiClient.INSTANCE.getClient().apiProductHighlight(
                        userId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), mSortByType.getValue() + "," + mOrderBy.getValue(), beginDateStr, endDateStr);
                callProductHighlight.enqueue(new Callback<ProductHighlightResponse>() {
                    @Override
                    public void onResponse(Call<ProductHighlightResponse> call, Response<ProductHighlightResponse> response) {
                        if (response.isSuccessful()) {
                            ProductHighlightResponse productHighlightResponse = response.body();
                            switch (productHighlightResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(productHighlightResponse.getData(), productHighlightResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, productHighlightResponse.getStatus());
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductHighlightResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case PRODUCT_FOCUS_FC:
                userId = mUserId;
            case PRODUCT_FOCUS:
                Call<ProductHighlightResponse> callProductFocus = ApiClient.INSTANCE.getClient().apiProductFocus(
                        userId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), mSortByType.getValue() + "," + mOrderBy.getValue(), beginDateStr, endDateStr);
                callProductFocus.enqueue(new Callback<ProductHighlightResponse>() {
                    @Override
                    public void onResponse(Call<ProductHighlightResponse> call, Response<ProductHighlightResponse> response) {
                        if (response.isSuccessful()) {
                            ProductHighlightResponse productHighlightResponse = response.body();
                            switch (productHighlightResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(productHighlightResponse.getData(), productHighlightResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, productHighlightResponse.getStatus());
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductHighlightResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_DAILY_SALES:
                Call<DailySalesResponse> callDailySales = ApiClient.INSTANCE.getClient().apiDailySales(
                        mLocationId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), "created_time,DESC", beginDateStr, endDateStr);
                callDailySales.enqueue(new Callback<DailySalesResponse>() {
                    @Override
                    public void onResponse(Call<DailySalesResponse> call, Response<DailySalesResponse> response) {
                        if (response.isSuccessful()) {
                            DailySalesResponse dailySalesResponse = response.body();
                            switch (dailySalesResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(dailySalesResponse.getData(), dailySalesResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<DailySalesResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_DAILY_PRODUCT_HIGHLIGHT:
                Call<ProductHighlightResponse> callDailyProductHighlight = ApiClient.INSTANCE.getClient().apiDailyProductHighlight(
                        mLocationId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), mSortByType.getValue() + "," + mOrderBy.getValue(), beginDateStr, endDateStr);
                callDailyProductHighlight.enqueue(new Callback<ProductHighlightResponse>() {
                    @Override
                    public void onResponse(Call<ProductHighlightResponse> call, Response<ProductHighlightResponse> response) {
                        if (response.isSuccessful()) {
                            ProductHighlightResponse productHighlightResponse = response.body();
                            switch (productHighlightResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(productHighlightResponse.getData(), productHighlightResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductHighlightResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_DAILY_PRODUCT_FOCUS:
                Call<ProductHighlightResponse> callDailyProductFocus = ApiClient.INSTANCE.getClient().apiDailyProductFocus(
                        mLocationId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), mSortByType.getValue() + "," + mOrderBy.getValue(), beginDateStr, endDateStr);
                callDailyProductFocus.enqueue(new Callback<ProductHighlightResponse>() {
                    @Override
                    public void onResponse(Call<ProductHighlightResponse> call, Response<ProductHighlightResponse> response) {
                        if (response.isSuccessful()) {
                            ProductHighlightResponse productHighlightResponse = response.body();
                            switch (productHighlightResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(productHighlightResponse.getData(), productHighlightResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductHighlightResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_MONTHLY_SALES:
                Call<MonthlySalesResponse> callMonthlySales = ApiClient.INSTANCE.getClient().apiMonthlySales(
                        mLocationId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), "created_time,DESC", beginDateStr, endDateStr);
                callMonthlySales.enqueue(new Callback<MonthlySalesResponse>() {
                    @Override
                    public void onResponse(Call<MonthlySalesResponse> call, Response<MonthlySalesResponse> response) {
                        if (response.isSuccessful()) {
                            MonthlySalesResponse monthlySalesResponse = response.body();
                            switch (monthlySalesResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(monthlySalesResponse.getData(), monthlySalesResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<MonthlySalesResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_MONTHLY_PRODUCT_HIGHLIGHT:
                if (beginDate != null && endDate != null) {
                    beginDateStr = DateTimeUtil.INSTANCE.getSdfMonthFilter().format(beginDate);
                    endDateStr = DateTimeUtil.INSTANCE.getSdfMonthFilter().format(endDate);
                }
                Call<ProductHighlightResponse> callMonthlyProductHighlight = ApiClient.INSTANCE.getClient().apiMonthlyProductHighlight(
                        mLocationId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), mSortByType.getValue() + "," + mOrderBy.getValue(), beginDateStr, endDateStr);
                callMonthlyProductHighlight.enqueue(new Callback<ProductHighlightResponse>() {
                    @Override
                    public void onResponse(Call<ProductHighlightResponse> call, Response<ProductHighlightResponse> response) {
                        if (response.isSuccessful()) {
                            ProductHighlightResponse productHighlightResponse = response.body();
                            switch (productHighlightResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(productHighlightResponse.getData(), productHighlightResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductHighlightResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_MONTHLY_PRODUCT_FOCUS:
                if (beginDate != null && endDate != null) {
                    beginDateStr = DateTimeUtil.INSTANCE.getSdfMonthFilter().format(beginDate);
                    endDateStr = DateTimeUtil.INSTANCE.getSdfMonthFilter().format(endDate);
                }
                Call<ProductHighlightResponse> callMonthlyProductFocus = ApiClient.INSTANCE.getClient().apiMonthlyProductFocus(
                        mLocationId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), mSortByType.getValue() + "," + mOrderBy.getValue(), beginDateStr, endDateStr);
                callMonthlyProductFocus.enqueue(new Callback<ProductHighlightResponse>() {
                    @Override
                    public void onResponse(Call<ProductHighlightResponse> call, Response<ProductHighlightResponse> response) {
                        if (response.isSuccessful()) {
                            ProductHighlightResponse productHighlightResponse = response.body();
                            switch (productHighlightResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahItemMutableLiveData().postValue(new WardahItem(productHighlightResponse.getData(), productHighlightResponse.getPagination()));
                                    break;
                                default:
                                    getWardahItemMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                                    break;
                            }
                        } else {
                            getWardahItemMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductHighlightResponse> call, Throwable t) {
                        getWardahItemMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
        }
    }

    public void callSalesEditProducts(long salesId) {
        Call<SalesEditProductResponse> callSalesEditProducts = ApiClient.INSTANCE.getClient().apiSalesEditProduct(salesId);
        callSalesEditProducts.enqueue(new Callback<SalesEditProductResponse>() {
            @Override
            public void onResponse(Call<SalesEditProductResponse> call, Response<SalesEditProductResponse> response) {
                if (response.isSuccessful()) {
                    SalesEditProductResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getSalesEditProductsMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<SalesEditProductResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callDailySalesEdit(long dailyReportId) {
        Call<DailySalesViewResponse> callDailySalesEdit = ApiClient.INSTANCE.getClient().apiDailySalesView(dailyReportId);
        callDailySalesEdit.enqueue(new Callback<DailySalesViewResponse>() {
            @Override
            public void onResponse(Call<DailySalesViewResponse> call, Response<DailySalesViewResponse> response) {
                if (response.isSuccessful()) {
                    DailySalesViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            DailySalesView dailySalesView = rps.getData();
                            dailySalesView.setDailyReportId(dailyReportId);
                            getDailySalesEditMutableLiveData().postValue(dailySalesView);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<DailySalesViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callMonthlySalesView(long monthlyReportId) {
        Call<MonthlySalesViewResponse> callMonthySalesView = ApiClient.INSTANCE.getClient().apiMonthlySalesView(monthlyReportId);
        callMonthySalesView.enqueue(new Callback<MonthlySalesViewResponse>() {
            @Override
            public void onResponse(Call<MonthlySalesViewResponse> call, Response<MonthlySalesViewResponse> response) {
                if (response.isSuccessful()) {
                    MonthlySalesViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            MonthlySalesView monthlySalesView = rps.getData();
                            monthlySalesView.setMonthlyReportId(monthlyReportId);
                            getMonthlySalesViewMutableLiveData().postValue(monthlySalesView);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<MonthlySalesViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }
}
