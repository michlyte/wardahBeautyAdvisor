package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.ChecklistBAListener;
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA;

public class ChecklistBAViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final LinearLayout mContainerLinearLayout;
    private final TextView mTitleTextView;
    private final RatingBar mRatingBar;
    private final TextView mRatingTextView;
    private final Button mEvaluationButton;
    private ChecklistBA mItem;

    public ChecklistBAViewHolder(View view) {
        super(view);

        this.view = view;
        mContainerLinearLayout = view.findViewById(R.id.container);
        mTitleTextView = view.findViewById(R.id.titleEditText);
        mRatingBar = view.findViewById(R.id.ratingBar);
        mRatingTextView = view.findViewById(R.id.ratingTextView);
        mEvaluationButton = view.findViewById(R.id.evaluationButton);
    }

    private void setupRowView() {
        if (mItem.getRatingStatus() != null && !mItem.getRatingStatus().equals("-")) {
            mRatingBar.setVisibility(View.VISIBLE);
            mRatingTextView.setVisibility(View.VISIBLE);
            mRatingTextView.setText(mItem.getRatingStatus());
        } else {
            mRatingBar.setVisibility(View.GONE);
            mRatingTextView.setVisibility(View.VISIBLE);
            mRatingTextView.setText("-");
        }

        if (mItem.isActive()) {
            mRatingTextView.setVisibility(View.GONE);
            mEvaluationButton.setVisibility(View.VISIBLE);
        } else {
            mEvaluationButton.setVisibility(View.GONE);
        }
    }

    private void setupActionEvent(ChecklistBAListener checklistBAListener) {
        mContainerLinearLayout.setOnClickListener(view -> checklistBAListener.onClick(mItem));
        mEvaluationButton.setOnClickListener(view -> checklistBAListener.onEvaluationButtonClicked(mItem));
    }

    public void setData(ChecklistBA checklistBA, ChecklistBAListener checklistBAListener) {
        mItem = checklistBA;

        mTitleTextView.setText(mItem.getName());
        setupRowView();
        setupActionEvent(checklistBAListener);
    }

    public void setData(ViewMode viewMode, ChecklistBA checklistBA, ChecklistBAListener checklistBAListener) {
        mItem = checklistBA;

        mTitleTextView.setText(mItem.getName());
        setupRowView();
        switch (viewMode) {
            case VIEW:
                break;
            case INPUT:
            case EDIT:
                setupActionEvent(checklistBAListener);
                break;
        }
    }
}