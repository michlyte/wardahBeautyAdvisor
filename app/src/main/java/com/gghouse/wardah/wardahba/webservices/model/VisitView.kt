package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import java.io.Serializable

class VisitView : Serializable {
    val calendarId: Long
    val category: String
    val startTime: Long
    val endTime: Long
    var alarmTime: Long? = null
    val notes: String
    val location: VisitLocation
    val dc: Dc
    val viewType: String
    var checkInOut: VisitCheckInOut? = null
    var storeInformation: List<VisitStoreInformation>? = null
    var checklist: List<ChecklistBA>? = null
    var alarmTimeStr: String? = null

    constructor(calendarId: Long, category: String, startTime: Long, endTime: Long, alarmTime: Long, notes: String, location: VisitLocation, dc: Dc, viewType: String) {
        this.calendarId = calendarId
        this.category = category
        this.startTime = startTime
        this.endTime = endTime
        this.alarmTime = alarmTime
        this.notes = notes
        this.location = location
        this.dc = dc
        this.viewType = viewType
    }

    val startTimeStr: String
        get() = DateTimeUtil.sdfEventStartEndTime.format(startTime)

    val endTimeStr: String
        get() = DateTimeUtil.sdfEventStartEndTime.format(endTime)

    val alarmStr: String
        get() = DateTimeUtil.sdfEventStartEndTime.format(alarmTime)
}