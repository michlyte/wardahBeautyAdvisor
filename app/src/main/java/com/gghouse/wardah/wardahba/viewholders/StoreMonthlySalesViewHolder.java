package com.gghouse.wardah.wardahba.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.SystemUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySales;

/**
 * Created by michaelhalim on 5/9/17.
 */

public class StoreMonthlySalesViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    private final CardView mContainerCardView;
    private final LinearLayout mContainerLinearLayout;
    public final TextView mTitleTextView;
    private final TextView mDateTextView;
    private final TextView mPcsTextView;
    private final TextView mPengunjungPembeliTextView;
    private final TextView mBuyingPowerFirstLineTextView;
    private final TextView mBuyingPowerSecondLineTextView;
    private final TextView mMonthlySalesTargetTextView;

    public MonthlySales mItem;

    public StoreMonthlySalesViewHolder(View view) {
        super(view);
        mView = view;
        mContainerCardView = mView.findViewById(R.id.cv);
        mContainerLinearLayout = mView.findViewById(R.id.ll_container);
        mTitleTextView = mView.findViewById(R.id.tv_title);
        mDateTextView = mView.findViewById(R.id.tv_date);
        mPcsTextView = mView.findViewById(R.id.tvPcs);
        mPengunjungPembeliTextView = mView.findViewById(R.id.tvPengunjungPembeli);
        mBuyingPowerFirstLineTextView = mView.findViewById(R.id.tvBuyingPowerFirstLine);
        mBuyingPowerSecondLineTextView = mView.findViewById(R.id.tvBuyingPowerSecondLine);
        mMonthlySalesTargetTextView = mView.findViewById(R.id.monthlySalesTargetTextView);
    }

    private void setupRowView(Context context) {
        if (mItem.getEditable() != null && mItem.getEditable()) {
            mContainerCardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

            int color = ContextCompat.getColor(context, android.R.color.white);
            mTitleTextView.setTextColor(color);
            mDateTextView.setTextColor(color);
            mMonthlySalesTargetTextView.setTextColor(color);
            mPcsTextView.setTextColor(color);
            mPengunjungPembeliTextView.setTextColor(color);
            mBuyingPowerFirstLineTextView.setTextColor(color);
            mBuyingPowerSecondLineTextView.setTextColor(color);
        } else {
            mContainerCardView.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.white));

            int primaryColor = ContextCompat.getColor(context, R.color.colorPrimary);
            int accentColor = ContextCompat.getColor(context, R.color.colorAccent);
            int blackColor = ContextCompat.getColor(context, android.R.color.black);
            mTitleTextView.setTextColor(primaryColor);
            mDateTextView.setTextColor(blackColor);
            mMonthlySalesTargetTextView.setTextColor(primaryColor);
            mPcsTextView.setTextColor(accentColor);
            mPengunjungPembeliTextView.setTextColor(accentColor);
            mBuyingPowerFirstLineTextView.setTextColor(accentColor);
            mBuyingPowerSecondLineTextView.setTextColor(accentColor);
        }
    }

    private void setupActionEvent(WardahListener wardahListener) {
        ViewMode viewMode;
        if (mItem.getEditable() != null && mItem.getEditable()) {
            viewMode = ViewMode.EDIT;
        } else {
            viewMode = ViewMode.VIEW;
        }

        mContainerLinearLayout.setOnClickListener(v -> wardahListener.onClick(mItem, viewMode));
    }

    public void setData(Context context, MonthlySales monthlySales, WardahListener wardahListener) {
        mItem = monthlySales;
        String month = DateTimeUtil.INSTANCE.getMonth(monthlySales.getMonth());
        String year = monthlySales.getYear().toString();
        mTitleTextView.setText(month);
        mDateTextView.setText(year);
        mMonthlySalesTargetTextView.setText(SystemUtil.withSuffix(monthlySales.getTarget()));
        mPcsTextView.setText(WardahUtil.INSTANCE.getStoreSalesPcsAndAmountContent(monthlySales.getQuantity(), monthlySales.getAmount()));
        mPengunjungPembeliTextView.setText(WardahUtil.INSTANCE.getStoreSalesVisitorAndBuyerContent(monthlySales.getVisitor(), monthlySales.getBuyer()));
        mBuyingPowerFirstLineTextView.setText(monthlySales.getBuyingPower().getBuyingPowerFirstLineStr());
        mBuyingPowerSecondLineTextView.setText(monthlySales.getBuyingPower().getBuyingPowerSecondLineStr());

        setupRowView(context);
        setupActionEvent(wardahListener);
    }
}
