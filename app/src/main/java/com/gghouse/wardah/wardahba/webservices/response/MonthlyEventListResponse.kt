package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.BPMonthlyEvent

class MonthlyEventListResponse : GenericResponse() {
    var data: List<BPMonthlyEvent>? = null
}
