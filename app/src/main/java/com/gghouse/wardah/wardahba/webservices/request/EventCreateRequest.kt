package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.EventLocation

open class EventCreateRequest {
    val userId: Long
    val title: String
    val category: String
    val startTime: Long
    val endTime: Long
    var alarmTime: Long?
    var alarmTimeStr: String?
    val notes: String
    val location: EventLocation

    constructor(userId: Long, title: String, category: String, startTime: Long, endTime: Long, alarmTime: Long?, alarmTimeStr: String?, notes: String, location: EventLocation) {
        this.userId = userId
        this.title = title
        this.category = category
        this.startTime = startTime
        this.endTime = endTime
        this.alarmTime = alarmTime
        this.alarmTimeStr = alarmTimeStr
        this.notes = notes
        this.location = location
    }
}