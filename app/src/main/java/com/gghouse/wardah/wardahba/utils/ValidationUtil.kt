package com.gghouse.wardah.wardahba.utils

import android.text.format.DateUtils
import java.util.*

/**
 * Created by michaelhalim on 1/25/17.
 */

object ValidationUtil {
    fun isPasswordValid(password: String): Boolean {
        return password.length > 4
    }

    fun isEmailValid(email: String): Boolean {
        return email.contains("@")
    }

    fun isPasswordMatch(password: String, confirmPassword: String): Boolean {
        return password == confirmPassword
    }

    fun isChecklistEditable(startTime: Long?, endTime: Long?): Boolean {
        return startTime != null && DateTimeUtil.isTodayOrTomorrow(Date(startTime)) || endTime != null && DateTimeUtil.isTodayOrTomorrow(Date(endTime))
    }

    fun isAttendantEnabled(checkInTime: Long?): Boolean {
        return checkInTime != null && checkInTime > 0L
    }

    fun isAttendantEditable(startTime: Long?, endTime: Long?): Boolean {
        return startTime != null && DateUtils.isToday(startTime) || endTime != null && DateUtils.isToday(endTime)
    }

    fun isCheckInEnabled(startTime: Long?, endTime: Long?, checkInTime: Long?): Boolean {
        return (startTime != null && DateUtils.isToday(startTime) || endTime != null && DateUtils.isToday(endTime)) && (checkInTime == null || checkInTime == 0L)
    }
}
