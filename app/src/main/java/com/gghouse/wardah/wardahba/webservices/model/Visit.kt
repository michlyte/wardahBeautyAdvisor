package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import com.gghouse.wardah.wardahba.utils.WardahUtil
import java.io.Serializable

class Visit : Serializable {
    var id: Long? = null
    var title: String? = null
    var startTime: Long? = null
    var endTime: Long? = null
    var dc: String? = null
    var alarm: Long? = null
    var category: String? = null
    var totalBA: String? = null
    var targetSales: Double? = null
    var totalSales: Double? = null
    var checkIn: Long? = null
    var checkOut: Long? = null
    var evaluation: String? = null

    constructor(id: Long?, title: String, startTime: Long?) {
        this.id = id
        this.title = title
        this.startTime = startTime
    }

    constructor(id: Long?, title: String, startTime: Long?, dc: String) {
        this.id = id
        this.title = title
        this.startTime = startTime
        this.dc = dc
    }

    constructor(id: Long?, title: String?, startTime: Long?, endTime: Long?, dc: String?, alarm: Long?, category: String?, totalBA:String?, targetSales:Double?, totalSales:Double?, checkIn: Long?, checkOut: Long?, evaluation: String) {
        this.id = id
        this.title = title
        this.startTime = startTime
        this.endTime = endTime
        this.dc = dc
        this.alarm = alarm
        this.category = category
        this.totalBA = totalBA
        this.targetSales = targetSales
        this.totalSales = totalSales
        this.checkIn = checkIn
        this.checkOut = checkOut
        this.evaluation = evaluation
    }

    val dateStr: String
        get() = DateTimeUtil.sdfDateMonthYear.format(startTime)

    val timeStr: String
        get() = DateTimeUtil.sdfEventTime.format(startTime) + " - " + DateTimeUtil.sdfEventTime.format(endTime)

    val alarmStr: String
        get() = DateTimeUtil.sdfEventStartEndTime.format(alarm)

    val targetSalesStr: String
        get() = WardahUtil.getAmount(targetSales)

    val totalSalesStr: String
        get() = WardahUtil.getAmount(totalSales)

    val checkInStr: String
        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkIn)

    val checkOutStr: String
        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkOut)

}