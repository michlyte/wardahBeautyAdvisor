package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Homepage

class HomepageResponse : GenericResponse() {
    var data: Homepage? = null
}
