package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class AssignedBp : Serializable {
    val id: Long
    val name: String

    constructor(id: Long, name: String) {
        this.id = id
        this.name = name
    }
}