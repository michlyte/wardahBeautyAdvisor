package com.gghouse.wardah.wardahba.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.WardahApp;
import com.gghouse.wardah.wardahba.webservices.model.Test;

import java.util.Calendar;

/**
 * Created by michael on 5/4/2017.
 */

public class TestViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final TextView scoreTextView;
    public final TextView titleTextView;
    public final TextView descriptionTextView;
    public final TextView dateTextView;
    public final TextView monthYearTextView;
    public Test mItem;

    private Calendar mCalendar = Calendar.getInstance();

    public TestViewHolder(View view) {
        super(view);
        mView = view;
        scoreTextView = view.findViewById(R.id.tv_score);
        titleTextView = view.findViewById(R.id.tv_title);
        descriptionTextView = view.findViewById(R.id.tv_description);
        dateTextView = view.findViewById(R.id.tv_date);
        monthYearTextView = view.findViewById(R.id.tv_month_year);
    }

    public int getDay(long time) {
        mCalendar.setTimeInMillis(time);
        return mCalendar.get(Calendar.DAY_OF_WEEK);
    }

    public void setData(Context context, Test test) {
        mItem = test;

        scoreTextView.setText(mItem.getScoreStr());
        titleTextView.setText(mItem.getDayStr());
        descriptionTextView.setText(WardahApp.getInstance().getAppContext().getString(R.string.title_jumlah_soal, mItem.getTotalQuestion() + ""));
        dateTextView.setText(mItem.getTestDateStr());
        monthYearTextView.setText(mItem.getMonthYearStr());

        if (mItem.getTestDate() != null) {
            switch (getDay(mItem.getTestDate())) {
                case Calendar.SATURDAY:
                case Calendar.SUNDAY:
                    titleTextView.setTextColor(ContextCompat.getColor(context, R.color.colorHistoryHeader6));
                    break;
                default:
                    titleTextView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    break;
            }
        }
    }
}