package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import java.io.Serializable

class DailySales : WardahHistoryRecyclerViewItem, Serializable {
    val dailyReportId: Long
    val buyer: Integer
    val visitor: Integer
    val buyingPower: BuyingPower
    val salesDate: Long
    val quantity: Integer
    val amount: Double
    val editable: Boolean

    constructor(dailyReportId: Long, buyer: Integer, visitor: Integer, buyingPower: BuyingPower, salesDate: Long, quantity: Integer, amount: Double, editable: Boolean) : super() {
        this.dailyReportId = dailyReportId
        this.buyer = buyer
        this.visitor = visitor
        this.buyingPower = buyingPower
        this.salesDate = salesDate
        this.quantity = quantity
        this.amount = amount
        this.editable = editable
    }
}