package com.gghouse.wardah.wardahba.enumerations

enum class StoreSalesType {
    DAILY, MONTHLY
}
