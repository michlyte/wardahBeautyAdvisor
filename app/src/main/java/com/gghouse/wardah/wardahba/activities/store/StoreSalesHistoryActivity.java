package com.gghouse.wardah.wardahba.activities.store;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.fragments.WardahHistoryFragment;
import com.gghouse.wardah.wardahba.fragments.WardahSimpleHistoryFragment;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.google.android.material.tabs.TabLayout;

public class StoreSalesHistoryActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private static final int COUNT_DAILY_TORE_HISTORY_TAB = 3;
    private static final int COUNT_MONTHLY_STORE_HISTORY_TAB = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        WardahItemType type = (WardahItemType) intent.getSerializableExtra(IntentUtil.Companion.getFREQUENT());
        Long locationId = intent.getLongExtra(IntentUtil.Companion.getLOCATION_ID(), 0L);

        switch (type) {
            case STORE_DAILY_SALES:
                setContentView(R.layout.activity_store_sales_history_harian);
                break;
            case STORE_MONTHLY_SALES:
                setContentView(R.layout.activity_store_sales_history_bulanan);
                break;
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), type, locationId);

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        switch (type) {
            case STORE_DAILY_SALES:
                getSupportActionBar().setTitle(R.string.text_riwayat_store_harian);
                break;
            case STORE_MONTHLY_SALES:
                getSupportActionBar().setTitle(R.string.text_riwayat_store_bulanan);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sales_history_p2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sales_history_p2, container, false);
            TextView textView = rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
        private WardahItemType mType;
        private Long mLocationId;

        public SectionsPagerAdapter(FragmentManager fm, WardahItemType type, Long locationId) {
            super(fm);
            mType = type;
            mLocationId = locationId;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return WardahHistoryFragment.newInstance(mType, mLocationId);
                case 1:
                    switch (mType) {
                        case STORE_DAILY_SALES:
                            return WardahHistoryFragment.newInstance(WardahItemType.STORE_DAILY_PRODUCT_HIGHLIGHT, mLocationId);
                        case STORE_MONTHLY_SALES:
                            return WardahHistoryFragment.newInstance(WardahItemType.STORE_MONTHLY_PRODUCT_HIGHLIGHT, mLocationId);
                        default:
                            return PlaceholderFragment.newInstance(position);
                    }
                case 2:
                    switch (mType) {
                        case STORE_DAILY_SALES:
                            return WardahHistoryFragment.newInstance(WardahItemType.STORE_DAILY_PRODUCT_FOCUS, mLocationId);
                        case STORE_MONTHLY_SALES:
                            return WardahHistoryFragment.newInstance(WardahItemType.STORE_MONTHLY_PRODUCT_FOCUS, mLocationId);
                        default:
                            return PlaceholderFragment.newInstance(position);
                    }
                case 3:
                    return WardahSimpleHistoryFragment.newInstance(WardahItemType.CATEGORY, mLocationId);
                default:
                    return PlaceholderFragment.newInstance(position);
            }
        }

        @Override
        public int getCount() {
            switch (mType) {
                case STORE_DAILY_SALES:
                    return COUNT_DAILY_TORE_HISTORY_TAB;
                case STORE_MONTHLY_SALES:
                    return COUNT_MONTHLY_STORE_HISTORY_TAB;
                default:
                    return 1;
            }
        }
    }
}
