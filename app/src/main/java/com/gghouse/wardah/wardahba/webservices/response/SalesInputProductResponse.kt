package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.SalesInputProducts

class SalesInputProductResponse : GenericResponse() {
    var data: SalesInputProducts? = null
}
