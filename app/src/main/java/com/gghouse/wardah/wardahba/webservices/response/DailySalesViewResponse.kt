package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.DailySalesView

class DailySalesViewResponse : GenericResponse() {
    var data: DailySalesView? = null
}
