package com.gghouse.wardah.wardahba.activities.event

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.text.TextUtils
import android.text.format.DateUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.WardahApp
import com.gghouse.wardah.wardahba.activities.attendant.AttendantActivity
import com.gghouse.wardah.wardahba.activities.attendant.AttendantHistoryActivity
import com.gghouse.wardah.wardahba.activities.attendant.InputAttendantActivity
import com.gghouse.wardah.wardahba.adapters.BannerSliderAdapter
import com.gghouse.wardah.wardahba.adapters.EventCategoryAdapter
import com.gghouse.wardah.wardahba.adapters.WardahSimpleAdapter
import com.gghouse.wardah.wardahba.enumerations.ReminderEnum
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum
import com.gghouse.wardah.wardahba.enumerations.ViewMode
import com.gghouse.wardah.wardahba.enumerations.WardahItemType
import com.gghouse.wardah.wardahba.interfaces.ChecklistListener
import com.gghouse.wardah.wardahba.interfaces.WardahListener
import com.gghouse.wardah.wardahba.models.IntentEventCategory
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import com.gghouse.wardah.wardahba.utils.*
import com.gghouse.wardah.wardahba.viewmodels.CreateEventActivityViewModel
import com.gghouse.wardah.wardahba.webservices.model.*
import com.gghouse.wardah.wardahba.webservices.request.EventCreateRequest
import com.gghouse.wardah.wardahba.webservices.request.EventEditRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_create_event.*
import kotlinx.android.synthetic.main.subview_attendant.*
import kotlinx.android.synthetic.main.subview_check_in_out.*
import kotlinx.android.synthetic.main.subview_checklist.*
import java.util.*
import kotlin.collections.ArrayList

class CreateEventActivity : AppCompatActivity(), OnMapReadyCallback, ChecklistListener, WardahListener, View.OnClickListener {
    private lateinit var mModel: CreateEventActivityViewModel

    // event date
    var isEventStartDialog = true
    private val TAG = CreateEventActivity::class.java.simpleName
    private val CHECKLIST_RESULT_ACTIVITY = 1

    // public static variable
    companion object {
        val REQUEST_REMINDER = 101
        val REQUEST_MAP = 102
    }

    // Map
    private lateinit var mMap: GoogleMap

    private var mAttendantAdapter: WardahSimpleAdapter? = null
    private var mChecklistAdapter: WardahSimpleAdapter? = null
    private lateinit var mCategoryAdapter: EventCategoryAdapter
    private lateinit var mDateSetListener: DatePickerDialog.OnDateSetListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_event)

        mModel = ViewModelProviders.of(this).get(CreateEventActivityViewModel::class.java)

        mModel.userType = UserTypeEnum.getUserTypeEnum(SessionUtil.getUserType())

        val timeSetListener = TimePickerDialog.OnTimeSetListener { view, hour, minute ->
            val cal = if (isEventStartDialog) mModel.startTimeCalendar else mModel.endTimeCalendar

            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)

            if (isEventStartDialog) {
                startTimeTextView?.text = DateTimeUtil.sdfEventStartEndTime.format(cal.time)
                mModel.startTimeCalendar = cal

                mModel.startTimeMutableLiveData.postValue(cal.timeInMillis)
            } else {
                endTimeTextView?.text = DateTimeUtil.sdfEventStartEndTime.format(cal.time)
                mModel.endTimeCalendar = cal
            }
        }

        mDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            val cal = if (isEventStartDialog) mModel.startTimeCalendar else mModel.endTimeCalendar

            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            if (isEventStartDialog) {
                mModel.startTimeCalendar = cal
            } else {
                mModel.endTimeCalendar = cal
            }

            TimePickerDialog(this@CreateEventActivity,
                    timeSetListener,
                    mModel.startTimeCalendar.get(Calendar.HOUR_OF_DAY),
                    mModel.startTimeCalendar.get(Calendar.MINUTE),
                    true).show()
        }

        startTimeTextView?.setOnClickListener(this)
        endTimeTextView?.setOnClickListener(this)
        alarmAddEditTextView?.setOnClickListener(this)
        eventLocationTextView?.setOnClickListener(this)

        alarmAddEditTextView.visibility = View.GONE
        checkOutTextView.visibility = View.GONE

        // Map
        val mEventMap = supportFragmentManager.findFragmentById(R.id.smfEventMap) as SupportMapFragment
        mEventMap.getMapAsync(this)

        mModel.viewModeMutableLiveData.observe(this, Observer<ViewMode> { viewMode ->
            if (viewMode != null) {
                when (viewMode) {
                    ViewMode.VIEW -> {
                        checklistLayout.visibility = View.VISIBLE
                        checkInLayout.visibility = View.VISIBLE
                        attendantLayout.visibility = View.VISIBLE

                        val textDisabledColor = ContextCompat.getColor(this, android.R.color.darker_gray)
                        titleEditText.isEnabled = false
                        startTimeTextView.setTextColor(textDisabledColor)
                        endTimeTextView.setTextColor(textDisabledColor)
                        alarmEditText.setTextColor(textDisabledColor)
                        noteEditText.isEnabled = false
                        eventLocationEditText.isEnabled = false
                        alarmEditText.isEnabled = false
                        categorySpinner.isEnabled = false
                        etEventCreatedBy.isEnabled = false

                        eventLocationTextView.visibility = View.GONE
                        alarmAddEditTextView.visibility = View.GONE

                        checkInTextView.setTextColor(textDisabledColor)
                    }
                    ViewMode.EDIT -> {
                        checklistLayout.visibility = View.VISIBLE
                        checkInLayout.visibility = View.VISIBLE
                        attendantLayout.visibility = View.VISIBLE
                    }
                    ViewMode.INPUT -> {

                    }
                }
            }
        })

        mModel.eventMutableLiveData.observe(this, Observer<Event> { event ->
            if (event != null) {
                supportActionBar?.title = event.title

                // Editable section
                titleEditText?.setText(event.title)
                startTimeTextView?.text = DateTimeUtil.sdfEventStartEndTime.format(event.startTime)
                mModel.startTimeCalendar.timeInMillis = event.startTime
                endTimeTextView?.text = DateTimeUtil.sdfEventStartEndTime.format(event.endTime)
                mModel.endTimeCalendar.timeInMillis = event.endTime
                alarmAddEditTextView.visibility = View.VISIBLE
                if (event.alarmTime != null) {
                    mModel.reminderEnum = ReminderEnum.getReminderEnumByText(event.alarmTimeStr.toString())
                    alarmEditText?.text = event.alarmTimeStr
                    alarmAddEditTextView.text = resources.getString(R.string.text_ubah)
                } else {
                    mModel.reminderEnum = ReminderEnum.NO_REMINDER
                    alarmEditText?.text = event.alarmTimeStr
                }
                mModel.locationEventMutableLiveData.postValue(event.location)
                var selectedPosition = 0
                event.categories?.forEach { eventCategory ->
                    if (eventCategory.name == event.category) {
                        return@forEach
                    }
                    selectedPosition++
                }
                categorySpinner.setSelection(selectedPosition)
                noteEditText?.setText(event.notes)

                // Checklist
                checklistHeaderMore.setOnClickListener(this)
                if (mChecklistAdapter == null) {
                    mChecklistAdapter = WardahSimpleAdapter(this, WardahItemType.EVENT_CHECKLIST, ViewMode.VIEW, event.checklist, this)
                    checklistRecyclerView.layoutManager = LinearLayoutManager(this)
                    checklistRecyclerView.adapter = mChecklistAdapter
                } else {
                    mChecklistAdapter?.data = event.checklist
                    mChecklistAdapter?.notifyDataSetChanged()
                }

                // Check In Rules
                if (event.checkIn != null && event.checkIn!!.checkInTime > 0L) {
                    mModel.viewModeMutableLiveData.postValue(ViewMode.VIEW)

                    val checkInStr = getString(R.string.title_waktu_check_in, event.checkInStr)
                    checkInTextView.text = checkInStr

                    val bannerImages: ArrayList<String> = ArrayList()
                    Log.d(TAG, "checkIn = ${WardahApp.getInstance().gson.toJson(event.checkIn)}")
                    Log.d(TAG, "bpImageURL = ${event.checkIn!!.bpImageURL}")
                    Log.d(TAG, "boothImageURL = ${event.checkIn!!.boothImageURL}")
                    val bpImageUrl = ImageUtil.getImageUrl(event.checkIn!!.bpImageURL)
                    val boothImageUrl = ImageUtil.getImageUrl(event.checkIn!!.boothImageURL)
                    bannerImages.add(bpImageUrl)
                    bannerImages.add(boothImageUrl)


                    checkInSlider.setAdapter(BannerSliderAdapter(bannerImages))
                } else {
                    if ((DateUtils.isToday(event.startTime)) || (DateUtils.isToday(event.endTime))) {
                        checkInActionTextView.visibility = View.VISIBLE
                        checkInActionTextView.setOnClickListener(this)
                        checkInOutRightArrowImageView.visibility = View.VISIBLE
                    } else {
                        checkInActionTextView.visibility = View.GONE
                        checkInActionTextView.setOnClickListener(null)
                        checkInOutRightArrowImageView.visibility = View.GONE
                    }
                    checkInContentLinearLayout.visibility = View.GONE
                    checkInEmptyTextView.visibility = View.VISIBLE
                }

                // Attendant
                attendantHeaderMore.setOnClickListener(this)
                if (event.attendant == null || event.attendant!!.isEmpty()) {
                    attendantEmptyMessageTextView.visibility = View.VISIBLE
                } else {
                    if (mAttendantAdapter == null) {
                        attendantRecyclerView.layoutManager = LinearLayoutManager(this@CreateEventActivity)
                        mAttendantAdapter = WardahSimpleAdapter(this@CreateEventActivity, WardahItemType.ATTENDANT, event.attendant, this@CreateEventActivity)
                        attendantRecyclerView.adapter = mAttendantAdapter

                        if (DateTimeUtil.isFutureEvent(event.startTime, event.endTime)) {
                            attendantHeaderMore.visibility = View.GONE
                            attendantRightArrowImageVIew.visibility = View.GONE
                            attendantRecyclerView.visibility = View.GONE
                            attendantEmptyMessageTextView.visibility = View.VISIBLE
                        }
                    } else {
                        mAttendantAdapter?.data = event.attendant
                        mAttendantAdapter?.notifyDataSetChanged()
                    }
                }

                // Created By
                when (UserTypeEnum.getUserTypeEnum(SessionUtil.getUserType())) {
                    UserTypeEnum.BEAUTY_PROMOTER_LEADER -> {
                        llCreatedBy!!.visibility = View.VISIBLE
                    }
                    else -> {

                    }
                }

            }
        })

        // Checklist (Selengkapnya)
        mModel.checklistEventMutableLiveData.observe(this, Observer<ChecklistEvent> { checklistEvent ->
            if (checklistEvent != null) {
                val iChecklist = Intent(baseContext, ChecklistActivity::class.java)
                if (checklistEvent.editButtonEnable) {
                    iChecklist.putExtra(IntentUtil.VIEW_MODE, ViewMode.EDIT)
                } else {
                    iChecklist.putExtra(IntentUtil.VIEW_MODE, ViewMode.VIEW)
                }
                iChecklist.putExtra(IntentUtil.CHECKLIST, checklistEvent)
                startActivityForResult(iChecklist, CHECKLIST_RESULT_ACTIVITY)
            }
        })

        mModel.isSubmitSucceedMutableLiveData.observe(this, Observer<Boolean> { isSuccess ->
            if (isSuccess != null && isSuccess) {
                setResult(Activity.RESULT_OK)
                finish()
            }
        })

        // Location
        mModel.locationEventMutableLiveData
                .observe(this, Observer<EventLocation> { eventLocation ->
                    if (eventLocation != null) {
                        eventLocationEditText.setText(eventLocation.formattedAddress)
                        val latLng = LatLng(eventLocation.latitude.toDouble(), eventLocation.longitude.toDouble())
                        miniMapLinearLayout?.visibility = View.VISIBLE
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
                        mMap.addMarker(MarkerOptions().position(latLng).title(eventLocation.formattedAddress)).showInfoWindow()
                    }
                })

        // Category
        mModel.eventCategoryListMutableLiveData.observe(this, Observer<List<EventCategory>> { eventCategories ->
            if (eventCategories != null) {
                mCategoryAdapter = EventCategoryAdapter(applicationContext, R.layout.spinner_item, eventCategories)
                categorySpinner.adapter = mCategoryAdapter
            }
        })

        // Start time and alarm
        mModel.startTimeMutableLiveData.observe(this, Observer<Long> { startTime ->
            if (startTime != null) {
                alarmAddEditTextView.visibility = View.VISIBLE
            } else {
                alarmAddEditTextView.visibility = View.GONE
            }
        })

        // Attendant Button
        mModel.attendantEventMutableLiveData.observe(this, Observer<AttendantButton> { attendantButton ->
            if (attendantButton != null) {
                if (attendantButton.inputButtonEnable) {
                    val iAttendant = Intent(baseContext, AttendantActivity::class.java)
                    iAttendant.putExtra(IntentUtil.ATTENDANT_BUTTON, attendantButton)
                    startActivity(iAttendant)
                } else {
                    val iAttendantHistory = Intent(baseContext, AttendantHistoryActivity::class.java)
                    iAttendantHistory.putExtra(IntentUtil.CALENDAR_ID, attendantButton.calendarId)
                    startActivity(iAttendantHistory)
                }
            }
        })
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            startTimeTextView.id -> {
                isEventStartDialog = true
                DatePickerDialog(this,
                        mDateSetListener,
                        mModel.startTimeCalendar.get(Calendar.YEAR),
                        mModel.startTimeCalendar.get(Calendar.MONTH),
                        mModel.startTimeCalendar.get(Calendar.DAY_OF_MONTH)).show()
            }
            endTimeTextView.id -> {
                isEventStartDialog = false
                DatePickerDialog(this,
                        mDateSetListener,
                        mModel.endTimeCalendar.get(Calendar.YEAR),
                        mModel.endTimeCalendar.get(Calendar.MONTH),
                        mModel.endTimeCalendar.get(Calendar.DAY_OF_MONTH)).show()
            }
            alarmAddEditTextView.id -> {
                val intent = Intent(this, ReminderActivity::class.java)
                intent.putExtra(IntentUtil.REMINDER, mModel.reminderEnum)
                startActivityForResult(intent, REQUEST_REMINDER)
            }
            eventLocationTextView.id -> {
                if (LocationUtil.isGPSEnabled) {
                    val intent = Intent(this, SelectEventLocationActivity::class.java)
                    startActivityForResult(intent, REQUEST_MAP)
                } else {
                    LocationUtil.popupEnableGPS(this)
                }
            }
            checklistHeaderMore.id -> {
                mModel.callChecklistEventData(mModel.eventMutableLiveData.value!!.calendarId)
            }
            checkInActionTextView.id -> {
                val iCheckIn = Intent(this, CheckInActivity::class.java)
                iCheckIn.putExtra(IntentUtil.EVENT, mModel.eventMutableLiveData.value)
                startActivity(iCheckIn)
            }
            attendantHeaderMore.id -> {
                when (mModel.userType) {
                    UserTypeEnum.BEAUTY_PROMOTER_LEADER -> {
                        val iAttendantHistory = Intent(this, AttendantHistoryActivity::class.java)
                        startActivity(iAttendantHistory)
                    }
                    else -> {
                        val event = mModel.eventMutableLiveData.value
                        mModel.callAttendantEventData(event!!.calendarId)
                    }
                }
            }
        }
    }

    override fun onClick(item: WardahHistoryRecyclerViewItem, viewMode: ViewMode) {
        val event = mModel.eventMutableLiveData.value
        val participant = item as Attendant

        val iPeserta = Intent(this, InputAttendantActivity::class.java)
        iPeserta.putExtra(IntentUtil.CUSTOMER, participant)
        if (DateUtils.isToday(event!!.startTime) || DateUtils.isToday(event.endTime)) {
            iPeserta.putExtra(IntentUtil.VIEW_MODE, ViewMode.EDIT)
        } else {
            iPeserta.putExtra(IntentUtil.VIEW_MODE, ViewMode.VIEW)
        }
        startActivity(iPeserta)
    }

    override fun onCheck(checklist: Checklist, isChecked: Boolean) {
        Log.d(TAG, "onCheck: checklist = " + WardahApp.getInstance().gson.toJson(checklist) + ", isChecked = " + isChecked)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        try {
            when (mModel.viewModeMutableLiveData.value) {
                ViewMode.INPUT -> {
                    menuInflater.inflate(R.menu.menu_input, menu)
                }
                ViewMode.EDIT -> {
                    menuInflater.inflate(R.menu.menu_edit, menu)
                }
                ViewMode.VIEW -> {
                    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
                }
            }
        } catch (npe: NullPointerException) {
            npe.printStackTrace()
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                attemptSubmit(ViewMode.INPUT)
                return true
            }
            R.id.action_edit -> {
                attemptSubmit(ViewMode.EDIT)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                var event = mModel.eventMutableLiveData.value
                if (event != null) {
                    mModel.callEventView(event.calendarId, SessionUtil.getUserId())
                }
            }
        }

        if (requestCode == REQUEST_REMINDER && resultCode == Activity.RESULT_OK) {
            val reminder = data?.getSerializableExtra(IntentUtil.REMINDER) as ReminderEnum
            alarmEditText?.text = reminder.text
            alarmAddEditTextView?.text = resources.getString(R.string.text_ubah)
            mModel.reminderEnum = reminder
        } else if (requestCode == REQUEST_MAP && resultCode == Activity.RESULT_OK) {
            val address = data?.extras?.get(SelectEventLocationActivity.PARAM_MAP_ADDRESS) as Address
            val eventLocation = EventLocation(0L, address.getAddressLine(0), address.latitude.toString(), address.longitude.toString())
            mModel.locationEventMutableLiveData.postValue(eventLocation)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!

        val i = intent

        if (i != null) {
            mModel.viewModeMutableLiveData.value = (i.getSerializableExtra(IntentUtil.VIEW_MODE) as ViewMode)
            val event = i.getSerializableExtra(IntentUtil.EVENT)
            if (event != null) {
                mModel.eventMutableLiveData.postValue(event as Event)
            }
            val intentEventCategory = i.getSerializableExtra(IntentUtil.EVENT_CATEGORY)
            if (intentEventCategory != null) {
                mModel.eventCategoryListMutableLiveData.postValue((intentEventCategory as IntentEventCategory).objects)
            }
        } else {
            mModel.viewModeMutableLiveData.postValue(ViewMode.INPUT)
        }
    }

    private fun attemptSubmit(viewMode: ViewMode?) {
        titleEditText.error = null
        startTimeTextView.error = null
        endTimeTextView.error = null
        eventLocationEditText.error = null

        val titleStr = titleEditText.text.toString()
        val startTimeStr = startTimeTextView.text.toString()
        val endTimeStr = endTimeTextView.text.toString()
        val alarmTimeStr = alarmEditText.text.toString()
        val eventLocation = mModel.locationEventMutableLiveData.value
        val noteStr = noteEditText.text.toString()

        var cancel = false
        var focusView: View? = null

        if (TextUtils.isEmpty(titleStr)) {
            titleEditText.error = getString(R.string.error_field_required)
            focusView = titleEditText
            cancel = true
        } else if (TextUtils.isEmpty(startTimeStr)) {
            startTimeTextView.error = getString(R.string.error_field_required)
            focusView = startTimeTextView
            cancel = true
        } else if (TextUtils.isEmpty(endTimeStr)) {
            endTimeTextView.error = getString(R.string.error_field_required)
            focusView = endTimeTextView
            cancel = true
        } else if (TextUtils.isEmpty(eventLocation?.formattedAddress)) {
            eventLocationEditText.error = getString(R.string.error_field_required)
            focusView = eventLocationEditText
            cancel = true
        }

        if (cancel) {
            focusView?.requestFocus()
        } else {
            when (viewMode) {
                ViewMode.INPUT -> {
                    val eventLocation = EventLocation(0L, eventLocation!!.formattedAddress, eventLocation.latitude, eventLocation.longitude)
                    val eventCreateRequest = EventCreateRequest(SessionUtil.getUserId(), titleStr,
                            mCategoryAdapter.getData(categorySpinner.selectedItemPosition).name,
                            mModel.startTimeCalendar.timeInMillis, mModel.endTimeCalendar.timeInMillis,
                            ReminderEnum.getReminder(mModel.startTimeCalendar.timeInMillis, mModel.reminderEnum), alarmTimeStr, noteStr, eventLocation)
                    mModel.submitEvent(eventCreateRequest)
                }
                ViewMode.EDIT -> {
                    val eventLocation = EventLocation(0L, eventLocation!!.formattedAddress, eventLocation.latitude, eventLocation.longitude)
                    val eventEditRequest = EventEditRequest(mModel.eventMutableLiveData.value!!.calendarId,
                            SessionUtil.getUserId(), titleStr,
                            mCategoryAdapter.getData(categorySpinner.selectedItemPosition).name,
                            mModel.startTimeCalendar.timeInMillis, mModel.endTimeCalendar.timeInMillis,
                            ReminderEnum.getReminder(mModel.startTimeCalendar.timeInMillis, mModel.reminderEnum), alarmTimeStr, noteStr, eventLocation)
                    mModel.editEvent(eventEditRequest)
                }
            }
        }
    }
}


