package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class AttendantButton : Serializable {
    var calendarId: Long
    val inputButtonEnable: Boolean
    val attendantsEditable: Boolean

    constructor(calendarId: Long, inputButtonEnable: Boolean, attendantsEditable: Boolean) {
        this.calendarId = calendarId
        this.inputButtonEnable = inputButtonEnable
        this.attendantsEditable = attendantsEditable
    }
}