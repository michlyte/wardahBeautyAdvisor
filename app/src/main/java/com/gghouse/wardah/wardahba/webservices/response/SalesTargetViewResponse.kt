package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.SalesTarget

class SalesTargetViewResponse : GenericResponse() {
    var data: SalesTarget? = null
}
