package com.gghouse.wardah.wardahba.webservices.model

class SalesLatestDate {
    var inputDate: Long? = null

    constructor(inputDate: Long?) {
        this.inputDate = inputDate
    }
}