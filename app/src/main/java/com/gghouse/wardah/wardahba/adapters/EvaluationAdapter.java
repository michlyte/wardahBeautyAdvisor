package com.gghouse.wardah.wardahba.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.webservices.model.EvaluationChecklist;

import java.util.List;

/**
 * Created by michaelhalim on 5/25/17.
 */

public class EvaluationAdapter extends RecyclerView.Adapter<EvaluationAdapter.ViewHolder> {
    private List<EvaluationChecklist> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mTVQuestion;
        public final RatingBar mRBRating;
        public EvaluationChecklist mItem;

        public ViewHolder(View v) {
            super(v);
            mTVQuestion = v.findViewById(R.id.tv_question);
            mRBRating = v.findViewById(R.id.rb_rating);
        }
    }

    public EvaluationAdapter(List<EvaluationChecklist> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public EvaluationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_questioner, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mItem = mDataset.get(position);
        holder.mTVQuestion.setText(holder.mItem.getContent());
        holder.mRBRating.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> mDataset.get(position).setRating(rating));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setData(List<EvaluationChecklist> dataSet) {
        mDataset = dataSet;
    }

    public List<EvaluationChecklist> getData() {
        return mDataset;
    }
}

