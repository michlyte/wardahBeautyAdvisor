package com.gghouse.wardah.wardahba.activities.event;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.fragments.WardahSimpleHistoryFragment;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.webservices.model.Event;

public class ChecklistBAHistoryActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist_ba_history);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Long calendarId = intent.getLongExtra(IntentUtil.Companion.getCALENDAR_ID(), 0L);
        Long locationId = intent.getLongExtra(IntentUtil.Companion.getLOCATION_ID(), 0L);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), calendarId, locationId);

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private Long mCalendarId;
        private Long mLocationId;

        public SectionsPagerAdapter(FragmentManager fm, Long calendarId, Long locationId) {
            super(fm);
            mCalendarId = calendarId;
            mLocationId = locationId;
        }

        @Override
        public Fragment getItem(int position) {
            return WardahSimpleHistoryFragment.newInstance(WardahItemType.EVENT_CHECKLIST_BA, mCalendarId, mLocationId);
        }

        @Override
        public int getCount() {
            return 1;
        }
    }
}
