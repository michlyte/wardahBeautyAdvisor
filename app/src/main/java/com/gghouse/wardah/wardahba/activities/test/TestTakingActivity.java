package com.gghouse.wardah.wardahba.activities.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.questionnaire.QuestionerActivity;
import com.gghouse.wardah.wardahba.adapters.TestTakingAdapter;
import com.gghouse.wardah.wardahba.models.IntentQuestions;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.LogUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.viewmodels.TestTakingActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.Question;
import com.gghouse.wardah.wardahba.webservices.request.AnswerRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class TestTakingActivity extends AppCompatActivity {

    public static final String TAG = TestTakingActivity.class.getSimpleName();

    private TestTakingActivityViewModel mModel;
    private TestTakingAdapter mAdapter;

    /*
     * Question
     */
    private Boolean mLock;
    private List<Question> mQuestionList;

    /*
     * Loading view
     */
    private MaterialDialog mMaterialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_taking);

        mModel = ViewModelProviders.of(this).get(TestTakingActivityViewModel.class);

        Intent i = getIntent();
        if (i == null) {
            mQuestionList = new ArrayList<>();
            mModel.setDate(new Date());
            mLock = true;
        } else {
            IntentQuestions intentQuestions = (IntentQuestions) i.getSerializableExtra(IntentUtil.Companion.getDATA());
            mQuestionList = intentQuestions.getObjects();
            mModel.setDate(DateTimeUtil.INSTANCE.getDateFromParam(i, IntentUtil.Companion.getDATE()));
            mLock = WardahUtil.INSTANCE.getBoolFromParam(i, IntentUtil.Companion.getLOCK());
        }

        LogUtil.INSTANCE.log("[onCreate] Date: " + DateTimeUtil.INSTANCE.getSdfFilter().format(mModel.getDate()));

        getSupportActionBar().setDisplayHomeAsUpEnabled(!mLock);
        String title = getSupportActionBar().getTitle().toString() + " - " + DateTimeUtil.INSTANCE.getSdfFilter().format(mModel.getDate());
        getSupportActionBar().setTitle(title);

        RecyclerView recyclerView = findViewById(R.id.rv_ATT_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new TestTakingAdapter(this, mQuestionList);
        recyclerView.setAdapter(mAdapter);

        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(R.string.title_loading)
                .content(R.string.title_submitting_answers)
                .cancelable(false)
                .progress(true, 0);
        mMaterialDialog = builder.build();

        mModel.getSubmitResponseMutableLiveData().observe(this, genericResponse -> {
            if (genericResponse != null) {
                mModel.callQuestionerLatestDate();
            }
        });

        mModel.getQuestionerLatestDateMutableLiveData().observe(this, questionerLatestDate -> {
            if (questionerLatestDate != null) {
                Intent iQuestioner = new Intent(this, QuestionerActivity.class);
                iQuestioner.putExtra(IntentUtil.Companion.getDATE(), questionerLatestDate);
                startActivity(iQuestioner);

                setResult(Activity.RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                attemptCancel();
                return true;
            case R.id.action_save:
                attemptDone();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        attemptCancel();
    }

    private void attemptCancel() {
        if (mLock) {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_warning)
                    .content(R.string.message_test_cancel)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .show();
        } else {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_cancel)
                    .content(R.string.message_back_confirmation)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .negativeText(R.string.action_cancel)
                    .negativeColorRes(R.color.colorAccent)
                    .onPositive((dialog, which) -> finish())
                    .show();
        }
    }

    private void attemptDone() {
        if (mQuestionList.size() != mAdapter.getResultMap().entrySet().size()) {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_warning)
                    .content(R.string.title_answer_all_questions)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .show();
        } else {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_create_test)
                    .content(R.string.message_test_confirmation)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .negativeText(R.string.action_cancel)
                    .negativeColorRes(R.color.colorAccent)
                    .cancelable(false)
                    .onPositive((dialog, which) -> {
                        if (SessionUtil.validateLoginSession()) {
                            Long userId = SessionUtil.getUserId();
                            List<AnswerRequest> answerRequestList = new ArrayList<AnswerRequest>();
                            for (Map.Entry<Long, Object> entry : mAdapter.getResultMap().entrySet()) {
                                answerRequestList.add(new AnswerRequest(entry.getKey(), (long) entry.getValue()));
                            }
                            mModel.submitAnswers(mMaterialDialog, userId, answerRequestList);
                        }
                    })
                    .show();
        }
    }
}
