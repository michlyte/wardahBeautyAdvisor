package com.gghouse.wardah.wardahba.activities;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class WardahHistoryActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = WardahHistoryActivity.class.getSimpleName();

    private Button mBReset;
    private Menu mMenu;

    /*
     * Refresh
     */
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    /*
     * Filters
     */
    protected LinearLayout mLlFilter;
    protected TextView mTvFilter;
    protected Drawable mDFilter;
    protected Date mDBegin;
    protected Date mDEnd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSwipeRefreshLayout = findViewById(R.id.srl_swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(() -> onSwipeToRefresh());

        /*
         * Filter
         */
        mLlFilter = findViewById(R.id.ll_filter);
        mTvFilter = findViewById(R.id.tv_filter);
        mBReset = findViewById(R.id.b_reset);
        mBReset.setOnClickListener(v -> {
            mDBegin = null;
            mDEnd = null;

            mMenu.findItem(R.id.action_filter).setIcon(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_filter));
            mDFilter = mMenu.findItem(R.id.action_filter).getIcon();
            mDFilter.setColorFilter(ContextCompat.getColor(getBaseContext(), android.R.color.white), PorterDuff.Mode.SRC_ATOP);

            mLlFilter.setVisibility(View.GONE);

            onClickResetButton();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.menu_history_filter, menu);
        mDFilter = menu.findItem(R.id.action_filter).getIcon();
        mDFilter.mutate();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_filter:
                Calendar cFrom = Calendar.getInstance();
                cFrom.setTime((mDBegin == null ? new Date() : mDBegin));
                Calendar cTo = Calendar.getInstance();
                cTo.setTime((mDEnd == null ? new Date() : mDEnd));
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        this,
                        cFrom.get(Calendar.YEAR),
                        cFrom.get(Calendar.MONTH),
                        cFrom.get(Calendar.DAY_OF_MONTH),
                        cTo.get(Calendar.YEAR),
                        cTo.get(Calendar.MONTH),
                        cTo.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        try {
            String from = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
            String to = dayOfMonthEnd + "/" + (monthOfYearEnd + 1) + "/" + yearEnd;
            mDBegin = DateTimeUtil.INSTANCE.getSdfFilter().parse(from);
            mDEnd = DateTimeUtil.INSTANCE.getSdfFilter().parse(to);

            /*
             * Change filter icon color
             */
            mMenu.findItem(R.id.action_filter).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_filter_fill));
            mDFilter = mMenu.findItem(R.id.action_filter).getIcon();
            mDFilter.mutate();
            mDFilter.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);

            if (mDBegin != null && mDEnd != null) {
                String dateRangeStr = DateTimeUtil.INSTANCE.getSdfFilter().format(mDBegin) + " - " + DateTimeUtil.INSTANCE.getSdfFilter().format(mDEnd);
                mTvFilter.setText(dateRangeStr);
                mLlFilter.setVisibility(View.VISIBLE);
            }

            onClickResetButton();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    protected void onSwipeToRefresh() {

    }

    protected void onClickResetButton() {

    }
}
