package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.StoreSales

/**
 * Created by michael on 3/13/2017.
 */

class StoreSalesLatestResponse : GenericResponse() {
    var data: List<StoreSales>? = null
}
