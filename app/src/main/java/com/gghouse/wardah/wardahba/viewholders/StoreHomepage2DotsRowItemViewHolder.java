package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.models.StoreHomepage2DotsRowItem;
import com.gghouse.wardah.wardahba.utils.SystemUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.squareup.picasso.Picasso;

public class StoreHomepage2DotsRowItemViewHolder extends RecyclerView.ViewHolder {
    private final TextView mLeftTitleTextView;
    private final TextView mRightTitleTextView;
    private final TextView mLeftValueTextView;
    private final TextView mRightValueTextView;
    private final ImageView mLeftImageView;
    private final ImageView mRightImageView;
    public StoreHomepage2DotsRowItem mItem;

    public StoreHomepage2DotsRowItemViewHolder(View v) {
        super(v);
        mLeftTitleTextView = (TextView) v.findViewById(R.id.tvLeftTitle);
        mRightTitleTextView = (TextView) v.findViewById(R.id.tvRightTitle);

        mLeftValueTextView = (TextView) v.findViewById(R.id.tvLeftValue);
        mRightValueTextView = (TextView) v.findViewById(R.id.tvRightValue);

        mLeftImageView = (ImageView) v.findViewById(R.id.ivLeftIcon);
        mRightImageView = (ImageView) v.findViewById(R.id.ivRightIcon);
    }

    public void setData(StoreHomepage2DotsRowItem storeHomepageRowItem) {
        mItem = storeHomepageRowItem;

        mLeftTitleTextView.setText(mItem.getLeftTitle());
        mRightTitleTextView.setText(mItem.getRightTitle());

        mLeftValueTextView.setText(SystemUtil.withSuffix(
                mItem.getLeftValue() != null ? Double.parseDouble(mItem.getLeftValue()) : 0.00, 0));
        mRightValueTextView.setText(SystemUtil.withSuffix(
                mItem.getRightValue() != null ? Double.parseDouble(mItem.getRightValue()) : 0.00, 0));

        Picasso.get()
                .load(WardahUtil.INSTANCE.validateResId(mItem.getLeftDrawableResId()))
                .into(mLeftImageView);
        Picasso.get()
                .load(WardahUtil.INSTANCE.validateResId(mItem.getRightDrawableResId()))
                .into(mRightImageView);
    }
}
