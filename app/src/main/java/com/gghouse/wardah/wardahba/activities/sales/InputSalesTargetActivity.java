package com.gghouse.wardah.wardahba.activities.sales;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.viewmodels.InputSalesTargetActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.DCDropdown;
import com.gghouse.wardah.wardahba.webservices.model.DCLocationDropdown;
import com.gghouse.wardah.wardahba.webservices.model.SalesTarget;
import com.gghouse.wardah.wardahba.webservices.request.SalesTargetCreateRequest;
import com.gghouse.wardah.wardahba.webservices.request.SalesTargetEditRequest;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class InputSalesTargetActivity extends AppCompatActivity implements MonthPickerDialog.OnDateSetListener {

    private static final String TAG = InputSalesTargetActivity.class.getSimpleName();

    private InputSalesTargetActivityViewModel mModel;

    private AutoCompleteTextView mDCAutoCompleteTextView;
    private ArrayAdapter<String> mDCSpinnerAdapter;
    private AutoCompleteTextView mLocationAutoCompleteTextView;
    private ArrayAdapter<String> mLocationSpinnerAdapter;
    private TextView mMonthYearTextView;
    private EditText mTargetEditText;

    private MonthPickerDialog.Builder mBuilder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_target_input);

        mModel = ViewModelProviders.of(this).get(InputSalesTargetActivityViewModel.class);

        Intent intent = getIntent();
        if (intent != null) {
            mModel.getViewModeMutableLiveData().postValue((ViewMode) intent.getSerializableExtra(IntentUtil.Companion.getVIEW_MODE()));
            mModel.getSalesTargetMutableLiveData().postValue((SalesTarget) intent.getSerializableExtra(IntentUtil.Companion.getINPUT_TARGET()));
        } else {
            mModel.getViewModeMutableLiveData().postValue(ViewMode.VIEW);
        }

        mDCAutoCompleteTextView = findViewById(R.id.dcAutoCompleteTextView);
        mLocationAutoCompleteTextView = findViewById(R.id.locationAutoCompleteTextView);
        mMonthYearTextView = findViewById(R.id.monthYearTextView);
        mMonthYearTextView.setOnClickListener(view -> {
            final Calendar today = Calendar.getInstance();
            mBuilder = new MonthPickerDialog.Builder(InputSalesTargetActivity.this, this, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

            Calendar selectedDateCalendar = Calendar.getInstance();
            selectedDateCalendar.set(mModel.getYear(), mModel.getMonth(), 1);

            mBuilder.setActivatedMonth(selectedDateCalendar.get(Calendar.MONTH))
                    .setMinYear(1990)
                    .setActivatedYear(selectedDateCalendar.get(Calendar.YEAR))
                    .setMaxYear(2030)
                    .setMinMonth(Calendar.JANUARY)
                    .setTitle(getString(R.string.title_select_date_time))
                    .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)
                    .setOnMonthChangedListener(selectedMonth -> Log.d(TAG, "Selected month : " + selectedMonth))
                    .setOnYearChangedListener(selectedYear -> Log.d(TAG, "Selected year : " + selectedYear))
                    .build()
                    .show();
        });
        mTargetEditText = findViewById(R.id.targetEditText);

        mModel.getViewModeMutableLiveData().observe(this, viewMode -> {
            mLocationAutoCompleteTextView.setEnabled(false);

            if (viewMode != null) {
                switch (viewMode) {
                    case INPUT:
                        setTitle(R.string.title_create_sales_target);
                        break;
                    case EDIT:
                        setTitle(R.string.title_edit_sales_target);
                        mMonthYearTextView.setEnabled(false);
                        mDCAutoCompleteTextView.setEnabled(false);
                        mLocationAutoCompleteTextView.setEnabled(false);
                        break;
                    case VIEW:
                        setTitle(R.string.title_view_sales_target);
                        mMonthYearTextView.setEnabled(false);
                        mDCAutoCompleteTextView.setEnabled(false);
                        mLocationAutoCompleteTextView.setEnabled(false);
                        mTargetEditText.setEnabled(false);
                        break;
                }
            }
        });
        mModel.getSalesTargetMutableLiveData().observe(this, salesTarget -> {
            if (salesTarget != null) {
                mMonthYearTextView.setText(DateTimeUtil.INSTANCE.getSdfMonthCommaYear().format(DateTimeUtil.INSTANCE.getDateFromMonthYear(salesTarget.getMonth(), salesTarget.getYear())));
                mTargetEditText.setText(String.valueOf(salesTarget.getSalesTarget()));
                mDCAutoCompleteTextView.setText(salesTarget.getDc());
                mLocationAutoCompleteTextView.setText(salesTarget.getLocation());
            }
        });
        mModel.getDcMutableLiveData().observe(this, dc -> {
            if (dc != null && mModel.getViewModeMutableLiveData().getValue() != null) {
                switch (mModel.getViewModeMutableLiveData().getValue()) {
                    case INPUT:
                    case EDIT:
                        mLocationAutoCompleteTextView.setEnabled(true);
                        break;
                    case VIEW:
                        mLocationAutoCompleteTextView.setEnabled(false);
                        break;
                }
                mModel.callDCLocationDropdown(dc, "");
            }
        });
        mModel.getDcDropdownMutableLiveData().observe(this, dcDropdowns -> {
            if (dcDropdowns != null && dcDropdowns.size() > 0) {
                List<String> dcList = new ArrayList<>();
                for (DCDropdown dcDropdown : dcDropdowns) {
                    dcList.add(dcDropdown.getName());
                }
                mDCSpinnerAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, dcList);
                mDCAutoCompleteTextView.setAdapter(mDCSpinnerAdapter);

                mDCAutoCompleteTextView.setOnItemClickListener((parent, view, position, id) -> {
                    mModel.getDcMutableLiveData().postValue(mDCSpinnerAdapter.getItem(position));
                });
            }
        });
        mModel.getDcLocationDropdownMutableLiveData().observe(this, dcLocationDropdowns -> {
            if (dcLocationDropdowns != null && dcLocationDropdowns.size() > 0) {
                List<String> dcLocationList = new ArrayList<>();
                for (DCLocationDropdown dcLocationDropdown : dcLocationDropdowns) {
                    dcLocationList.add(dcLocationDropdown.getName());
                }
                mLocationSpinnerAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, dcLocationList);
                mLocationAutoCompleteTextView.setAdapter(mLocationSpinnerAdapter);
            }
        });
        mModel.isSubmitSucceedMutableLiveData().observe(this, isSuccess -> {
            if (isSuccess != null && isSuccess) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mModel.getViewModeMutableLiveData().getValue() != null) {
            switch (mModel.getViewModeMutableLiveData().getValue()) {
                case VIEW:
                    break;
                case INPUT:
                    getMenuInflater().inflate(R.menu.menu_input, menu);
                    break;
                case EDIT:
                    getMenuInflater().inflate(R.menu.menu_edit, menu);
                    break;
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                attemptCancel();
                return true;
            case R.id.action_save:
                attemptSubmit();
                return true;
            case R.id.action_edit:
                attemptEdit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        attemptCancel();
    }

    @Override
    public void onDateSet(int selectedMonth, int selectedYear) {
        mModel.setMonth(selectedMonth);
        mModel.setYear(selectedYear);

        Calendar resultDate = Calendar.getInstance();
        resultDate.set(selectedYear, selectedMonth, 1);

        mMonthYearTextView.setText(DateTimeUtil.INSTANCE.getSdfMonthCommaYear().format(resultDate.getTime()));
    }

    private void attemptSubmit() {
        mDCAutoCompleteTextView.setError(null);
        mLocationAutoCompleteTextView.setError(null);
        mTargetEditText.setError(null);

        boolean cancel = false;
        View focusView = null;

        String dc = mDCAutoCompleteTextView.getText().toString();
        String location = mLocationAutoCompleteTextView.getText().toString();
        DCDropdown dcDropdown = getDCDropdown(dc);
        DCLocationDropdown dcLocationDropdown = getDCLocationDropdown(location);
        String salesTargetIDRStr = mTargetEditText.getText().toString();

        if (TextUtils.isEmpty(dc)) {
            mDCAutoCompleteTextView.setError(getString(R.string.error_field_required));
            focusView = mDCAutoCompleteTextView;
            cancel = true;
        } else if (dcDropdown == null) {
            mDCAutoCompleteTextView.setError(getString(R.string.error_dc_invalid));
            focusView = mDCAutoCompleteTextView;
            cancel = true;
        } else if (TextUtils.isEmpty(location)) {
            mLocationAutoCompleteTextView.setError(getString(R.string.error_field_required));
            focusView = mLocationAutoCompleteTextView;
            cancel = true;
        } else if (dcLocationDropdown == null) {
            mLocationAutoCompleteTextView.setError(getString(R.string.error_location_invalid));
            focusView = mLocationAutoCompleteTextView;
            cancel = true;
        } else if (TextUtils.isEmpty(salesTargetIDRStr)) {
            mTargetEditText.setError(getString(R.string.error_dc_invalid));
            focusView = mTargetEditText;
            cancel = true;
        } else {
            try {
                Double.parseDouble(salesTargetIDRStr);
            } catch (Exception e) {
                mTargetEditText.setError(getString(R.string.error_field_invalid));
                focusView = mTargetEditText;
                cancel = true;
            }
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_create_sales_target)
                    .content(R.string.message_create_sales_target_confirmation)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .negativeText(R.string.action_cancel)
                    .negativeColorRes(R.color.colorAccent)
                    .onPositive((dialog, which) -> {
                        SalesTargetCreateRequest salesTargetCreateRequest = new SalesTargetCreateRequest(SessionUtil.getUserId(), dcLocationDropdown.getId(),
                                mModel.getMonth() + 1, mModel.getYear(), Double.parseDouble(salesTargetIDRStr));
                        mModel.salesTargetSubmit(salesTargetCreateRequest);
                    })
                    .show();
        }
    }

    private void attemptEdit() {
        mTargetEditText.setError(null);

        boolean cancel = false;
        View focusView = null;

        String salesTargetIDRStr = mTargetEditText.getText().toString().replace(",", "");

        if (TextUtils.isEmpty(salesTargetIDRStr)) {
            mTargetEditText.setError(getString(R.string.error_dc_invalid));
            focusView = mTargetEditText;
            cancel = true;
        } else {
            try {
                Double.parseDouble(salesTargetIDRStr);
            } catch (Exception e) {
                mTargetEditText.setError(getString(R.string.error_field_invalid));
                focusView = mTargetEditText;
                cancel = true;
            }
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_edit_sales_target)
                    .content(R.string.message_edit_sales_target_confirmation)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .negativeText(R.string.action_cancel)
                    .negativeColorRes(R.color.colorAccent)
                    .onPositive((dialog, which) -> {
                        SalesTargetEditRequest salesTargetEditRequest = new SalesTargetEditRequest(
                                mModel.getSalesTargetMutableLiveData().getValue().getSalesTargetId(), Double.parseDouble(salesTargetIDRStr));
                        mModel.salesTargetEdit(salesTargetEditRequest);
                    })
                    .show();
        }
    }

    private void attemptCancel() {
        if (mModel.getViewModeMutableLiveData().getValue() != null) {
            switch (mModel.getViewModeMutableLiveData().getValue()) {
                case INPUT:
                    new MaterialDialog.Builder(this)
                            .title(R.string.title_cancel)
                            .content(R.string.message_back_confirmation)
                            .positiveText(R.string.action_ok)
                            .positiveColorRes(R.color.colorPrimary)
                            .negativeText(R.string.action_cancel)
                            .negativeColorRes(R.color.colorAccent)
                            .onPositive((dialog, which) -> finish())
                            .show();
                    break;
                default:
                    finish();
                    break;
            }
        } else {
            finish();
        }
    }

    private DCDropdown getDCDropdown(String name) {
        List<DCDropdown> dcDropdownList = mModel.getDcDropdownMutableLiveData().getValue();
        if (dcDropdownList != null) {
            for (DCDropdown dcDropdown : dcDropdownList) {
                if (name.equals(dcDropdown.getName())) {
                    return dcDropdown;
                }
            }
        }

        return null;
    }

    private DCLocationDropdown getDCLocationDropdown(String name) {
        List<DCLocationDropdown> dcLocationDropdownList = mModel.getDcLocationDropdownMutableLiveData().getValue();
        if (dcLocationDropdownList != null) {
            for (DCLocationDropdown dcLocationDropdown : dcLocationDropdownList) {
                if (name.equals(dcLocationDropdown.getName())) {
                    return dcLocationDropdown;
                }
            }
        }

        return null;
    }
}
