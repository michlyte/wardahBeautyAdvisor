package com.gghouse.wardah.wardahba.webservices.request

/**
 * Created by michaelhalim on 5/8/17.
 */

class AnswersRequest(private val userId: Long?, private val answerDate: Long?, private val answers: List<AnswerRequest>)
