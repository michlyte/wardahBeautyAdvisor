package com.gghouse.wardah.wardahba.utils

import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.Button
import androidx.core.content.ContextCompat
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.WardahApp
import com.gghouse.wardah.wardahba.webservices.model.EventCalendar
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

object WardahUtil {

    fun getStringFromInteger(value: Int?): String {
        return value?.toString() ?: "0"
    }

    fun getIntegerFromString(value: String?): Int {
        return if (value == null || value.isEmpty() || value == "null") {
            0
        } else {
            Integer.parseInt(value)
        }
    }

    fun getBoolFromParam(intent: Intent, param: String): Boolean? {
        val `object` = intent.extras!!.get(param)
        return if (`object` == null) {
            false
        } else {
            `object` as Boolean
        }
    }

    fun getAmount(prefix: String, amount: Double?, suffix: String): String {
        val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
        val symbols = formatter.decimalFormatSymbols

        symbols.groupingSeparator = ','
        formatter.decimalFormatSymbols = symbols

        return prefix + formatter.format(amount) + suffix
    }

    fun getAmount(amount: Double?): String {
        return if (amount == null) {
            WardahApp.getInstance().getString(R.string.text_empty_value)
        } else {
            getAmount("Rp. ", amount, ",-")
        }
    }

    fun getPcs(pcs: Int?): String {
        if (pcs == null) {
            return WardahApp.getInstance().getString(R.string.text_empty_value)
        } else {
            val pcsText = WardahApp.getInstance().appContext.getString(R.string.text_pieces)
            return "$pcs $pcsText"
        }
    }

    fun getSummaryInputSales(pcs: Int?, amount: Double?): String {
        val pcsText = getPcs(pcs)
        val amountText = getAmount(amount)
        return "$pcsText | $amountText"
    }

    fun getSummarySalesAmountAndPcs(amount: Double?, delimiter: String, pcs: Int?): String {
        val pcsText = getPcs(pcs)
        val amountText = getAmount(amount)
        return "$amountText $delimiter $pcsText"
    }

    fun getStoreSalesPcsAndAmountContent(pcs: Int?, amount: Double?): String {
        val pcsText = WardahApp.getInstance().appContext.getString(R.string.text_pieces) //pcs
        val amountWithFormat = getAmount(amount)
        return pcs.toString() + "" + pcsText + " | " + amountWithFormat
    }

    fun getStoreSalesAmountAndPcsContent(amount: Double?, pcs: Int?): String {
        val amountWithFormat = getAmount(amount)
        val pcsText = WardahApp.getInstance().appContext.getString(R.string.text_pieces) //pcs
        return "$amountWithFormat / $pcs $pcsText"
    }

    fun getStoreSalesVisitorAndBuyerContent(numOfVisitor: Int?, numOfBuyer: Int?): String {
        val pengunjungText = WardahApp.getInstance().appContext.getString(R.string.text_pengunjung)
        val pembeliText = WardahApp.getInstance().appContext.getString(R.string.text_pembeli)
        return numOfVisitor.toString() + " " + pengunjungText + " | " + numOfBuyer + " " + pembeliText
    }

    fun validateResId(resId: Int): Int {
        return if (resId == 0) {
            R.mipmap.ic_launcher
        } else {
            resId
        }
    }

    fun setEventButtonStyle(context: Context, button: Button, enabled: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (enabled)
                button.backgroundTintList = ContextCompat.getColorStateList(context, R.color.colorPrimary)
            else
                button.backgroundTintList = ContextCompat.getColorStateList(context, android.R.color.darker_gray)
        } else {
            if (enabled)
                button.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            else
                button.setBackgroundColor(ContextCompat.getColor(context, android.R.color.darker_gray))
        }
    }

    fun setEventButtons(checkIn: Boolean, attendant: Boolean, checklistButton: Button, checkInButton: Button, attendantButton: Button) {
        checklistButton.isEnabled = true
        setEventButtonStyle(WardahApp.getInstance().baseContext, checklistButton, true)

        checkInButton.isEnabled = checkIn
        setEventButtonStyle(WardahApp.getInstance().baseContext, checkInButton, checkIn)

        attendantButton.isEnabled = attendant
        setEventButtonStyle(WardahApp.getInstance().baseContext, attendantButton, attendant)
    }

    fun setEventButtons(event: EventCalendar, checklistButton: Button, checkInButton: Button, participantButton: Button) {
        checklistButton.isEnabled = true
        setEventButtonStyle(WardahApp.getInstance().baseContext, checklistButton, true)

        participantButton.isEnabled = event.attendantButtonEnable
        setEventButtonStyle(WardahApp.getInstance().baseContext, participantButton, event.attendantButtonEnable)

        checkInButton.isEnabled = event.checkInButtonEnable
        setEventButtonStyle(WardahApp.getInstance().baseContext, checkInButton, event.checkInButtonEnable)
    }

    fun setTabButton(context: Context, button: Button, enabled: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (enabled) {
                button.backgroundTintList = ContextCompat.getColorStateList(context, R.color.colorPrimary)
                button.setTextColor(ContextCompat.getColor(context, android.R.color.white))
            } else {
                button.backgroundTintList = ContextCompat.getColorStateList(context, android.R.color.white)
                button.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            }
        } else {
            if (enabled) {
                button.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
                button.setTextColor(ContextCompat.getColor(context, android.R.color.white))
            } else {
                button.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
                button.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            }
        }
    }

    fun getEventNumberOfAttendantStr(totalAttendant: Int?): String {
        return totalAttendant.toString() + " " + WardahApp.getInstance().getString(R.string.suffix_peserta)
    }

    fun getEventChecklistStr(checklistCompleted: Int?, totalChecklist: Int?): String {
        return checklistCompleted.toString() + "/" + totalChecklist.toString() + " " + WardahApp.getInstance().getString(R.string.suffix_checklist_completed)
    }
}
