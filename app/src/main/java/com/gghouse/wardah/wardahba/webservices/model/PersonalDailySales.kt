package com.gghouse.wardah.wardahba.webservices.model

class PersonalDailySales(var userId: Long?, var salesDate: Long?, var salesPcs: Int?, var productHighlightPcs: Int?, var productFocusPcs: Int?, var salesAmount: Double?, var productHighlightAmount: Double?, var productFocusAmount: Double?)
