package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.webservices.model.Question

import java.io.Serializable

class IntentQuestions(var objects: List<Question>?) : Serializable
