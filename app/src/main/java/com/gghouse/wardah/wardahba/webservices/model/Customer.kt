package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import java.io.Serializable


class Customer(var customerId: Long?, var name: String?, var mobileNumber: String?, var email: String?, var address: String?, var updatedDate: Long?) : WardahHistoryRecyclerViewItem(), Serializable
