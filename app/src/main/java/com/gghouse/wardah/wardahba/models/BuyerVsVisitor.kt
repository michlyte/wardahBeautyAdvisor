package com.gghouse.wardah.wardahba.models

class BuyerVsVisitor {
    var param: String? = null
    var value: Int? = null

    constructor(param: String, value: Int?) {
        this.param = param
        this.value = value
    }

    constructor(value: Int?) {
        this.value = value
    }

}
