package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.webservices.model.EvaluationQuestion
import java.io.Serializable

class EvaluationQuestionInner : Serializable {
    var userId: Long? = null
    var calendarId: Long? = null
    val data: List<EvaluationQuestion>

    constructor(userId: Long?, calendarId: Long?, data: List<EvaluationQuestion>) {
        this.userId = userId
        this.calendarId = calendarId
        this.data = data
    }
}