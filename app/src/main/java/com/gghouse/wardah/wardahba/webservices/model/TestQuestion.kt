package com.gghouse.wardah.wardahba.webservices.model

class TestQuestion {
    var TestDate: Long? = null
    var questions: List<Question>? = null

    constructor(TestDate: Long?, questions: List<Question>?) {
        this.TestDate = TestDate
        this.questions = questions
    }
}