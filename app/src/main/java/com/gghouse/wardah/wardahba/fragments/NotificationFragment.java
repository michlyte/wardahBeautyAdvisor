package com.gghouse.wardah.wardahba.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.adapters.NotifAdapter;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.dummy.NotifDummy;
import com.gghouse.wardah.wardahba.enumerations.WsMode;
import com.gghouse.wardah.wardahba.interfaces.OnLoadMoreListListener;
import com.gghouse.wardah.wardahba.interfaces.WardahTabInterface;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.LogUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.Notification;
import com.gghouse.wardah.wardahba.webservices.model.Pagination;
import com.gghouse.wardah.wardahba.webservices.response.NotificationsResponse;

import java.util.ArrayList;
import java.util.List;

import mehdi.sakout.dynamicbox.DynamicBox;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment implements WardahTabInterface {

    public static final String TAG = NotificationFragment.class.getSimpleName();

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private RecyclerView mRecyclerView;
    private NotifAdapter mAdapter;
    private List<Notification> mDataSet;

    /*
     * Pagination
     */
    private int mPage;
    private OnLoadMoreListListener mOnLoadMoreListListener;

    /*
     * DynamicBox
     */
    private DynamicBox mDynamicBox;

    public NotificationFragment() {
    }

    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().registerReceiver(brIncomingFCMMessage, new IntentFilter(IntentUtil.Companion.getNOTIF()));

        mDataSet = new ArrayList<>();
        mPage = 0;

        mOnLoadMoreListListener = () -> {
            new Handler().post(() -> {
                mAdapter.add(null);
                mAdapter.notifyItemInserted(mAdapter.getItemCount() - 1);
            });
            ws(WsMode.LOAD_MORE);
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getActivity().unregisterReceiver(brIncomingFCMMessage);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notif_list, container, false);

        mSwipeRefreshLayout = view.findViewById(R.id.srl_FNL_swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(() -> ws(WsMode.REFRESH));

        mRecyclerView = view.findViewById(R.id.rv_FNL_recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new NotifAdapter(getContext(), mRecyclerView, mDataSet);
        mRecyclerView.setAdapter(mAdapter);

        mDynamicBox = new DynamicBox(getActivity(), mRecyclerView);
        View emptyView = inflater.inflate(R.layout.dynamic_box_empty, container, false);
        TextView tvMessage = emptyView.findViewById(R.id.tv_message);
        tvMessage.setText(getString(R.string.message_empty_notification));
        mDynamicBox.addCustomView(emptyView, IntentUtil.Companion.getEMPTY_VIEW());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ws(WsMode.REFRESH);
    }

    @Override
    public void ws(WsMode wsMode) {
        if (SessionUtil.validateLoginSession()) {
            Long userLocationId = SessionUtil.getUserLocationId();
            switch (Config.Companion.getMode()) {
                case DUMMY_DEVELOPMENT:
                    switch (wsMode) {
                        case REFRESH:
                            if (!mSwipeRefreshLayout.isRefreshing()) {
                                mDynamicBox.showLoadingLayout();
                            }

                            mRecyclerView.setAdapter(null);
                            mDataSet.clear();
                            mDataSet.addAll(NotifDummy.ITEMS);
                            mAdapter.setData(mDataSet);
                            mAdapter.setOnLoadMoreListener(mOnLoadMoreListListener);
                            mRecyclerView.setAdapter(mAdapter);

                            mSwipeRefreshLayout.setRefreshing(false);
                            mDynamicBox.hideAll();
                            break;
                        case LOAD_MORE:
                            new Handler().postDelayed(() -> {
                                //Remove loading item
                                mAdapter.remove(mAdapter.getItemCount() - 1);
                                mAdapter.notifyItemRemoved(mAdapter.getItemCount());

                                //Load data
                                int index = mAdapter.getItemCount();
                                int end = index + Config.Companion.getNOTIF_ITEM_PER_PAGE();

                                for (Long i = 0L; i < Config.Companion.getNOTIF_ITEM_PER_PAGE(); i++) {
                                    Notification notification = new Notification(
                                            i,
                                            "Description is the fiction-writing mode for transmitting a mental image of the particulars of a story. Together with dialogue, narration, exposition, and summarization, description is one of the most widely recognized of the fiction-writing modes. As stated in Writing from A to Z, edited by Kirk Polking, description is more than the amassing of details; it is bringing something to life by carefully choosing and arranging words and phrases to produce the desired effect.[3] The most appropriate and effective techniques for presenting description are a matter of ongoing discussion among writers and writing coaches.",
                                            "PRODUCT_LAUNCH",
                                            R.drawable.pic_quote_1,
                                            System.currentTimeMillis());
                                    mAdapter.add(notification);
                                }
                                mAdapter.notifyDataSetChanged();
                                mAdapter.setLoaded();
                            }, Config.Companion.getDELAY_LOAD_MORE());
                            break;
                    }
                    break;
                case DEVELOPMENT:
                case PRODUCTION:
                    switch (wsMode) {
                        case REFRESH:
                            if (!mSwipeRefreshLayout.isRefreshing()) {
                                mDynamicBox.showLoadingLayout();
                            }

                            Call<NotificationsResponse> callNotification = ApiClient.INSTANCE.getClient().apiNotifications(userLocationId, 0, Config.Companion.getNOTIF_ITEM_PER_PAGE(), Config.Companion.getNotificationSort());
                            callNotification.enqueue(new Callback<NotificationsResponse>() {
                                @Override
                                public void onResponse(Call<NotificationsResponse> call, Response<NotificationsResponse> response) {
                                    LogUtil.INSTANCE.log(LogUtil.INSTANCE.getON_RESPONSE());

                                    mSwipeRefreshLayout.setRefreshing(false);
                                    mDynamicBox.hideAll();

                                    if (response.isSuccessful()) {
                                        NotificationsResponse rps = response.body();
                                        switch (rps.getCode()) {
                                            case Config.CODE_200:
                                                manageOnLoadMoreListener(rps.getPagination());
                                                mRecyclerView.setAdapter(null);
                                                mDataSet = rps.getData();
                                                mAdapter.setData(mDataSet);
                                                mRecyclerView.setAdapter(mAdapter);

                                                if (mDataSet.size() <= 0) {
                                                    mDynamicBox.showCustomView(IntentUtil.Companion.getEMPTY_VIEW());
                                                }
                                                break;
                                            default:
                                                ToastUtil.INSTANCE.toastMessage(TAG, rps.getStatus());
                                                break;
                                        }
                                    } else {
                                        ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                                    }
                                }

                                @Override
                                public void onFailure(Call<NotificationsResponse> call, Throwable t) {
                                    ToastUtil.INSTANCE.toastMessage(TAG, t.getMessage());

                                    mSwipeRefreshLayout.setRefreshing(false);
                                    mDynamicBox.hideAll();
                                }
                            });
                            break;
                        case LOAD_MORE:
                            Call<NotificationsResponse> callNotifLoadMore = ApiClient.INSTANCE.getClient().apiNotifications(userLocationId, ++mPage, Config.Companion.getNOTIF_ITEM_PER_PAGE(), Config.Companion.getNotificationSort());
                            callNotifLoadMore.enqueue(new Callback<NotificationsResponse>() {
                                @Override
                                public void onResponse(Call<NotificationsResponse> call, Response<NotificationsResponse> response) {
                                    LogUtil.INSTANCE.log(LogUtil.INSTANCE.getON_RESPONSE());

                                    //Remove loading item
                                    mAdapter.remove(mAdapter.getItemCount() - 1);
                                    mAdapter.notifyItemRemoved(mAdapter.getItemCount());

                                    if (response.isSuccessful()) {
                                        NotificationsResponse notificationsResponse = response.body();
                                        switch (notificationsResponse.getCode()) {
                                            case Config.CODE_200:
                                                manageOnLoadMoreListener(notificationsResponse.getPagination());
                                                List<Notification> newDataSet = notificationsResponse.getData();
                                                mDataSet.addAll(newDataSet);
                                                break;
                                            default:
                                                LogUtil.INSTANCE.log("Status" + "[" + notificationsResponse.getCode() + "]: " + notificationsResponse.getStatus());
                                                break;
                                        }
                                        mAdapter.notifyDataSetChanged();
                                    } else {
                                        ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                                    }
                                }

                                @Override
                                public void onFailure(Call<NotificationsResponse> call, Throwable t) {
                                    LogUtil.INSTANCE.log(LogUtil.INSTANCE.getON_FAILURE() + ": " + t.getMessage());

                                    //Remove loading item
                                    mAdapter.remove(mAdapter.getItemCount() - 1);
                                    mAdapter.notifyItemRemoved(mAdapter.getItemCount());
                                    mAdapter.setLoaded();
                                }
                            });
                            break;
                    }
                    break;
            }
        }
    }

    private void manageOnLoadMoreListener(Pagination pagination) {
        /*
         * Controlling load more
         */
        mPage = pagination.getNumber();
        if (pagination.getLast()) {
            mAdapter.removeOnLoadMoreListener();
        } else {
            mAdapter.setLoaded();
            mAdapter.setOnLoadMoreListener(mOnLoadMoreListListener);
        }
    }

    /*
     * Broadcast
     */
    BroadcastReceiver brIncomingFCMMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ws(WsMode.REFRESH);
        }
    };

}