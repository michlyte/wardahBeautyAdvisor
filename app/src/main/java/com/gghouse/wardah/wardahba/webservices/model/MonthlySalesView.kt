package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class MonthlySalesView : Serializable {
    var monthlyReportId: Long? = null
    val productTypes: List<ProductType>
    val month: Int
    val year: Int
    val competitorTurnover: Double
    val target: Double
    val editable: Boolean
    val visitor: Int
    val highlight: List<ProductHighlight>
    val focus: List<ProductHighlight>
    val salesOverview: BAHomeSalesOverview
    val buyingPower: BuyingPower

    constructor(productTypes: List<ProductType>, month: Int, year: Int, competitorTurnover: Double, target: Double, editable: Boolean, visitor: Int, highlight: List<ProductHighlight>, focus: List<ProductHighlight>, salesOverview: BAHomeSalesOverview, buyingPower: BuyingPower) {
        this.productTypes = productTypes
        this.month = month
        this.year = year
        this.competitorTurnover = competitorTurnover
        this.target = target
        this.editable = editable
        this.visitor = visitor
        this.highlight = highlight
        this.focus = focus
        this.salesOverview = salesOverview
        this.buyingPower = buyingPower
    }
}