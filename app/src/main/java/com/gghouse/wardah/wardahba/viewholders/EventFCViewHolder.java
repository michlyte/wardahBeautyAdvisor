package com.gghouse.wardah.wardahba.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.interfaces.EventFCListener;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.webservices.model.VisitCalendar;
import com.squareup.picasso.Picasso;

public class EventFCViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final CardView mCardView;
    private final TextView mTitleTextView;
    private final TextView mDateTextView;
    private final TextView mTimeTextView;
    private final TextView mDCTextView;
    private final TextView mCategoryTextView;
    private final Button mCheckInButton;
    private final Button mInformasiTokoButton;
    private final Button mChecklistBAButton;

    public VisitCalendar mItem;

    public EventFCViewHolder(View view) {
        super(view);

        this.view = view;
        mCardView = view.findViewById(R.id.cardView);

        mTitleTextView = view.findViewById(R.id.titleEditText);
        mDateTextView = view.findViewById(R.id.dateTextView);
        mTimeTextView = view.findViewById(R.id.timeTextView);
        mDCTextView = view.findViewById(R.id.dcTextView);
        mCategoryTextView = view.findViewById(R.id.categoryTextView);

        mCheckInButton = view.findViewById(R.id.checkInButton);
        mInformasiTokoButton = view.findViewById(R.id.informasiTokoButton);
        mChecklistBAButton = view.findViewById(R.id.checklistBAButton);

        ImageView dateImageView = view.findViewById(R.id.dateImageView);
        ImageView timeImageView = view.findViewById(R.id.timeImageView);
        ImageView dcImageView = view.findViewById(R.id.dcImageView);
        ImageView categoryImageView = view.findViewById(R.id.categoryImageView);

        // Set icons
        Picasso.get().load(R.drawable.ic_visit_time).into(dateImageView);
        Picasso.get().load(R.drawable.ic_visit_time).into(timeImageView);
        Picasso.get().load(R.drawable.ic_visit_dc).into(dcImageView);
        Picasso.get().load(R.drawable.ic_visit_category).into(categoryImageView);
    }

    private void setupRowView(Context context) {
        mCheckInButton.setEnabled(mItem.getCheckInOutbuttonEnable());
        mCheckInButton.setText(mItem.getCheckInOutButtonShown());
        WardahUtil.INSTANCE.setEventButtonStyle(context, mCheckInButton, mItem.getCheckInOutbuttonEnable());
    }

    private void setupActionEvent(EventFCListener eventFCListener) {
        mCardView.setOnClickListener((View view) -> eventFCListener.onEventClicked(mItem));
        mChecklistBAButton.setOnClickListener(view -> eventFCListener.onChecklistBAClicked(mItem));
        mCheckInButton.setOnClickListener(view -> eventFCListener.onCheckInOutClicked(mItem));
        mInformasiTokoButton.setOnClickListener(view -> eventFCListener.onStoreInformationClicked(mItem));
    }

    public void setData(Context context, VisitCalendar visitCalendar, EventFCListener eventFCListener) {
        mItem = visitCalendar;

        mTitleTextView.setText(mItem.getDc().getName());
        mDateTextView.setText(mItem.getDateStr());
        mTimeTextView.setText(mItem.getTimeStr());
        mDCTextView.setText(mItem.getDc().getName());
        mCategoryTextView.setText(mItem.getCategory());

        setupRowView(context);
        setupActionEvent(eventFCListener);
    }
}