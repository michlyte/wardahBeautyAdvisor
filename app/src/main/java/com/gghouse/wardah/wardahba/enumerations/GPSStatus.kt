package com.gghouse.wardah.wardahba.enumerations

import com.gghouse.wardah.wardahba.R

enum class GPSStatus(var resId: Int) {
    A(R.string.title_location_match),
    F(R.string.title_location_not_found),
    O(R.string.title_outside_radius),
    E(R.string.title_event_location_not_found)
}
