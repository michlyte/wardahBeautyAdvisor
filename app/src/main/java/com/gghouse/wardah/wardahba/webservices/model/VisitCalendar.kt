package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.utils.DateTimeUtil

class VisitCalendar {
    val storeId: Long
    val calendarId: Long
    val category: String
    val startTime: Long
    val endTime: Long
    val location: VisitLocation
    val dc: Dc
    val checkInOutButtonShown: String
    val checkInOutbuttonEnable: Boolean

    constructor(storeId: Long, calendarId: Long, category: String, startTime: Long, endTime: Long, location: VisitLocation, dc: Dc, checkInOutButtonShown: String, checkInOutbuttonEnable: Boolean) {
        this.storeId = storeId
        this.calendarId = calendarId
        this.category = category
        this.startTime = startTime
        this.endTime = endTime
        this.location = location
        this.dc = dc
        this.checkInOutButtonShown = checkInOutButtonShown
        this.checkInOutbuttonEnable = checkInOutbuttonEnable
    }

    val dateStr: String
        get() = DateTimeUtil.sdfDateMonthYear.format(startTime)

    val timeStr: String
        get() = DateTimeUtil.sdfEventTime.format(startTime) + " - " + DateTimeUtil.sdfEventTime.format(endTime)
}