package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.dummy.EventDummy
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.*
import com.gghouse.wardah.wardahba.webservices.response.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeBPActivityViewModel : ViewModel() {

    val userMutableLiveData: MutableLiveData<User> by lazy {
        MutableLiveData<User>()
    }
    val menuMutableLiveData: MutableLiveData<List<Int>> by lazy {
        MutableLiveData<List<Int>>()
    }
    val profileMutableLiveData: MutableLiveData<Profile> by lazy {
        MutableLiveData<Profile>()
    }
    val bpHomeMutableLiveData: MutableLiveData<BPHome> by lazy {
        MutableLiveData<BPHome>()
    }
    val eventMutableLiveData: MutableLiveData<Event> by lazy {
        MutableLiveData<Event>()
    }
    val checklistEventMutableLiveData: MutableLiveData<ChecklistEvent> by lazy {
        MutableLiveData<ChecklistEvent>()
    }
    val attendantButtonMutableLiveData: MutableLiveData<AttendantButton> by lazy {
        MutableLiveData<AttendantButton>()
    }
    val checkInRadiusBPMutableLiveData: MutableLiveData<CheckInRadius> by lazy {
        MutableLiveData<CheckInRadius>()
    }

    fun getData() {
        when (Config.mode) {
            ModeEnum.DUMMY_DEVELOPMENT -> {
                userMutableLiveData.postValue(SessionUtil.getUser())
                bpHomeMutableLiveData.postValue(EventDummy.bpHome)
            }
            ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> if (SessionUtil.validateLoginSession()) {
                userMutableLiveData.postValue(SessionUtil.getUser())
                callBPHomeData()
            }
        }
    }

    private fun callBPHomeData() {
        val callBPHome = ApiClient.client.apiBPHome(SessionUtil.getUserId()!!)
        callBPHome.enqueue(object : Callback<BPHomeResponse> {
            override fun onResponse(call: Call<BPHomeResponse>, response: Response<BPHomeResponse>) {
                if (response.isSuccessful) {
                    val bpHomeResponse = response.body()
                    when (bpHomeResponse!!.code) {
                        Config.CODE_200 -> bpHomeMutableLiveData.postValue(bpHomeResponse.data)
                        else -> {
                            bpHomeMutableLiveData.postValue(null)
                            ToastUtil.retrofitFailMessage(TAG, bpHomeResponse.status!!)
                        }
                    }
                } else {
                    bpHomeMutableLiveData.postValue(null)
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<BPHomeResponse>, t: Throwable) {
                bpHomeMutableLiveData.postValue(null)
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callProfileData() {
        if (SessionUtil.validateLoginSession()) {
            val callProfile = ApiClient.client.apiProfile(SessionUtil.getUserId()!!)
            callProfile.enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(call: Call<ProfileResponse>, response: Response<ProfileResponse>) {
                    if (response.isSuccessful) {
                        val profileResponse = response.body()
                        when (profileResponse!!.code) {
                            Config.CODE_200 -> profileMutableLiveData.postValue(profileResponse.data)
                            else -> ToastUtil.retrofitFailMessage(TAG, profileResponse.status!!)
                        }
                    } else {
                        ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                    }
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    ToastUtil.retrofitFailMessage(TAG, t.message.toString())
                }
            })
        }
    }

    fun callEventView(calendarId: Long, userId: Long) {
        val callEventView = ApiClient.client.apiEventView(calendarId, userId)
        callEventView.enqueue(object : Callback<EventViewResponse> {
            override fun onResponse(call: Call<EventViewResponse>, response: Response<EventViewResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> eventMutableLiveData.postValue(rps.data)
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<EventViewResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callChecklistEventData(calendarId: Long) {
        val callChecklistEvent = ApiClient.client.apiChecklistEvent(calendarId)
        callChecklistEvent.enqueue(object : Callback<ChecklistEventResponse> {
            override fun onResponse(call: Call<ChecklistEventResponse>, response: Response<ChecklistEventResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> {
                            val checklistEvent = rps.data
                            checklistEvent!!.calendarId = calendarId
                            checklistEventMutableLiveData.postValue(checklistEvent)
                        }
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<ChecklistEventResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callCheckInRadius(userId: Long, calendarId: Long) {
        val callCheckInRadius = ApiClient.client.apiCheckInRadius(userId, calendarId)
        callCheckInRadius.enqueue(object : Callback<CheckInRadiusResponse> {
            override fun onResponse(call: Call<CheckInRadiusResponse>, response: Response<CheckInRadiusResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> {
                            val checkInRadius = rps.data
                            checkInRadius!!.calendarId = calendarId
                            checkInRadiusBPMutableLiveData.postValue(checkInRadius)
                        }
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<CheckInRadiusResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callAttendantEventData(calendarId: Long) {
        val callAttendantButton = ApiClient.client.apiAttendantButton(calendarId)
        callAttendantButton.enqueue(object : Callback<AttendantButtonResponse> {
            override fun onResponse(call: Call<AttendantButtonResponse>, response: Response<AttendantButtonResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> {
                            val attendantButton = rps.data
                            attendantButton!!.calendarId = calendarId
                            attendantButtonMutableLiveData.postValue(attendantButton)
                        }
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<AttendantButtonResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    companion object {
        private val TAG = HomeBPActivityViewModel::class.java.simpleName
    }
}
