package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.CheckInRadius

class CheckInRadiusResponse : GenericResponse() {
    var data: CheckInRadius? = null
}
