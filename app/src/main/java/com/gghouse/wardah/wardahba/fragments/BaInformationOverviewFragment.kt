package com.gghouse.wardah.wardahba.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.activities.visit.EvaluationBAActivity
import com.gghouse.wardah.wardahba.adapters.HomeStoreAdapter
import com.gghouse.wardah.wardahba.adapters.WardahSimpleAdapter
import com.gghouse.wardah.wardahba.enumerations.HomepageStoreSection
import com.gghouse.wardah.wardahba.enumerations.HomepageStoreSection.DAILY_SALES_PERSONAL
import com.gghouse.wardah.wardahba.enumerations.HomepageStoreSection.SALES_RECAP
import com.gghouse.wardah.wardahba.enumerations.ViewMode
import com.gghouse.wardah.wardahba.enumerations.WardahItemType
import com.gghouse.wardah.wardahba.models.BAHomepageItem
import com.gghouse.wardah.wardahba.models.EvaluationQuestionInner
import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import com.gghouse.wardah.wardahba.utils.IntentUtil
import com.gghouse.wardah.wardahba.viewmodels.BAInformationOverviewFragmentViewModel
import com.gghouse.wardah.wardahba.webservices.model.BAInformationOverview
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA
import kotlinx.android.synthetic.main.subview_bainformation_current_sales_recap.*
import kotlinx.android.synthetic.main.subview_bainformation_test_result.*
import kotlinx.android.synthetic.main.view_ba_information_overview.*
import java.util.*
import kotlin.collections.ArrayList

class BaInformationOverviewFragment : Fragment(), View.OnClickListener {

    private val REQUEST_EVALUATION = 101

    private lateinit var mModel: BAInformationOverviewFragmentViewModel

    private var isEvaluation = false
    private val ACTIVITY_RESULT = 1

    lateinit var mCurrentSalesAdapter: HomeStoreAdapter
    lateinit var mCurrentTestAdapter: HomeStoreAdapter
    lateinit var mOverallSalesAdapter: HomeStoreAdapter
    lateinit var mOverallTestAdapter: HomeStoreAdapter

    lateinit var mEvaluationAdapter: WardahSimpleAdapter
    lateinit var mEvaluationRv: RecyclerView
    lateinit var mEvaluationLayout: LinearLayout

    companion object {
        const val ARG_VIEW_MODE = "ARG_VIEW_MODE"
        const val ARG_USER_ID = "ARG_USER_ID"

        private val TAG = BaInformationOverviewFragment::class.java.simpleName

        fun newInstance(viewMode: ViewMode, userId: Long): BaInformationOverviewFragment {
            val fragment = BaInformationOverviewFragment()
            val args = Bundle()
            args.putSerializable(ARG_VIEW_MODE, viewMode)
            args.putLong(ARG_USER_ID, userId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.view_ba_information_overview, container, false)

        setView()

        mModel = ViewModelProviders.of(this).get(BAInformationOverviewFragmentViewModel::class.java)
        mModel.userId = arguments?.getLong(ARG_USER_ID, 0L) ?: 0L
        mModel.baInformationOverviewMutableLiveData.observe(this, Observer<BAInformationOverview> { baInformationOverview ->
            if (baInformationOverview != null) {
                tvOverviewNip.text = baInformationOverview.nip
                tvOverviewName.text = baInformationOverview.fullName
                tvOverviewJoinedAt.text = DateTimeUtil.sdfFilter.format(baInformationOverview.firstDateWork)
                tvOverviewLongWork.text = baInformationOverview.workingDuration

                baCurrentSalesDateTextView.text = DateTimeUtil.sdfMonthYear.format(Date())
                val currentSalesRecap: List<BAHomepageItem> = object : java.util.ArrayList<BAHomepageItem>() {
                    init {
                        add(BAHomepageItem(R.drawable.ic_sales_turnover, baInformationOverview.currentSalesOverview.sales.amount, baInformationOverview.currentSalesOverview.sales.quantity))
                        add(BAHomepageItem(R.drawable.ic_ba_product_highlight, baInformationOverview.currentSalesOverview.highlight.amount, baInformationOverview.currentSalesOverview.highlight.quantity))
                        add(BAHomepageItem(R.drawable.ic_ba_product_focus, baInformationOverview.currentSalesOverview.focus.amount, baInformationOverview.currentSalesOverview.focus.quantity))
                    }
                }
                notifyDataAdapter(mCurrentSalesAdapter, currentSalesRecap)

                val overallSalesRecap: List<BAHomepageItem> = object : java.util.ArrayList<BAHomepageItem>() {
                    init {
                        add(BAHomepageItem(R.drawable.ic_sales_turnover, baInformationOverview.overallSalesOverview.sales.amount, baInformationOverview.overallSalesOverview.sales.quantity))
                        add(BAHomepageItem(R.drawable.ic_ba_product_highlight, baInformationOverview.overallSalesOverview.highlight.amount, baInformationOverview.overallSalesOverview.highlight.quantity))
                        add(BAHomepageItem(R.drawable.ic_ba_product_focus, baInformationOverview.overallSalesOverview.focus.amount, baInformationOverview.overallSalesOverview.focus.quantity))
                    }
                }
                notifyDataAdapter(mOverallSalesAdapter, overallSalesRecap)

                baCurrentTestDateTextView.text = DateTimeUtil.sdfMonthYear.format(DateTimeUtil.getDateFromMonthYear(baInformationOverview.currentTestOverview.month, baInformationOverview.currentTestOverview.year))
                val currentTestResult: List<BAHomepageItem> = object : java.util.ArrayList<BAHomepageItem>() {
                    init {
                        add(BAHomepageItem(R.drawable.ic_test, baInformationOverview.currentTestOverview.totalQuestionsAnswered))
                        add(BAHomepageItem(R.drawable.ic_test,  baInformationOverview.currentTestOverview.totalAccumulativeScore.toInt()))
                    }
                }
                notifyDataAdapter(mCurrentTestAdapter, currentTestResult)

                baCurrentTestDateTextView.text = DateTimeUtil.sdfMonthYear.format(DateTimeUtil.getDateFromMonthYear(baInformationOverview.currentTestOverview.month, baInformationOverview.currentTestOverview.year))
                val overallTestResult: List<BAHomepageItem> = object : java.util.ArrayList<BAHomepageItem>() {
                    init {
                        add(BAHomepageItem(R.drawable.ic_test, baInformationOverview.overallTestOverview.totalQuestionsAnswered))
                        add(BAHomepageItem(R.drawable.ic_test,  baInformationOverview.overallTestOverview.totalAccumulativeScore.toInt()))
                    }
                }
                notifyDataAdapter(mOverallTestAdapter, overallTestResult)
//
//                if (baInformation.evaluations == null || baInformation.evaluations.size == 0) {
//                    mEvaluationLayout.visibility = View.GONE
//                    isEvaluation = false
//                } else {
//                    mEvaluationLayout.visibility = View.VISIBLE
//                    mEvaluationAdapter.data = baInformation.evaluations
//                    mEvaluationAdapter!!.notifyDataSetChanged()
//                    isEvaluation = true
//                }
                activity!!.invalidateOptionsMenu()
            }
        })

        mModel.evaluationQuestionsMutableLiveData.observe(this, Observer<EvaluationQuestionInner> { evaluationQuestions ->
            if (evaluationQuestions != null) {
                val iEvaluation = Intent(context, EvaluationBAActivity::class.java)
                iEvaluation.putExtra(IntentUtil.VIEW_MODE, ViewMode.INPUT)
                iEvaluation.putExtra(IntentUtil.EVALUTION_QUESTIONS, evaluationQuestions)
                startActivityForResult(iEvaluation, ACTIVITY_RESULT)
            }
        })

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mEvaluationRv = view.findViewById(R.id.baEvaluationResultRv)
        mEvaluationLayout = view.findViewById(R.id.baEvaluationLinearLayout)

        mEvaluationAdapter = WardahSimpleAdapter(view.context, WardahItemType.BA_EVALUATION, ArrayList<ChecklistBA>())
        mEvaluationRv.layoutManager = LinearLayoutManager(view.context)
        mEvaluationRv.adapter = mEvaluationAdapter

        mCurrentSalesAdapter = HomeStoreAdapter(view.context, SALES_RECAP, ArrayList())
        baMonthlySalesGridView.adapter = mCurrentSalesAdapter

        mOverallSalesAdapter = HomeStoreAdapter(view.context, DAILY_SALES_PERSONAL, ArrayList())
        baAllSalesGridView.adapter = mOverallSalesAdapter

        mCurrentTestAdapter = HomeStoreAdapter(view.context, HomepageStoreSection.BUYER_VS_VISITOR, ArrayList())
        baMonthlyTestGridView.adapter = mCurrentTestAdapter

        mOverallTestAdapter = HomeStoreAdapter(view.context, HomepageStoreSection.BUYER_VS_VISITOR, ArrayList())
        baAllTestGridView.adapter = mOverallTestAdapter

        mModel.getData()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (!isEvaluation) {
            inflater?.inflate(R.menu.menu_ba_information, menu)
        } else {
            inflater?.inflate(R.menu.menu_none, menu)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_evaluation -> {
                mModel.callEvaluationQuestions(mModel.userId)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun notifyDataAdapter(adapter: HomeStoreAdapter, data: List<BAHomepageItem>) {
        adapter.clear()
        adapter.addAll(data)
        adapter.notifyDataSetChanged()
    }

    fun setView() {
        var dt = Date()
//        mMontlySalesTv.text = DateTimeUtil.sdfMonthYear.format(dt) + " (15% dari penjualan toko)"
//        mMontlyTestTv.text = DateTimeUtil.sdfMonthYear.format(dt) + " (15% dari Tes)"

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ACTIVITY_RESULT && resultCode == Activity.RESULT_OK) {
            mModel.setEvaluations()
            mModel.getData()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.moreTextView -> {
            }
        }
    }
}