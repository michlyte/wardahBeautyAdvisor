package com.gghouse.wardah.wardahba.activities.profile;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.viewmodels.SelectEventLocationViewModel;
import com.gghouse.wardah.wardahba.webservices.request.StoreGPSRequest;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;

public class SelectStoreGPSActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener, View.OnClickListener {

    private final String TAG = SelectStoreGPSActivity.class.getSimpleName();
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    private SelectEventLocationViewModel mModel;
    private TextView mTvLocAddress;

    private GoogleMap mMap;

    private Geocoder mGeocoder;

    private FusedLocationProviderClient mFusedLocationProviderClient;

    private final LatLng mDefaultLocation = new LatLng(-6.893400380132707, 107.60556701570749);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;
    private Location mLastKnownLocation;

    private static final String KEY_LOCATION = "location";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
        }

        mModel = ViewModelProviders.of(this).get(SelectEventLocationViewModel.class);

        Intent i = getIntent();

        if (i != null) {
            mModel.setStoreId(i.getLongExtra(IntentUtil.Companion.getSTORE_ID(), 0L));
        }

        setContentView(R.layout.activity_select_event_location);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mGeocoder = new Geocoder(this, DateTimeUtil.INSTANCE.getLocale());
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mTvLocAddress = findViewById(R.id.tvLocAddress);
        TextView selectLocationTextView = findViewById(R.id.tvSelectLocation);
        selectLocationTextView.setOnClickListener(this);
        ImageView markerImageView = findViewById(R.id.ivPin);
        markerImageView.setColorFilter(getResources().getColor(R.color.colorWardahGreen), PorterDuff.Mode.SRC_ATOP);

        mModel = ViewModelProviders.of(this).get(SelectEventLocationViewModel.class);
        mModel.getAddress().observe(this, address -> {
            if (address != null && address.getAddressLine(0) != null) {
                String addr = address.getAddressLine(0);
                mTvLocAddress.setText(addr);
            }
        });
        mModel.isSubmitSuccess().observe(this, isSuccess -> {
            if (isSuccess != null && isSuccess) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_select_event_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
            case R.id.action_search:
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .build(this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    Log.e(TAG, e.getMessage());
                } catch (GooglePlayServicesNotAvailableException e) {
                    Log.e(TAG, e.getMessage());
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(this);
        getLocationPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    getDeviceLocation();
                } else {
                    mLastKnownLocation = null;
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                try {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), DEFAULT_ZOOM));
                    mModel.getAddress().setValue(getAddressFromLatLng(place.getLatLng()));
                } catch (IOException ioe) {
                    Log.d(TAG, ioe.getMessage());
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }

    @Override
    public void onCameraIdle() {
        LatLng midLatLng = mMap.getCameraPosition().target;

        try {
            mModel.getAddress().setValue(getAddressFromLatLng(midLatLng));
        } catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSelectLocation:
                Address address = mModel.getAddress().getValue();
                StoreGPSRequest storeGPSRequest = new StoreGPSRequest(mModel.getStoreId(), address.getLatitude(), address.getLongitude());
                mModel.submitStoreGPS(storeGPSRequest);
                break;
        }
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            getDeviceLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void getDeviceLocation() {
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            double lat = (mLastKnownLocation == null) ? -6.9220405 : mLastKnownLocation.getLatitude();
                            double lng = (mLastKnownLocation == null) ? 107.616701 : mLastKnownLocation.getLongitude();

//                            LatLng latLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                            LatLng latLng = new LatLng(lat, lng);
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
                            try {
                                Address address = getAddressFromLatLng(latLng);
                                // Jika lokasi tidak ditemukan, gunakan lokasi default
                                if (address == null) {
                                    address = getAddressFromLatLng(mDefaultLocation);
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                                }

                                mModel.getAddress().setValue(address);
                            } catch (IOException ioe) {
                                Log.e(TAG, ioe.getMessage());
                            }
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            try {
                                mModel.getAddress().setValue(getAddressFromLatLng(mDefaultLocation));
                            } catch (IOException ioe) {
                                Log.e(TAG, ioe.getMessage());
                            }
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private Address getAddressFromLatLng(LatLng latLng) throws IOException {
        List<Address> mAddresses = mGeocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        for (Address addr : mAddresses) {
            return addr;
        }

        return null;
    }
}
