package com.gghouse.wardah.wardahba.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.WardahApp;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomepageMenuItemAdapter extends BaseAdapter {
    private List<Integer> mMenuList;

    public HomepageMenuItemAdapter(List<Integer> menuList) {
        mMenuList = menuList;
    }

    @Override
    public int getCount() {
        return mMenuList == null ? 0 : mMenuList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_home, parent, false);

            TextView tvTitle = view.findViewById(R.id.tvTitle);
            ImageView iconImageView = view.findViewById(R.id.ivIcon);
            switch (mMenuList.get(position)) {
                case R.string.title_store:
                    Picasso.get().load(R.drawable.ic_store).into(iconImageView);
                    break;
                case R.string.title_sales:
                    Picasso.get().load(R.drawable.ic_ba_sales).into(iconImageView);
                    break;
                case R.string.title_tes:
                    Picasso.get().load(R.drawable.ic_test).into(iconImageView);
                    break;
                case R.string.title_pelanggan:
                    Picasso.get().load(R.drawable.ic_customer).into(iconImageView);
                    break;
                case R.string.title_event:
                    Picasso.get().load(R.drawable.ic_event).into(iconImageView);
                    break;
                case R.string.title_kunjungan:
                    Picasso.get().load(R.drawable.ic_visit).into(iconImageView);
                    break;
                case R.string.text_input_target:
                    Picasso.get().load(R.drawable.ic_input_target).into(iconImageView);
                    break;
            }
            tvTitle.setText(WardahApp.getInstance().getString(mMenuList.get(position)));
        } else {
            view = convertView;
        }
        return view;
    }

    public void setData(List<Integer> integerList) {
        mMenuList.clear();
        mMenuList.addAll(integerList);
    }
}