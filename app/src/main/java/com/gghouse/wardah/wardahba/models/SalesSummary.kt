package com.gghouse.wardah.wardahba.models

class SalesSummary(var productHighlightPcs: Int?, var productFocusPcs: Int?, var productHighlightAmount: Double?, var productFocusAmount: Double?)
