package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.afollestad.materialdialogs.MaterialDialog
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.request.AnswerRequest
import com.gghouse.wardah.wardahba.webservices.request.AnswersRequest
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse
import com.gghouse.wardah.wardahba.webservices.response.QuestionerLatestDateResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class TestTakingActivityViewModel : ViewModel() {

    val submitResponseMutableLiveData: MutableLiveData<GenericResponse> by lazy { MutableLiveData<GenericResponse>() }
    val questionerLatestDateMutableLiveData: MutableLiveData<Long> by lazy { MutableLiveData<Long>() }

    var date: Date? = null

    fun submitAnswers(materialDialog: MaterialDialog, userId: Long?, answerRequestList: List<AnswerRequest>) {
        when (Config.mode) {
            ModeEnum.DUMMY_DEVELOPMENT -> submitResponseMutableLiveData.postValue(GenericResponse())
            ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> {
                materialDialog.show()

                val answersRequest = AnswersRequest(userId, date!!.time, answerRequestList)

                val callSubmitTest = ApiClient.client.apiSubmitTest(answersRequest)
                callSubmitTest.enqueue(object : Callback<GenericResponse> {
                    override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                        materialDialog.dismiss()

                        if (response.isSuccessful) {
                            val genericResponse = response.body()
                            when (genericResponse!!.code) {
                                Config.CODE_200 -> submitResponseMutableLiveData.postValue(genericResponse)
                                else -> {
                                    submitResponseMutableLiveData.postValue(null)
                                    ToastUtil.toastMessage(TAG, genericResponse.status!!)
                                }
                            }
                        } else {
                            submitResponseMutableLiveData.postValue(null)
                            ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                        }
                    }

                    override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                        ToastUtil.toastMessage(TAG, t.message.toString())
                        materialDialog.dismiss()
                    }
                })
            }
        }
    }

    fun callQuestionerLatestDate() {
        if (SessionUtil.validateLoginSession()) {
            val callQuestionerLatestDate = ApiClient.client.apiQuestionerLatestDate(SessionUtil.getUserId()!!)
            callQuestionerLatestDate.enqueue(object : Callback<QuestionerLatestDateResponse> {
                override fun onResponse(call: Call<QuestionerLatestDateResponse>, response: Response<QuestionerLatestDateResponse>) {
                    if (response.isSuccessful) {
                        val questionerLatestDateResponse = response.body()
                        when (questionerLatestDateResponse!!.code) {
                            Config.CODE_200 -> questionerLatestDateMutableLiveData!!.postValue(questionerLatestDateResponse.data!!.questionnaireDate)
                            else -> {
                                questionerLatestDateMutableLiveData!!.postValue(null)
                                ToastUtil.toastMessage(TAG, questionerLatestDateResponse.status!!)
                            }
                        }
                    } else {
                        questionerLatestDateMutableLiveData!!.postValue(null)
                        ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                    }
                }

                override fun onFailure(call: Call<QuestionerLatestDateResponse>, t: Throwable) {
                    questionerLatestDateMutableLiveData!!.postValue(null)
                    ToastUtil.toastMessage(TAG, t.message.toString())
                }
            })
        }
    }

    companion object {
        private val TAG = TestTakingActivityViewModel::class.java.simpleName
    }
}
