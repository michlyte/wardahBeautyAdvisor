package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.webservices.model.EventCategory

import java.io.Serializable

class IntentEventCategory(var objects: List<EventCategory>?) : Serializable
