package com.gghouse.wardah.wardahba.utils

import android.content.Context
import android.location.Location
import android.location.LocationManager

import com.afollestad.materialdialogs.MaterialDialog
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.WardahApp
import com.google.android.gms.maps.model.LatLng

object LocationUtil {

    val isGPSEnabled: Boolean
        get() {
            val locationManager = WardahApp.getInstance().appContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }

    fun getDistranceInMeters(param1: LatLng, param2: LatLng): Float {
        val loc1 = Location("")
        loc1.latitude = param1.latitude
        loc1.longitude = param1.longitude

        val loc2 = Location("")
        loc2.latitude = param2.latitude
        loc2.longitude = param2.longitude

        return loc1.distanceTo(loc2)
    }

    fun popupEnableGPS(context: Context) {
        MaterialDialog.Builder(context)
                .title(R.string.title_gps)
                .content(R.string.error_gps_enabled)
                .positiveText(R.string.action_understand)
                .positiveColorRes(R.color.colorPrimary)
                .show()
    }
}
