package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA

class ChecklistBAListResponse : GenericResponse() {
    var data: List<ChecklistBA>? = null
}
