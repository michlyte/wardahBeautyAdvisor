package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.FCHome

class FCHomeResponse : GenericResponse() {
    var data: FCHome? = null
}
