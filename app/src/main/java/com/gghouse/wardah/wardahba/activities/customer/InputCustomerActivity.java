package com.gghouse.wardah.wardahba.activities.customer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.viewmodels.InputCustomerActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.Customer;

import mehdi.sakout.dynamicbox.DynamicBox;

public class InputCustomerActivity extends AppCompatActivity {

    private static final String TAG = InputCustomerActivity.class.getSimpleName();

    private InputCustomerActivityViewModel mModel;

    private EditText metName;
    private EditText metPhoneNumber;
    private EditText metEmail;
    private EditText metAddress;

    private DynamicBox mDynamicBox;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelanggan_input);

        mModel = ViewModelProviders.of(this).get(InputCustomerActivityViewModel.class);

        mDynamicBox = new DynamicBox(this, R.layout.activity_pelanggan_input);

        metName = findViewById(R.id.etName);
        metPhoneNumber = findViewById(R.id.etPhoneNumber);
        metEmail = findViewById(R.id.etEmail);
        metAddress = findViewById(R.id.etAddress);

        Intent prevIntent = getIntent();
        mModel.getViewModeMutableLiveData().setValue(
                (ViewMode) prevIntent.getSerializableExtra(IntentUtil.Companion.getVIEW_MODE()));
        mModel.getCustomerMutableLiveData().postValue(
                (Customer) prevIntent.getSerializableExtra(IntentUtil.Companion.getCUSTOMER()));

        mModel.getCustomerMutableLiveData().observe(this, customer -> {
            try {
                switch (mModel.getViewModeMutableLiveData().getValue()) {
                    case EDIT:
                    case VIEW:
                        metName.setText(customer.getName());
                        metPhoneNumber.setText(customer.getMobileNumber());
                        metEmail.setText(customer.getEmail());
                        metAddress.setText(customer.getAddress());
                        break;
                }
            } catch (NullPointerException npe) {
                Log.e(TAG, npe.getMessage());
            }
        });

        mModel.getViewModeMutableLiveData().observe(this, viewMode -> {
            if (viewMode != null) {
                switch (viewMode) {
                    case VIEW:
                        metName.setEnabled(false);
                        metPhoneNumber.setEnabled(false);
                        metEmail.setEnabled(false);
                        metAddress.setEnabled(false);
                    case EDIT:
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        break;
                }
            }
        });

        mModel.isSubmitSuccessMutableLiveData().observe(this, isSuccess -> {
            if (isSuccess != null && isSuccess) {
                setResult(Activity.RESULT_OK);
                finish();
            } else {
                mDynamicBox.hideAll();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (mModel.getViewModeMutableLiveData().getValue()) {
            case INPUT:
                getMenuInflater().inflate(R.menu.menu_input, menu);
                break;
            case EDIT:
                getMenuInflater().inflate(R.menu.menu_edit, menu);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                attemptSubmit(ViewMode.INPUT);
                return true;
            case R.id.action_edit:
                attemptSubmit(ViewMode.EDIT);
                return true;
            case android.R.id.home:
                switch (mModel.getViewModeMutableLiveData().getValue()) {
                    case INPUT:
                    case EDIT:
                        new MaterialDialog.Builder(this)
                                .title(R.string.placeholder_konfirmasi)
                                .content(R.string.message_back_confirmation)
                                .positiveText(R.string.action_ok)
                                .positiveColorRes(R.color.colorPrimary)
                                .negativeText(R.string.action_cancel)
                                .negativeColorRes(R.color.colorAccent)
                                .onPositive((dialog, which) -> finish())
                                .show();
                        break;
                    case VIEW:
                        finish();
                        break;
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void attemptSubmit(ViewMode viewMode) {
        // Reset errors
        metName.setError(null);
        metPhoneNumber.setError(null);
        metEmail.setError(null);
        metAddress.setError(null);

        String name = metName.getText().toString();
        String mobileNumber = metPhoneNumber.getText().toString();
        String email = metEmail.getText().toString();
        String address = metAddress.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(name)) {
            metName.setError(getString(R.string.error_field_required));
            focusView = metName;
            cancel = true;
        } else if (TextUtils.isEmpty(mobileNumber)) {
            metPhoneNumber.setError(getString(R.string.error_field_required));
            focusView = metPhoneNumber;
            cancel = true;
        } else if (TextUtils.isEmpty(email)) {
            metEmail.setError(getString(R.string.error_field_required));
            focusView = metEmail;
            cancel = true;
        } else if (TextUtils.isEmpty(mobileNumber)) {
            metAddress.setError(getString(R.string.error_field_required));
            focusView = metAddress;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            hideKeyboard();

            mDynamicBox.showLoadingLayout();

            switch (viewMode) {
                case INPUT:
                    mModel.submit(name, mobileNumber, email, address);
                    break;
                case EDIT:
                    mModel.edit(mModel.getCustomerMutableLiveData().getValue().getCustomerId(), name, mobileNumber, email, address);
                    break;
            }
        }
    }

    private void hideKeyboard() {
        try {
            // use application level context to avoid unnecessary leaks.
            InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
