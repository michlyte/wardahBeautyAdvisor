package com.gghouse.wardah.wardahba.activities.event;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.adapters.WardahSimpleAdapter;
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.ChecklistListener;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.viewmodels.ChecklistActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.Checklist;
import com.gghouse.wardah.wardahba.webservices.model.ChecklistEvent;
import com.gghouse.wardah.wardahba.webservices.request.ChecklistEventEditRequest;

import java.util.ArrayList;
import java.util.List;

public class ChecklistActivity extends AppCompatActivity implements ChecklistListener {

    private static final String TAG = ChecklistActivity.class.getSimpleName();

    private ChecklistActivityViewModel mModel;
    private WardahSimpleAdapter mAdapter;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mModel = ViewModelProviders.of(this).get(ChecklistActivityViewModel.class);

        Intent i = getIntent();
        try {
            mModel.getViewModeMutableLiveData().postValue((ViewMode) i.getSerializableExtra(IntentUtil.Companion.getVIEW_MODE()));
            mModel.getChecklistEventMutableLiveData().postValue((ChecklistEvent) i.getSerializableExtra(IntentUtil.Companion.getCHECKLIST()));
//            mModel.getEventMutableLiveData().postValue((Event) i.getSerializableExtra(IntentUtil.EVENT));
        } catch (NullPointerException npe) {
            mModel.getViewModeMutableLiveData().postValue(ViewMode.VIEW);
        }

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new WardahSimpleAdapter(this, WardahItemType.CHECKLIST, mModel.getViewModeMutableLiveData().getValue(), new ArrayList<>(), this);
        mRecyclerView.setAdapter(mAdapter);

        mModel.getChecklistEventMutableLiveData().observe(this, checklistEvent -> {
            if (checklistEvent != null && checklistEvent.getChecklist().size() > 0) {
                mAdapter.setData(checklistEvent.getChecklist());
                mAdapter.notifyDataSetChanged();
            }
        });

        mModel.getViewModeMutableLiveData().observe(this, viewMode -> {
            if (viewMode != null) {
                invalidateOptionsMenu();
                mRecyclerView.setAdapter(null);
                mAdapter.setViewMode(viewMode);
                mRecyclerView.setAdapter(mAdapter);
            }
        });

        mModel.getEventMutableLiveData().observe(this, event -> {
            if (event != null) {
                getSupportActionBar().setTitle(event.getTitle());

                if (DateTimeUtil.INSTANCE.isActiveEvent(event.getStartTime(), event.getEndTime())) {

                } else {

                }
            }
        });

        mModel.isSubmitSucceedMutableLiveData().observe(this, isSuccess -> {
            if (isSuccess != null && isSuccess) {
                mModel.getViewModeMutableLiveData().postValue(ViewMode.VIEW);
                setResult(Activity.RESULT_OK);
            }
        });

        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (mModel.getViewModeMutableLiveData().getValue()) {
            case INPUT:
            case EDIT:
                getMenuInflater().inflate(R.menu.menu_input, menu);
                break;
            case VIEW:
                try {
                    switch (UserTypeEnum.Companion.getUserTypeEnum(SessionUtil.getUserType())) {
                        case BEAUTY_PROMOTER_LEADER:
                            break;
                        default:
                            if (mModel.getChecklistEventMutableLiveData().getValue().getEditButtonEnable()) {
                                getMenuInflater().inflate(R.menu.menu_edit, menu);
                            }
                            break;
                    }
                } catch (NullPointerException npe) {
                    npe.printStackTrace();
                }
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_save:
                ChecklistEventEditRequest checklistEventEditRequest = new ChecklistEventEditRequest(
                        mModel.getChecklistEventMutableLiveData().getValue().getCalendarId(),
                        (List<Checklist>) mAdapter.getData());
                mModel.editChecklistEvent(checklistEventEditRequest);
                return true;
            case R.id.action_edit:
                mModel.getViewModeMutableLiveData().postValue(ViewMode.EDIT);
                return true;
            case R.id.action_done:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheck(Checklist checklist, boolean isChecked) {
        ChecklistEvent checklistEvent = mModel.getChecklistEventMutableLiveData().getValue();
        for (Checklist ck : checklistEvent.getChecklist()) {
            if (ck.getChecklistId() == checklist.getChecklistId()) {
                ck.setStatus(isChecked);
                break;
            }
        }
        mModel.getChecklistEventMutableLiveData().postValue(checklistEvent);
    }
}
