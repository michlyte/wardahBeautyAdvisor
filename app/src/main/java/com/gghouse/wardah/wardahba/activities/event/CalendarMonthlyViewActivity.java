package com.gghouse.wardah.wardahba.activities.event;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.attendant.AttendantActivity;
import com.gghouse.wardah.wardahba.activities.attendant.AttendantHistoryActivity;
import com.gghouse.wardah.wardahba.activities.store.StoreInformationActivity;
import com.gghouse.wardah.wardahba.activities.visit.CreateVisitActivity;
import com.gghouse.wardah.wardahba.adapters.WardahSimpleAdapter;
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.EventBPListener;
import com.gghouse.wardah.wardahba.interfaces.EventFCListener;
import com.gghouse.wardah.wardahba.models.IntentEventCategory;
import com.gghouse.wardah.wardahba.models.IntentVisitCategory;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.viewmodels.CalendarMonthlyViewActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.BPMonthlyEvent;
import com.gghouse.wardah.wardahba.webservices.model.EventCalendar;
import com.gghouse.wardah.wardahba.webservices.model.FCMonthlyVisit;
import com.gghouse.wardah.wardahba.webservices.model.VisitCalendar;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import mehdi.sakout.dynamicbox.DynamicBox;

public class CalendarMonthlyViewActivity extends AppCompatActivity implements View.OnClickListener, MonthPickerDialog.OnDateSetListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = CalendarMonthlyViewActivity.class.getSimpleName();
    public static final int CREATE_EVENT_FC_RESULT_ACTIVITY = 1;
    public static final int EDIT_EVENT_FC_RESULT_ACTIVITY = 2;
    private static final int CREATE_EVENT_RESULT_ACTIVITY = 3;
    public static final int EDIT_EVENT_RESULT_ACTIVITY = 4;
    private static final int CHECK_IN_RESULT_ACTIVITY = 5;

    private CalendarMonthlyViewActivityViewModel mModel;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CompactCalendarView mCompactCalendarView;
    private DynamicBox mDynamicBox;
    private RecyclerView mRecyclerView;
    private WardahSimpleAdapter mAdapter;
    private MonthPickerDialog.Builder mBuilder;

    private EventBPListener mEventBPListener;
    private EventFCListener mEventFCListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_monthly_view);

        mModel = ViewModelProviders.of(this).get(CalendarMonthlyViewActivityViewModel.class);

        mModel.setUserType(UserTypeEnum.Companion.getUserTypeEnum(SessionUtil.getUserType()));

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setOnClickListener(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mCompactCalendarView = findViewById(R.id.calendarView);
        mCompactCalendarView.setFirstDayOfWeek(Calendar.SUNDAY);
        mCompactCalendarView.setLocale(TimeZone.getDefault(), DateTimeUtil.INSTANCE.getLocale());
        mCompactCalendarView.setUseThreeLetterAbbreviation(true);

        mCompactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                mModel.getSelectedDateMutableLiveData().postValue(dateClicked);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                mModel.getCurrentMonthMutableLiveData().postValue(firstDayOfNewMonth);
                mModel.getSelectedDateMutableLiveData().postValue(firstDayOfNewMonth);
            }
        });

        mEventBPListener = new EventBPListener() {
            @Override
            public void onEventClicked(EventCalendar event) {
                mModel.callEventView(event.getCalendarId(), SessionUtil.getUserId());
            }

            @Override
            public void onChecklistClicked(EventCalendar event) {
                mModel.callChecklistEventData(event.getCalendarId());
            }

            @Override
            public void onCheckInClicked(EventCalendar event) {
                mModel.callCheckInRadius(SessionUtil.getUserTypeEnum(), SessionUtil.getUserId(), event.getCalendarId());
            }

            @Override
            public void onAttendantClicked(EventCalendar event) {
                mModel.callAttendantEventData(event.getCalendarId());
            }
        };

        mEventFCListener = new EventFCListener() {
            @Override
            public void onEventClicked(VisitCalendar visitCalendar) {
                mModel.callVisitView(visitCalendar.getCalendarId());
            }

            @Override
            public void onChecklistBAClicked(VisitCalendar visitCalendar) {
                Intent iChecklistBAHistory = new Intent(getBaseContext(), ChecklistBAHistoryActivity.class);
                iChecklistBAHistory.putExtra(IntentUtil.Companion.getCALENDAR_ID(), visitCalendar.getCalendarId());
                iChecklistBAHistory.putExtra(IntentUtil.Companion.getLOCATION_ID(), visitCalendar.getLocation().getId());
                startActivity(iChecklistBAHistory);
            }

            @Override
            public void onCheckInOutClicked(VisitCalendar visitCalendar) {
                mModel.callCheckInRadius(SessionUtil.getUserTypeEnum(), SessionUtil.getUserId(), visitCalendar.getCalendarId());
            }

            @Override
            public void onStoreInformationClicked(VisitCalendar visitCalendar) {
                mModel.callStoreInformation(visitCalendar.getStoreId());
            }
        };

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setNestedScrollingEnabled(false);

        mDynamicBox = new DynamicBox(this, mRecyclerView);
        View emptyView = getLayoutInflater().inflate(R.layout.dynamic_box_empty, null);
        mDynamicBox.addCustomView(emptyView, IntentUtil.Companion.getEMPTY_VIEW());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);

        applyLocale();

        // BP and BPL
        mModel.getCalendarEventMutableLiveData().observe(this, bpMonthlyEventList -> {
            mSwipeRefreshLayout.setRefreshing(false);
            mCompactCalendarView.removeAllEvents();

            if (bpMonthlyEventList != null) {
                // Loop for each day
                for (BPMonthlyEvent bpMonthlyEvent : bpMonthlyEventList) {
                    com.github.sundeepk.compactcalendarview.domain.Event evt = new com.github.sundeepk.compactcalendarview.domain.Event(
                            ContextCompat.getColor(getBaseContext(), R.color.colorPrimary), bpMonthlyEvent.getDate(), "");
                    mCompactCalendarView.addEvent(evt);
                }
            }

            Date selectedDate = mModel.getSelectedDateMutableLiveData().getValue();
            mModel.getSelectedDateMutableLiveData().postValue(selectedDate != null ? selectedDate : new Date());
        });

        // FC
        mModel.getCalendarVisitMutableLiveData().observe(this, fcMonthlyVisits -> {
            mSwipeRefreshLayout.setRefreshing(false);
            mCompactCalendarView.removeAllEvents();

            if (fcMonthlyVisits != null) {
                for (FCMonthlyVisit fcMonthlyVisit : fcMonthlyVisits) {
                    com.github.sundeepk.compactcalendarview.domain.Event evt = new com.github.sundeepk.compactcalendarview.domain.Event(
                            ContextCompat.getColor(getBaseContext(), R.color.colorPrimary), fcMonthlyVisit.getDate(), "");
                    mCompactCalendarView.addEvent(evt);
                }
            }

            Date selectedDate = mModel.getSelectedDateMutableLiveData().getValue();
            mModel.getSelectedDateMutableLiveData().postValue(selectedDate != null ? selectedDate : new Date());
        });

        // Event view or edit
        mModel.getEventMutableLiveData().observe(this, event -> {
            if (event != null) {
                Intent iCreateEvent = new Intent(getBaseContext(), CreateEventActivity.class);
                iCreateEvent.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.valueOf(event.getViewType()));
                iCreateEvent.putExtra(IntentUtil.Companion.getEVENT(), event);
                iCreateEvent.putExtra(IntentUtil.Companion.getEVENT_CATEGORY(), new IntentEventCategory(event.getCategories()));
                startActivity(iCreateEvent);
            }
        });

        // Visit view or edit
        mModel.getVisitViewMutableLiveData().observe(this, visitView -> {
            if (visitView != null) {
                Intent iCreateEvent = new Intent(getBaseContext(), CreateVisitActivity.class);
                iCreateEvent.putExtra(IntentUtil.Companion.getVISIT(), visitView);
                iCreateEvent.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.valueOf(visitView.getViewType()));
                startActivityForResult(iCreateEvent, EDIT_EVENT_FC_RESULT_ACTIVITY);
            }
        });

        mModel.getSelectedDateMutableLiveData().observe(this, selectedDate -> {
            mDynamicBox.hideAll();

            if (selectedDate != null) {
                mCompactCalendarView.setCurrentDate(selectedDate);

                switch (mModel.getUserType()) {
                    case FIELD_CONTROLLER:
                        if (mAdapter == null) {
                            mAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.EVENT_FC, new ArrayList<>(), mEventFCListener);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                        List<VisitCalendar> visitCalendars = populateVisitCalendars(selectedDate);
                        if (visitCalendars.size() == 0) {
                            mDynamicBox.showCustomView(IntentUtil.Companion.getEMPTY_VIEW());
                        } else {
                            mAdapter.setData(visitCalendars);
                        }
                        break;
                    case BEAUTY_PROMOTOR:
                    case BEAUTY_PROMOTER_LEADER:
                        if (mAdapter == null) {
                            mAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.EVENT, mModel.getUserType(), new ArrayList<>(), mEventBPListener);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                        List<EventCalendar> eventCalendars = populateEventCalendars(selectedDate);
                        if (eventCalendars.size() == 0) {
                            mDynamicBox.showCustomView(IntentUtil.Companion.getEMPTY_VIEW());
                        } else {
                            mAdapter.setData(eventCalendars);
                        }
                        break;
                    default:
                        break;
                }
                mAdapter.notifyDataSetChanged();
            }
        });

        mModel.getCurrentMonthMutableLiveData().observe(this, currentMonthYearDate -> {
            if (currentMonthYearDate != null) {
                getSupportActionBar().setTitle(DateTimeUtil.INSTANCE.getSdfMonthYear().format(currentMonthYearDate));

                mModel.getData(currentMonthYearDate);
            }
        });

        // Checklist
        mModel.getChecklistEventMutableLiveData().observe(this, checklistEvent -> {
            if (checklistEvent != null) {
                Intent iChecklist = new Intent(getBaseContext(), ChecklistActivity.class);
                if (checklistEvent.getEditButtonEnable()) {
                    iChecklist.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                } else {
                    iChecklist.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.VIEW);
                }
                iChecklist.putExtra(IntentUtil.Companion.getCHECKLIST(), checklistEvent);
                startActivity(iChecklist);
            }
        });

        // Attendant
        mModel.getAttendantButtonMutableLiveData().observe(this, attendantButton -> {
            if (attendantButton != null) {
                if (attendantButton.getInputButtonEnable()) {
                    Intent iAttendant = new Intent(getBaseContext(), AttendantActivity.class);
                    iAttendant.putExtra(IntentUtil.Companion.getATTENDANT_BUTTON(), attendantButton);
                    startActivity(iAttendant);
                } else {
                    Intent iAttendantHistory = new Intent(getBaseContext(), AttendantHistoryActivity.class);
                    iAttendantHistory.putExtra(IntentUtil.Companion.getCALENDAR_ID(), attendantButton.getCalendarId());
                    startActivity(iAttendantHistory);
                }
            }
        });

        // Event Category
        mModel.getEventCategoryListMutableLiveData().observe(this, eventCategories -> {
            if (eventCategories != null && eventCategories.size() > 0) {
                Intent iCreateEvent = new Intent(this, CreateEventActivity.class);
                iCreateEvent.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
                iCreateEvent.putExtra(IntentUtil.Companion.getEVENT_CATEGORY(), new IntentEventCategory(eventCategories));
                startActivityForResult(iCreateEvent, CREATE_EVENT_RESULT_ACTIVITY);
            }
        });

        // Visit Category
        mModel.getVisitCategoryListMutableLiveData().observe(this, visitCategories -> {
            if (visitCategories != null && visitCategories.size() > 0) {
                Intent iCreateEvent = new Intent(this, CreateVisitActivity.class);
                iCreateEvent.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
                iCreateEvent.putExtra(IntentUtil.Companion.getVISIT_CATEGORY(), new IntentVisitCategory(visitCategories));
                startActivityForResult(iCreateEvent, CREATE_EVENT_FC_RESULT_ACTIVITY);
            }
        });

        // Check In
        mModel.getCheckInEventMutableLiveData().observe(this, event -> {
            if (event != null) {
                Intent iCheckIn = new Intent(getBaseContext(), CheckInActivity.class);
                iCheckIn.putExtra(IntentUtil.Companion.getEVENT(), event);
                startActivityForResult(iCheckIn, CHECK_IN_RESULT_ACTIVITY);
            }
        });
        mModel.getCheckInRadiusBPMutableLiveData().observe(this, checkInRadius -> {
            if (checkInRadius != null) {
                Intent iCheckIn = new Intent(getBaseContext(), CheckInActivity.class);
                iCheckIn.putExtra(IntentUtil.Companion.getCHECK_IN_RADIUS(), checkInRadius);
                startActivityForResult(iCheckIn, CHECK_IN_RESULT_ACTIVITY);
            }
        });
        mModel.getCheckInRadiusFCMutableLiveData().observe(this, checkInRadius -> {
            if (checkInRadius != null) {
                Intent iCheckInOut = new Intent(getBaseContext(), CheckInOutActivity.class);
                iCheckInOut.putExtra(IntentUtil.Companion.getCHECK_IN_RADIUS(), checkInRadius);
                startActivityForResult(iCheckInOut, CHECK_IN_RESULT_ACTIVITY);
            }
        });

        // Store Information
        mModel.getStoreInformationMutableLiveData().observe(this, storeInformation -> {
            if (storeInformation != null) {
                Intent iStoreInformation = new Intent(this, StoreInformationActivity.class);
                iStoreInformation.putExtra(IntentUtil.Companion.getSTORE_INFORMATION(), storeInformation);
                startActivity(iStoreInformation);
            }
        });

        mModel.getCurrentMonthMutableLiveData().postValue(new Date());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_calendar_monthly_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        Date date = mModel.getSelectedDateMutableLiveData().getValue();
        if (date != null) {
            mModel.getCurrentMonthMutableLiveData().setValue(date);
        } else {
            mModel.getCurrentMonthMutableLiveData().setValue(new Date());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                switch (mModel.getUserType()) {
                    case FIELD_CONTROLLER:
                        mModel.callVisitCategory(SessionUtil.getUserId());
                        break;
                    default:
                        mModel.callEventCategory(SessionUtil.getUserId());
                        break;
                }
                break;
            case R.id.toolbar:
                final Calendar today = Calendar.getInstance();
                mBuilder = new MonthPickerDialog.Builder(CalendarMonthlyViewActivity.this, this, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

                Date selectedDate = mModel.getSelectedDateMutableLiveData().getValue() != null ? mModel.getSelectedDateMutableLiveData().getValue() : new Date();
                Calendar selectedDateCalendar = Calendar.getInstance();
                selectedDateCalendar.setTime(selectedDate);

                mBuilder.setActivatedMonth(selectedDateCalendar.get(Calendar.MONTH))
                        .setMinYear(1990)
                        .setActivatedYear(selectedDateCalendar.get(Calendar.YEAR))
                        .setMaxYear(2030)
                        .setMinMonth(Calendar.JANUARY)
                        .setTitle(getString(R.string.title_select_date_time))
                        .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)
                        .setOnMonthChangedListener(selectedMonth -> Log.d(TAG, "Selected month : " + selectedMonth))
                        .setOnYearChangedListener(selectedYear -> Log.d(TAG, "Selected year : " + selectedYear))
                        .build()
                        .show();
                break;
        }
    }

    @Override
    public void onDateSet(int selectedMonth, int selectedYear) {
        Calendar resultDate = Calendar.getInstance();
        resultDate.set(selectedYear, selectedMonth, 1);
        mModel.getSelectedDateMutableLiveData().postValue(resultDate.getTime());

        // Retrieve new data from server
        Date currentMonth = mModel.getCurrentMonthMutableLiveData().getValue();
        if (selectedMonth != DateTimeUtil.INSTANCE.getMonthFromDate(currentMonth) ||
                selectedYear != DateTimeUtil.INSTANCE.getYearFromDate(currentMonth)) {
            mModel.getCurrentMonthMutableLiveData().postValue(resultDate.getTime());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            onRefresh();

            switch (requestCode) {
                case CREATE_EVENT_RESULT_ACTIVITY:
                case CREATE_EVENT_FC_RESULT_ACTIVITY:
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content), R.string.message_create_event_success, Snackbar.LENGTH_LONG);
                    snackbar.show();
                    break;
                case EDIT_EVENT_RESULT_ACTIVITY:
                case EDIT_EVENT_FC_RESULT_ACTIVITY:
                    snackbar = Snackbar.make(findViewById(R.id.main_content), R.string.message_edit_event_success, Snackbar.LENGTH_LONG);
                    snackbar.show();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void applyLocale() {
        Locale.setDefault(DateTimeUtil.INSTANCE.getLocale());

        Resources res = getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = DateTimeUtil.INSTANCE.getLocale();
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private List<EventCalendar> populateEventCalendars(Date selectedDate) {
        List<EventCalendar> eventCalendars = new ArrayList<>();

        List<BPMonthlyEvent> bpMonthlyEventList = mModel.getCalendarEventMutableLiveData().getValue();

        if (bpMonthlyEventList != null) {
            for (BPMonthlyEvent bpMonthlyEvent : bpMonthlyEventList) {
                for (EventCalendar eventCalendar : bpMonthlyEvent.getEvents()) {
                    if ((DateTimeUtil.INSTANCE.isSameDay(selectedDate, new Date(eventCalendar.getStartTime())))
                            || (DateTimeUtil.INSTANCE.isSameDay(selectedDate, new Date(eventCalendar.getEndTime())))) {
                        eventCalendars.add(eventCalendar);
                    }
                }
            }
        }

        return eventCalendars;
    }

    private List<VisitCalendar> populateVisitCalendars(Date selectedDate) {
        List<VisitCalendar> visitCalendars = new ArrayList<>();

        List<FCMonthlyVisit> fcMonthlyVisitList = mModel.getCalendarVisitMutableLiveData().getValue();

        if (fcMonthlyVisitList != null) {
            for (FCMonthlyVisit fcMonthlyVisit : fcMonthlyVisitList) {
                for (VisitCalendar visitCalendar : fcMonthlyVisit.getVisits()) {
                    if ((DateTimeUtil.INSTANCE.isSameDay(selectedDate, new Date(visitCalendar.getStartTime())))
                            || (DateTimeUtil.INSTANCE.isSameDay(selectedDate, new Date(visitCalendar.getEndTime())))) {
                        visitCalendars.add(visitCalendar);
                    }
                }
            }
        }

        return visitCalendars;
    }
}
