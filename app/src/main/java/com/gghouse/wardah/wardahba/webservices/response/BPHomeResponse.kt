package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.BPHome

class BPHomeResponse : GenericResponse() {
    var data: BPHome? = null
}
