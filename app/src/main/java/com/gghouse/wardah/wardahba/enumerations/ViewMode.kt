package com.gghouse.wardah.wardahba.enumerations

import java.io.Serializable

enum class ViewMode : Serializable {
    INPUT,
    EDIT,
    VIEW
}
