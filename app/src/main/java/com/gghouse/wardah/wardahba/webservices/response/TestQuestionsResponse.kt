package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.TestQuestion

/**
 * Created by michaelhalim on 5/6/17.
 */

class TestQuestionsResponse : GenericResponse() {
    var data: TestQuestion? = null
}
