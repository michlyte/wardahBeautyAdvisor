package com.gghouse.wardah.wardahba.webservices.model

class BAInformationOverview {
    val nip: String
    val fullName: String
    val firstDateWork: Long
    val workingDuration: String
    val currentSalesOverview: BAHomeSalesOverview
    val overallSalesOverview: BAHomeSalesOverview
    val currentTestOverview: CurrentTestOverview
    val overallTestOverview: OverallTestOverview

    constructor(nip: String, fullName: String, firstDateWork: Long, workingDuration: String, currentSalesOverview: BAHomeSalesOverview, overallSalesOverview: BAHomeSalesOverview, currentTestOverview: CurrentTestOverview, overallTestOverview: OverallTestOverview) {
        this.nip = nip
        this.fullName = fullName
        this.firstDateWork = firstDateWork
        this.workingDuration = workingDuration
        this.currentSalesOverview = currentSalesOverview
        this.overallSalesOverview = overallSalesOverview
        this.currentTestOverview = currentTestOverview
        this.overallTestOverview = overallTestOverview
    }
}