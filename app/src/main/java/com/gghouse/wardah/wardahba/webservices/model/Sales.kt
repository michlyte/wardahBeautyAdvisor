package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import java.io.Serializable

class Sales : WardahHistoryRecyclerViewItem, Serializable {
    var id: Long? = null
    var amount: Double? = null
    var salesDate: Long? = null
    var editable: Boolean? = null
    var quantity: Integer? = null
    var productHighlight: Integer? = null
    var productFocus: Integer? = null
    var userId: Long? = null

    constructor(id: Long?, amount: Double?, salesDate: Long?, editable: Boolean?, quantity: Integer?, productHighlight: Integer?, productFocus: Integer?, userId: Long?) : super() {
        this.id = id
        this.amount = amount
        this.salesDate = salesDate
        this.editable = editable
        this.quantity = quantity
        this.productHighlight = productHighlight
        this.productFocus = productFocus
        this.userId = userId
    }
}