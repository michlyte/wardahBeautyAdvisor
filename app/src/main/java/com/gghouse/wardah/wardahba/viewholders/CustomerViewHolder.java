package com.gghouse.wardah.wardahba.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.webservices.model.Customer;

public class CustomerViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final LinearLayout mLlContainer;
    public final CardView mCardview;
    public final TextView mTVName;
    public final TextView mTVNoHp;
    public final TextView mDateTextView;
    public final TextView mMonthYearTextView;
    public Customer mItem;

    public CustomerViewHolder(View view) {
        super(view);
        mView = view;
        mLlContainer = mView.findViewById(R.id.ll_container);
        mCardview = mView.findViewById(R.id.cardView);
        mTVName = mView.findViewById(R.id.tv_name);
        mTVNoHp = mView.findViewById(R.id.noHpTextView);
        mDateTextView = mView.findViewById(R.id.dateTextView);
        mMonthYearTextView = mView.findViewById(R.id.monthYearTextView);
    }

    private void setupWeekendView(Context context) {
        if (DateTimeUtil.INSTANCE.isWeekend(mItem.getUpdatedDate())) {
            mDateTextView.setTextColor(ContextCompat.getColor(context, R.color.colorHistoryHeader6));
        } else {
            mDateTextView.setTextColor(ContextCompat.getColor(context, android.R.color.black));
        }
    }

    private void setupActionEvent(WardahListener wardahListener) {
        mLlContainer.setOnClickListener(v -> wardahListener.onClick(mItem, ViewMode.EDIT));
    }

    public void setData(Context context, Customer customer, WardahListener wardahListener) {
        mItem = customer;
        mTVName.setText(mItem.getName());
        mTVNoHp.setText(mItem.getMobileNumber());
        String date = DateTimeUtil.INSTANCE.getSdfDate().format(customer.getUpdatedDate());
        mDateTextView.setText(date);
        String monthYear = DateTimeUtil.INSTANCE.getSdfMonthCommaYear().format(customer.getUpdatedDate());
        mMonthYearTextView.setText(monthYear);

        setupWeekendView(context);
//        setupRowView(context);
        setupActionEvent(wardahListener);
    }
}
