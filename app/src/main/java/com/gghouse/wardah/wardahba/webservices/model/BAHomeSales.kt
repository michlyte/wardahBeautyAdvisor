package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class BAHomeSales : Serializable {
    var amount: Double? = null
    var quantity: Int? = null
}