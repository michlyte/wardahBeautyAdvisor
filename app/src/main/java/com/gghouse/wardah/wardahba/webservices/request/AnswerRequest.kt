package com.gghouse.wardah.wardahba.webservices.request

/**
 * Created by michaelhalim on 5/9/17.
 */

class AnswerRequest(private val questionId: Long?, private val answerId: Long?)
