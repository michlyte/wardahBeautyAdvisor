package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import java.io.Serializable

class VisitStoreInformation : Serializable {
    val competitorTurnover: Double
    val productTypes: List<ProductType>
    val month: Int
    val year: Int
    val editable: Boolean
    val totalBA: Int

    constructor(competitorTurnover: Double, productTypes: List<ProductType>, month: Int, year: Int, editable: Boolean, totalBA: Int) {
        this.competitorTurnover = competitorTurnover
        this.productTypes = productTypes
        this.month = month
        this.year = year
        this.editable = editable
        this.totalBA = totalBA
    }

    val evaluationPeriodStr: String
        get() = DateTimeUtil.sdfMonthYear.format(DateTimeUtil.getDateFromMonthYear(month, year))
}