package com.gghouse.wardah.wardahba.webservices.request

class VisitCheckOutRequest {
    val calendarId: Long

    constructor(calendarId: Long) {
        this.calendarId = calendarId
    }
}