package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.afollestad.materialdialogs.MaterialDialog
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.enumerations.ViewMode
import com.gghouse.wardah.wardahba.models.SalesSummary
import com.gghouse.wardah.wardahba.utils.LogUtil
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight
import com.gghouse.wardah.wardahba.webservices.model.SalesEditProducts
import com.gghouse.wardah.wardahba.webservices.request.ProductHighlightRequest
import com.gghouse.wardah.wardahba.webservices.request.SalesEditRequest
import com.gghouse.wardah.wardahba.webservices.request.SalesInputRequest
import com.gghouse.wardah.wardahba.webservices.request.SalesRequest
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class InputSalesViewModel : ViewModel() {

    val salesSummary: MutableLiveData<SalesSummary> by lazy {
        MutableLiveData<SalesSummary>()
    }
    val isSubmitSuccess: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
    val isEditSuccess: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
    val salesEditProductsMutableLiveData: MutableLiveData<SalesEditProducts> by lazy {
        MutableLiveData<SalesEditProducts>()
    }
    val productHighlightLiveData: MutableLiveData<List<ProductHighlight>> by lazy {
        MutableLiveData<List<ProductHighlight>>()
    }
    val productFocusMutableLiveData: MutableLiveData<List<ProductHighlight>> by lazy {
        MutableLiveData<List<ProductHighlight>>()
    }

    var date: Date? = null
    var viewMode: ViewMode? = null

    fun getData() {

    }

    fun submit(materialDialog: MaterialDialog, salesRequest: SalesRequest, highlight: List<ProductHighlightRequest>, focus: List<ProductHighlightRequest>) {
        if (SessionUtil.validateLoginSession()) {

            val salesInputRequest = SalesInputRequest(SessionUtil.getUserId()!!, salesRequest, highlight, focus, date!!.time)

            when (Config.mode) {
                ModeEnum.DUMMY_DEVELOPMENT -> isSubmitSuccess.postValue(true)
                ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> {
                    materialDialog.show()
                    val callSalesSubmit = ApiClient.client.apiSalesCreate(salesInputRequest)
                    callSalesSubmit.enqueue(object : Callback<GenericResponse> {
                        override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                            LogUtil.log(LogUtil.ON_RESPONSE)

                            if (response.isSuccessful) {
                                val rps = response.body()
                                when (rps!!.code) {
                                    Config.CODE_200 -> isSubmitSuccess.postValue(true)
                                    else -> {
                                        isSubmitSuccess.postValue(false)
                                        ToastUtil.toastMessage(TAG, rps.status!!)
                                    }
                                }
                            } else {
                                isSubmitSuccess.postValue(false)
                                ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                            }
                        }

                        override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                            isSubmitSuccess.postValue(false)
                            ToastUtil.toastMessage(TAG, t.message.toString())
                        }
                    })
                }
            }
        }
    }

    fun edit(materialDialog: MaterialDialog, salesId: Long?, salesRequest: SalesRequest, highlight: List<ProductHighlightRequest>, focus: List<ProductHighlightRequest>, salesDate: Long?) {
        if (SessionUtil.validateLoginSession()) {

            val salesEditRequest = SalesEditRequest(salesId!!, SessionUtil.getUserId()!!, salesRequest, highlight, focus, salesDate!!)

            when (Config.mode) {
                ModeEnum.DUMMY_DEVELOPMENT -> isEditSuccess.postValue(true)
                ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> {
                    materialDialog.show()

                    val callSalesEdit = ApiClient.client.apiSalesEdit(salesEditRequest)
                    callSalesEdit.enqueue(object : Callback<GenericResponse> {
                        override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                            LogUtil.log(LogUtil.ON_RESPONSE)

                            if (response.isSuccessful) {
                                val rps = response.body()
                                when (rps!!.code) {
                                    Config.CODE_200 -> isEditSuccess.postValue(true)
                                    else -> {
                                        isEditSuccess.postValue(false)
                                        ToastUtil.toastMessage(TAG, rps.status!!)
                                    }
                                }
                            } else {
                                isEditSuccess.postValue(false)
                                ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                            }
                        }

                        override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                            isEditSuccess.postValue(false)
                            ToastUtil.toastMessage(TAG, t.message.toString())
                        }
                    })
                }
            }
        }
    }

    fun addOrRemoveProductHighlightQty(productHighlight: ProductHighlight) {
        val productHighlightList = productHighlightLiveData.value
        for (ph in productHighlightList!!) {
            if (ph.id === productHighlight.id) {
                ph.quantity = productHighlight.quantity
                break
            }
        }
        productHighlightLiveData.postValue(productHighlightList)
    }

    fun addOrRemoveProductFocusQty(productHighlight: ProductHighlight) {
        val productFocusList = productFocusMutableLiveData.value
        for (ph in productFocusList!!) {
            if (ph.id === productHighlight.id) {
                ph.quantity = productHighlight.quantity
                break
            }
        }
        productFocusMutableLiveData.postValue(productFocusList)
    }

    companion object {
        private val TAG = InputSalesViewModel::class.java.simpleName
    }
}
