package com.gghouse.wardah.wardahba.webservices.test;

import android.app.Activity;
import android.os.Bundle;

import com.gghouse.wardah.wardahba.R;

public class WebServiceTestActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service_test);

        wsRunTest();
    }

    private void wsRunTest() {
        // Michael Halim : Retrofit get only example
//        Call<List<Dummy>> callDummyList = ApiClient.INSTANCE.getClient().dummyGetList();
//        callDummyList.enqueue(new Callback<List<Dummy>>() {
//            @Override
//            public void onResponse(Call<List<Dummy>> call, Response<List<Dummy>> response) {
//                LogUtil.INSTANCE.log("onResponse");
//                List<Dummy> dummies = response.body();
//                for (Dummy dummy : dummies) {
//                    LogUtil.INSTANCE.log(dummy.getUserId() + ", " + dummy.getId());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Dummy>> call, Throwable t) {
//                LogUtil.INSTANCE.log("onFailure : " + t.getMessage());
//            }
//        });
    }
}
