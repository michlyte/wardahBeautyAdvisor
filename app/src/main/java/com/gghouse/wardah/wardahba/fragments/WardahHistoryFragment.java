package com.gghouse.wardah.wardahba.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.WardahApp;
import com.gghouse.wardah.wardahba.activities.WardahHistoryActivity;
import com.gghouse.wardah.wardahba.activities.sales.InputSalesActivity;
import com.gghouse.wardah.wardahba.activities.store.StoreInputActivity;
import com.gghouse.wardah.wardahba.adapters.WardahHistoryAdapterWithHeader;
import com.gghouse.wardah.wardahba.enumerations.OrderByEnum;
import com.gghouse.wardah.wardahba.enumerations.SortByTypeEnum;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.helper.WrapContentLinearLayoutManager;
import com.gghouse.wardah.wardahba.interfaces.OnLoadMoreListListener;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.models.IntentProductHighlight;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.viewmodels.WardahHistoryFragmentViewModel;
import com.gghouse.wardah.wardahba.webservices.model.DailySales;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySales;
import com.gghouse.wardah.wardahba.webservices.model.Pagination;
import com.gghouse.wardah.wardahba.webservices.model.Sales;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mehdi.sakout.dynamicbox.DynamicBox;

public class WardahHistoryFragment extends Fragment implements WardahListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = WardahHistoryActivity.class.getSimpleName();
    private static final String ARG_ITEM_TYPE = "ARG_ITEM_TYPE";
    private static final String ARG_ITEM_LOCATION_ID = "ARG_ITEM_LOCATION_ID";
    private static final String TAG_DATE_PICKER = "Datepickerdialog";
    private static final int EDIT_SALES_ACTIVITY_RESULT = 4;

    // Store Sales
    private static final int EDIT_DAILY_STORE_SALES_ACTIVITY_RESULT = 10;
    private static final int EDIT_MONTHLY_STORE_SALES_ACTIVITY_RESULT = 12;

    private WardahHistoryFragmentViewModel mModel;

    private Menu mMenu;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    // Filter
    private LinearLayout mFilterLinearLayout;
    private TextView mFilterTextView;
    private Drawable mFilterDrawable;
    private Date mFilterBeginDate;
    private Date mFilterEndDate;
    // Sort
    private View mSortView;

    private RecyclerView mRecyclerView;
    private WardahHistoryAdapterWithHeader mAdapter;
    private TextView mMessageTextView;

    private OnLoadMoreListListener mOnLoadMoreListListener;

    private DynamicBox mDynamicBox;

    public WardahHistoryFragment() {

    }

    public static WardahHistoryFragment newInstance(WardahItemType type) {
        WardahHistoryFragment fragment = new WardahHistoryFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    public static WardahHistoryFragment newInstance(WardahItemType type, long locationId) {
        WardahHistoryFragment fragment = new WardahHistoryFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM_TYPE, type);
        args.putSerializable(ARG_ITEM_LOCATION_ID, locationId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_history, container, false);

        mModel = ViewModelProviders.of(this).get(WardahHistoryFragmentViewModel.class);

        mModel.setType((WardahItemType) getArguments().get(ARG_ITEM_TYPE));
        switch (mModel.getType()) {
            case SALES_FC:
            case PRODUCT_HIGHLIGHT_FC:
            case PRODUCT_FOCUS_FC:
                mModel.setUserId(getArguments().getLong(ARG_ITEM_LOCATION_ID, 0L));
                break;
            default:
                mModel.setLocationId(getArguments().getLong(ARG_ITEM_LOCATION_ID, 0L));
                break;
        }

        mSwipeRefreshLayout = view.findViewById(R.id.srl_swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this::onSwipeToRefresh);

        // Filter
        mFilterLinearLayout = view.findViewById(R.id.ll_filter);
        mFilterTextView = view.findViewById(R.id.tv_filter);
        Button resetButton = view.findViewById(R.id.b_reset);
        resetButton.setOnClickListener(v -> {
            mFilterBeginDate = null;
            mFilterEndDate = null;

            if (getContext() != null) {
                mMenu.findItem(R.id.action_filter).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_filter));
                mFilterDrawable = mMenu.findItem(R.id.action_filter).getIcon();
                mFilterDrawable.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.white), PorterDuff.Mode.SRC_ATOP);
            }

            mFilterLinearLayout.setVisibility(View.GONE);

            onClickResetButton();
        });

        // Sort
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        mSortView = layoutInflater.inflate(R.layout.popup_sort, container, false);

        mRecyclerView = view.findViewById(R.id.rv_list);

        // use a linear layout manager
        WrapContentLinearLayoutManager linearLayoutManager = new WrapContentLinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new WardahHistoryAdapterWithHeader(
                mModel.getType(), getContext(), mRecyclerView, new ArrayList<>(), this);
        mRecyclerView.setAdapter(mAdapter);

        mOnLoadMoreListListener = () -> {
            new Handler().post(() -> {
                mAdapter.add(null);
                mAdapter.notifyItemInserted(mAdapter.getItemCount() - 1);
            });
            mModel.getData(mAdapter, mModel.getPage() + 1, mFilterBeginDate, mFilterEndDate);
        };

        /*
         * Empty Message
         */
        mMessageTextView = view.findViewById(R.id.tv_message);

        mDynamicBox = new DynamicBox(getActivity(), mRecyclerView);

        mModel.getDataHeaderHistoryMutableLiveData().observe(this, header -> {
            if (header != null) {
                mAdapter.clear();
                mAdapter.add(header);

                Long userId = SessionUtil.getUserId();
                if (userId == null) {
                    SessionUtil.loggingOut();
                } else {
                    mModel.callHistoryData(userId, mModel.getPage(), mFilterBeginDate, mFilterEndDate);
                }
            }
        });

        mModel.getWardahItemMutableLiveData().observe(this, wardahItem -> {
            mDynamicBox.hideAll();
            mSwipeRefreshLayout.setRefreshing(false);

            if (wardahItem != null) {
                Pagination pagination = wardahItem.getPagination();
                Log.d(TAG, WardahApp.getInstance().getGson().toJson(pagination));
                if (pagination.getLast()) {
                    mAdapter.removeOnLoadMoreListener();
                } else {
                    mAdapter.setLoaded();
                    mAdapter.setOnLoadMoreListener(mOnLoadMoreListListener);
                }

                List<WardahHistoryRecyclerViewItem> myDataList = wardahItem.getData();
                if (mModel.getPage() == 0) {
                    mAdapter.addAll(myDataList);
                    mRecyclerView.setAdapter(mAdapter);

                    // Empty Message
                    if (myDataList != null && myDataList.size() <= 0) {
                        mMessageTextView.setVisibility(View.VISIBLE);
                    }
                } else {
                    // Load More
                    mAdapter.addAll(myDataList);
                    mRecyclerView.post(() -> mAdapter.notifyDataSetChanged());
                }
            }
        });

        mModel.getSalesEditProductsMutableLiveData().observe(this, salesEditProducts -> {
            if (salesEditProducts != null) {
                Intent iSalesInput = new Intent(getContext(), InputSalesActivity.class);
                iSalesInput.putExtra(IntentUtil.Companion.getPRODUCT_HIGHLIGHT(), new IntentProductHighlight(salesEditProducts.getHighlight()));
                iSalesInput.putExtra(IntentUtil.Companion.getPRODUCT_FOCUS(), new IntentProductHighlight(salesEditProducts.getFocus()));
                iSalesInput.putExtra(IntentUtil.Companion.getDATE(), salesEditProducts.getSalesDate());
                iSalesInput.putExtra(IntentUtil.Companion.getLOCK(), false);
                iSalesInput.putExtra(IntentUtil.Companion.getSALES(), salesEditProducts);
                iSalesInput.putExtra(IntentUtil.Companion.getVIEW_MODE(), salesEditProducts.getEditable() ? ViewMode.EDIT : ViewMode.VIEW);
                startActivityForResult(iSalesInput, EDIT_SALES_ACTIVITY_RESULT);
            }
        });

        mModel.getDailySalesEditMutableLiveData().observe(this, dailySalesEdit -> {
            if (dailySalesEdit != null) {
                Intent iEditDailyStoreSales = new Intent(getActivity(), StoreInputActivity.class);
                iEditDailyStoreSales.putExtra(IntentUtil.Companion.getDATA(), dailySalesEdit);

                if (dailySalesEdit.getEditable()) {
                    iEditDailyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                    iEditDailyStoreSales.putExtra(IntentUtil.Companion.getTYPE(), mModel.getType());
                    startActivityForResult(iEditDailyStoreSales, EDIT_DAILY_STORE_SALES_ACTIVITY_RESULT);
                } else {
                    iEditDailyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.VIEW);
                    iEditDailyStoreSales.putExtra(IntentUtil.Companion.getTYPE(), mModel.getType());
                    startActivity(iEditDailyStoreSales);
                }
            }
        });
        mModel.getMonthlySalesViewMutableLiveData().observe(this, monthlySalesView -> {
            if (monthlySalesView != null) {
                Intent iEditMonthlyStoreSales = new Intent(getActivity(), StoreInputActivity.class);
                iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getDATA(), monthlySalesView);
                iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getDATE(), DateTimeUtil.INSTANCE.getDateFromMonthYear(monthlySalesView.getMonth(), monthlySalesView.getYear()).getTime());
                iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getTYPE(), mModel.getType());

                if (monthlySalesView.getEditable()) {
                    iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                    startActivityForResult(iEditMonthlyStoreSales, EDIT_MONTHLY_STORE_SALES_ACTIVITY_RESULT);
                } else {
                    iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.VIEW);
                    startActivity(iEditMonthlyStoreSales);
                }
            }
        });

        mDynamicBox.showLoadingLayout();
        mModel.getData(mAdapter, 0, mFilterBeginDate, mFilterEndDate);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        switch (mModel.getType()) {
            case SALES:
            case SALES_FC:
            case STORE_DAILY_SALES:
            case STORE_MONTHLY_SALES:
                mMenu = menu;
                inflater.inflate(R.menu.menu_history_filter, menu);
                mFilterDrawable = menu.findItem(R.id.action_filter).getIcon();
                mFilterDrawable.mutate();
                break;
            case PRODUCT_HIGHLIGHT:
            case PRODUCT_FOCUS:
            case STORE_DAILY_PRODUCT_HIGHLIGHT:
            case STORE_DAILY_PRODUCT_FOCUS:
            case STORE_MONTHLY_PRODUCT_HIGHLIGHT:
            case STORE_MONTHLY_PRODUCT_FOCUS:
            case PRODUCT_HIGHLIGHT_FC:
            case PRODUCT_FOCUS_FC:
                mMenu = menu;
                inflater.inflate(R.menu.menu_history_sort, menu);
                Drawable sortDrawable = menu.findItem(R.id.action_sort).getIcon();
                sortDrawable.mutate();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getActivity() != null)
                    getActivity().finish();
                return true;
            case R.id.action_filter:
                Calendar cFrom = Calendar.getInstance();
                cFrom.setTime((mFilterBeginDate == null ? new Date() : mFilterBeginDate));
                Calendar cTo = Calendar.getInstance();
                cTo.setTime((mFilterEndDate == null ? new Date() : mFilterEndDate));
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        this,
                        cFrom.get(Calendar.YEAR),
                        cFrom.get(Calendar.MONTH),
                        cFrom.get(Calendar.DAY_OF_MONTH),
                        cTo.get(Calendar.YEAR),
                        cTo.get(Calendar.MONTH),
                        cTo.get(Calendar.DAY_OF_MONTH)
                );
                if (getActivity() != null)
                    dpd.show(getActivity().getFragmentManager(), TAG_DATE_PICKER);
                return true;
            case R.id.action_sort:
                showSortByDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        try {
            String from = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
            String to = dayOfMonthEnd + "/" + (monthOfYearEnd + 1) + "/" + yearEnd;
            mFilterBeginDate = DateTimeUtil.INSTANCE.getSdfFilter().parse(from);
            mFilterEndDate = DateTimeUtil.INSTANCE.getSdfFilter().parse(to);

            /*
             * Change filter icon color
             */
            if (getContext() != null) {
                mMenu.findItem(R.id.action_filter).setIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_filter_fill));
                mFilterDrawable = mMenu.findItem(R.id.action_filter).getIcon();
                mFilterDrawable.mutate();
                mFilterDrawable.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.white), PorterDuff.Mode.SRC_ATOP);
            }

            if (mFilterBeginDate != null && mFilterEndDate != null) {
                String dateRangeStr = DateTimeUtil.INSTANCE.getSdfFilter().format(mFilterBeginDate) + " - " + DateTimeUtil.INSTANCE.getSdfFilter().format(mFilterEndDate);
                mFilterTextView.setText(dateRangeStr);
                mFilterLinearLayout.setVisibility(View.VISIBLE);
            }

            onClickResetButton();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(WardahHistoryRecyclerViewItem item, ViewMode viewMode) {
        switch (mModel.getType()) {
            case SALES:
                Sales sales = (Sales) item;
                mModel.callSalesEditProducts(sales.getId());
                break;
            case STORE_DAILY_SALES:
                DailySales dailySales = (DailySales) item;
                mModel.callDailySalesEdit(dailySales.getDailyReportId());
                break;
            case STORE_MONTHLY_SALES:
                MonthlySales monthlySales = (MonthlySales) item;
                mModel.callMonthlySalesView(monthlySales.getMonthlyReportId());
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (getActivity() != null && getActivity().findViewById(R.id.main_content) != null) {

            if (requestCode == EDIT_SALES_ACTIVITY_RESULT && resultCode == Activity.RESULT_OK) {
                mModel.getData(mAdapter, 0, mFilterBeginDate, mFilterEndDate);
                Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_edit_sales_success, Snackbar.LENGTH_LONG).show();
            } else if (requestCode == EDIT_DAILY_STORE_SALES_ACTIVITY_RESULT || requestCode == EDIT_MONTHLY_STORE_SALES_ACTIVITY_RESULT) {
                mModel.getData(mAdapter, 0, mFilterBeginDate, mFilterEndDate);
                Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_edit_store_sales_success, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    protected void onSwipeToRefresh() {
        mModel.getData(mAdapter, 0, mFilterBeginDate, mFilterEndDate);
    }

    protected void onClickResetButton() {
        mDynamicBox.showLoadingLayout();
        mModel.getData(mAdapter, 0, mFilterBeginDate, mFilterEndDate);
    }

    private void showSortByDialog() {
        if (getContext() != null) {
            Spinner sortByTypeSpinner = mSortView.findViewById(R.id.sortByTypeSpinner);
            ArrayAdapter<CharSequence> sortByTypeAdapter = ArrayAdapter.createFromResource(getContext(),
                    R.array.sort_by_type, android.R.layout.simple_spinner_item);
            sortByTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sortByTypeSpinner.setAdapter(sortByTypeAdapter);

            int sortByPosition = sortByTypeAdapter.getPosition(mModel.getSortByType().getTitle());
            sortByTypeSpinner.setSelection(sortByPosition);

            Spinner orderBySpinner = mSortView.findViewById(R.id.sortBySpinner);
            ArrayAdapter<CharSequence> orderByAdapter = ArrayAdapter.createFromResource(getContext(),
                    R.array.sort_by, android.R.layout.simple_spinner_item);
            orderByAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            orderBySpinner.setAdapter(orderByAdapter);

            int orderByPosition = orderByAdapter.getPosition(mModel.getOrderBy().getTitle());
            orderBySpinner.setSelection(orderByPosition);

            if (mSortView.getParent() != null) {
                ((ViewGroup) mSortView.getParent()).removeView(mSortView);
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.title_sort_by);
            builder.setView(mSortView);
            builder.setPositiveButton(R.string.action_ok, (dialog, id) -> {
                mModel.setSortByType(SortByTypeEnum.Companion.getSortByTypeEnum(sortByTypeSpinner.getSelectedItem().toString()));
                mModel.setOrderBy(OrderByEnum.Companion.getSortByEnum(orderBySpinner.getSelectedItem().toString()));

                // Refresh Data
                mDynamicBox.showLoadingLayout();
                mModel.getData(mAdapter, 0, mFilterBeginDate, mFilterEndDate);
            });
            builder.setNegativeButton(R.string.action_cancel, (dialogInterface, i) -> {

            });
            builder.create().show();
        }
    }
}
