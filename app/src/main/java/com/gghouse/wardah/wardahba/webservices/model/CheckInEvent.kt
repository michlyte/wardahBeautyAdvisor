package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class CheckInEvent : Serializable {
    val checkInTime: Long
    val status: String
    val bpImageURL: String
    val boothImageURL: String
    val checkInButtonEnable: Boolean

    constructor(checkInTime: Long, status: String, bpImageURL: String, boothImageURL: String, checkInButtonEnable: Boolean) {
        this.checkInTime = checkInTime
        this.status = status
        this.bpImageURL = bpImageURL
        this.boothImageURL = boothImageURL
        this.checkInButtonEnable = checkInButtonEnable
    }
}