package com.gghouse.wardah.wardahba.webservices.model

class CheckInLocation {
    val formattedAddress: String
    val latitude: String
    val longitude: String
    val status: String

    constructor(formattedAddress: String, latitude: String, longitude: String, status: String) {
        this.formattedAddress = formattedAddress
        this.latitude = latitude
        this.longitude = longitude
        this.status = status
    }
}