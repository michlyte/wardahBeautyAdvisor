package com.gghouse.wardah.wardahba.enumerations

enum class HomepageStoreSection {
    DAILY_SALES_PERSONAL,
    SALES_RECAP,
    SALES_BY_CATEGORY,
    TOTAL_SALES_HARIAN_VS_BULANAN,
    SALES_VS_SALES_TARGET_VS_OMSET_COMPETITOR,
    BUYER_VS_VISITOR,
    BUYING_POWER,
    TEST
}
