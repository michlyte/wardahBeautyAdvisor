package com.gghouse.wardah.wardahba.utils

import android.util.Log

import com.gghouse.wardah.wardahba.common.Config

/**
 * Created by michaelhalim on 9/17/16.
 */
object LogUtil {
    val LOG_BEGIN = "- BEGIN : "
    val LOG_END = "- END : "
    val ON_RESPONSE = "ON RESPONSE"
    val ON_FAILURE = "ON FAILURE"
    val ON_ERROR = "ON ERROR"
    val STATUS = "STATUS"

    val TAG = LogUtil::class.java.simpleName

    fun log(o: Any, log: String) {
        if (Config.LOG_ENABLE) {
            val lineNumber = Thread.currentThread().stackTrace[3].lineNumber
            Log.d(TAG + " [" + o.javaClass.name + ":" + lineNumber + "]", log)
        }
    }

    fun log(log: String) {
        if (Config.LOG_ENABLE) {
            val lineNumber = Thread.currentThread().stackTrace[3].lineNumber
            val className = Thread.currentThread().stackTrace[3].className
            Log.d("$TAG [$className:$lineNumber]", log)
        }
    }

    fun log(c: Class<*>, log: String) {
        if (Config.LOG_ENABLE) {
            val lineNumber = Thread.currentThread().stackTrace[3].lineNumber
            Log.d(TAG + " [" + c.name + ":" + lineNumber + "]", log)
        }
    }

    fun log(c: String, log: String) {
        if (Config.LOG_ENABLE) {
            val lineNumber = Thread.currentThread().stackTrace[3].lineNumber
            Log.d("$TAG [$c:$lineNumber]", log)
        }
    }

    fun error(log: String) {
        if (Config.LOG_ENABLE) {
            val lineNumber = Thread.currentThread().stackTrace[3].lineNumber
            val className = Thread.currentThread().stackTrace[3].className
            Log.e("$TAG [$className:$lineNumber]", log)
        }
    }

    fun error(c: Class<*>, log: String) {
        if (Config.LOG_ENABLE) {
            val lineNumber = Thread.currentThread().stackTrace[3].lineNumber
            Log.e(TAG + " [" + c.name + ":" + lineNumber + "]", log)
        }
    }

    fun error(c: String, log: String) {
        if (Config.LOG_ENABLE) {
            val lineNumber = Thread.currentThread().stackTrace[3].lineNumber
            Log.e("$TAG [$c:$lineNumber]", log)
        }
    }
}
