package com.gghouse.wardah.wardahba.webservices.model

class BAHomeLocation {
    val id: Long
    val name: String
    val salesOverview: BAHomeStoreSalesOverview

    constructor(id: Long, name: String, salesOverview: BAHomeStoreSalesOverview) {
        this.id = id
        this.name = name
        this.salesOverview = salesOverview
    }
}