package com.gghouse.wardah.wardahba.webservices

import com.gghouse.wardah.wardahba.common.Config
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by michaelhalim on 9/17/16.
 */
object ApiClient {
    private var mRetrofit: Retrofit? = null

    val client: ApiService
        get() {
            if (mRetrofit == null) {
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

                val okHttpClient = OkHttpClient.Builder()
                        .connectTimeout(Config.CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
                        .readTimeout(Config.READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
                        .addInterceptor(httpLoggingInterceptor)
                        .build()

                mRetrofit = Retrofit.Builder()
                        .baseUrl(Config.BASE_URL)
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
            }
            return mRetrofit!!.create(ApiService::class.java)
        }

    fun generateClientWithNewIP(ip: String): ApiService {
        mRetrofit = null
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(Config.CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(Config.READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .build()

        mRetrofit = Retrofit.Builder()
                .baseUrl(ip)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        return mRetrofit!!.create(ApiService::class.java)
    }
}