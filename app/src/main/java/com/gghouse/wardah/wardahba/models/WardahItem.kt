package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.webservices.model.Pagination

class WardahItem(val data: List<WardahHistoryRecyclerViewItem>?, val pagination: Pagination?)