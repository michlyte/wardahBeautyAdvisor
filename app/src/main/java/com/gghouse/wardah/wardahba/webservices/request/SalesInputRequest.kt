package com.gghouse.wardah.wardahba.webservices.request


class SalesInputRequest(val userId: Long, val sales: SalesRequest, val highlight: List<ProductHighlightRequest>, val focus: List<ProductHighlightRequest>, val salesDate: Long)