package com.gghouse.wardah.wardahba.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.attendant.AttendantHistoryActivity;
import com.gghouse.wardah.wardahba.activities.attendant.InputAttendantActivity;
import com.gghouse.wardah.wardahba.activities.customer.CustomerHistoryActivity;
import com.gghouse.wardah.wardahba.activities.customer.InputCustomerActivity;
import com.gghouse.wardah.wardahba.activities.questionnaire.QuestionerActivity;
import com.gghouse.wardah.wardahba.activities.sales.InputSalesActivity;
import com.gghouse.wardah.wardahba.activities.sales.InputSalesTargetActivity;
import com.gghouse.wardah.wardahba.activities.sales.SalesHistoryActivity;
import com.gghouse.wardah.wardahba.activities.sales.SalesTargetHistoryActivity;
import com.gghouse.wardah.wardahba.activities.store.StoreInputActivity;
import com.gghouse.wardah.wardahba.activities.store.StoreSalesHistoryActivity;
import com.gghouse.wardah.wardahba.activities.test.TestHistoryActivity;
import com.gghouse.wardah.wardahba.activities.test.TestTakingActivity;
import com.gghouse.wardah.wardahba.adapters.WardahSimpleAdapter;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.models.IntentProductHighlight;
import com.gghouse.wardah.wardahba.models.IntentProductType;
import com.gghouse.wardah.wardahba.models.IntentQuestions;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.viewmodels.WardahSimpleFragmentViewModel;
import com.gghouse.wardah.wardahba.webservices.model.Attendant;
import com.gghouse.wardah.wardahba.webservices.model.Customer;
import com.gghouse.wardah.wardahba.webservices.model.DailySales;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySales;
import com.gghouse.wardah.wardahba.webservices.model.Sales;
import com.gghouse.wardah.wardahba.webservices.model.SalesTarget;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

import mehdi.sakout.dynamicbox.DynamicBox;

public class WardahSimpleFragment extends Fragment implements View.OnClickListener, WardahListener {

    private static final String TAG = WardahSimpleFragment.class.getSimpleName();
    private static final String ARG_ITEM_TYPE = "ARG_ITEM_TYPE";
    private static final String ARG_CALENDAR_ID = "ARG_CALENDAR_ID";

    // Sales Target
    private static final int INPUT_SALES_TARGET_ACTIVITY_RESULT = 1;
    private static final int EDIT_SALES_TARGET_ACTIVITY_RESULT = 2;

    // Sales
    private static final int INPUT_SALES_ACTIVITY_RESULT = 11;
    private static final int EDIT_SALES_ACTIVITY_RESULT = 12;
    private static final int HISTORY_SALES_ACTIVITY_RESULT = 13;

    // Test
    private static final int INPUT_TEST_ACTIVITY_RESULT = 21;

    // Customer (Pelanggan)
    private static final int INPUT_CUSTOMER_ACTIVITY_RESULT = 31;
    private static final int EDIT_CUSTOMER_ACTIVITY_RESULT = 32;
    private static final int HISTORY_CUSTOMER_ACTIVITY_RESULT = 33;

    // Store Sales
    private static final int INPUT_DAILY_STORE_SALES_ACTIVITY_RESULT = 41;
    private static final int EDIT_DAILY_STORE_SALES_ACTIVITY_RESULT = 42;
    private static final int HISTORY_DAILY_STORE_SALES_ACTIVITY_RESULT = 43;
    private static final int INPUT_MONTHLY_STORE_SALES_ACTIVITY_RESULT = 44;
    private static final int EDIT_MONTHLY_STORE_SALES_ACTIVITY_RESULT = 45;
    private static final int HISTORY_MONTHLY_STORE_SALES_ACTIVITY_RESULT = 46;

    // Attendant
    private static final int INPUT_ATTENDANT_ACTIVITY_RESULT = 51;
    private static final int EDIT_ATTENDANT_ACTIVITY_RESULT = 52;

    // Questionnaire
    private static final int INPUT_QUESTIONNAIRE_ACTIVITY_RESULT = 61;

    private static final int INPUT_ACTIVITY_RESULT = 91;
    private static final int HISTORY_ACTIVITY_RESULT = 92;

    private WardahSimpleFragmentViewModel mModel;

    /**
     * Loading View
     */
    private LinearLayout mLoadingLinearLayout;

    /**
     * Views
     */

    private SwipeRefreshLayout mSwipeToRefreshLayout;
    private FrameLayout mInputFrameLayout;
    private ImageView mInputImageView;
    private Button mInputButton;

    private FrameLayout mDoneFrameLayout;
    private ImageView mDoneImageView;
    private TextView mDoneTextView;

    private FrameLayout mRunOutFrameLayout;
    private ImageView mRunOutImageView;
    private Button mRunOutButton;

    private TextView mMoreTextView;
    private ImageView mMoreImageView;

    private TextView mHistoryTextView;

    private RecyclerView mRecyclerView;
    private WardahSimpleAdapter mAdapter;

    /**
     * Empty Data View
     */
    private DynamicBox mDynamicBox;

    private WardahItemType mType;

    public WardahSimpleFragment() {
    }

    public static WardahSimpleFragment newInstance(WardahItemType type) {
        WardahSimpleFragment fragment = new WardahSimpleFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    public static WardahSimpleFragment newInstance(WardahItemType type, Long calendarId) {
        WardahSimpleFragment fragment = new WardahSimpleFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM_TYPE, type);
        args.putLong(ARG_CALENDAR_ID, calendarId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simple_wardah, container, false);

        mModel = ViewModelProviders.of(this).get(WardahSimpleFragmentViewModel.class);

        mType = (WardahItemType) getArguments().get(ARG_ITEM_TYPE);
        mModel.setCalendarId(getArguments().getLong(ARG_CALENDAR_ID));

        mLoadingLinearLayout = view.findViewById(R.id.loadingLinearLayout);

        // Views
        mSwipeToRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        mSwipeToRefreshLayout.setOnRefreshListener(() -> mModel.getData(mType));
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new WardahSimpleAdapter(getContext(), mType, new ArrayList<>(), this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);

        mInputFrameLayout = view.findViewById(R.id.inputFrameLayout);
        mInputImageView = view.findViewById(R.id.inputImageView);
        mInputButton = view.findViewById(R.id.inputButton);
        mInputButton.setOnClickListener(this);

        mDoneFrameLayout = view.findViewById(R.id.doneFrameLayout);
        mDoneImageView = view.findViewById(R.id.doneImageView);
        mDoneTextView = view.findViewById(R.id.doneTextView);

        mRunOutFrameLayout = view.findViewById(R.id.runOutFrameLayout);
        mRunOutImageView = view.findViewById(R.id.runOutImageView);
        mRunOutButton = view.findViewById(R.id.fillQuestionerButton);
        mRunOutButton.setOnClickListener(this);

        mHistoryTextView = view.findViewById(R.id.historyTextView);

        mMoreTextView = view.findViewById(R.id.moreTextView);
        mMoreImageView = view.findViewById(R.id.moreImageView);
        mMoreImageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
        mMoreTextView.setOnClickListener(this);
        mMoreImageView.setOnClickListener(this);

        // Empty Data View
        mDynamicBox = new DynamicBox(getActivity(), mRecyclerView);
        View emptyView = inflater.inflate(R.layout.dynamic_box_empty, container, false);
        mDynamicBox.addCustomView(emptyView, IntentUtil.Companion.getEMPTY_VIEW());

        setupViewByType(mType);

        // Observer
        mModel.getViewModeMutableLiveData().observe(this, ViewMode -> {
            if (ViewMode != null) {
                switch (ViewMode) {
                    case INPUT:
                        mInputFrameLayout.setVisibility(View.VISIBLE);
                        mRunOutFrameLayout.setVisibility(View.GONE);
                        mDoneFrameLayout.setVisibility(View.GONE);
                        break;
                    case VIEW:
                        mInputFrameLayout.setVisibility(View.GONE);
                        mRunOutFrameLayout.setVisibility(View.GONE);
                        mDoneFrameLayout.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        mModel.getDataSetMutableLiveData().observe(this, dataSet -> {
            mLoadingLinearLayout.setVisibility(View.GONE);
            mSwipeToRefreshLayout.setRefreshing(false);
            mDynamicBox.hideAll();

            if (dataSet != null) {
                if (dataSet.size() == 0) {
                    mDynamicBox.showCustomView(IntentUtil.Companion.getEMPTY_VIEW());
                }

                mRecyclerView.setAdapter(null);
                mAdapter.setData(dataSet);
                mRecyclerView.setAdapter(mAdapter);
            }
        });

        // Test
        mModel.getTestLatestDateMutableLiveData().observe(this, testLatestDate -> {
            if (testLatestDate != null) {
                if (testLatestDate.getNoLeftTest() != null && testLatestDate.getNoLeftTest() > 0) {
                    if (testLatestDate.getTestDate() != null) {
                        mInputFrameLayout.setVisibility(View.VISIBLE);
                        mRunOutFrameLayout.setVisibility(View.GONE);
                        mDoneFrameLayout.setVisibility(View.GONE);
                    } else {
                        mInputFrameLayout.setVisibility(View.GONE);
                        mRunOutFrameLayout.setVisibility(View.GONE);
                        mDoneFrameLayout.setVisibility(View.VISIBLE);
                    }
                } else {
                    mModel.callQuestionerLatestDate(SessionUtil.getUserId());
                }

            }
        });
        mModel.getQuestionerLatestDateMutableLiveData().observe(this, questionnaireLatestDate -> {
            if (questionnaireLatestDate != null &&
                    (DateUtils.isToday(questionnaireLatestDate) || new Date(questionnaireLatestDate).before(new Date()))) {
                mInputFrameLayout.setVisibility(View.GONE);
                mRunOutFrameLayout.setVisibility(View.VISIBLE);
                mDoneFrameLayout.setVisibility(View.GONE);
            } else {
                mInputFrameLayout.setVisibility(View.GONE);
                mRunOutFrameLayout.setVisibility(View.GONE);
                mDoneFrameLayout.setVisibility(View.VISIBLE);
            }
        });
        mModel.getTestQuestionListMutableLiveData().observe(this, testQuestion -> {
            mLoadingLinearLayout.setVisibility(View.GONE);
            mSwipeToRefreshLayout.setRefreshing(false);
            mDynamicBox.hideAll();

            if (testQuestion != null && testQuestion.getQuestions() != null) {
                if (testQuestion.getQuestions().size() > 0) {
                    Intent iTestTaking = new Intent(getContext(), TestTakingActivity.class);
                    iTestTaking.putExtra(IntentUtil.Companion.getDATE(), testQuestion.getTestDate());
                    iTestTaking.putExtra(IntentUtil.Companion.getDATA(), new IntentQuestions(testQuestion.getQuestions()));
                    startActivityForResult(iTestTaking, INPUT_TEST_ACTIVITY_RESULT);
                } else {
                    Intent iQuestioner = new Intent(getContext(), QuestionerActivity.class);
                    iQuestioner.putExtra(IntentUtil.Companion.getDATE(), mModel.getQuestionerLatestDateMutableLiveData().getValue());
                    startActivityForResult(iQuestioner, INPUT_QUESTIONNAIRE_ACTIVITY_RESULT);
                }
            }
        });

        // Sales
        mModel.getSalesInputProductsMutableLiveData().observe(this, salesInputProducts -> {
            if (salesInputProducts != null) {
                Intent iSalesInput = new Intent(getActivity(), InputSalesActivity.class);
                iSalesInput.putExtra(IntentUtil.Companion.getPRODUCT_HIGHLIGHT(), new IntentProductHighlight(salesInputProducts.getHighlight()));
                iSalesInput.putExtra(IntentUtil.Companion.getPRODUCT_FOCUS(), new IntentProductHighlight(salesInputProducts.getFocus()));
                iSalesInput.putExtra(IntentUtil.Companion.getDATE(), mModel.getLatestDate());
                iSalesInput.putExtra(IntentUtil.Companion.getLOCK(), false);
                iSalesInput.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
                startActivityForResult(iSalesInput, INPUT_SALES_ACTIVITY_RESULT);
            }
        });
        mModel.getSalesEditProductsMutableLiveData().observe(this, salesEditProducts -> {
            if (salesEditProducts != null) {
                Intent iSalesInput = new Intent(getContext(), InputSalesActivity.class);
                iSalesInput.putExtra(IntentUtil.Companion.getPRODUCT_HIGHLIGHT(), new IntentProductHighlight(salesEditProducts.getHighlight()));
                iSalesInput.putExtra(IntentUtil.Companion.getPRODUCT_FOCUS(), new IntentProductHighlight(salesEditProducts.getFocus()));
                iSalesInput.putExtra(IntentUtil.Companion.getDATE(), salesEditProducts.getSalesDate());
                iSalesInput.putExtra(IntentUtil.Companion.getLOCK(), false);
                iSalesInput.putExtra(IntentUtil.Companion.getSALES(), salesEditProducts);
                iSalesInput.putExtra(IntentUtil.Companion.getVIEW_MODE(), salesEditProducts.getEditable() ? ViewMode.EDIT : ViewMode.VIEW);
                startActivityForResult(iSalesInput, EDIT_SALES_ACTIVITY_RESULT);
            }
        });

        // Daily Sales
        mModel.getDailySalesViewMutableLiveData().observe(this, dailySalesView -> {
            if (dailySalesView != null) {
                Intent iInputDailyStoreSales = new Intent(getActivity(), StoreInputActivity.class);
                iInputDailyStoreSales.putExtra(IntentUtil.Companion.getDATA(), dailySalesView);
                iInputDailyStoreSales.putExtra(IntentUtil.Companion.getDATE(), dailySalesView.getInputDate());
                iInputDailyStoreSales.putExtra(IntentUtil.Companion.getTYPE(), mType);

                if (dailySalesView.getEditable()) {
                    iInputDailyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                    startActivityForResult(iInputDailyStoreSales, EDIT_DAILY_STORE_SALES_ACTIVITY_RESULT);
                } else {
                    iInputDailyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.VIEW);
                    startActivity(iInputDailyStoreSales);
                }
            }
        });

        // Monthly Sales
        mModel.getMonthlyProductTypeListMutableLiveData().observe(this, monthlyProductTypeList -> {
            if (monthlyProductTypeList != null) {
                Intent iInputMonthlyStoreSales = new Intent(getActivity(), StoreInputActivity.class);
                iInputMonthlyStoreSales.putExtra(IntentUtil.Companion.getDATE(), DateTimeUtil.INSTANCE.getDateFromMonthYear(mModel.getMonth(), mModel.getYear()).getTime());
                iInputMonthlyStoreSales.putExtra(IntentUtil.Companion.getPRODUCT_TYPE(), new IntentProductType(monthlyProductTypeList));
                iInputMonthlyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
                iInputMonthlyStoreSales.putExtra(IntentUtil.Companion.getTYPE(), mType);
                startActivityForResult(iInputMonthlyStoreSales, INPUT_MONTHLY_STORE_SALES_ACTIVITY_RESULT);
            }
        });
        mModel.getMonthlySalesViewMutableLiveData().observe(this, monthlySalesView -> {
            if (monthlySalesView != null) {
                Intent iEditMonthlyStoreSales = new Intent(getActivity(), StoreInputActivity.class);
                iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getDATA(), monthlySalesView);
                iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getDATE(), DateTimeUtil.INSTANCE.getDateFromMonthYear(monthlySalesView.getMonth(), monthlySalesView.getYear()).getTime());
                iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getTYPE(), mType);

                if (monthlySalesView.getEditable()) {
                    iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                    startActivityForResult(iEditMonthlyStoreSales, EDIT_MONTHLY_STORE_SALES_ACTIVITY_RESULT);
                } else {
                    iEditMonthlyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.VIEW);
                    startActivity(iEditMonthlyStoreSales);
                }
            }
        });

        // Attendant
        mModel.getAttendantViewMutableLiveData().observe(this, attendantView -> {
            if (attendantView != null) {
                Intent iInputAttendant = new Intent(getContext(), InputAttendantActivity.class);
                iInputAttendant.putExtra(IntentUtil.Companion.getATTENDANT(), attendantView);
                iInputAttendant.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                startActivityForResult(iInputAttendant, EDIT_ATTENDANT_ACTIVITY_RESULT);
            }
        });

        // Sales Target
        mModel.getSalesTargetMutableLiveData().observe(this, salesTarget -> {
            if (salesTarget != null) {
                Intent iInputSalesTarget = new Intent(getContext(), InputSalesTargetActivity.class);
                iInputSalesTarget.putExtra(IntentUtil.Companion.getINPUT_TARGET(), salesTarget);

                if (salesTarget.getEditable()) {
                    iInputSalesTarget.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                    startActivityForResult(iInputSalesTarget, EDIT_SALES_TARGET_ACTIVITY_RESULT);
                } else {
                    iInputSalesTarget.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.VIEW);
                    startActivity(iInputSalesTarget);
                }
            }
        });

        mLoadingLinearLayout.setVisibility(View.VISIBLE);
        mModel.getData(mType);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode) {
            case Activity.RESULT_OK:
                mModel.getData(mType);

                if (getActivity() != null && getActivity().findViewById(R.id.main_content) != null) {

                    switch (requestCode) {
                        case INPUT_SALES_TARGET_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_create_sales_target_success, Snackbar.LENGTH_LONG).show();
                            break;
                        case EDIT_SALES_TARGET_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_edit_sales_target_success, Snackbar.LENGTH_LONG).show();
                            break;

                        case INPUT_SALES_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_create_sales_success, Snackbar.LENGTH_LONG).show();
                            break;
                        case EDIT_SALES_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_edit_sales_success, Snackbar.LENGTH_LONG).show();
                            break;

                        case INPUT_TEST_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_create_test_success, Snackbar.LENGTH_LONG).show();
                            break;

                        case INPUT_QUESTIONNAIRE_ACTIVITY_RESULT:
                            break;

                        case INPUT_CUSTOMER_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_create_customer_success, Snackbar.LENGTH_LONG).show();
                            break;
                        case EDIT_CUSTOMER_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_edit_customer_success, Snackbar.LENGTH_LONG).show();
                            break;

                        case INPUT_DAILY_STORE_SALES_ACTIVITY_RESULT:
                        case INPUT_MONTHLY_STORE_SALES_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_create_store_sales_success, Snackbar.LENGTH_LONG).show();
                            break;
                        case EDIT_DAILY_STORE_SALES_ACTIVITY_RESULT:
                        case EDIT_MONTHLY_STORE_SALES_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_edit_store_sales_success, Snackbar.LENGTH_LONG).show();
                            break;

                        case INPUT_ATTENDANT_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_create_attendant_success, Snackbar.LENGTH_LONG).show();
                            break;

                        case EDIT_ATTENDANT_ACTIVITY_RESULT:
                            Snackbar.make(getActivity().findViewById(R.id.main_content), R.string.message_edit_attendant_success, Snackbar.LENGTH_LONG).show();
                            break;
                    }
                }

                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.moreTextView:
            case R.id.moreImageView:
                switch (mType) {
                    case TEST:
                        Intent iTestHistory = new Intent(getContext(), TestHistoryActivity.class);
                        startActivityForResult(iTestHistory, HISTORY_ACTIVITY_RESULT);
                        break;
                    case STORE_DAILY_SALES:
                        Intent iDailyStoreSales = new Intent(getContext(), StoreSalesHistoryActivity.class);
                        iDailyStoreSales.putExtra(IntentUtil.Companion.getFREQUENT(), mType);
                        iDailyStoreSales.putExtra(IntentUtil.Companion.getLOCATION_ID(), SessionUtil.getUserLocationId());
                        startActivityForResult(iDailyStoreSales, HISTORY_DAILY_STORE_SALES_ACTIVITY_RESULT);
                        break;
                    case STORE_MONTHLY_SALES:
                        Intent iMonthlyStoreSales = new Intent(getContext(), StoreSalesHistoryActivity.class);
                        iMonthlyStoreSales.putExtra(IntentUtil.Companion.getFREQUENT(), mType);
                        iMonthlyStoreSales.putExtra(IntentUtil.Companion.getLOCATION_ID(), SessionUtil.getUserLocationId());
                        startActivityForResult(iMonthlyStoreSales, HISTORY_MONTHLY_STORE_SALES_ACTIVITY_RESULT);
                        break;
                    case SALES:
                        Intent iSalesHistoryP2 = new Intent(getContext(), SalesHistoryActivity.class);
                        startActivityForResult(iSalesHistoryP2, HISTORY_SALES_ACTIVITY_RESULT);
                        break;
                    case CUSTOMER:
                        Intent iCustomerHistory = new Intent(getContext(), CustomerHistoryActivity.class);
                        startActivityForResult(iCustomerHistory, HISTORY_CUSTOMER_ACTIVITY_RESULT);
                        break;
                    case ATTENDANT:
                        Intent iAttendantHistory = new Intent(getContext(), AttendantHistoryActivity.class);
                        iAttendantHistory.putExtra(IntentUtil.Companion.getCALENDAR_ID(), mModel.getCalendarId());
                        startActivityForResult(iAttendantHistory, HISTORY_ACTIVITY_RESULT);
                        break;
                    case INPUT_TARGET:
                        Intent iInputTargetHistory = new Intent(getContext(), SalesTargetHistoryActivity.class);
                        startActivityForResult(iInputTargetHistory, HISTORY_ACTIVITY_RESULT);
                        break;
                }
                break;
            case R.id.inputButton:
                if (SessionUtil.validateLoginSession()) {
                    switch (mType) {
                        case TEST:
                            mModel.callQuestionData(SessionUtil.getUserId());
                            break;
                        case STORE_DAILY_SALES:
                            Intent iInputDailyStoreSales = new Intent(getActivity(), StoreInputActivity.class);
                            iInputDailyStoreSales.putExtra(IntentUtil.Companion.getDATE(), mModel.getLatestDate());
                            iInputDailyStoreSales.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
                            iInputDailyStoreSales.putExtra(IntentUtil.Companion.getTYPE(), mType);
                            startActivityForResult(iInputDailyStoreSales, INPUT_DAILY_STORE_SALES_ACTIVITY_RESULT);
                            break;
                        case STORE_MONTHLY_SALES:
                            mModel.callMonthlySalesCreate(SessionUtil.getUserLocationId());
                            break;
                        case SALES:
                            mModel.callSalesInputProducts(SessionUtil.getUserLocationId());
                            break;
                        case CUSTOMER:
                            Intent iPelangganInput = new Intent(getContext(), InputCustomerActivity.class);
                            iPelangganInput.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
                            startActivityForResult(iPelangganInput, INPUT_CUSTOMER_ACTIVITY_RESULT);
                            break;
                        case ATTENDANT:
                            Intent iAttendantCreate = new Intent(getActivity(), InputAttendantActivity.class);
                            iAttendantCreate.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
                            iAttendantCreate.putExtra(IntentUtil.Companion.getCALENDAR_ID(), mModel.getCalendarId());
                            startActivityForResult(iAttendantCreate, INPUT_ATTENDANT_ACTIVITY_RESULT);
                            break;
                        case INPUT_TARGET:
                            Intent iInputSalesTarget = new Intent(getActivity(), InputSalesTargetActivity.class);
                            iInputSalesTarget.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
                            startActivityForResult(iInputSalesTarget, INPUT_SALES_TARGET_ACTIVITY_RESULT);
                            break;
                    }
                    break;
                }
            case R.id.fillQuestionerButton:
                mModel.callQuestionData(SessionUtil.getUserId());
                break;
        }
    }

    @Override
    public void onClick(WardahHistoryRecyclerViewItem item, ViewMode viewMode) {
        switch (mType) {
            case STORE_DAILY_SALES:
                DailySales dailySales = (DailySales) item;
                mModel.callDailySalesView(dailySales.getDailyReportId());
                break;
            case STORE_MONTHLY_SALES:
                MonthlySales monthlySales = (MonthlySales) item;
                mModel.callMonthlySalesView(monthlySales.getMonthlyReportId());
                break;
            case SALES:
                Sales sales = (Sales) item;
                mModel.callSalesEditProducts(sales.getId());
                break;
            case CUSTOMER:
                Customer customer = (Customer) item;
                Intent iInputCustomer = new Intent(getContext(), InputCustomerActivity.class);
                iInputCustomer.putExtra(IntentUtil.Companion.getCUSTOMER(), customer);
                iInputCustomer.putExtra(IntentUtil.Companion.getVIEW_MODE(), viewMode);

                switch (viewMode) {
                    case EDIT:
                        startActivityForResult(iInputCustomer, EDIT_CUSTOMER_ACTIVITY_RESULT);
                        break;
                    case VIEW:
                        startActivity(iInputCustomer);
                        break;
                }
                break;
            case ATTENDANT:
                Attendant attendant = (Attendant) item;
                mModel.callAttendantView(attendant.getAttendantId());
                break;
            case INPUT_TARGET:
                SalesTarget salesTarget = (SalesTarget) item;
                mModel.callSalesTargetView(salesTarget.getSalesTargetId(), salesTarget.getEditable());
                break;
        }
    }

    private void setupViewByType(WardahItemType type) {
        int inputTextResId = R.string.action_input_store_sales;
        int doneTextResId = R.string.message_store_sales_input_today_done;
        int runOutTextResId = R.string.action_fill_questioner;
        int historyTextResId = R.string.title_store_sales_history;
        int inputImageResId = R.drawable.pic_sales;
        int doneImageResId = R.drawable.pic_sales;
        int runOutImageResId = R.drawable.pic_sales;

        switch (type) {
            case TEST:
                inputTextResId = R.string.action_start_test;
                doneTextResId = R.string.message_test_has_taken;
                runOutTextResId = R.string.action_fill_questioner;
                historyTextResId = R.string.title_test_history;
                inputImageResId = ImageUtil.INSTANCE.getTest();
                doneImageResId = ImageUtil.INSTANCE.getTest();
                runOutImageResId = ImageUtil.INSTANCE.getQuestionnaire();
                break;
            case STORE_DAILY_SALES:
                inputTextResId = R.string.action_input_store_harian;
                doneTextResId = R.string.message_store_sales_input_today_done;
                historyTextResId = R.string.title_store_sales_history;
                inputImageResId = ImageUtil.INSTANCE.getDailyStore();
                doneImageResId = ImageUtil.INSTANCE.getDailyStore();
                break;
            case STORE_MONTHLY_SALES:
                inputTextResId = R.string.action_input_store_bulanan;
                doneTextResId = R.string.message_store_sales_input_today_done;
                historyTextResId = R.string.title_store_sales_history;
                inputImageResId = ImageUtil.INSTANCE.getMonthlyStore();
                doneImageResId = ImageUtil.INSTANCE.getMonthlyStore();
                break;
            case SALES:
                inputTextResId = R.string.action_input_sales;
                doneTextResId = R.string.message_sales_input_today_done;
                historyTextResId = R.string.title_sales_history;
                inputImageResId = ImageUtil.INSTANCE.getSales();
                doneImageResId = ImageUtil.INSTANCE.getSales();
                break;
            case CUSTOMER:
                inputTextResId = R.string.action_input_customer;
                inputImageResId = ImageUtil.INSTANCE.getCustomer();
                historyTextResId = R.string.title_riwayat_pelanggan;
                break;
            case ATTENDANT:
                inputTextResId = R.string.action_input_attendant;
                historyTextResId = R.string.title_daftar_peserta;
                break;
            case INPUT_TARGET:
                inputTextResId = R.string.action_input_target;
                historyTextResId = R.string.title_riwayat_sales_target;
                break;
        }

        mInputButton.setText(inputTextResId);
        mRunOutButton.setText(runOutTextResId);
        mDoneTextView.setText(doneTextResId);
        mHistoryTextView.setText(historyTextResId);
        Picasso.get()
                .load(inputImageResId)
                .fit()
                .centerCrop()
                .into(mInputImageView);

        Picasso.get()
                .load(doneImageResId)
                .fit()
                .centerCrop()
                .into(mDoneImageView);

        Picasso.get()
                .load(runOutImageResId)
                .fit()
                .centerInside()
                .into(mRunOutImageView);
    }
}