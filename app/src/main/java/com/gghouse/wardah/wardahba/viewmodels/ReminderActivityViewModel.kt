package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.gghouse.wardah.wardahba.enumerations.ReminderEnum

class ReminderActivityViewModel : ViewModel() {
    val reminderMutableLiveData: MutableLiveData<ReminderEnum> by lazy {
        MutableLiveData<ReminderEnum>()
    }
}
