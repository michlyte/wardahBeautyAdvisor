package com.gghouse.wardah.wardahba.enumerations;

@Deprecated
public enum StoreHomepageSection {
    SALES,
    SALES_TARGET,
    OMSET_KOMPETITOR,
    PEMBELI,
    PENGUNJUNG,
    BUYING_POWER_1,
    BUYING_POWER_2,
    BUYING_POWER_3,
    BUYING_POWER_4,
    BUYING_POWER_5
}
