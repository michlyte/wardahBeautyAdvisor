package com.gghouse.wardah.wardahba.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.SalesSummaryListener;
import com.gghouse.wardah.wardahba.viewholders.ProductHighlightViewHolder;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;

import java.util.List;

/**
 * Created by michaelhalim on 2/26/17.
 */

public class SalesInputAdapter extends RecyclerView.Adapter<ProductHighlightViewHolder> {

    private final Context mContext;
    private final List<ProductHighlight> mValues;
    private WardahItemType mType;
    private SalesSummaryListener mListener;
    private ViewMode mViewMode;

    public SalesInputAdapter(Context context, WardahItemType type, List<ProductHighlight> items, SalesSummaryListener salesSummaryListener, ViewMode viewMode) {
        mContext = context;
        mValues = items;
        mListener = salesSummaryListener;
        mViewMode = viewMode;
        mType = type;
    }

    @Override
    public ProductHighlightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_product_highlight, parent, false);
        return new ProductHighlightViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductHighlightViewHolder holder, int position) {
        holder.setData(mContext, mValues.get(position), mViewMode);

        final int curPosition = position;
        holder.mBAdd.setOnClickListener(v -> {
            Integer qty = Integer.parseInt(holder.mETQty.getText().toString());
            holder.mETQty.setText(String.valueOf(++qty));
            mValues.get(curPosition).setQuantity(qty);

            if (mListener != null)
                mListener.onAdd(mType, holder.mItem);
        });

        holder.mBRemove.setOnClickListener(v -> {
            Integer qty = Integer.parseInt(holder.mETQty.getText().toString());
            if (qty > 0) {
                holder.mETQty.setText(String.valueOf(--qty));
                mValues.get(curPosition).setQuantity(qty);
            }

            if (mListener != null)
                mListener.onRemove(mType, holder.mItem);
        });
    }

    @Override
    public int getItemCount() {
        return mValues == null ? 0 : mValues.size();
    }

    public List<ProductHighlight> getData() {
        return mValues;
    }
}
