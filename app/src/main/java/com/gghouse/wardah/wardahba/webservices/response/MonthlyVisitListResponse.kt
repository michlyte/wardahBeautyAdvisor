package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.FCMonthlyVisit

class MonthlyVisitListResponse : GenericResponse() {
    var data: List<FCMonthlyVisit>? = null
}
