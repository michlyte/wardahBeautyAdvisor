package com.gghouse.wardah.wardahba.common

import com.gghouse.wardah.wardahba.enumerations.ModeEnum

/**
 * Created by michaelhalim on 1/24/17.
 */

interface Config {
    companion object {
        /**
         * Development Mode
         */
        val mode = ModeEnum.DEVELOPMENT

        /**
         * Host
         */
        //    String BASE_URL = "http://10.0.2.2:8081";
        val BASE_URL = "http://35.240.147.170:8081"
        val PROD_URL = "http://inspiring.wardahbeauty.com:8081"

        /**
         * Web Service Related
         */
        val CONNECT_TIMEOUT = 30
        val READ_TIMEOUT = 30
        const val CODE_99 = 99
        const val CODE_200 = 200
        const val CODE_211 = 211
        const val CODE_401 = 401
        val notificationSort = "periodEnd,DESC"

        /**
         * Log
         */
        val LOG_ENABLE = true

        /**
         * Page
         */
        val SPLASH_SCREEN_DELAY = 3000
        val NOTIF_ITEM_PER_PAGE = 3
        val HISTORY_ITEM_PER_PAGE = 20
        val MAX_LOAD_MORE_FOR_DUMMY_DATA = 2

        val DELAY_LOAD_MORE = 1000 //ms
    }
}
