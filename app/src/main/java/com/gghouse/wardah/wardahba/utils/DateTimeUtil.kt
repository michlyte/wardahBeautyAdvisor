package com.gghouse.wardah.wardahba.utils

import android.content.Intent
import android.text.format.DateUtils
import com.gghouse.wardah.wardahba.models.Reminder
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*


object DateTimeUtil {
    val locale = Locale("in", "ID")
    val sdfDate = SimpleDateFormat("dd", locale)
    val sdfDateMonthYear = SimpleDateFormat("dd MMMM yyyy", locale)
    val sdfMonthCommaYear = SimpleDateFormat("MMMM, yyyy", locale)
    val sdfMonthYear = SimpleDateFormat("MMMM yyyy", locale)
    val sdfDay = SimpleDateFormat("EEEE", locale)
    val sdfFilter = SimpleDateFormat("dd/MM/yyyy", locale)
    val sdfMonthFilter = SimpleDateFormat("MM/yyyy", locale)
    val sdfDayCommaDateMonthYear = SimpleDateFormat("EEEE, dd MMMM yyyy", locale)
    val sdfEventTime = SimpleDateFormat("hh:mm a", locale)
    val sdfEventCheckInOutTime = SimpleDateFormat("dd/MM/yy hh:mm a", locale)
    val sdfEventStartEndTime = SimpleDateFormat("EEE, dd MMM yyyy HH:mm", locale)

    // Event
    fun getEventDateStr(startTime: Long?): String {
        return sdfDateMonthYear.format(startTime)
    }

    fun getEventTimeStr(startTime: Long?, endTime: Long?): String {
        return sdfEventTime.format(startTime) + "-" + sdfEventTime.format(endTime)
    }

    fun getEventAlarmStr(alarmTime: Long?): String {
        return sdfEventStartEndTime.format(alarmTime)
    }

    fun getEventCheckInStr(checkInTime: Long?): String {
        return sdfEventCheckInOutTime.format(checkInTime)
    }

    fun getDateFromMonthYear(month: Int, year: Int): Date {
        val calendar: Calendar = Calendar.getInstance()
        calendar.clear()
        calendar.set(Calendar.MONTH, month - 1)
        calendar.set(Calendar.YEAR, year)
        return calendar.time
    }

    fun getMonthFromDate(date: Date): Int {
        val calendar: Calendar = Calendar.getInstance()
        calendar.time = date
        return calendar.get(Calendar.MONTH) + 1
    }

    fun getYearFromDate(date: Date): Int {
        val calendar: Calendar = Calendar.getInstance()
        calendar.time = date
        return calendar.get(Calendar.YEAR)
    }

    fun getDateFromParam(intent: Intent, param: String): Date {
        val `object` = intent.extras!!.get(param)
        return if (`object` == null) {
            Date()
        } else {
            Date(`object` as Long)
        }
    }

    fun getAlarmTime(date: Date, reminder: Reminder): Date? {
        return if (reminder.frequent == null && reminder.value == null) {
            null
        } else if (reminder.frequent == 0 && reminder.value == 0) {
            date
        } else {
            val calendar: Calendar = Calendar.getInstance()
            calendar.time = date
            calendar.add(reminder.frequent!!, reminder.value!!)
            calendar.time
        }
    }

    fun isWeekend(time: Long?): Boolean {
        val calendar = Calendar.getInstance()

        calendar.timeInMillis = time!!
        when (calendar.get(Calendar.DAY_OF_WEEK)) {
            Calendar.SATURDAY, Calendar.SUNDAY -> return true
            else -> return false
        }
    }

    fun getNextMonth(numberOfMonth: Int): Date {
        val cal = Calendar.getInstance()
        cal.time = Date()
        cal.add(Calendar.MONTH, numberOfMonth)
        return cal.time
    }

    fun getPreviousMonth(numberOfMonth: Int): Date {
        val cal = Calendar.getInstance()
        cal.time = Date()
        cal.add(Calendar.MONTH, -numberOfMonth)
        return cal.time
    }

    fun getNextDay(numberOfDay: Int): Date {
        val cal = Calendar.getInstance()
        cal.time = Date()
        cal.add(Calendar.DAY_OF_WEEK, numberOfDay)
        return cal.time
    }

    fun getPreviousDay(numberOfDay: Int): Date {
        val cal = Calendar.getInstance()
        cal.time = Date()
        cal.add(Calendar.DAY_OF_WEEK, -numberOfDay)
        return cal.time
    }

    fun isSameDay(date1: Date, date2: Date): Boolean {
        val fmt = SimpleDateFormat("yyyyMMdd")
        return fmt.format(date1) == fmt.format(date2)
    }

    fun isTomorrow(d: Date): Boolean {
        return DateUtils.isToday(d.time - DateUtils.DAY_IN_MILLIS)
    }

    fun isTodayOrTomorrow(d: Date): Boolean {
        return DateUtils.isToday(d.time) || isTomorrow(d)
    }

    fun isActiveEvent(startTime: Long?, endTime: Long?): Boolean {
        return ((startTime != null && (Date(startTime).after(Date()) || DateUtils.isToday(startTime)))
                || (endTime != null && (Date(endTime).after(Date()) || DateUtils.isToday(endTime))))
    }

    fun isFutureEvent(startTime: Long?, endTime: Long?): Boolean {
        return ((startTime != null && (Date(startTime).after(Date())))
                || (endTime != null && (Date(endTime).after(Date()))))
    }

    fun isTodayEvent(startTime: Long?, endTime: Long?): Boolean {
        return ((startTime != null && DateUtils.isToday(startTime)) || (endTime != null && DateUtils.isToday(endTime)))
    }

    fun getMonth(month: Int): String {
        return DateFormatSymbols().getMonths()[month - 1]
    }

    fun createNotificationID(): Int {
        val now = Date()
        return Integer.parseInt(SimpleDateFormat("ddHHmmss", locale).format(now))
    }
}