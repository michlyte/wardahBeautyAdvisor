package com.gghouse.wardah.wardahba.activities.event

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.enumerations.ReminderEnum
import com.gghouse.wardah.wardahba.interfaces.CheckedListener
import com.gghouse.wardah.wardahba.models.Reminder
import com.gghouse.wardah.wardahba.utils.IntentUtil
import com.gghouse.wardah.wardahba.viewmodels.ReminderActivityViewModel
import kotlinx.android.synthetic.main.activity_reminder.*

class ReminderActivity : AppCompatActivity(), CheckedListener, RadioGroup.OnCheckedChangeListener {

    private val TAG = ReminderActivity::class.java.simpleName

    private lateinit var mModel: ReminderActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reminder)

        mModel = ViewModelProviders.of(this).get(ReminderActivityViewModel::class.java)

        if (intent != null) {
            val reminderEnum = intent.getSerializableExtra(IntentUtil.REMINDER)
            if (reminderEnum != null) {
                mModel.reminderMutableLiveData.postValue(reminderEnum as ReminderEnum)
            } else {
                mModel.reminderMutableLiveData.postValue(ReminderEnum.NO_REMINDER)
            }
        }

        reminderRadioGroup.setOnCheckedChangeListener(this)

        mModel.reminderMutableLiveData.observe(this, Observer<ReminderEnum> { reminderEnum ->
            if (reminderEnum != null) {
                when (reminderEnum) {
                    ReminderEnum.NO_REMINDER -> noAlarmRadioButton.isChecked = true
                    ReminderEnum.ON_TIME -> onTimeRadioButton.isChecked = true
                    ReminderEnum.FIVE_MINUTES -> fiveMinutesBeforeRadioButton.isChecked = true
                    ReminderEnum.TEN_MINUTES -> tenMinutesBeforeRadioButton.isChecked = true
                    ReminderEnum.FIFTEEN_MINUTES -> fifteenMinutesBeforeRadioButton.isChecked = true
                    ReminderEnum.THIRTY_MINUTES -> thirtyMinutesBeforeRadioButton.isChecked = true
                    ReminderEnum.ONE_HOUR -> oneHourBeforeRadioButton.isChecked = true
                    ReminderEnum.ONE_DAY -> oneDayBeforeRadioButton.isChecked = true
                }
            }
        })
    }

    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
        when (p1) {
            noAlarmRadioButton.id -> mModel.reminderMutableLiveData.postValue(ReminderEnum.NO_REMINDER)
            onTimeRadioButton.id -> mModel.reminderMutableLiveData.postValue(ReminderEnum.ON_TIME)
            fiveMinutesBeforeRadioButton.id -> mModel.reminderMutableLiveData.postValue(ReminderEnum.FIVE_MINUTES)
            tenMinutesBeforeRadioButton.id -> mModel.reminderMutableLiveData.postValue(ReminderEnum.TEN_MINUTES)
            fifteenMinutesBeforeRadioButton.id -> mModel.reminderMutableLiveData.postValue(ReminderEnum.FIFTEEN_MINUTES)
            thirtyMinutesBeforeRadioButton.id -> mModel.reminderMutableLiveData.postValue(ReminderEnum.THIRTY_MINUTES)
            oneHourBeforeRadioButton.id -> mModel.reminderMutableLiveData.postValue(ReminderEnum.ONE_HOUR)
            oneDayBeforeRadioButton.id -> mModel.reminderMutableLiveData.postValue(ReminderEnum.ONE_DAY)
        }
    }

    override fun onChecked(reminder: Reminder) {

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_reminder, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val data = Intent()
                data.putExtra(IntentUtil.REMINDER, mModel.reminderMutableLiveData.value)
                setResult(Activity.RESULT_OK, data)
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // Method to show an alert dialog with yes, no and cancel button
//    private fun showDialog() {
//        // Late initialize an alert dialog object
//        lateinit var dialog: AlertDialog
//        // Initialize a new instance of alert dialog builder object
//        val builder = AlertDialog.Builder(this)
//        // Set a title for alert dialog
//        builder.setTitle(getResources().getString(R.string.text_tambah_notifikasi))
//        // Set a message for alert dialog
////        builder.setMessage("This is a sample message of AlertDialog.")
//
//        val inflater = layoutInflater
//        val dialogLayout = inflater.inflate(R.layout.dialog_custom_reminder, null)
//        val mTime = dialogLayout.findViewById<EditText>(R.id.etTime)
//        val mType = dialogLayout.findViewById<Spinner>(R.id.spType)
//
//        // add spinner into reminder type
//        val adapter = ArrayAdapter<String>(applicationContext, android.R.layout.simple_spinner_dropdown_item, EventDummy.eventReminderType)
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        mType.setAdapter(adapter)
//        mTime.setText("1")
//        builder.setView(dialogLayout)
//
//        // On click listener for dialog buttons
//        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
//            when (which) {
//                DialogInterface.BUTTON_NEGATIVE -> {
//
//                }
//                DialogInterface.BUTTON_POSITIVE -> {
//                    reminderSelectedCustom = mTime.text.toString() + " " + mType.selectedItem.toString()
//                    toast(reminderSelectedCustom)
//                    setResult()
//                    super.onBackPressed()
//
//                }
//            }
//        }
//
//        // Set the alert dialog positive/yes button
//        builder.setPositiveButton(getResources().getString(R.string.text_tambah_notifikasi), dialogClickListener)
//        // Set the alert dialog negative/no button
//        builder.setNegativeButton("CANCEL", dialogClickListener)
//
//        // Initialize the AlertDialog using builder object
//        dialog = builder.create()
//        // Finally, display the alert dialog
//        dialog.show()
//    }
}


