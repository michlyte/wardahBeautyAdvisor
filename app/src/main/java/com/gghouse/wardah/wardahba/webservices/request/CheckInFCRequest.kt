package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.CheckInLocation

class CheckInFCRequest {
    val userId: Long
    val calendarId: Long
    val checkInLocation: CheckInLocation
    val fcImage: String

    constructor(userId: Long, calendarId: Long, checkInLocation: CheckInLocation, fcImage: String) {
        this.userId = userId
        this.calendarId = calendarId
        this.checkInLocation = checkInLocation
        this.fcImage = fcImage
    }
}