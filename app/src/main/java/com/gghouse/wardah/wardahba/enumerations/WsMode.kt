package com.gghouse.wardah.wardahba.enumerations

enum class WsMode {
    REFRESH,
    LOAD_MORE
}
