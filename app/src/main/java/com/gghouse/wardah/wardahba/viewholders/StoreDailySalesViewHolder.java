package com.gghouse.wardah.wardahba.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.webservices.model.DailySales;

import java.util.Calendar;

/**
 * Created by michaelhalim on 5/9/17.
 */

public class StoreDailySalesViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    private final CardView mContainerCardView;
    private final LinearLayout mContainerLinearLayout;
    public final TextView mTitleTextView;
    private final TextView mDateTextView;
    private final TextView mPcsTextView;
    private final TextView mVisitorBuyerTextView;
    private final TextView mBuyingPowerFirstLineTextView;
    private final TextView mBuyingPowerSecondLineTextView;

    public DailySales mItem;

    private Calendar mCalendar = Calendar.getInstance();

    public StoreDailySalesViewHolder(View view) {
        super(view);
        mView = view;
        mContainerCardView = mView.findViewById(R.id.cv);
        mContainerLinearLayout = mView.findViewById(R.id.ll_container);
        mTitleTextView = mView.findViewById(R.id.tv_title);
        mDateTextView = mView.findViewById(R.id.tv_date);
        mPcsTextView = mView.findViewById(R.id.tvPcs);
        mVisitorBuyerTextView = mView.findViewById(R.id.tvPengunjungPembeli);
        mBuyingPowerFirstLineTextView = mView.findViewById(R.id.tvBuyingPowerFirstLine);
        mBuyingPowerSecondLineTextView = mView.findViewById(R.id.tvBuyingPowerSecondLine);
    }

    private void setupWeekendView(Context context) {
        mCalendar.setTimeInMillis(mItem.getSalesDate());
        switch (mCalendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.SATURDAY:
            case Calendar.SUNDAY:
                mTitleTextView.setTextColor(ContextCompat.getColor(context, R.color.colorHistoryHeader6));
                break;
            default:
                mTitleTextView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;
        }
    }

    public void setupRowView(Context context) {
        if (mItem.getEditable()) {
            mContainerCardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

            int color = ContextCompat.getColor(context, android.R.color.white);
            mTitleTextView.setTextColor(color);
            mDateTextView.setTextColor(color);
            mPcsTextView.setTextColor(color);
            mVisitorBuyerTextView.setTextColor(color);
            mBuyingPowerFirstLineTextView.setTextColor(color);
            mBuyingPowerSecondLineTextView.setTextColor(color);
        } else {
            mContainerCardView.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.white));

            int primaryColor = ContextCompat.getColor(context, R.color.colorPrimary);
            int accentColor = ContextCompat.getColor(context, R.color.colorAccent);
            int blackColor = ContextCompat.getColor(context, android.R.color.black);
            mDateTextView.setTextColor(blackColor);
            mPcsTextView.setTextColor(accentColor);
            mVisitorBuyerTextView.setTextColor(accentColor);
            mBuyingPowerFirstLineTextView.setTextColor(accentColor);
            mBuyingPowerSecondLineTextView.setTextColor(accentColor);
        }
    }

    private void setupActionEvent(WardahListener wardahListener) {
        ViewMode viewMode;
        if (mItem.getEditable()) {
            viewMode = ViewMode.EDIT;
        } else {
            viewMode = ViewMode.VIEW;
        }

        mContainerLinearLayout.setOnClickListener(v -> wardahListener.onClick(mItem, viewMode));
    }

    public void setData(Context context, DailySales dailySales, WardahListener wardahListener) {
        mItem = dailySales;
        String day = DateTimeUtil.INSTANCE.getSdfDay().format(dailySales.getSalesDate());
        String date = DateTimeUtil.INSTANCE.getSdfDateMonthYear().format(dailySales.getSalesDate());
        mTitleTextView.setText(day);
        mDateTextView.setText(date);
        mPcsTextView.setText(WardahUtil.INSTANCE.getStoreSalesPcsAndAmountContent(dailySales.getQuantity(), dailySales.getAmount()));
        mVisitorBuyerTextView.setText(WardahUtil.INSTANCE.getStoreSalesVisitorAndBuyerContent(dailySales.getVisitor(), dailySales.getBuyer()));
        mBuyingPowerFirstLineTextView.setText(dailySales.getBuyingPower().getBuyingPowerFirstLineStr());
        mBuyingPowerSecondLineTextView.setText(dailySales.getBuyingPower().getBuyingPowerSecondLineStr());

        setupWeekendView(context);
        setupRowView(context);
        setupActionEvent(wardahListener);
    }
}
