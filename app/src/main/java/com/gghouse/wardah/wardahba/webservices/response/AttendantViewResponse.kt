package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.AttendantView

class AttendantViewResponse : GenericResponse() {
    var data: AttendantView? = null
}
