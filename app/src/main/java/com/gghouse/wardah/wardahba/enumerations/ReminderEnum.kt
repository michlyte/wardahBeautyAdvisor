package com.gghouse.wardah.wardahba.enumerations

import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.WardahApp
import java.util.*

enum class ReminderEnum(private val calendarConst: Int?, private val duration: Int?, val text: String) {
    NO_REMINDER(null, null, WardahApp.getInstance().getString(R.string.reminder_empty)),
    ON_TIME(Calendar.MINUTE, 0, WardahApp.getInstance().getString(R.string.reminder_on_time)),
    FIVE_MINUTES(Calendar.MINUTE, -5, WardahApp.getInstance().getString(R.string.reminder_5_minutes_before)),
    TEN_MINUTES(Calendar.MINUTE, -10, WardahApp.getInstance().getString(R.string.reminder_10_minutes_before)),
    FIFTEEN_MINUTES(Calendar.MINUTE, -15, WardahApp.getInstance().getString(R.string.reminder_15_minutes_before)),
    THIRTY_MINUTES(Calendar.MINUTE, -30, WardahApp.getInstance().getString(R.string.reminder_30_minutes_before)),
    ONE_HOUR(Calendar.HOUR, -1, WardahApp.getInstance().getString(R.string.reminder_1_hour_before)),
    ONE_DAY(Calendar.DAY_OF_MONTH, -1, WardahApp.getInstance().getString(R.string.reminder_1_day_before));

    companion object {

        fun getReminderEnumByText(text: String): ReminderEnum {
            for (reminderEnum in values()) {
                if (reminderEnum.text == text) {
                    return reminderEnum
                }
            }
            return NO_REMINDER
        }

        fun getReminder(startTime: Long, reminderEnum: ReminderEnum): Long? {
            when (reminderEnum) {
                NO_REMINDER -> return null
                ON_TIME -> return startTime
                else -> {
                    val calendar = Calendar.getInstance()
                    calendar.timeInMillis = startTime
                    calendar.add(reminderEnum.calendarConst!!, reminderEnum.duration!!)
                    return calendar.timeInMillis
                }
            }
        }
    }
}
