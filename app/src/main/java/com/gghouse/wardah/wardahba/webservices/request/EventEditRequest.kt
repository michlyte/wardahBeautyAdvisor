package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.EventLocation

class EventEditRequest : EventCreateRequest {
    val calendarId: Long

    constructor(calendarId: Long, userId: Long, title: String, category: String, startTime: Long, endTime: Long, alarmTime: Long?, alarmTimeStr: String?, notes: String, location: EventLocation) : super(userId, title, category, startTime, endTime, alarmTime, alarmTimeStr, notes, location) {
        this.calendarId = calendarId
    }
}