package com.gghouse.wardah.wardahba.interfaces

import com.gghouse.wardah.wardahba.enumerations.ViewMode
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem

interface WardahListener {
    fun onClick(item: WardahHistoryRecyclerViewItem, viewMode: ViewMode)
}
