package com.gghouse.wardah.wardahba.webservices.model;

import com.gghouse.wardah.wardahba.enumerations.StoreHomepageSection;

public class StoreHomepageItem {
    private StoreHomepageSection section;
    private String title;
    private String value;

    public StoreHomepageItem(StoreHomepageSection section, String title, String value) {
        this.section = section;
        this.title = title;
        this.value = value;
    }

    public StoreHomepageSection getSection() {
        return section;
    }

    public void setSection(StoreHomepageSection section) {
        this.section = section;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
