package com.gghouse.wardah.wardahba.webservices.request

class AttendantCreateRequest {
    val calendarId: Long
    val userId: Long
    val name: String
    val mobileNumber: String
    val email: String
    val address: String

    constructor(calendarId: Long, userId: Long, name: String, mobileNumber: String, email: String, address: String) {
        this.calendarId = calendarId
        this.userId = userId
        this.name = name
        this.mobileNumber = mobileNumber
        this.email = email
        this.address = address
    }
}