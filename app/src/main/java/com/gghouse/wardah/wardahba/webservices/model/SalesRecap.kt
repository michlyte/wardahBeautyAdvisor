package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem

class SalesRecap : WardahHistoryRecyclerViewItem {
    var amount: Double? = null
    var quantity: Integer? = null
    var thisMonthAmount: Double? = null
    var thisMonthQuantity: Integer? = null

    constructor(amount: Double?, quantity: Integer?, thisMonthAmount: Double?, thisMonthQuantity: Integer?) : super() {
        this.amount = amount
        this.quantity = quantity
        this.thisMonthAmount = thisMonthAmount
        this.thisMonthQuantity = thisMonthQuantity
    }
}