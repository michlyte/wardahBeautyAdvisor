package com.gghouse.wardah.wardahba.webservices.model

class DCDropdown {
    val id: Long
    val name: String
    val province: String
    val city: String

    constructor(id: Long, name: String, province: String, city: String) {
        this.id = id
        this.name = name
        this.province = province
        this.city = city
    }

    override fun toString(): String {
        return name
    }
}