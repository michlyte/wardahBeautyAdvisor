package com.gghouse.wardah.wardahba.webservices.model

class BPHome {
    val userId: Long
    val lastQuestionId: Long
    val userType: String
    var nearestEvent: NearestEvent? = null
    var latestEvent: LatestEvent? = null

    constructor(userId: Long, lastQuestionId: Long, userType: String, nearestEvent: NearestEvent?, latestEvent: LatestEvent?) {
        this.userId = userId
        this.lastQuestionId = lastQuestionId
        this.userType = userType
        this.nearestEvent = nearestEvent
        this.latestEvent = latestEvent
    }
}