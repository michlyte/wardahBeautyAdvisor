package com.gghouse.wardah.wardahba.activities.attendant;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.fragments.WardahSimpleFragment;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.webservices.model.AttendantButton;

public class AttendantActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendant);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Long calendarId = null;

        Intent i = getIntent();
        if (i != null) {
            AttendantButton attendantButton = (AttendantButton) i.getSerializableExtra(IntentUtil.Companion.getATTENDANT_BUTTON());
            calendarId = attendantButton.getCalendarId();
        }

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), calendarId);

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private Long calendarId;

        public SectionsPagerAdapter(FragmentManager fm, Long calendarId) {
            super(fm);
            this.calendarId = calendarId;
        }

        @Override
        public Fragment getItem(int position) {
            return WardahSimpleFragment.newInstance(WardahItemType.ATTENDANT, calendarId);
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.title_activity_peserta);
            }
            return null;
        }
    }
}
