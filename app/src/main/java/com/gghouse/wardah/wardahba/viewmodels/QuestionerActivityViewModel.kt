package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.Questioner
import com.gghouse.wardah.wardahba.webservices.model.QuestionerQuestions
import com.gghouse.wardah.wardahba.webservices.request.QuestionerRequest
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse
import com.gghouse.wardah.wardahba.webservices.response.QuestionerQuestionsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class QuestionerActivityViewModel : ViewModel() {

    val questionerListMutableLiveData: MutableLiveData<QuestionerQuestions> by lazy {
        MutableLiveData<QuestionerQuestions>()
    }
    val isSubmitSuccessMutableLiveData: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    var date: Date? = null

    fun getData() {
        when (Config.mode) {
            ModeEnum.DUMMY_DEVELOPMENT -> {
            }
            ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> if (SessionUtil.validateLoginSession()) {
                callQuestioner(SessionUtil.getUserId())
            }
        }
    }

    private fun callQuestioner(userId: Long?) {
        val callQuestioner = ApiClient.client.apiQuestioner(userId!!)
        callQuestioner.enqueue(object : Callback<QuestionerQuestionsResponse> {
            override fun onResponse(call: Call<QuestionerQuestionsResponse>, response: Response<QuestionerQuestionsResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> questionerListMutableLiveData!!.postValue(rps.data)
                        else -> questionerListMutableLiveData!!.postValue(null)
                    }
                } else {
                    questionerListMutableLiveData!!.postValue(null)
                    ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<QuestionerQuestionsResponse>, t: Throwable) {
                questionerListMutableLiveData!!.postValue(null)
                ToastUtil.toastMessage(TAG, t.message.toString())
            }
        })
    }

    fun submitQuestioner(questionerList: List<Questioner>) {
        if (SessionUtil.validateLoginSession()) {
            when (Config.mode) {
                ModeEnum.DUMMY_DEVELOPMENT -> isSubmitSuccessMutableLiveData.postValue(true)
                ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> {
                    val questionerRequest = QuestionerRequest(SessionUtil.getUserId(), date!!.time, questionerList)

                    val callPostQuestioner = ApiClient.client.apiPostQuestioner(questionerRequest)
                    callPostQuestioner.enqueue(object : Callback<GenericResponse> {
                        override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                            if (response.isSuccessful) {
                                val rps = response.body()
                                when (rps!!.code) {
                                    Config.CODE_200 -> isSubmitSuccessMutableLiveData.postValue(true)
                                    else -> {
                                        isSubmitSuccessMutableLiveData.postValue(false)
                                        ToastUtil.toastMessage(TAG, rps.status!!)
                                    }
                                }
                            } else {
                                isSubmitSuccessMutableLiveData.postValue(false)
                                ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                            }
                        }

                        override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                            isSubmitSuccessMutableLiveData.postValue(false)
                            ToastUtil.toastMessage(TAG, t.message.toString())
                        }
                    })
                }
            }
        }
    }

    companion object {

        private val TAG = QuestionerActivityViewModel::class.java.simpleName
    }
}
