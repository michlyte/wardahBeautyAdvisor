package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.ProductType

class StoreMonthlyCreateRequest {
    val userId: Long
    val inputMonth: Int
    val inputYear: Int
    val locationId: Long
    val competitorTurnover: Double
    val productTypes: List<ProductType>

    constructor(userId: Long, inputMonth: Int, inputYear: Int, locationId: Long, competitorTurnover: Double, productTypes: List<ProductType>) {
        this.userId = userId
        this.inputMonth = inputMonth
        this.inputYear = inputYear
        this.locationId = locationId
        this.competitorTurnover = competitorTurnover
        this.productTypes = productTypes
    }
}