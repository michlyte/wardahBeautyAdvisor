package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Customer
import com.gghouse.wardah.wardahba.webservices.model.Pagination

/**
 * Created by michael on 3/20/2017.
 */

class CustomerListResponse : GenericResponse() {
    var data: List<Customer>? = null
    var pagination: Pagination? = null
}