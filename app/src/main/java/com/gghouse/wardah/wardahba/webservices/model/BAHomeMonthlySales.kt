package com.gghouse.wardah.wardahba.webservices.model

class BAHomeMonthlySales {
    var target: Double? = null
    var amount: Double? = null
    var quantity: Int? = null
    var competitorTurnover: Double? = null
    val inputPerProductTypes: BAHomeInputPerProductTypes

    constructor(target: Double, amount: Double, quantity: Int, competitorTurnover: Double, inputPerProductTypes: BAHomeInputPerProductTypes) {
        this.target = target
        this.amount = amount
        this.quantity = quantity
        this.competitorTurnover = competitorTurnover
        this.inputPerProductTypes = inputPerProductTypes
    }
}
