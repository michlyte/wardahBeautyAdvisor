package com.gghouse.wardah.wardahba.webservices.request

class SalesTargetCreateRequest {
    val userId: Long
    val locationId: Long
    val month: Int
    val year: Int
    val salesTarget: Double

    constructor(userId: Long, locationId: Long, month: Int, year: Int, salesTarget: Double) {
        this.userId = userId
        this.locationId = locationId
        this.month = month
        this.year = year
        this.salesTarget = salesTarget
    }
}