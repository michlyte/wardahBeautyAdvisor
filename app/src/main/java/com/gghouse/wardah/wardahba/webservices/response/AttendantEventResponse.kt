package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Attendant
import com.gghouse.wardah.wardahba.webservices.model.Pagination

class AttendantEventResponse : GenericResponse() {
    var data: List<Attendant>? = null
    var pagination: Pagination? = null
}
