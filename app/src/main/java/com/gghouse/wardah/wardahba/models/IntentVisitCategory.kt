package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.webservices.model.VisitCategory

import java.io.Serializable

class IntentVisitCategory(var objects: List<VisitCategory>?) : Serializable
