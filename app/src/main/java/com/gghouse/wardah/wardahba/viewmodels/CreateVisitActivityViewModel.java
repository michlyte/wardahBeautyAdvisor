package com.gghouse.wardah.wardahba.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.enumerations.ReminderEnum;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.CheckInRadius;
import com.gghouse.wardah.wardahba.webservices.model.DCDropdown;
import com.gghouse.wardah.wardahba.webservices.model.DCLocationDropdown;
import com.gghouse.wardah.wardahba.webservices.model.StoreInformation;
import com.gghouse.wardah.wardahba.webservices.model.VisitCategory;
import com.gghouse.wardah.wardahba.webservices.model.VisitStoreInformation;
import com.gghouse.wardah.wardahba.webservices.model.VisitView;
import com.gghouse.wardah.wardahba.webservices.request.VisitCheckOutRequest;
import com.gghouse.wardah.wardahba.webservices.request.VisitCreateRequest;
import com.gghouse.wardah.wardahba.webservices.request.VisitEditRequest;
import com.gghouse.wardah.wardahba.webservices.response.CheckInRadiusResponse;
import com.gghouse.wardah.wardahba.webservices.response.DCDropdownResponse;
import com.gghouse.wardah.wardahba.webservices.response.DCLocationDropdownResponse;
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse;
import com.gghouse.wardah.wardahba.webservices.response.StoreInformationResponse;
import com.gghouse.wardah.wardahba.webservices.response.VisitCategoryResponse;
import com.gghouse.wardah.wardahba.webservices.response.VisitViewResponse;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateVisitActivityViewModel extends ViewModel {
    private static final String TAG = CreateVisitActivityViewModel.class.getSimpleName();

    private MutableLiveData<ViewMode> viewModeMutableLiveData;
    private MutableLiveData<VisitView> visitViewMutableLiveData;
    private MutableLiveData<List<VisitCategory>> visitCategoryListMutableLiveData;
    private MutableLiveData<Calendar> startTimeMutableLiveData;
    private MutableLiveData<Calendar> endTimeMutableLiveData;
    private MutableLiveData<VisitStoreInformation> visitStoreInformationMutableLiveData;
    private MutableLiveData<StoreInformation> storeInformationMutableLiveData;
    private MutableLiveData<String> dcMutableLiveData;
    private MutableLiveData<List<DCDropdown>> dcDropdownMutableLiveData;
    private MutableLiveData<List<DCLocationDropdown>> dcLocationDropdownMutableLiveData;
    private MutableLiveData<Boolean> isSubmitSucceedMutableLiveData;
    private MutableLiveData<Boolean> isCheckedInOrOutMutableLiveData;
    private MutableLiveData<CheckInRadius> checkInRadiusMutableLiveData;

    private Calendar startTimeCalendar = Calendar.getInstance();
    private Calendar endTimeCalendar = Calendar.getInstance();
    private ReminderEnum reminderEnum = ReminderEnum.NO_REMINDER;

    public MutableLiveData<ViewMode> getViewModeMutableLiveData() {
        if (viewModeMutableLiveData == null) {
            viewModeMutableLiveData = new MutableLiveData<>();
        }
        return viewModeMutableLiveData;
    }

    public MutableLiveData<VisitView> getVisitViewMutableLiveData() {
        if (visitViewMutableLiveData == null) {
            visitViewMutableLiveData = new MutableLiveData<>();
        }
        return visitViewMutableLiveData;
    }

    public MutableLiveData<List<VisitCategory>> getVisitCategoryListMutableLiveData() {
        if (visitCategoryListMutableLiveData == null) {
            visitCategoryListMutableLiveData = new MutableLiveData<>();
        }
        return visitCategoryListMutableLiveData;
    }

    public MutableLiveData<String> getDcMutableLiveData() {
        if (dcMutableLiveData == null) {
            dcMutableLiveData = new MutableLiveData<>();
        }
        return dcMutableLiveData;
    }

    public MutableLiveData<Calendar> getStartTimeMutableLiveData() {
        if (startTimeMutableLiveData == null) {
            startTimeMutableLiveData = new MutableLiveData<>();
        }
        return startTimeMutableLiveData;
    }

    public MutableLiveData<Calendar> getEndTimeMutableLiveData() {
        if (endTimeMutableLiveData == null) {
            endTimeMutableLiveData = new MutableLiveData<>();
        }
        return endTimeMutableLiveData;
    }

    public MutableLiveData<VisitStoreInformation> getVisitStoreInformationMutableLiveData() {
        if (visitStoreInformationMutableLiveData == null) {
            visitStoreInformationMutableLiveData = new MutableLiveData<>();
        }
        return visitStoreInformationMutableLiveData;
    }

    public MutableLiveData<StoreInformation> getStoreInformationMutableLiveData() {
        if (storeInformationMutableLiveData == null) {
            storeInformationMutableLiveData = new MutableLiveData<>();
        }
        return storeInformationMutableLiveData;
    }

    public MutableLiveData<List<DCDropdown>> getDcDropdownMutableLiveData() {
        if (dcDropdownMutableLiveData == null) {
            dcDropdownMutableLiveData = new MutableLiveData<>();
        }
        return dcDropdownMutableLiveData;
    }

    public MutableLiveData<List<DCLocationDropdown>> getDcLocationDropdownMutableLiveData() {
        if (dcLocationDropdownMutableLiveData == null) {
            dcLocationDropdownMutableLiveData = new MutableLiveData<>();
        }
        return dcLocationDropdownMutableLiveData;
    }

    public MutableLiveData<Boolean> getIsSubmitSucceedMutableLiveData() {
        if (isSubmitSucceedMutableLiveData == null) {
            isSubmitSucceedMutableLiveData = new MutableLiveData<>();
        }
        return isSubmitSucceedMutableLiveData;
    }

    public MutableLiveData<Boolean> getIsCheckedInOrOutMutableLiveData() {
        if (isCheckedInOrOutMutableLiveData == null) {
            isCheckedInOrOutMutableLiveData = new MutableLiveData<>();
        }
        return isCheckedInOrOutMutableLiveData;
    }

    public MutableLiveData<CheckInRadius> getCheckInRadiusMutableLiveData() {
        if (checkInRadiusMutableLiveData == null) {
            checkInRadiusMutableLiveData = new MutableLiveData<>();
        }
        return checkInRadiusMutableLiveData;
    }

    public Calendar getStartTimeCalendar() {
        return startTimeCalendar;
    }

    public void setStartTimeCalendar(Calendar startTimeCalendar) {
        this.startTimeCalendar = startTimeCalendar;
    }

    public Calendar getEndTimeCalendar() {
        return endTimeCalendar;
    }

    public void setEndTimeCalendar(Calendar endTimeCalendar) {
        this.endTimeCalendar = endTimeCalendar;
    }

    public ReminderEnum getReminderEnum() {
        return reminderEnum;
    }

    public void setReminderEnum(ReminderEnum reminderEnum) {
        this.reminderEnum = reminderEnum;
    }

    public void getData() {
        callDCDropdown("");
    }

    private void callDCDropdown(String q) {
        Call<DCDropdownResponse> callDCDropdown = ApiClient.INSTANCE.getClient().apiDCDropdown(q);
        callDCDropdown.enqueue(new Callback<DCDropdownResponse>() {
            @Override
            public void onResponse(Call<DCDropdownResponse> call, Response<DCDropdownResponse> response) {
                if (response.isSuccessful()) {
                    DCDropdownResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getDcDropdownMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<DCDropdownResponse> call, Throwable t) {

            }
        });
    }

    public void callDCLocationDropdown(String dc, String q) {
        Call<DCLocationDropdownResponse> callDCLocationDropdown = ApiClient.INSTANCE.getClient().apiDCLocationDropdown(dc, q);
        callDCLocationDropdown.enqueue(new Callback<DCLocationDropdownResponse>() {
            @Override
            public void onResponse(Call<DCLocationDropdownResponse> call, Response<DCLocationDropdownResponse> response) {
                if (response.isSuccessful()) {
                    DCLocationDropdownResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getDcLocationDropdownMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<DCLocationDropdownResponse> call, Throwable t) {

            }
        });
    }

    public void submitVisit(VisitCreateRequest visitCreateRequest) {
        Call<GenericResponse> callVisitCreate = ApiClient.INSTANCE.getClient().apiVisitCreate(visitCreateRequest);
        callVisitCreate.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    GenericResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getIsSubmitSucceedMutableLiveData().postValue(true);
                            break;
                        default:
                            getIsSubmitSucceedMutableLiveData().postValue(false);
                            ToastUtil.INSTANCE.toastMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    getIsSubmitSucceedMutableLiveData().postValue(false);
                    ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                getIsSubmitSucceedMutableLiveData().postValue(false);
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void editVisit(VisitEditRequest visitEditRequest) {
        Call<GenericResponse> callVisitEdit = ApiClient.INSTANCE.getClient().apiVisitEdit(visitEditRequest);
        callVisitEdit.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    GenericResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getIsSubmitSucceedMutableLiveData().postValue(true);
                            break;
                        default:
                            getIsSubmitSucceedMutableLiveData().postValue(false);
                            ToastUtil.INSTANCE.toastMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    getIsSubmitSucceedMutableLiveData().postValue(false);
                    ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                getIsSubmitSucceedMutableLiveData().postValue(false);
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callVisitCategory(long userId) {
        Call<VisitCategoryResponse> callVisitCategory = ApiClient.INSTANCE.getClient().apiVisitCategory(userId);
        callVisitCategory.enqueue(new Callback<VisitCategoryResponse>() {
            @Override
            public void onResponse(Call<VisitCategoryResponse> call, Response<VisitCategoryResponse> response) {
                if (response.isSuccessful()) {
                    VisitCategoryResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getVisitCategoryListMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<VisitCategoryResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callStoreInformation(long storeId) {
        Call<StoreInformationResponse> callStoreInformation = ApiClient.INSTANCE.getClient().apiStoreInformation(storeId);
        callStoreInformation.enqueue(new Callback<StoreInformationResponse>() {
            @Override
            public void onResponse(Call<StoreInformationResponse> call, Response<StoreInformationResponse> response) {
                if (response.isSuccessful()) {
                    StoreInformationResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            StoreInformation storeInformation = rps.getData();
                            storeInformation.setStoreId(storeId);
                            getStoreInformationMutableLiveData().postValue(storeInformation);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<StoreInformationResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callCheckOut(VisitCheckOutRequest visitCheckOutRequest) {
        Call<GenericResponse> callVisitCheckOut = ApiClient.INSTANCE.getClient().apiVisitCheckOut(visitCheckOutRequest);
        callVisitCheckOut.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    GenericResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getIsCheckedInOrOutMutableLiveData().postValue(true);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callVisitView(long calendarId) {
        Call<VisitViewResponse> callVisitView = ApiClient.INSTANCE.getClient().apiVisitView(calendarId);
        callVisitView.enqueue(new Callback<VisitViewResponse>() {
            @Override
            public void onResponse(Call<VisitViewResponse> call, Response<VisitViewResponse> response) {
                if (response.isSuccessful()) {
                    VisitViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getVisitViewMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<VisitViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callCheckInRadius(long userId, long calendarId) {
        Call<CheckInRadiusResponse> callCheckInRadius = ApiClient.INSTANCE.getClient().apiCheckInRadius(userId, calendarId);
        callCheckInRadius.enqueue(new Callback<CheckInRadiusResponse>() {
            @Override
            public void onResponse(Call<CheckInRadiusResponse> call, Response<CheckInRadiusResponse> response) {
                if (response.isSuccessful()) {
                    CheckInRadiusResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            CheckInRadius checkInRadius = rps.getData();
                            checkInRadius.setCalendarId(calendarId);
                            getCheckInRadiusMutableLiveData().postValue(checkInRadius);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<CheckInRadiusResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }
}
