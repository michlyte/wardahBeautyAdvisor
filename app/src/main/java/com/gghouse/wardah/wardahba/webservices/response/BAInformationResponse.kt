package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.BAInformationOverview

class BAInformationResponse : GenericResponse() {
    var data: BAInformationOverview? = null
}
