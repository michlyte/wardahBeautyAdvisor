package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.enumerations.StoreSalesType
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.BAHome
import com.gghouse.wardah.wardahba.webservices.model.Profile
import com.gghouse.wardah.wardahba.webservices.model.User
import com.gghouse.wardah.wardahba.webservices.response.BAHomeResponse
import com.gghouse.wardah.wardahba.webservices.response.ProfileResponse

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeBAActivityViewModel : ViewModel() {
    val userMutableLiveData: MutableLiveData<User> by lazy {
        MutableLiveData<User>()
    }
    val menuMutableLiveData: MutableLiveData<List<Int>> by lazy {
        MutableLiveData<List<Int>>()
    }
    val storeSalesTypeMutableLiveData: MutableLiveData<StoreSalesType> by lazy {
        MutableLiveData<StoreSalesType>()
    }
    val baHomeMutableLiveData: MutableLiveData<BAHome> by lazy {
        MutableLiveData<BAHome>()
    }
    val profileMutableLiveData: MutableLiveData<Profile> by lazy {
        MutableLiveData<Profile>()
    }

    fun getData() {
        when (Config.mode) {
            ModeEnum.DUMMY_DEVELOPMENT -> userMutableLiveData.postValue(SessionUtil.getUser())
            ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> if (SessionUtil.validateLoginSession()) {
                userMutableLiveData.postValue(SessionUtil.getUser())
                callBAHomeData()
            }
        }
    }

    private fun callBAHomeData() {
        val callBAHome = ApiClient.client.apiBAHome(SessionUtil.getUserId()!!)
        callBAHome.enqueue(object : Callback<BAHomeResponse> {
            override fun onResponse(call: Call<BAHomeResponse>, response: Response<BAHomeResponse>) {
                if (response.isSuccessful) {
                    val baHomeResponse = response.body()
                    when (baHomeResponse!!.code) {
                        Config.CODE_200 -> baHomeMutableLiveData.postValue(baHomeResponse.data)
                        else -> ToastUtil.retrofitFailMessage(TAG, baHomeResponse.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<BAHomeResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callProfileData() {
        if (SessionUtil.validateLoginSession()) {
            val callProfile = ApiClient.client.apiProfile(SessionUtil.getUserId()!!)
            callProfile.enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(call: Call<ProfileResponse>, response: Response<ProfileResponse>) {
                    if (response.isSuccessful) {
                        val profileResponse = response.body()
                        when (profileResponse!!.code) {
                            Config.CODE_200 -> profileMutableLiveData.postValue(profileResponse.data)
                            else -> ToastUtil.retrofitFailMessage(TAG, profileResponse.status!!)
                        }
                    } else {
                        ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                    }
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    ToastUtil.retrofitFailMessage(TAG, t.message.toString())
                }
            })
        }
    }

    companion object {
        private val TAG = HomeBAActivityViewModel::class.java.simpleName
    }
}
