package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.enumerations.ViewMode
import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.DCDropdown
import com.gghouse.wardah.wardahba.webservices.model.DCLocationDropdown
import com.gghouse.wardah.wardahba.webservices.model.SalesTarget
import com.gghouse.wardah.wardahba.webservices.request.SalesTargetCreateRequest
import com.gghouse.wardah.wardahba.webservices.request.SalesTargetEditRequest
import com.gghouse.wardah.wardahba.webservices.response.DCDropdownResponse
import com.gghouse.wardah.wardahba.webservices.response.DCLocationDropdownResponse
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class InputSalesTargetActivityViewModel : ViewModel() {

    val viewModeMutableLiveData: MutableLiveData<ViewMode> by lazy {
        MutableLiveData<ViewMode>()
    }
    val salesTargetMutableLiveData: MutableLiveData<SalesTarget> by lazy {
        MutableLiveData<SalesTarget>()
    }
    val dcMutableLiveData: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
    val dcDropdownMutableLiveData: MutableLiveData<List<DCDropdown>> by lazy {
        MutableLiveData<List<DCDropdown>>()
    }
    val dcLocationDropdownMutableLiveData: MutableLiveData<List<DCLocationDropdown>> by lazy {
        MutableLiveData<List<DCLocationDropdown>>()
    }
    val isSubmitSucceedMutableLiveData: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    var month = DateTimeUtil.getMonthFromDate(Date())
    var year = DateTimeUtil.getYearFromDate(Date())

    fun getData() {
        when (Config.mode) {
            ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> callDCDropdown("")
            else -> {
            }
        }
    }

    private fun callDCDropdown(q: String) {
        val callDCDropdown = ApiClient.client.apiDCDropdown(q)
        callDCDropdown.enqueue(object : Callback<DCDropdownResponse> {
            override fun onResponse(call: Call<DCDropdownResponse>, response: Response<DCDropdownResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> dcDropdownMutableLiveData.postValue(rps.data)
                        else -> {
                        }
                    }
                }
            }

            override fun onFailure(call: Call<DCDropdownResponse>, t: Throwable) {

            }
        })
    }

    fun callDCLocationDropdown(dc: String, q: String) {
        val callDCLocationDropdown = ApiClient.client.apiDCLocationDropdown(dc, q)
        callDCLocationDropdown.enqueue(object : Callback<DCLocationDropdownResponse> {
            override fun onResponse(call: Call<DCLocationDropdownResponse>, response: Response<DCLocationDropdownResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> dcLocationDropdownMutableLiveData.postValue(rps.data)
                        else -> {
                        }
                    }
                }
            }

            override fun onFailure(call: Call<DCLocationDropdownResponse>, t: Throwable) {

            }
        })
    }

    fun salesTargetSubmit(salesTargetCreateRequest: SalesTargetCreateRequest) {
        val callSalesTargetCreate = ApiClient.client.apiSalesTargetCreate(salesTargetCreateRequest)
        callSalesTargetCreate.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                        else -> {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.toastMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSucceedMutableLiveData.postValue(false)
                    ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSucceedMutableLiveData.postValue(false)
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun salesTargetEdit(salesTargetEditRequest: SalesTargetEditRequest) {
        val callSalesTargetEdit = ApiClient.client.apiSalesTargetEdit(salesTargetEditRequest)
        callSalesTargetEdit.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                        else -> {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.toastMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSucceedMutableLiveData.postValue(false)
                    ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSucceedMutableLiveData.postValue(false)
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    companion object {
        private val TAG = InputSalesTargetActivityViewModel::class.java.simpleName
    }
}
