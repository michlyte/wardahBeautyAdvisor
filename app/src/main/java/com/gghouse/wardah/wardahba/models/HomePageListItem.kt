package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.enumerations.SalesCategory

class HomePageListItem(var salesCategory: SalesCategory?, var amount: Double?, var pcs: Int?)
