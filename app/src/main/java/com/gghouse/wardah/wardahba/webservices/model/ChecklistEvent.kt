package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class ChecklistEvent : Serializable {
    var calendarId: Long
    val editButtonEnable: Boolean
    val checklist: List<Checklist>

    constructor(calendarId: Long, editButtonEnable: Boolean, checklist: List<Checklist>) {
        this.calendarId = calendarId
        this.editButtonEnable = editButtonEnable
        this.checklist = checklist
    }
}