package com.gghouse.wardah.wardahba.webservices.model

class LatestEvent {
    val calendarId: Long
    val title: String
    val category: String
    val startTime: Long
    val endTime: Long
    val alarmTime: Long
    val notes: String
    val location: EventLocation
    val locationAddress: String
    val bpImageURL: String?
    val boothImageURL: String?
    val totalChecklist: Int
    val checklistCompleted: Int
    val totalAttendant: Int
    val checkInTime: Long
    val checkInButtonEnable: Boolean
    val attendantButtonEnable: Boolean
    val updatedBy: String

    constructor(calendarId: Long, title: String, category: String, startTime: Long, endTime: Long, alarmTime: Long, notes: String, location: EventLocation, locationAddress: String, bpImageURL: String?, boothImageURL: String?, totalChecklist: Int, checklistCompleted: Int, totalAttendant: Int, checkInTime: Long, checkInButtonEnable: Boolean, attendantButtonEnable: Boolean, updatedBy: String) {
        this.calendarId = calendarId
        this.title = title
        this.category = category
        this.startTime = startTime
        this.endTime = endTime
        this.alarmTime = alarmTime
        this.notes = notes
        this.location = location
        this.locationAddress = locationAddress
        this.bpImageURL = bpImageURL
        this.boothImageURL = boothImageURL
        this.totalChecklist = totalChecklist
        this.checklistCompleted = checklistCompleted
        this.totalAttendant = totalAttendant
        this.checkInTime = checkInTime
        this.checkInButtonEnable = checkInButtonEnable
        this.attendantButtonEnable = attendantButtonEnable
        this.updatedBy = updatedBy
    }
}