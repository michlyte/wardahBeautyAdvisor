package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.BuyingPower

class StoreDailyCreateRequest {
    val userId: Long
    val inputDate: Long
    val locationId: Long
    val buyer: Integer
    val visitor: Integer
    val buyingPower: BuyingPower

    constructor(userId: Long, inputDate: Long, locationId: Long, buyer: Integer, visitor: Integer, buyingPower: BuyingPower) {
        this.userId = userId
        this.inputDate = inputDate
        this.locationId = locationId
        this.buyer = buyer
        this.visitor = visitor
        this.buyingPower = buyingPower
    }
}