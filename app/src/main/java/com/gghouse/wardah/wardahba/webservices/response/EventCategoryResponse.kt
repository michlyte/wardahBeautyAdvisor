package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.EventCategory

class EventCategoryResponse : GenericResponse() {
    var data: List<EventCategory>? = null
}
