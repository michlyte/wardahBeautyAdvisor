package com.gghouse.wardah.wardahba.dummy;

import com.gghouse.wardah.wardahba.models.BaInformation;
import com.gghouse.wardah.wardahba.models.StoreSalesHistoryHeader;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;
import com.gghouse.wardah.wardahba.webservices.model.StoreSales;
import com.gghouse.wardah.wardahba.webservices.model.StoreSalesBulanan;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class StoreDummy {

    public static StoreSalesHistoryHeader storeSalesHistoryHeader = new StoreSalesHistoryHeader(
            40000.00, 20, 2000, 10000, 10, 20, 30, 40, 50);

    public static List<StoreSales> storeSalesList = new ArrayList<StoreSales>() {{
        add(new StoreSales(new Date().getTime(), 0L, 100000.00, 1L,
                true, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 24 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 48 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 72 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 96 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
    }};

    public static List<StoreSales> storeSalesNonEditableList = new ArrayList<StoreSales>() {{
        add(new StoreSales(new Date().getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 24 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 48 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 72 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 96 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
    }};

    public static List<StoreSales> storeSalesMonthlyList = new ArrayList<StoreSales>() {{
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(0).getTime(), 0L, 100000.00, 1L,
                true, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(1).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(2).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(3).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(4).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
    }};

    public static List<StoreSales> storeSalesMonthlyNonEditableList = new ArrayList<StoreSales>() {{
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(0).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(1).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(2).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(3).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(4).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
    }};

    public static List<ProductHighlight> storeSalesProductHighlight =
            new ArrayList<ProductHighlight>() {{
                add(new ProductHighlight(0L, "item 1", "", 10000.00, 1));
                add(new ProductHighlight(1L, "item 2", "", 20000.00, 2));
                add(new ProductHighlight(2L, "item 3", "", 30000.00, 3));
                add(new ProductHighlight(3L, "item 4", "", 4000.00, 4));
                add(new ProductHighlight(4L, "item 5", "", 50000.00, 5));
                add(new ProductHighlight(5L, "item 6", "", 60000.00, 6));
            }};

    public static List<ProductHighlight> storeSalesProductFocus =
            new ArrayList<ProductHighlight>() {{
                add(new ProductHighlight(0L, "item 1", "", 5000.00, 6));
                add(new ProductHighlight(1L, "item 2", "", 10000.00, 5));
                add(new ProductHighlight(2L, "item 3", "", 15000.00, 4));
                add(new ProductHighlight(3L, "item 4", "", 2000.00, 3));
                add(new ProductHighlight(4L, "item 5", "", 25000.00, 2));
                add(new ProductHighlight(5L, "item 6", "", 30000.00, 1));
            }};


    public static BaInformation baInformationOverview = new BaInformation(
            "Rian",
            "130123421",
            new Date(),
            2,
            HomePageDummy.INSTANCE.getPersonalDailySales(),
            HomePageDummy.INSTANCE.getPersonalDailySales(),
            HomePageDummy.INSTANCE.getBuyerVsVisitor(),
            HomePageDummy.INSTANCE.getBuyerVsVisitor(),
            new ArrayList<ChecklistBA>());


    /**
     * Store Information
     */
    private static List<StoreSales> storeInformationDailyStoreSales = new ArrayList<StoreSales>() {{
        add(new StoreSales(new Date().getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 24 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
        add(new StoreSales(new Date().getTime() - 48 * 60 * 60 * 1000, 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>()));
    }};

    private static List<StoreSales> storeInformationMonthyStoreSales = new ArrayList<StoreSales>() {{
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(0).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(1).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
        add(new StoreSalesBulanan(DateTimeUtil.INSTANCE.getPreviousMonth(2).getTime(), 0L, 100000.00, 1L,
                false, 50, 20, 20, new ArrayList<>(), 200000000.00, 210000000.00));
    }};
}
