package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import java.io.Serializable

class ProductHighlight : WardahHistoryRecyclerViewItem, Serializable {
    var id: Long? = null
    var name: String? = null
    var imgURL: String? = null
    var price: Double? = null
    var quantity: Integer? = null

    constructor(id: Long?, name: String?, imgURL: String?, price: Double?, quantity: Integer?) : super() {
        this.id = id
        this.name = name
        this.imgURL = imgURL
        this.price = price
        this.quantity = quantity
    }
}