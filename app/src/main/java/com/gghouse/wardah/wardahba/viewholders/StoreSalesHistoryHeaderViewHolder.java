package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.utils.SystemUtil;
import com.gghouse.wardah.wardahba.webservices.model.StoreSalesRecap;

public class StoreSalesHistoryHeaderViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final TextView mPcs;
    private final TextView mAmount;
    private final TextView mNumOfVisitor;
    private final TextView mNumOfBuyer;
    private final TextView mBuyingPower1TextView;
    private final TextView mBuyingPower2TextView;
    private final TextView mBuyingPower3TextView;
    private final TextView mBuyingPower4TextView;
    private final TextView mBuyingPower5TextView;
    public StoreSalesRecap mItem;

    public StoreSalesHistoryHeaderViewHolder(View view) {
        super(view);

        this.view = view;
        mPcs = view.findViewById(R.id.tvPenjualanPcs);
        mAmount = view.findViewById(R.id.tvPenjualanRp);
        mNumOfVisitor = view.findViewById(R.id.tvPengunjung);
        mNumOfBuyer = view.findViewById(R.id.tvPembeli);
        mBuyingPower1TextView = view.findViewById(R.id.tvBuyingPower1);
        mBuyingPower2TextView = view.findViewById(R.id.tvBuyingPower2);
        mBuyingPower3TextView = view.findViewById(R.id.tvBuyingPower3);
        mBuyingPower4TextView = view.findViewById(R.id.tvBuyingPower4);
        mBuyingPower5TextView = view.findViewById(R.id.tvBuyingPower5);
    }

    public void setData(StoreSalesRecap storeSalesRecap) {
        mItem = storeSalesRecap;

        String pcs = String.valueOf(mItem.getQuantity());
        String amount = SystemUtil.withSuffix(mItem.getAmount());
        String numOfVisitor = String.valueOf(mItem.getVisitor());
        String numOfBuyer = String.valueOf(mItem.getBuyer());

        mPcs.setText(pcs);
        mAmount.setText(amount);
        mNumOfVisitor.setText(numOfVisitor);
        mNumOfBuyer.setText(numOfBuyer);

//        mBuyingPower1TextView.setText(WardahUtil.getBuyingPower(mItem.getBuyingPower1(), "", " | "));
//        mBuyingPower2TextView.setText(WardahUtil.getBuyingPower(mItem.getBuyingPower2(), "", " | "));
//        mBuyingPower3TextView.setText(WardahUtil.getBuyingPower(mItem.getBuyingPower3(), "", " | "));
//        mBuyingPower4TextView.setText(WardahUtil.getBuyingPower(mItem.getBuyingPower4(), "", " | "));
//        mBuyingPower5TextView.setText(String.valueOf(mItem.getBuyingPower5()));
    }
}