package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.DCLocationDropdown

class DCLocationDropdownResponse : GenericResponse() {
    var data: List<DCLocationDropdown>? = null
}
