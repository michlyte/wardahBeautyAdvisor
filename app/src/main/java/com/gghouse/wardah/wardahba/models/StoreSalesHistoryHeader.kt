package com.gghouse.wardah.wardahba.models

class StoreSalesHistoryHeader(var amount: Double?, var pcs: Int?, var numOfVisitor: Int?, var numOfBuyer: Int?, var buyingPower1: Int?, var buyingPower2: Int?, var buyingPower3: Int?, var buyingPower4: Int?, var buyingPower5: Int?) : WardahHistoryRecyclerViewItem()
