package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.ChecklistBAListener;
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA;

public class EvaluationBAViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final LinearLayout mContainerLinearLayout;
    private final TextView mTitleTextView;
    private final RatingBar mRatingBar;
    //    private final TextView mRatingTextView;
//    private final Button mEvaluationButton;
    private ChecklistBA mItem;

    public EvaluationBAViewHolder(View view) {
        super(view);

        this.view = view;
        mContainerLinearLayout = view.findViewById(R.id.container);
        mTitleTextView = view.findViewById(R.id.titleEditText);
        mRatingBar = view.findViewById(R.id.ratingBar);
//        mRatingTextView = view.findViewById(R.id.ratingTextView);
//        mEvaluationButton = view.findViewById(R.id.evaluationButton);
    }

    private void setupActionEvent(ChecklistBAListener checklistBAListener) {
        mContainerLinearLayout.setOnClickListener(view -> checklistBAListener.onClick(mItem));
//        mEvaluationButton.setOnClickListener(view -> checklistBAListener.onEvaluationButtonClicked(mItem));
    }

    public void setData(ChecklistBA checklistBA, ChecklistBAListener checklistBAListener) {
        mItem = checklistBA;

        mTitleTextView.setText(mItem.getName());
        if (mItem.getRatingStatus() != null) {
            mRatingBar.setVisibility(View.VISIBLE);
//            mRatingTextView.setVisibility(View.VISIBLE);
//            mRatingTextView.setText(String.valueOf(mItem.getRating()));

//            mEvaluationButton.setVisibility(View.GONE);
        } else {
//            mEvaluationButton.setVisibility(View.VISIBLE);

            mRatingBar.setVisibility(View.GONE);
//            mRatingTextView.setVisibility(View.GONE);
        }
        setupActionEvent(checklistBAListener);
    }

    public void setData(ViewMode viewMode, ChecklistBA checklistBA, ChecklistBAListener checklistBAListener) {
        mItem = checklistBA;

        mTitleTextView.setText(mItem.getName());
        if (mItem.getRatingStatus() != null) {
            mRatingBar.setVisibility(View.VISIBLE);
//            mRatingTextView.setVisibility(View.VISIBLE);
//            mRatingTextView.setText(String.valueOf(mItem.getRating()));

//            mEvaluationButton.setVisibility(View.GONE);
        } else {
//            mEvaluationButton.setVisibility(View.VISIBLE);

            mRatingBar.setVisibility(View.GONE);
//            mRatingTextView.setVisibility(View.GONE);
        }

        switch (viewMode) {
            case VIEW:
                break;
            case INPUT:
            case EDIT:
                setupActionEvent(checklistBAListener);
                break;
        }
    }
}