package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Pagination
import com.gghouse.wardah.wardahba.webservices.model.Sales

/**
 * Created by michael on 3/20/2017.
 */

class SalesListResponse : GenericResponse() {
    var data: List<Sales>? = null
    var pagination: Pagination? = null
}
