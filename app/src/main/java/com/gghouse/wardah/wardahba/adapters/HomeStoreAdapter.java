package com.gghouse.wardah.wardahba.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.enumerations.HomepageStoreSection;
import com.gghouse.wardah.wardahba.models.BAHomepageItem;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeStoreAdapter extends BaseAdapter {
    private final Context mContext;
    private HomepageStoreSection mHomepageStoreSection;
    private List<BAHomepageItem> mValues;

    public HomeStoreAdapter(Context c, HomepageStoreSection homepageStoreSection, List<BAHomepageItem> list) {
        mContext = c;
        mHomepageStoreSection = homepageStoreSection;
        mValues = list;
    }

    @Override
    public int getCount() {
        return mValues.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        HomeStoreViewHolder homeStoreViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_homepage_daily_sales, parent, false);

            homeStoreViewHolder = new HomeStoreViewHolder(convertView);
            convertView.setTag(homeStoreViewHolder);
        } else {
            homeStoreViewHolder = (HomeStoreViewHolder) convertView.getTag();
        }

        homeStoreViewHolder.setData(mValues.get(position));
        return convertView;
    }

    public List<BAHomepageItem> getData() {
        return mValues;
    }

    public void clear() {
        mValues.clear();
    }

    public void addAll(List<BAHomepageItem> BAHomepageItemList) {
        mValues.addAll(BAHomepageItemList);
    }

    private class HomeStoreViewHolder extends RecyclerView.ViewHolder {
        private ImageView mIconImageView;
        private TextView mAmountTextView;
        private TextView mPcsTextView;

        public HomeStoreViewHolder(View view) {
            super(view);

            mIconImageView = view.findViewById(R.id.ivDailySalesPersonalIcon);
            mAmountTextView = view.findViewById(R.id.tvDailySalesPersonalAmount);
            mPcsTextView = view.findViewById(R.id.tvDailySalesPersonalPcs);
        }

        public void setData(BAHomepageItem BAHomepageItem) {
            switch (Config.Companion.getMode()) {
                case DUMMY_DEVELOPMENT:
                case DEVELOPMENT:
                    switch (mHomepageStoreSection) {
                        case SALES_RECAP:
                        case DAILY_SALES_PERSONAL:
                            if (BAHomepageItem.getImageResId() != null) {
                                Picasso.get().load(BAHomepageItem.getImageResId()).into(mIconImageView);
                            } else {
                                mIconImageView.setImageDrawable(BAHomepageItem.getTextDrawable());
                            }

                            if (validate(BAHomepageItem.getValue(), BAHomepageItem.getPcs())) {
                                mPcsTextView.setVisibility(View.VISIBLE);
                                mAmountTextView.setText(WardahUtil.INSTANCE.getAmount(BAHomepageItem.getValue()));
                                mPcsTextView.setText(WardahUtil.INSTANCE.getPcs(BAHomepageItem.getPcs()));
                            } else {
                                mAmountTextView.setText(R.string.text_empty_value);
                                mPcsTextView.setVisibility(View.GONE);
                            }
                            break;
                        case BUYING_POWER:
                            mIconImageView.setImageDrawable(BAHomepageItem.getTextDrawable());
                            mAmountTextView.setVisibility(View.GONE);
                            mPcsTextView.setText(BAHomepageItem.getPcsStr());
                            break;
                        case BUYER_VS_VISITOR:
                        case TEST:
                            Picasso.get().load(BAHomepageItem.getImageResId()).into(mIconImageView);
                            mAmountTextView.setVisibility(View.GONE);
                            mPcsTextView.setText(BAHomepageItem.getPcsStr());
                            break;
                        case SALES_VS_SALES_TARGET_VS_OMSET_COMPETITOR:
                            Picasso.get().load(BAHomepageItem.getImageResId()).into(mIconImageView);
                            mAmountTextView.setText(WardahUtil.INSTANCE.getAmount(BAHomepageItem.getValue()));
                            mPcsTextView.setVisibility(View.GONE);
                            break;
                        case SALES_BY_CATEGORY:
                            Picasso.get().load(ImageUtil.INSTANCE.getImageUrl(BAHomepageItem.getImageUrl())).into(mIconImageView);
                            if (validate(BAHomepageItem.getValue(), BAHomepageItem.getPcs())) {
                                mPcsTextView.setVisibility(View.VISIBLE);
                                mAmountTextView.setText(WardahUtil.INSTANCE.getAmount(BAHomepageItem.getValue()));
                                mPcsTextView.setText(WardahUtil.INSTANCE.getPcs(BAHomepageItem.getPcs()));
                            } else {
                                mAmountTextView.setText(R.string.text_empty_value);
                                mPcsTextView.setVisibility(View.GONE);
                            }
                            break;
                        default:
                            Picasso.get().load(BAHomepageItem.getImageResId()).into(mIconImageView);
                            if (validate(BAHomepageItem.getValue(), BAHomepageItem.getPcs())) {
                                mPcsTextView.setVisibility(View.VISIBLE);
                                mAmountTextView.setText(WardahUtil.INSTANCE.getAmount(BAHomepageItem.getValue()));
                                mPcsTextView.setText(WardahUtil.INSTANCE.getPcs(BAHomepageItem.getPcs()));
                            } else {
                                mAmountTextView.setText(R.string.text_empty_value);
                                mPcsTextView.setVisibility(View.GONE);
                            }
                            break;
                    }
                    break;
            }
        }

        private boolean validate(Double amount, Integer pcs) {
            if (amount == null || pcs == null) {
                return false;
            } else {
                return true;
            }
        }
    }
}
