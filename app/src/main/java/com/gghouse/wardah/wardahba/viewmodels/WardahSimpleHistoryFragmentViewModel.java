package com.gghouse.wardah.wardahba.viewmodels;

import android.os.Handler;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.adapters.WardahHistoryAdapter;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.dummy.AttendantDummy;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.models.EvaluationQuestionInner;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.Attendant;
import com.gghouse.wardah.wardahba.webservices.model.Event;
import com.gghouse.wardah.wardahba.webservices.model.Pagination;
import com.gghouse.wardah.wardahba.webservices.response.AttendantEventResponse;
import com.gghouse.wardah.wardahba.webservices.response.CategoryResponse;
import com.gghouse.wardah.wardahba.webservices.response.ChecklistBAListResponse;
import com.gghouse.wardah.wardahba.webservices.response.EvaluationQuestionListResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesTargetHistoryResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WardahSimpleHistoryFragmentViewModel extends ViewModel {
    private static final String TAG = WardahSimpleHistoryFragmentViewModel.class.getSimpleName();

    private MutableLiveData<List<? extends WardahHistoryRecyclerViewItem>> wardahHistoryMutableLiveData;
    private MutableLiveData<Pagination> paginationMutableLiveData;
    private MutableLiveData<EvaluationQuestionInner> evaluationQuestionsMutableLiveData;

    private Event mEvent;
    private Long mCalendarId;
    private Long mLocationId;
    public int mPage;

    public MutableLiveData<List<? extends WardahHistoryRecyclerViewItem>> getWardahHistoryMutableLiveData() {
        if (wardahHistoryMutableLiveData == null) {
            wardahHistoryMutableLiveData = new MutableLiveData<>();
        }
        return wardahHistoryMutableLiveData;
    }

    public MutableLiveData<Pagination> getPaginationMutableLiveData() {
        if (paginationMutableLiveData == null) {
            paginationMutableLiveData = new MutableLiveData<>();
        }
        return paginationMutableLiveData;
    }

    public MutableLiveData<EvaluationQuestionInner> getEvaluationQuestionsMutableLiveData() {
        if (evaluationQuestionsMutableLiveData == null) {
            evaluationQuestionsMutableLiveData = new MutableLiveData<>();
        }
        return evaluationQuestionsMutableLiveData;
    }

    public Event getEvent() {
        return mEvent;
    }

    public void setEvent(Event mEvent) {
        this.mEvent = mEvent;
    }

    public Long getCalendarId() {
        return mCalendarId;
    }

    public void setCalendarId(Long mCalendarId) {
        this.mCalendarId = mCalendarId;
    }

    public Long getLocationId() {
        return mLocationId;
    }

    public void setLocationId(Long mLocationId) {
        this.mLocationId = mLocationId;
    }

    public void getData(WardahItemType type, WardahHistoryAdapter adapter, int page) {
        Long userId = SessionUtil.getUserId();
        if (userId == null) {
            SessionUtil.loggingOut();
        } else {
            mPage = page;
            switch (Config.Companion.getMode()) {
                case DUMMY_DEVELOPMENT:
                    callWardahSimpleHistoryDummyData(type, adapter, page);
                    break;
                case DEVELOPMENT:
                case PRODUCTION:
                    callWardahSimpleHistoryData(type, adapter, page);
                    break;
            }
        }
    }

    private void callWardahSimpleHistoryDummyData(WardahItemType type, WardahHistoryAdapter adapter, int page) {
        new Handler().postDelayed(() -> {
            if (page == 0) {
                Pagination pagination = new Pagination();
                pagination.setLast(false);
                switch (type) {
                    case CATEGORY:
//                        getWardahHistoryMutableLiveData().postValue(CategoryDummy.categoryList);
                        break;
                    case ATTENDANT:
                        getWardahHistoryMutableLiveData().postValue(AttendantDummy.INSTANCE.getATTENDANT_EVENT_LIST());
                        break;
                    case INPUT_TARGET:
//                        getWardahHistoryMutableLiveData().postValue(SalesDummy.salesTargetSimpleList);
                        break;
                    case EVENT_CHECKLIST_BA:
//                        if ((mEvent != null && DateTimeUtil.INSTANCE.isTodayEvent(mEvent.getStartTime(), mEvent.getEndTime()))
//                                || (mVisit != null && DateTimeUtil.INSTANCE.isTodayEvent(mVisit.getStartTime(), mVisit.getEndTime()))) {
//                            getWardahHistoryMutableLiveData().postValue(ChecklistDummy.INSTANCE.getChecklistBATodayEvent());
//                        } else if ((mEvent != null && DateTimeUtil.INSTANCE.isFutureEvent(mEvent.getStartTime(), mEvent.getEndTime()))
//                                || (mVisit != null && DateTimeUtil.INSTANCE.isFutureEvent(mVisit.getStartTime(), mVisit.getEndTime()))) {
//                            getWardahHistoryMutableLiveData().postValue(ChecklistDummy.INSTANCE.getChecklistBAFutureEvent());
//                        } else {
//                            getWardahHistoryMutableLiveData().postValue(ChecklistDummy.INSTANCE.getChecklistBAPastEvent());
//                        }
                        break;
                }
                getPaginationMutableLiveData().postValue(pagination);
            } else {
                /// Remove loading item
                adapter.remove(adapter.getItemCount() - 1);

                List<WardahHistoryRecyclerViewItem> wardahHistoryRecyclerViewItemList = new ArrayList<>();
                //Load data
                int index = adapter.getItemCount();
                int end = index + 20;

                for (long i = index; i < end; i++) {

                    switch (type) {
                        case CATEGORY:
//                            Category category = new Category(
//                                    (int) i,
//                                    "Category " + i,
//                                    (Double.parseDouble(i + "") + 1000000),
//                                    "");
//                            wardahHistoryRecyclerViewItemList.add(category);
                            break;
                        case ATTENDANT:
                            Attendant attendant = new Attendant(
                                    i, "Name " + String.valueOf(i), "081123456789", "Email@email.com", "Alamat");
                            wardahHistoryRecyclerViewItemList.add(attendant);
                            break;
                        case INPUT_TARGET:
//                            SalesTarget salesTarget = new SalesTarget(0L, new Date().getTime(), "Bandung", "ABC Store", 4000000.00 + (int) i, false);
//                            wardahHistoryRecyclerViewItemList.add(salesTarget);
                            break;
                        case EVENT_CHECKLIST_BA:
//                            if ((mEvent != null && DateTimeUtil.INSTANCE.isTodayEvent(mEvent.getStartTime(), mEvent.getEndTime()))
//                                    || (mVisit != null && DateTimeUtil.INSTANCE.isTodayEvent(mVisit.getStartTime(), mVisit.getEndTime()))) {
//                                ChecklistBA checklistBA = new ChecklistBA(0L, "Dummy " + (int) i, null);
//                                wardahHistoryRecyclerViewItemList.add(checklistBA);
//                            } else if ((mEvent != null && DateTimeUtil.INSTANCE.isFutureEvent(mEvent.getStartTime(), mEvent.getEndTime()))
//                                    || (mVisit != null && DateTimeUtil.INSTANCE.isFutureEvent(mVisit.getStartTime(), mVisit.getEndTime()))) {
//                                ChecklistBA checklistBA = new ChecklistBA(0L, "Dummy " + (int) i, null);
//                                wardahHistoryRecyclerViewItemList.add(checklistBA);
//                            } else {
//                                ChecklistBA checklistBA = new ChecklistBA(0L, "Dummy " + (int) i, 4.5f);
//                                wardahHistoryRecyclerViewItemList.add(checklistBA);
//                            }
                            break;
                    }
                }

                // Manipulate pagination for dummy data
                Pagination pagination = new Pagination();
                if (mPage == Config.Companion.getMAX_LOAD_MORE_FOR_DUMMY_DATA()) {
                    pagination.setLast(true);
                } else {
                    pagination.setLast(false);
                }

                getWardahHistoryMutableLiveData().postValue(wardahHistoryRecyclerViewItemList);
                getPaginationMutableLiveData().postValue(pagination);
            }
        }, Config.Companion.getDELAY_LOAD_MORE());
    }

    private void callWardahSimpleHistoryData(WardahItemType type, WardahHistoryAdapter adapter, int page) {
        switch (type) {
            case CATEGORY:
                // Note: mCalendarId = locationId for Category
                Call<CategoryResponse> callMonthlyCategory = ApiClient.INSTANCE.getClient().apiMonthlyCategory(mCalendarId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE(), null, null);
                callMonthlyCategory.enqueue(new Callback<CategoryResponse>() {
                    @Override
                    public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                        if (response.isSuccessful()) {
                            CategoryResponse categoryResponse = response.body();
                            switch (categoryResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahHistoryMutableLiveData().postValue(categoryResponse.getData());
                                    getPaginationMutableLiveData().postValue(categoryResponse.getPagination());
                                    break;
                                default:
                                    getWardahHistoryMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, categoryResponse.getStatus());
                                    break;
                            }
                        } else {
                            getWardahHistoryMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryResponse> call, Throwable t) {
                        getWardahHistoryMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case ATTENDANT:
                Call<AttendantEventResponse> callAttendantEvent = ApiClient.INSTANCE.getClient().apiAttendantEvent(mCalendarId, page, Config.Companion.getHISTORY_ITEM_PER_PAGE());
                callAttendantEvent.enqueue(new Callback<AttendantEventResponse>() {
                    @Override
                    public void onResponse(Call<AttendantEventResponse> call, Response<AttendantEventResponse> response) {
                        if (response.isSuccessful()) {
                            AttendantEventResponse attendantEventResponse = response.body();
                            switch (attendantEventResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahHistoryMutableLiveData().postValue(attendantEventResponse.getData());
                                    break;
                                default:
                                    getWardahHistoryMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, attendantEventResponse.getStatus());
                                    break;
                            }
                        } else {
                            getWardahHistoryMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<AttendantEventResponse> call, Throwable t) {
                        getWardahHistoryMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case EVENT_CHECKLIST_BA:
                Call<ChecklistBAListResponse> callChechlistBAList = ApiClient.INSTANCE.getClient().apiChecklistBAList(mLocationId, mCalendarId);
                callChechlistBAList.enqueue(new Callback<ChecklistBAListResponse>() {
                    @Override
                    public void onResponse(Call<ChecklistBAListResponse> call, Response<ChecklistBAListResponse> response) {
                        if (response.isSuccessful()) {
                            ChecklistBAListResponse checklistBAListResponse = response.body();
                            switch (checklistBAListResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahHistoryMutableLiveData().postValue(checklistBAListResponse.getData());
                                    break;
                                default:
                                    getWardahHistoryMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, checklistBAListResponse.getStatus());
                                    break;
                            }
                        } else {
                            getWardahHistoryMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<ChecklistBAListResponse> call, Throwable t) {
                        getWardahHistoryMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case INPUT_TARGET:
                Call<SalesTargetHistoryResponse> callSalesTargetHistory = ApiClient.INSTANCE.getClient().apiSalesTargetHistory(SessionUtil.getUserId(), page, Config.Companion.getHISTORY_ITEM_PER_PAGE());
                callSalesTargetHistory.enqueue(new Callback<SalesTargetHistoryResponse>() {
                    @Override
                    public void onResponse(Call<SalesTargetHistoryResponse> call, Response<SalesTargetHistoryResponse> response) {
                        if (response.isSuccessful()) {
                            SalesTargetHistoryResponse salesTargetHistoryResponse = response.body();
                            switch (salesTargetHistoryResponse.getCode()) {
                                case Config.CODE_200:
                                    getWardahHistoryMutableLiveData().postValue(salesTargetHistoryResponse.getData());
                                    break;
                                default:
                                    getWardahHistoryMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, salesTargetHistoryResponse.getStatus());
                                    break;
                            }
                        } else {
                            getWardahHistoryMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesTargetHistoryResponse> call, Throwable t) {
                        getWardahHistoryMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
        }
    }

    public void callEvaluationQuestions(long userId) {
        Call<EvaluationQuestionListResponse> callEvalutionQuestions = ApiClient.INSTANCE.getClient().apiEvaluationQuestions(userId);
        callEvalutionQuestions.enqueue(new Callback<EvaluationQuestionListResponse>() {
            @Override
            public void onResponse(Call<EvaluationQuestionListResponse> call, Response<EvaluationQuestionListResponse> response) {
                if (response.isSuccessful()) {
                    EvaluationQuestionListResponse evaluationQuestionListResponse = response.body();
                    switch (evaluationQuestionListResponse.getCode()) {
                        case Config.CODE_200:
                            EvaluationQuestionInner evaluationQuestionInner = new EvaluationQuestionInner(userId, mCalendarId, evaluationQuestionListResponse.getData());
                            getEvaluationQuestionsMutableLiveData().postValue(evaluationQuestionInner);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, evaluationQuestionListResponse.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<EvaluationQuestionListResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }
}
