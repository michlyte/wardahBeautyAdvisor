package com.gghouse.wardah.wardahba.enumerations

import com.gghouse.wardah.wardahba.R

enum class UserTypeEnum(var title: String?, var resourceId: Int) {
    BEAUTY_ADVISOR("BEAUTY_ADVISOR", R.string.user_type_ba),
    BEAUTY_PROMOTOR("BEAUTY_PROMOTOR", R.string.user_type_bp),
    BEAUTY_PROMOTER_LEADER("BEAUTY_PROMOTOR_LEADER", R.string.user_type_bpl),
    FIELD_CONTROLLER("FIELD_CONTROLLER", R.string.user_type_fc);

    companion object {
        fun getUserTypeEnum(title: String): UserTypeEnum? {
            for (userTypeEnum in values()) {
                if (userTypeEnum.title == title) {
                    return userTypeEnum
                }
            }
            return null
        }
    }
}
