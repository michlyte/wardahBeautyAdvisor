package com.gghouse.wardah.wardahba.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.gghouse.wardah.wardahba.activities.home.HomeBAActivity;
import com.gghouse.wardah.wardahba.activities.home.HomeBPActivity;
import com.gghouse.wardah.wardahba.activities.home.HomeBPLActivity;
import com.gghouse.wardah.wardahba.activities.home.HomeFCActivity;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.google.firebase.iid.FirebaseInstanceId;

public class SplashActivity extends Activity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String mToken = instanceIdResult.getToken();
            Log.d("WardahApp Token", mToken);
        });

        switch (Config.Companion.getMode()) {
            case DUMMY_DEVELOPMENT:
            case DEVELOPMENT:
                ApiClient.INSTANCE.generateClientWithNewIP(SessionUtil.loadIPAddress());
                break;
            case PRODUCTION:
                ApiClient.INSTANCE.generateClientWithNewIP(Config.Companion.getPROD_URL());
                break;
        }

        // Delay
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            Intent intent;
            if (SessionUtil.isLoggedIn()) {
                try {
                    UserTypeEnum userTypeEnum = UserTypeEnum.Companion.getUserTypeEnum(SessionUtil.getUserType());
                    switch (userTypeEnum) {
                        case BEAUTY_ADVISOR:
                            intent = new Intent(getBaseContext(), HomeBAActivity.class);
                            startActivity(intent);
                            break;
                        case BEAUTY_PROMOTOR:
                            intent = new Intent(getBaseContext(), HomeBPActivity.class);
                            startActivity(intent);
                            break;
                        case BEAUTY_PROMOTER_LEADER:
                            intent = new Intent(getBaseContext(), HomeBPLActivity.class);
                            startActivity(intent);
                            break;
                        case FIELD_CONTROLLER:
                            intent = new Intent(getBaseContext(), HomeFCActivity.class);
                            startActivity(intent);
                            break;
                        default:
                            intent = new Intent(getBaseContext(), WelcomeActivity.class);
                            startActivity(intent);
                            break;
                    }
                } catch (NullPointerException npe) {
                    Log.e(TAG, SessionUtil.getUserType());
                }
            } else {
                intent = new Intent(getBaseContext(), WelcomeActivity.class);
                startActivity(intent);
            }
            finish();
        }, Config.Companion.getSPLASH_SCREEN_DELAY());
    }
}
