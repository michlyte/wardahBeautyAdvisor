package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import java.io.Serializable

class ChecklistBA : WardahHistoryRecyclerViewItem, Serializable {
    val userId: Long
    val name: String
    var ratingStatus: String? = null
    val isActive: Boolean

    constructor(userId: Long, name: String, ratingStatus: String?, isActive: Boolean) : super() {
        this.userId = userId
        this.name = name
        this.ratingStatus = ratingStatus
        this.isActive = isActive
    }
}