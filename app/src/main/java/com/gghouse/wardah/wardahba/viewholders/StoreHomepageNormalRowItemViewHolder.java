package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.models.StoreHomepageNormalItem;
import com.gghouse.wardah.wardahba.utils.SystemUtil;

public class StoreHomepageNormalRowItemViewHolder extends RecyclerView.ViewHolder {
    private final TextView titleTextView;
    private final TextView valueTextView;
    public StoreHomepageNormalItem mItem;

    public StoreHomepageNormalRowItemViewHolder(View v) {
        super(v);
        titleTextView = (TextView) v.findViewById(R.id.tvTitle);
        valueTextView = (TextView) v.findViewById(R.id.tvValue);
    }

    public void setData(StoreHomepageNormalItem storeHomepageNormalItem) {
        mItem = storeHomepageNormalItem;

        titleTextView.setText(mItem.getTitle());

        valueTextView.setText(SystemUtil.withSuffix(
                mItem.getValue() != null ? Double.parseDouble(mItem.getValue()) : 0.00, 0));
    }
}
