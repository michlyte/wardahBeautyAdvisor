package com.gghouse.wardah.wardahba.webservices.request

/**
 * Created by michael on 3/10/2017.
 */

class LoginRequest(private val username: String, private val password: String, private val imei: String, private val regId: String)
