package com.gghouse.wardah.wardahba.webservices.request

class SalesTargetEditRequest {
    val salesTargetId: Long
    val salesTarget: Double

    constructor(salesTargetId: Long, salesTarget: Double) {
        this.salesTargetId = salesTargetId
        this.salesTarget = salesTarget
    }
}