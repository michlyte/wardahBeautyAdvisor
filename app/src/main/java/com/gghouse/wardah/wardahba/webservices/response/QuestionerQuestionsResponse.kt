package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.QuestionerQuestions

/**
 * Created by ecquaria-macmini on 5/26/17.
 */

class QuestionerQuestionsResponse : GenericResponse() {
    var data: QuestionerQuestions? = null
}
