package com.gghouse.wardah.wardahba.activities.store;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.adapters.WardahSimpleAdapter;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.helper.NumberTextWatcher;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.models.IntentProductType;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.viewmodels.StoreInputActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.BuyingPower;
import com.gghouse.wardah.wardahba.webservices.model.DailySalesView;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySalesView;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;
import com.gghouse.wardah.wardahba.webservices.request.StoreDailyCreateRequest;
import com.gghouse.wardah.wardahba.webservices.request.StoreDailyEditRequest;
import com.gghouse.wardah.wardahba.webservices.request.StoreMonthlyCreateRequest;
import com.gghouse.wardah.wardahba.webservices.request.StoreMonthlyEditRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mehdi.sakout.dynamicbox.DynamicBox;

public class StoreInputActivity extends AppCompatActivity implements WardahListener {

    private static final String TAG = StoreInputActivity.class.getSimpleName();

    private StoreInputActivityViewModel mModel;

    private TextView mDateTextView;
    // Overview
    private LinearLayout mSalesPersonalLinearLayout;
    private LinearLayout mSalesTargetLinearLayout;
    private LinearLayout mOmsetKompetitorLinearLayout;
    private LinearLayout mJumlahPengunjungLinearLayout;

    private TextView mSalesPersonalTextView;
    private TextView mSalesTargetTextView;
    private TextView mOmsetKompetitorTextView;
    private TextView mJumlahPengunjungTextView;

    private EditText mSalesPeronalEditText;
    private EditText mSalesTargetEditText;
    private EditText mOmsetKompetitorEditText;
    private EditText mVisitorEditText;
    // Jumlah Pembeli
    private LinearLayout mJumlahPembeliLinearLayout;
    private LinearLayout mJumlahPembeliContentLinearLayout;
    private EditText mBuyingPowerLessThan50EditText;
    private EditText mBuyingPowerLessThan100EditText;
    private EditText mBuyingPowerLessThan150EditText;
    private EditText mBuyingPowerLessThan300EditText;
    private EditText mBuyingPowerMoreThan300EditText;
    private TextView mJumlahPembeliTextView;
    // Product Highlight
    private LinearLayout mProductHighlightLinearLayout;
    private TextView mProductHighlightTextView;
    private TextView mProductHighlightTitleTextView;
    private RecyclerView mProductHighlightRecyclerView;
    private WardahSimpleAdapter mProductHighlightAdapter;
    // Product Focus
    private LinearLayout mProductFocusLinearLayout;
    private TextView mProductFocusTextView;
    private TextView mProductFocusTitleTextView;
    private RecyclerView mProductFocusRecyclerView;
    private WardahSimpleAdapter mProductFocusAdapter;
    // Category
    private LinearLayout mCategoryLinearLayout;
    private TextView mCategoryTextView;
    private RecyclerView mCategoryRecyclerView;
    private WardahSimpleAdapter mCategoryAdapter;
    // Category Input
    private LinearLayout mCategoryInputLinearLayout;
    private TextView mCategoryInputTextView;
    private RecyclerView mCategoryInputRecyclerView;
    private WardahSimpleAdapter mCategoryInputAdapter;

    private TextWatcher mTextWatcher;

    private DynamicBox mCategoryInputDynamicBox;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_input);

        mModel = ViewModelProviders.of(this).get(StoreInputActivityViewModel.class);

        mTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                calculateBuyer();
            }
        };

        mDateTextView = findViewById(R.id.dateTextView);
        // Overview
        mSalesPersonalLinearLayout = findViewById(R.id.salesPersonalLinearLayout);
        mSalesTargetLinearLayout = findViewById(R.id.salesTargetLinearLayout);
        mOmsetKompetitorLinearLayout = findViewById(R.id.omsetKompetitorLinearLayout);
        mJumlahPengunjungLinearLayout = findViewById(R.id.jumlahPengunjungLinearLayout);

        mSalesPersonalTextView = findViewById(R.id.salesPersonalTextView);
        mSalesTargetTextView = findViewById(R.id.salesTargetTextView);
        mOmsetKompetitorTextView = findViewById(R.id.competitorTurnoverTextView);
        mJumlahPengunjungTextView = findViewById(R.id.jumlahPengunjungTextView);

        mSalesPeronalEditText = findViewById(R.id.salesPersonalEditText);
        mSalesPeronalEditText.addTextChangedListener(new NumberTextWatcher(mSalesPeronalEditText));
        mSalesTargetEditText = findViewById(R.id.salesTargetEditText);
        mSalesTargetEditText.addTextChangedListener(new NumberTextWatcher(mSalesTargetEditText));
        mOmsetKompetitorEditText = findViewById(R.id.omsetKompetitorEditText);
        mOmsetKompetitorEditText.addTextChangedListener(new NumberTextWatcher(mOmsetKompetitorEditText));
        mVisitorEditText = findViewById(R.id.jumlahPengunjungEditText);
        // Jumlah Pembeli
        mJumlahPembeliLinearLayout = findViewById(R.id.jumlahPembeliLinearLayout);
        mJumlahPembeliContentLinearLayout = findViewById(R.id.jumlahPembeliContentLinearLayout);
        mBuyingPowerLessThan50EditText = findViewById(R.id.buyingPowerLessThan50EditText);
        mBuyingPowerLessThan50EditText.addTextChangedListener(mTextWatcher);
        mBuyingPowerLessThan100EditText = findViewById(R.id.buyingPowerLessThan100EditText);
        mBuyingPowerLessThan100EditText.addTextChangedListener(mTextWatcher);
        mBuyingPowerLessThan150EditText = findViewById(R.id.buyingPowerLessThan150EditText);
        mBuyingPowerLessThan150EditText.addTextChangedListener(mTextWatcher);
        mBuyingPowerLessThan300EditText = findViewById(R.id.buyingPowerLessThan300EditText);
        mBuyingPowerLessThan300EditText.addTextChangedListener(mTextWatcher);
        mBuyingPowerMoreThan300EditText = findViewById(R.id.buyingPowerMoreThan300EditText);
        mBuyingPowerMoreThan300EditText.addTextChangedListener(mTextWatcher);
        mJumlahPembeliTextView = findViewById(R.id.jumlahPembeliTextView);
        // Product Highlight
        mProductHighlightLinearLayout = findViewById(R.id.productHighlightLinearLayout);
        mProductHighlightTextView = findViewById(R.id.productHighlightTextView);
        mProductHighlightTitleTextView = findViewById(R.id.productHighlightTitleTextView);
        mProductHighlightRecyclerView = findViewById(R.id.salesProductHighlightRecyclerView);
        mProductHighlightRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager productHighlightLayoutManager = new LinearLayoutManager(this);
        mProductHighlightRecyclerView.setLayoutManager(productHighlightLayoutManager);
        mProductHighlightAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.PRODUCT_HIGHLIGHT_STORE_DETAIL_HISTORY, new ArrayList<>());
        mProductHighlightRecyclerView.setAdapter(mProductHighlightAdapter);
        // Product Focus
        mProductFocusLinearLayout = findViewById(R.id.productFocusLinearLayout);
        mProductFocusTextView = findViewById(R.id.productFocusTextView);
        mProductFocusTitleTextView = findViewById(R.id.productFocusTitleTextView);
        mProductFocusRecyclerView = findViewById(R.id.storeSalesProductFocusRecyclerView);
        mProductFocusRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager productFocusLayoutManager = new LinearLayoutManager(this);
        mProductFocusRecyclerView.setLayoutManager(productFocusLayoutManager);
        mProductFocusAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.PRODUCT_FOCUS_STORE_DETAIL_HISTORY, new ArrayList<>());
        mProductFocusRecyclerView.setAdapter(mProductFocusAdapter);
        // Category
        mCategoryLinearLayout = findViewById(R.id.salesByCategoryLinearLayout);
        mCategoryTextView = findViewById(R.id.salesByCategoryTextView);
        mCategoryRecyclerView = findViewById(R.id.storeSalesCategoryRecyclerView);
        mCategoryRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager categoryLayoutManager = new LinearLayoutManager(this);
        mCategoryRecyclerView.setLayoutManager(categoryLayoutManager);
        mCategoryAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.CATEGORY, new ArrayList<>());
        mCategoryRecyclerView.setAdapter(mCategoryAdapter);
        // Category Input
        mCategoryInputLinearLayout = findViewById(R.id.salesByCategoryInputLinearLayout);
        mCategoryInputTextView = findViewById(R.id.salesByCategoryInputTextView);
        mCategoryInputRecyclerView = findViewById(R.id.categoryInputRecyclerView);
        mCategoryInputRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager categoryInputLayoutManager = new LinearLayoutManager(this);
        mCategoryInputRecyclerView.setLayoutManager(categoryInputLayoutManager);
        mCategoryInputAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.CATEGORY_INPUT, new ArrayList<>(), this);
        mCategoryInputRecyclerView.setAdapter(mCategoryInputAdapter);

        mCategoryInputDynamicBox = new DynamicBox(this, mCategoryInputRecyclerView);
        View emptyView = getLayoutInflater().inflate(R.layout.dynamic_box_empty, null);
        mCategoryInputDynamicBox.addCustomView(emptyView, IntentUtil.Companion.getEMPTY_VIEW());

        // Smooth scrolling
        mProductHighlightRecyclerView.setNestedScrollingEnabled(false);
        mProductFocusRecyclerView.setNestedScrollingEnabled(false);
        mCategoryRecyclerView.setNestedScrollingEnabled(false);
        mCategoryInputRecyclerView.setNestedScrollingEnabled(false);

        Intent i = getIntent();

        try {
            mModel.setDate(DateTimeUtil.INSTANCE.getDateFromParam(i, IntentUtil.Companion.getDATE()));
            mModel.getViewModeMutableLiveData().setValue((ViewMode) i.getSerializableExtra(IntentUtil.Companion.getVIEW_MODE()));

            WardahItemType type = (WardahItemType) i.getSerializableExtra(IntentUtil.Companion.getTYPE());
            mModel.getTypeMutableLiveData().setValue(type);
            switch (type) {
                case STORE_DAILY_SALES:
                    mDateTextView.setText(DateTimeUtil.INSTANCE.getSdfDateMonthYear().format(mModel.getDate()));
                    mModel.getDailySalesViewMutableLiveData().setValue((DailySalesView) i.getSerializableExtra(IntentUtil.Companion.getDATA()));
                    break;
                case STORE_MONTHLY_SALES:
                    mDateTextView.setText(DateTimeUtil.INSTANCE.getSdfMonthYear().format(mModel.getDate()));
                    mModel.getMonthlySalesViewMutableLiveData().setValue((MonthlySalesView) i.getSerializableExtra(IntentUtil.Companion.getDATA()));
                    break;
            }

            // Input monthly store
            IntentProductType intentProductType = (IntentProductType) i.getSerializableExtra(IntentUtil.Companion.getPRODUCT_TYPE());
            if (intentProductType != null) {
                mModel.getCategoryInputMutableLiveData().postValue(intentProductType.getObjects());
            }
        } catch (NullPointerException npe) {
            Log.e(TAG, npe.getMessage());

            mModel.setDate(new Date());
            mModel.getViewModeMutableLiveData().setValue(ViewMode.VIEW);
            mModel.getTypeMutableLiveData().setValue(WardahItemType.STORE_DAILY_SALES);
        }

        mModel.getViewModeMutableLiveData().observe(this, viewMode -> {
            if (viewMode != null) {
                switch (viewMode) {
                    case VIEW:
                        personalizeOverviewView(mSalesPersonalLinearLayout, mSalesPersonalTextView, mSalesPeronalEditText);
                        personalizeOverviewView(mSalesTargetLinearLayout, mSalesTargetTextView, mSalesTargetEditText);
                        personalizeOverviewView(mOmsetKompetitorLinearLayout, mOmsetKompetitorTextView, mOmsetKompetitorEditText);
                        personalizeOverviewView(mJumlahPengunjungLinearLayout, mJumlahPengunjungTextView, mVisitorEditText);

                        personalizeJumlahPembeliView(mBuyingPowerLessThan50EditText);
                        personalizeJumlahPembeliView(mBuyingPowerLessThan100EditText);
                        personalizeJumlahPembeliView(mBuyingPowerLessThan150EditText);
                        personalizeJumlahPembeliView(mBuyingPowerLessThan300EditText);
                        personalizeJumlahPembeliView(mBuyingPowerMoreThan300EditText);

                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        break;
                    case INPUT:
                    case EDIT:
                        mSalesPersonalLinearLayout.setVisibility(View.GONE);

                        mProductHighlightLinearLayout.setVisibility(View.GONE);
                        mProductHighlightRecyclerView.setVisibility(View.GONE);

                        mProductFocusLinearLayout.setVisibility(View.GONE);
                        mProductFocusRecyclerView.setVisibility(View.GONE);
                        break;
                }
            }
        });

        mModel.getTypeMutableLiveData().observe(this, wardahItemType -> {
            if (wardahItemType != null) {
                switch (wardahItemType) {
                    case STORE_MONTHLY_SALES:
                        switch (mModel.getViewModeMutableLiveData().getValue()) {
                            case INPUT:
                                mSalesTargetLinearLayout.setVisibility(View.GONE);
                                mJumlahPembeliLinearLayout.setVisibility(View.GONE);
                                mJumlahPembeliContentLinearLayout.setVisibility(View.GONE);
                                mJumlahPengunjungLinearLayout.setVisibility(View.GONE);
                                mCategoryLinearLayout.setVisibility(View.GONE);
                                mCategoryRecyclerView.setVisibility(View.GONE);

                                getSupportActionBar().setTitle(R.string.title_input_toko_bulanan);
                                break;
                            case EDIT:
                                mSalesTargetLinearLayout.setVisibility(View.GONE);
                                mJumlahPembeliLinearLayout.setVisibility(View.GONE);
                                mJumlahPembeliContentLinearLayout.setVisibility(View.GONE);
                                mJumlahPengunjungLinearLayout.setVisibility(View.GONE);
                                mCategoryLinearLayout.setVisibility(View.GONE);
                                mCategoryRecyclerView.setVisibility(View.GONE);

                                getSupportActionBar().setTitle(R.string.title_edit_toko_bulanan);
                                break;
                            case VIEW:
                                mCategoryInputLinearLayout.setVisibility(View.GONE);
                                mCategoryInputRecyclerView.setVisibility(View.GONE);

                                getSupportActionBar().setTitle(R.string.title_view_toko_bulanan);
                                break;
                        }
                        break;
                    case STORE_DAILY_SALES:
                        switch (mModel.getViewModeMutableLiveData().getValue()) {
                            case INPUT:
                                getSupportActionBar().setTitle(R.string.title_input_toko_harian);
                                break;
                            case EDIT:
                                getSupportActionBar().setTitle(R.string.title_edit_toko_harian);
                                break;
                            case VIEW:
                                getSupportActionBar().setTitle(R.string.title_view_toko_harian);
                                break;
                        }
                        mCategoryInputLinearLayout.setVisibility(View.GONE);
                        mCategoryLinearLayout.setVisibility(View.GONE);
                        mCategoryInputRecyclerView.setVisibility(View.GONE);

                        mSalesTargetLinearLayout.setVisibility(View.GONE);
                        mOmsetKompetitorLinearLayout.setVisibility(View.GONE);

                        mCategoryLinearLayout.setVisibility(View.GONE);
                        mCategoryRecyclerView.setVisibility(View.GONE);
                        break;
                }
            }
        });

        mModel.getDailySalesViewMutableLiveData().observe(this, dailySalesView -> {
            if (dailySalesView != null) {
                if (dailySalesView.getEditable()) {
                    mDateTextView.setText(DateTimeUtil.INSTANCE.getSdfDateMonthYear().format(dailySalesView.getInputDate()));
                    mVisitorEditText.setText(String.valueOf(dailySalesView.getVisitor()));
                    mBuyingPowerLessThan50EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getLess50()));
                    mBuyingPowerLessThan100EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getLess100()));
                    mBuyingPowerLessThan150EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getLess150()));
                    mBuyingPowerLessThan300EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getLess300()));
                    mBuyingPowerMoreThan300EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getMore300()));
                    mJumlahPembeliTextView.setText(String.valueOf(dailySalesView.getBuyer()));
                } else {
                    mDateTextView.setText(DateTimeUtil.INSTANCE.getSdfDateMonthYear().format(dailySalesView.getInputDate()));
                    mVisitorEditText.setText(String.valueOf(dailySalesView.getVisitor()));
                    mBuyingPowerLessThan50EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getLess50()));
                    mBuyingPowerLessThan100EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getLess100()));
                    mBuyingPowerLessThan150EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getLess150()));
                    mBuyingPowerLessThan300EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getLess300()));
                    mBuyingPowerMoreThan300EditText.setText(String.valueOf(dailySalesView.getBuyingPower().getMore300()));
                    mJumlahPembeliTextView.setText(String.valueOf(dailySalesView.getBuyer()));
                    mSalesPeronalEditText.setText(WardahUtil.INSTANCE.getAmount(dailySalesView.getSalesOverview().getSales().getAmount()));
                    mModel.getProductHighlightMutableLiveData().postValue(dailySalesView.getHighlight());
                    mModel.getProductFocusMutableLiveData().postValue(dailySalesView.getFocus());
                }
            }
        });

        mModel.getMonthlySalesViewMutableLiveData().observe(this, monthlySalesView -> {
            if (monthlySalesView != null) {
                if (monthlySalesView.getEditable()) {
                    mOmsetKompetitorEditText.setText(String.valueOf(monthlySalesView.getCompetitorTurnover()));
                    mModel.getCategoryInputMutableLiveData().postValue(monthlySalesView.getProductTypes());
                } else {
                    mOmsetKompetitorEditText.setText(WardahUtil.INSTANCE.getAmount(monthlySalesView.getCompetitorTurnover()));
                    mSalesTargetEditText.setText(WardahUtil.INSTANCE.getAmount(monthlySalesView.getTarget()));
                    mVisitorEditText.setText(String.valueOf(monthlySalesView.getVisitor()));
                    mSalesPeronalEditText.setText(WardahUtil.INSTANCE.getSummarySalesAmountAndPcs(monthlySalesView.getSalesOverview().getSales().getAmount(),
                            "/", monthlySalesView.getSalesOverview().getSales().getQuantity()));
                    mBuyingPowerLessThan50EditText.setText(WardahUtil.INSTANCE.getStringFromInteger(monthlySalesView.getBuyingPower().getLess50()));
                    mBuyingPowerLessThan100EditText.setText(WardahUtil.INSTANCE.getStringFromInteger(monthlySalesView.getBuyingPower().getLess100()));
                    mBuyingPowerLessThan150EditText.setText(WardahUtil.INSTANCE.getStringFromInteger(monthlySalesView.getBuyingPower().getLess150()));
                    mBuyingPowerLessThan300EditText.setText(WardahUtil.INSTANCE.getStringFromInteger(monthlySalesView.getBuyingPower().getLess300()));
                    mBuyingPowerMoreThan300EditText.setText(WardahUtil.INSTANCE.getStringFromInteger(monthlySalesView.getBuyingPower().getMore300()));
                    mModel.getCategoryMutableLiveData().postValue(monthlySalesView.getProductTypes());
                    mModel.getProductHighlightMutableLiveData().postValue(monthlySalesView.getHighlight());
                    mModel.getProductFocusMutableLiveData().postValue(monthlySalesView.getFocus());
                }
            }
        });

        mModel.getProductHighlightMutableLiveData().observe(this, productHighlightList -> {
            Double productHighlightAmount = 0.00;
            int productHighlightPcs = 0;
            for (ProductHighlight productHighlight : productHighlightList) {
                productHighlightAmount += productHighlight.getPrice();
                productHighlightPcs += productHighlight.getQuantity();
            }
            mProductHighlightTextView.setText(WardahUtil.INSTANCE.getStoreSalesAmountAndPcsContent(productHighlightAmount, productHighlightPcs));
            mProductHighlightAdapter.setData(productHighlightList);
            mProductHighlightAdapter.notifyDataSetChanged();
        });

        mModel.getProductFocusMutableLiveData().observe(this, productFocusList -> {
            Double productFocusAmount = 0.00;
            int productFocusPcs = 0;
            for (ProductHighlight productFocus : productFocusList) {
                productFocusAmount += productFocus.getPrice();
                productFocusPcs += productFocus.getQuantity();
            }
            mProductFocusTextView.setText(WardahUtil.INSTANCE.getStoreSalesAmountAndPcsContent(productFocusAmount, productFocusPcs));
            mProductFocusAdapter.setData(productFocusList);
            mProductFocusAdapter.notifyDataSetChanged();
        });

        mModel.getCategoryMutableLiveData().observe(this, categoryList -> {
            try {
                Double salesByCategoryAmount = 0.00;
                int salesByCateogryPcs = 0;
                for (ProductType productType : categoryList) {
                    salesByCategoryAmount += productType.getAmount();
                    salesByCateogryPcs += productType.getQuantity();
                }
                mCategoryTextView.setText(WardahUtil.INSTANCE.getStoreSalesAmountAndPcsContent(salesByCategoryAmount, salesByCateogryPcs));
                mCategoryAdapter.setData(categoryList);
                mCategoryAdapter.notifyDataSetChanged();
            } catch (NullPointerException npe) {
                Log.e(TAG, npe.getMessage());
            }
        });

        mModel.getCategoryInputMutableLiveData().observe(this, categoryInputList -> {
            mCategoryInputDynamicBox.hideAll();

            if (categoryInputList != null && categoryInputList.size() > 0) {
                Double salesByCategoryAmount = 0.00;
                int salesByCateogryPcs = 0;
                for (ProductType productType : categoryInputList) {
                    salesByCategoryAmount += productType.getAmount();
                    salesByCateogryPcs += productType.getQuantity();
                }
                mCategoryInputTextView.setText(WardahUtil.INSTANCE.getStoreSalesAmountAndPcsContent(salesByCategoryAmount, salesByCateogryPcs));
                mCategoryInputAdapter.setData(categoryInputList);
                mCategoryInputAdapter.notifyDataSetChanged();
            } else {
                mCategoryInputDynamicBox.showCustomView(IntentUtil.Companion.getEMPTY_VIEW());
            }
        });

        mModel.isSubmitSucceedMutableLiveData().observe(this, success -> {
            if (success != null && success) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        final Observer<Integer> numberOfBuyerMutableLiveDataObserver = integer -> mJumlahPembeliTextView.setText(String.valueOf(integer));
        mModel.getNumberOfBuyerMutableLiveData().observe(this, numberOfBuyerMutableLiveDataObserver);

        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (mModel.getViewModeMutableLiveData().getValue()) {
            case VIEW:
                return true;
            case EDIT:
                getMenuInflater().inflate(R.menu.menu_edit, menu);
                break;
            case INPUT:
                getMenuInflater().inflate(R.menu.menu_input, menu);
                return true;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                attemptCancel();
                return true;
            case R.id.action_save:
                attemptSubmit(ViewMode.INPUT);
                return true;
            case R.id.action_edit:
                attemptSubmit(ViewMode.EDIT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(WardahHistoryRecyclerViewItem item, ViewMode viewMode) {
        Double salesByCategoryAmount = 0.00;
        int salesByCateogryPcs = 0;
        for (Object obj : mCategoryInputAdapter.getData()) {
            ProductType productType = (ProductType) obj;
            salesByCategoryAmount += productType.getAmount();
            salesByCateogryPcs += productType.getQuantity();
        }
        mCategoryInputTextView.setText(WardahUtil.INSTANCE.getStoreSalesAmountAndPcsContent(salesByCategoryAmount, salesByCateogryPcs));
    }

    private void attemptCancel() {
        switch (mModel.getViewModeMutableLiveData().getValue()) {
            case EDIT:
            case VIEW:
                finish();
                break;
            default:
                new MaterialDialog.Builder(this)
                        .title(R.string.placeholder_konfirmasi)
                        .content(R.string.message_back_confirmation)
                        .positiveText(R.string.action_ok)
                        .positiveColorRes(R.color.colorPrimary)
                        .negativeText(R.string.action_cancel)
                        .negativeColorRes(R.color.colorAccent)
                        .onPositive((dialog, which) -> finish())
                        .show();
                break;
        }
    }

    private void attemptSubmit(ViewMode viewMode) {
        switch (mModel.getTypeMutableLiveData().getValue()) {
            case STORE_DAILY_SALES:
                mVisitorEditText.setError(null);
                mBuyingPowerLessThan50EditText.setError(null);
                mBuyingPowerLessThan100EditText.setError(null);
                mBuyingPowerLessThan150EditText.setError(null);
                mBuyingPowerLessThan300EditText.setError(null);
                mBuyingPowerMoreThan300EditText.setError(null);

                String visitorStr = mVisitorEditText.getText().toString();
                String buyerStr = mJumlahPembeliTextView.getText().toString();
                String buyingPowerLessThan50Str = mBuyingPowerLessThan50EditText.getText().toString();
                String buyingPowerLessThan100Str = mBuyingPowerLessThan100EditText.getText().toString();
                String buyingPowerLessThan150Str = mBuyingPowerLessThan150EditText.getText().toString();
                String buyingPowerLessThan300Str = mBuyingPowerLessThan300EditText.getText().toString();
                String buyingPowerMoreThan300Str = mBuyingPowerMoreThan300EditText.getText().toString();

                boolean cancel = false;
                View focusView = null;

                if (TextUtils.isEmpty(visitorStr)) {
                    mVisitorEditText.setError(getString(R.string.error_field_required));
                    focusView = mVisitorEditText;
                    cancel = true;
                } else if (TextUtils.isEmpty(buyerStr)) {
                    mJumlahPembeliTextView.setError(getString(R.string.error_field_required));
                    focusView = mJumlahPembeliTextView;
                    cancel = true;
                } else if (TextUtils.isEmpty(buyingPowerLessThan50Str)) {
                    mBuyingPowerLessThan50EditText.setError(getString(R.string.error_field_required));
                    focusView = mBuyingPowerLessThan50EditText;
                    cancel = true;
                } else if (TextUtils.isEmpty(buyingPowerLessThan100Str)) {
                    mBuyingPowerLessThan100EditText.setError(getString(R.string.error_field_required));
                    focusView = mBuyingPowerLessThan100EditText;
                    cancel = true;
                } else if (TextUtils.isEmpty(buyingPowerLessThan150Str)) {
                    mBuyingPowerLessThan150EditText.setError(getString(R.string.error_field_required));
                    focusView = mBuyingPowerLessThan150EditText;
                    cancel = true;
                } else if (TextUtils.isEmpty(buyingPowerLessThan300Str)) {
                    mBuyingPowerLessThan300EditText.setError(getString(R.string.error_field_required));
                    focusView = mBuyingPowerLessThan300EditText;
                    cancel = true;
                } else if (TextUtils.isEmpty(buyingPowerMoreThan300Str)) {
                    mBuyingPowerMoreThan300EditText.setError(getString(R.string.error_field_required));
                    focusView = mBuyingPowerMoreThan300EditText;
                    cancel = true;
                }

                if (cancel) {
                    focusView.requestFocus();
                } else {
                    switch (viewMode) {
                        case INPUT:
                            new MaterialDialog.Builder(this)
                                    .title(R.string.title_create_daily_store_sales)
                                    .content(R.string.message_create_store_sales_confirmation)
                                    .positiveText(R.string.action_ok)
                                    .positiveColorRes(R.color.colorPrimary)
                                    .negativeText(R.string.action_cancel)
                                    .negativeColorRes(R.color.colorAccent)
                                    .onPositive((dialog, which) -> {
                                        if (SessionUtil.validateLoginSession()) {
                                            int visitor = Integer.parseInt(visitorStr);
                                            int buyer = Integer.parseInt(buyerStr);
                                            int buyingPowerLessThan50 = Integer.parseInt(buyingPowerLessThan50Str);
                                            int buyingPowerLessThan100 = Integer.parseInt(buyingPowerLessThan100Str);
                                            int buyingPowerLessThan150 = Integer.parseInt(buyingPowerLessThan150Str);
                                            int buyingPowerLessThan300 = Integer.parseInt(buyingPowerLessThan300Str);
                                            int buyingPowerMoreThan300 = Integer.parseInt(buyingPowerMoreThan300Str);

                                            BuyingPower buyingPower = new BuyingPower(buyingPowerLessThan50, buyingPowerLessThan100, buyingPowerLessThan150, buyingPowerLessThan300, buyingPowerMoreThan300);
                                            StoreDailyCreateRequest storeDailyCreateRequest = new StoreDailyCreateRequest(SessionUtil.getUserId(), mModel.getDate().getTime(), SessionUtil.getUserLocationId(),
                                                    buyer, visitor, buyingPower);
                                            mModel.submitDailySales(storeDailyCreateRequest);
                                        }
                                    })
                                    .show();
                            break;
                        case EDIT:
                            new MaterialDialog.Builder(this)
                                    .title(R.string.title_edit_daily_store_sales)
                                    .content(R.string.message_edit_sales_confirmation)
                                    .positiveText(R.string.action_ok)
                                    .positiveColorRes(R.color.colorPrimary)
                                    .negativeText(R.string.action_cancel)
                                    .negativeColorRes(R.color.colorAccent)
                                    .onPositive((dialog, which) -> {
                                        if (SessionUtil.validateLoginSession()) {
                                            int visitor = Integer.parseInt(visitorStr);
                                            int buyer = Integer.parseInt(buyerStr);
                                            int buyingPowerLessThan50 = Integer.parseInt(buyingPowerLessThan50Str);
                                            int buyingPowerLessThan100 = Integer.parseInt(buyingPowerLessThan100Str);
                                            int buyingPowerLessThan150 = Integer.parseInt(buyingPowerLessThan150Str);
                                            int buyingPowerLessThan300 = Integer.parseInt(buyingPowerLessThan300Str);
                                            int buyingPowerMoreThan300 = Integer.parseInt(buyingPowerMoreThan300Str);

                                            BuyingPower buyingPower = new BuyingPower(buyingPowerLessThan50, buyingPowerLessThan100, buyingPowerLessThan150, buyingPowerLessThan300, buyingPowerMoreThan300);
                                            StoreDailyEditRequest storeDailyEditRequest = new StoreDailyEditRequest(mModel.getDailySalesViewMutableLiveData().getValue().getDailyReportId(), buyer, visitor, buyingPower);
                                            mModel.editDailySales(storeDailyEditRequest);
                                        }
                                    })
                                    .show();
                            break;
                    }
                }
                break;
            case STORE_MONTHLY_SALES:
                mOmsetKompetitorEditText.setError(null);

                String omsetKompetitorStr = mOmsetKompetitorEditText.getText().toString().replace(",", "");

                cancel = false;
                focusView = null;

                if (TextUtils.isEmpty(omsetKompetitorStr)) {
                    mOmsetKompetitorEditText.setError(getString(R.string.error_field_required));
                    focusView = mOmsetKompetitorEditText;
                    cancel = true;
                }

                if (cancel) {
                    focusView.requestFocus();
                } else {
                    switch (viewMode) {
                        case INPUT:
                            new MaterialDialog.Builder(this)
                                    .title(R.string.title_create_daily_store_sales)
                                    .content(R.string.message_create_store_sales_confirmation)
                                    .positiveText(R.string.action_ok)
                                    .positiveColorRes(R.color.colorPrimary)
                                    .negativeText(R.string.action_cancel)
                                    .negativeColorRes(R.color.colorAccent)
                                    .onPositive((dialog, which) -> {
                                        double omsetCompetitor = Double.parseDouble(omsetKompetitorStr);
                                        StoreMonthlyCreateRequest storeMonthlyCreateRequest = new StoreMonthlyCreateRequest(SessionUtil.getUserId(),
                                                DateTimeUtil.INSTANCE.getMonthFromDate(mModel.getDate()), DateTimeUtil.INSTANCE.getYearFromDate(mModel.getDate()),
                                                SessionUtil.getUserLocationId(), omsetCompetitor, (List<ProductType>) mCategoryInputAdapter.getData());
                                        mModel.submitMonthlySales(storeMonthlyCreateRequest);
                                    })
                                    .show();
                            break;
                        case EDIT:
                            new MaterialDialog.Builder(this)
                                    .title(R.string.title_edit_daily_store_sales)
                                    .content(R.string.message_edit_sales_confirmation)
                                    .positiveText(R.string.action_ok)
                                    .positiveColorRes(R.color.colorPrimary)
                                    .negativeText(R.string.action_cancel)
                                    .negativeColorRes(R.color.colorAccent)
                                    .onPositive((dialog, which) -> {
                                        double omsetCompetitor = Double.parseDouble(omsetKompetitorStr);
                                        StoreMonthlyEditRequest storeMonthlyEditRequest = new StoreMonthlyEditRequest(mModel.getMonthlySalesViewMutableLiveData().getValue().getMonthlyReportId(),
                                                omsetCompetitor, (List<ProductType>) mCategoryInputAdapter.getData());
                                        mModel.editMonthlySales(storeMonthlyEditRequest);
                                    })
                                    .show();
                            break;
                    }
                }
                break;
        }
    }

    private void calculateBuyer() {
        int buyingPowerLessThan50 = WardahUtil.INSTANCE.getIntegerFromString(mBuyingPowerLessThan50EditText.getText().toString());
        int buyingPowerLessThan100 = WardahUtil.INSTANCE.getIntegerFromString(mBuyingPowerLessThan100EditText.getText().toString());
        int buyingPowerLessThan150 = WardahUtil.INSTANCE.getIntegerFromString(mBuyingPowerLessThan150EditText.getText().toString());
        int buyingPowerLessThan300 = WardahUtil.INSTANCE.getIntegerFromString(mBuyingPowerLessThan300EditText.getText().toString());
        int buyingPowerMoreThan300 = WardahUtil.INSTANCE.getIntegerFromString(mBuyingPowerMoreThan300EditText.getText().toString());

        int numberOfBuyer = buyingPowerLessThan50 + buyingPowerLessThan100 + buyingPowerLessThan150 + buyingPowerLessThan300 + buyingPowerMoreThan300;

        mModel.getNumberOfBuyerMutableLiveData().postValue(numberOfBuyer);
    }

    private void personalizeOverviewView(LinearLayout linearLayout, TextView textView, EditText editText) {
        float whiteColorTextSize = 14f;
        textView.setTextSize(whiteColorTextSize);
        editText.setTextSize(whiteColorTextSize);

        textView.setTypeface(mSalesPersonalTextView.getTypeface(), Typeface.BOLD_ITALIC);
        editText.setTypeface(mSalesPeronalEditText.getTypeface(), Typeface.BOLD_ITALIC);

        editText.setEnabled(false);
        editText.setBackground(null);
    }

    private void personalizeJumlahPembeliView(EditText editText) {
        editText.setEnabled(false);

        int disabledResIdColor = getResources().getColor(android.R.color.darker_gray);
        editText.setTextColor(disabledResIdColor);

        // Remove underline
        editText.setBackground(null);
    }
}
