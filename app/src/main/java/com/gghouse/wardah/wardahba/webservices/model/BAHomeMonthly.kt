package com.gghouse.wardah.wardahba.webservices.model

class BAHomeMonthly {
    val month: Integer
    val year: Integer
    val sales: BAHomeMonthlySales
    val highlight: BAHomeSales
    val focus: BAHomeSales
    val buyer: Integer
    val visitor: Integer
    val buyingPower: BuyingPower

    constructor(month: Integer, year: Integer, sales: BAHomeMonthlySales, highlight: BAHomeSales, focus: BAHomeSales, buyer: Integer, visitor: Integer, buyingPower: BuyingPower) {
        this.month = month
        this.year = year
        this.sales = sales
        this.highlight = highlight
        this.focus = focus
        this.buyer = buyer
        this.visitor = visitor
        this.buyingPower = buyingPower
    }
}