package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class EvaluationQuestion : Serializable {
    val name: String
    val checklist: List<EvaluationChecklist>

    constructor(name: String, checklist: List<EvaluationChecklist>) {
        this.name = name
        this.checklist = checklist
    }
}