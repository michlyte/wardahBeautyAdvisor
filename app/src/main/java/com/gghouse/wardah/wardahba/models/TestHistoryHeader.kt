package com.gghouse.wardah.wardahba.models

/**
 * Created by michael on 5/4/2017.
 */

class TestHistoryHeader : WardahHistoryRecyclerViewItem {
    var average: Double? = null
    var correctRate: Float? = null
    var sumTestTaken: Int? = null

    constructor() {
        this.average = 0.00
        this.correctRate = 0f
        this.sumTestTaken = 0
    }

    constructor(average: Double?, correctRate: Float?, sumTestTaken: Int?) {
        this.average = average
        this.correctRate = correctRate
        this.sumTestTaken = sumTestTaken
    }
}
