package com.gghouse.wardah.wardahba.viewholders;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.DailySalesView;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySalesView;
import com.gghouse.wardah.wardahba.webservices.model.StoreInformation;
import com.gghouse.wardah.wardahba.webservices.response.DailySalesViewResponse;
import com.gghouse.wardah.wardahba.webservices.response.MonthlySalesViewResponse;
import com.gghouse.wardah.wardahba.webservices.response.StoreInformationResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreInformationActivityViewModel extends ViewModel {
    private static final String TAG = StoreInformationActivityViewModel.class.getSimpleName();

    private MutableLiveData<StoreInformation> storeInformationMutableLiveData;
    private MutableLiveData<DailySalesView> dailySalesViewMutableLiveData;
    private MutableLiveData<MonthlySalesView> monthlySalesViewMutableLiveData;
    private long mStoreId;

    public MutableLiveData<StoreInformation> getStoreInformationMutableLiveData() {
        if (storeInformationMutableLiveData == null) {
            storeInformationMutableLiveData = new MutableLiveData<>();
        }
        return storeInformationMutableLiveData;
    }

    public MutableLiveData<DailySalesView> getDailySalesViewMutableLiveData() {
        if (dailySalesViewMutableLiveData == null) {
            dailySalesViewMutableLiveData = new MutableLiveData<>();
        }
        return dailySalesViewMutableLiveData;
    }

    public MutableLiveData<MonthlySalesView> getMonthlySalesViewMutableLiveData() {
        if (monthlySalesViewMutableLiveData == null) {
            monthlySalesViewMutableLiveData = new MutableLiveData<>();
        }
        return monthlySalesViewMutableLiveData;
    }

    public long getStoreId() {
        return mStoreId;
    }

    public void setStoreId(long mStoreId) {
        this.mStoreId = mStoreId;
    }

    public void getData() {
        callStoreInformation(mStoreId);
    }

    public void callDailySalesView(long dailyReportId) {
        Call<DailySalesViewResponse> callDailySalesEdit = ApiClient.INSTANCE.getClient().apiDailySalesView(dailyReportId);
        callDailySalesEdit.enqueue(new Callback<DailySalesViewResponse>() {
            @Override
            public void onResponse(Call<DailySalesViewResponse> call, Response<DailySalesViewResponse> response) {
                if (response.isSuccessful()) {
                    DailySalesViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            DailySalesView dailySalesView = rps.getData();
                            dailySalesView.setDailyReportId(dailyReportId);
                            getDailySalesViewMutableLiveData().postValue(dailySalesView);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<DailySalesViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callMonthlySalesView(long monthlyReportId) {
        Call<MonthlySalesViewResponse> callMonthySalesView = ApiClient.INSTANCE.getClient().apiMonthlySalesView(monthlyReportId);
        callMonthySalesView.enqueue(new Callback<MonthlySalesViewResponse>() {
            @Override
            public void onResponse(Call<MonthlySalesViewResponse> call, Response<MonthlySalesViewResponse> response) {
                if (response.isSuccessful()) {
                    MonthlySalesViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            MonthlySalesView monthlySalesView = rps.getData();
                            monthlySalesView.setMonthlyReportId(monthlyReportId);
                            getMonthlySalesViewMutableLiveData().postValue(monthlySalesView);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<MonthlySalesViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callStoreInformation(long storeId) {
        Call<StoreInformationResponse> callStoreInformation = ApiClient.INSTANCE.getClient().apiStoreInformation(storeId);
        callStoreInformation.enqueue(new Callback<StoreInformationResponse>() {
            @Override
            public void onResponse(Call<StoreInformationResponse> call, Response<StoreInformationResponse> response) {
                if (response.isSuccessful()) {
                    StoreInformationResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getStoreInformationMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<StoreInformationResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }
}
