package com.gghouse.wardah.wardahba.webservices.model

class TestLatestDate {
    var testDate: Long? = null
    var noLeftTest: Integer? = null

    constructor(testDate: Long?, noLeftTest: Integer?) {
        this.testDate = testDate
        this.noLeftTest = noLeftTest
    }
}