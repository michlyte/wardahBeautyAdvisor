package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.*
import com.gghouse.wardah.wardahba.webservices.response.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFCActivityViewModel : ViewModel() {
    val userMutableLiveData: MutableLiveData<User> by lazy {
        MutableLiveData<User>()
    }
    val menuMutableLiveData: MutableLiveData<List<Int>> by lazy {
        MutableLiveData<List<Int>>()
    }
    val fcHomeMutableLiveData: MutableLiveData<FCHome> by lazy {
        MutableLiveData<FCHome>()
    }
    val visitViewMutableLiveData: MutableLiveData<VisitView> by lazy {
        MutableLiveData<VisitView>()
    }
    val checkInRadiusBPMutableLiveData: MutableLiveData<CheckInRadius> by lazy {
        MutableLiveData<CheckInRadius>()
    }
    val storeInformationMutableLiveData: MutableLiveData<StoreInformation> by lazy {
        MutableLiveData<StoreInformation>()
    }
    val checkInRadiusFCMutableLiveData: MutableLiveData<CheckInRadius> by lazy {
        MutableLiveData<CheckInRadius>()
    }
    val profileMutableLiveData: MutableLiveData<Profile> by lazy {
        MutableLiveData<Profile>()
    }

    fun getData() {
        when (Config.mode) {
            ModeEnum.DUMMY_DEVELOPMENT -> userMutableLiveData.postValue(SessionUtil.getUser())
            ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> if (SessionUtil.validateLoginSession()) {
                userMutableLiveData.postValue(SessionUtil.getUser())
                callFCHomeData()
            }
        }//                getHomepageFCMutableLiveData().postValue(VisitDummy.INSTANCE.getHomepageFC());
    }

    private fun callFCHomeData() {
        val callBAHome = ApiClient.client.apiFCHome(SessionUtil.getUserId()!!)
        callBAHome.enqueue(object : Callback<FCHomeResponse> {
            override fun onResponse(call: Call<FCHomeResponse>, response: Response<FCHomeResponse>) {
                if (response.isSuccessful) {
                    val fcHomeResponse = response.body()
                    when (fcHomeResponse!!.code) {
                        Config.CODE_200 -> fcHomeMutableLiveData.postValue(fcHomeResponse.data)
                        else -> ToastUtil.retrofitFailMessage(TAG, fcHomeResponse.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<FCHomeResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callProfileData() {
        if (SessionUtil.validateLoginSession()) {
            val callProfile = ApiClient.client.apiProfile(SessionUtil.getUserId()!!)
            callProfile.enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(call: Call<ProfileResponse>, response: Response<ProfileResponse>) {
                    if (response.isSuccessful) {
                        val profileResponse = response.body()
                        when (profileResponse!!.code) {
                            Config.CODE_200 -> profileMutableLiveData.postValue(profileResponse.data)
                            else -> ToastUtil.retrofitFailMessage(TAG, profileResponse.status!!)
                        }
                    } else {
                        ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                    }
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    ToastUtil.retrofitFailMessage(TAG, t.message.toString())
                }
            })
        }
    }

    fun callVisitView(calendarId: Long) {
        val callVisitView = ApiClient.client.apiVisitView(calendarId)
        callVisitView.enqueue(object : Callback<VisitViewResponse> {
            override fun onResponse(call: Call<VisitViewResponse>, response: Response<VisitViewResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> visitViewMutableLiveData.postValue(rps.data)
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<VisitViewResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callCheckInRadius(userTypeEnum: UserTypeEnum, userId: Long, calendarId: Long) {
        val callCheckInRadius = ApiClient.client.apiCheckInRadius(userId, calendarId)
        callCheckInRadius.enqueue(object : Callback<CheckInRadiusResponse> {
            override fun onResponse(call: Call<CheckInRadiusResponse>, response: Response<CheckInRadiusResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> when (userTypeEnum) {
                            UserTypeEnum.FIELD_CONTROLLER -> {
                                var checkInRadius = rps.data
                                checkInRadius!!.calendarId = calendarId
                                checkInRadiusFCMutableLiveData.postValue(checkInRadius)
                            }
                            UserTypeEnum.BEAUTY_PROMOTOR, UserTypeEnum.BEAUTY_PROMOTER_LEADER -> {
                                var checkInRadius = rps.data
                                checkInRadius!!.calendarId = calendarId
                                checkInRadiusBPMutableLiveData.postValue(checkInRadius)
                            }
                        }
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<CheckInRadiusResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    fun callStoreInformation(storeId: Long) {
        val callStoreInformation = ApiClient.client.apiStoreInformation(storeId)
        callStoreInformation.enqueue(object : Callback<StoreInformationResponse> {
            override fun onResponse(call: Call<StoreInformationResponse>, response: Response<StoreInformationResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> {
                            val storeInformation = rps.data
                            storeInformation!!.storeId = storeId
                            storeInformationMutableLiveData.postValue(storeInformation)
                        }
                        else -> ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                    }
                } else {
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<StoreInformationResponse>, t: Throwable) {
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    companion object {
        private val TAG = HomeFCActivityViewModel::class.java.simpleName
    }
}
