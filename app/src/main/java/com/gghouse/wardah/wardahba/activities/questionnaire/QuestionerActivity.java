package com.gghouse.wardah.wardahba.activities.questionnaire;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.adapters.QuestionerAdapter;
import com.gghouse.wardah.wardahba.models.Questioner;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.viewmodels.QuestionerActivityViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mehdi.sakout.dynamicbox.DynamicBox;

/**
 * Created by michaelhalim on 5/25/17.
 */

public class QuestionerActivity extends AppCompatActivity {

    public static final String TAG = QuestionerActivity.class.getSimpleName();

    private QuestionerActivityViewModel mModel;

    private RecyclerView mRecyclerView;
    private QuestionerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<Questioner> mDataSet;

    /*
     * Loading View
     */
    private DynamicBox mDynamicBox;
    private MaterialDialog mLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questioner);

        mModel = ViewModelProviders.of(this).get(QuestionerActivityViewModel.class);

        Intent i = getIntent();
        if (i == null) {
            mModel.setDate(new Date());
        } else {
            mModel.setDate(DateTimeUtil.INSTANCE.getDateFromParam(i, IntentUtil.Companion.getDATE()));
        }

        String title = getSupportActionBar().getTitle().toString() + " - " + DateTimeUtil.INSTANCE.getSdfFilter().format(mModel.getDate());
        getSupportActionBar().setTitle(title);

        mDataSet = new ArrayList<>();

        mRecyclerView = findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new QuestionerAdapter(mDataSet);
        mRecyclerView.setAdapter(mAdapter);

        mDynamicBox = new DynamicBox(this, mRecyclerView);
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(null)
                .content(R.string.message_questioner_send)
                .cancelable(false)
                .progress(true, 0);
        mLoading = builder.build();

        mModel.getQuestionerListMutableLiveData().observe(this, questionerQuestions -> {
            mDynamicBox.hideAll();

            if (questionerQuestions != null) {
                mRecyclerView.setAdapter(null);
                mDataSet.clear();
                mDataSet.addAll(questionerQuestions.getQuestionnaires());
                mAdapter.setData(mDataSet);
                mRecyclerView.setAdapter(mAdapter);
            }
        });

        mModel.isSubmitSuccessMutableLiveData().observe(this, isSubmitSuccess -> {
            if (isSubmitSuccess != null && isSubmitSuccess) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        mDynamicBox.showLoadingLayout();
        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_questioner, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(this)
                .title(R.string.title_warning)
                .content(R.string.message_questioner_cancel)
                .positiveText(R.string.action_ok)
                .positiveColorRes(R.color.colorPrimary)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                mModel.getData();
                return true;
            case R.id.action_done:
                attemptDone();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void attemptDone() {
        boolean cancel = false;

        for (Questioner questioner : mDataSet) {
            if (questioner.getRating() == null || questioner.getRating() < 1f) {
                cancel = true;
            }
        }

        if (cancel) {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_warning)
                    .content(R.string.message_questioner_invalid)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .show();
        } else {
            List<com.gghouse.wardah.wardahba.webservices.model.Questioner> questionerList = new ArrayList<>();
            for (com.gghouse.wardah.wardahba.models.Questioner questioner : mDataSet) {
                questionerList.add(new com.gghouse.wardah.wardahba.webservices.model.Questioner(questioner.getId(), questioner.getRating()));
            }
            mModel.submitQuestioner(questionerList);
        }
    }
}
