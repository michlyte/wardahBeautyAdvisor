package com.gghouse.wardah.wardahba.activities.profile;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.WardahApp;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.utils.LogUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.utils.ValidationUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.request.ChangePasswordRequest;
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    public static final String TAG = ChangePasswordActivity.class.getSimpleName();

    private EditText mEtCurPassword;
    private EditText mEtNewPassword;
    private EditText mEtConfirmNewPassword;

    private MaterialDialog mLoading;
    private MaterialDialog mSuccessDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mEtCurPassword = findViewById(R.id.et_cur_password);
        mEtNewPassword = findViewById(R.id.et_new_password);
        mEtConfirmNewPassword = findViewById(R.id.et_confirm_new_password);

        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(R.string.title_loading)
                .content(R.string.title_processing)
                .cancelable(false)
                .progress(true, 0);
        mLoading = builder.build();

        MaterialDialog.Builder successDialogBuilder = new MaterialDialog.Builder(this)
                .title(null)
                .content(R.string.message_change_password_success)
                .positiveText(R.string.action_ok)
                .positiveColorRes(R.color.colorPrimary)
                .cancelable(false)
                .onPositive((dialog, which) -> finish());
        mSuccessDialog = successDialogBuilder.build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_apply:
                attemptChangePassword();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void attemptChangePassword() {
        // Reset errors.
        mEtCurPassword.setError(null);
        mEtNewPassword.setError(null);
        mEtConfirmNewPassword.setError(null);

        // Store values at the time of the login attempt.
        final String curPassword = mEtCurPassword.getText().toString();
        final String newPassword = mEtNewPassword.getText().toString();
        String confirmNewPassword = mEtConfirmNewPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(curPassword)) {
            mEtCurPassword.setError(getString(R.string.error_field_required));
            focusView = mEtCurPassword;
            cancel = true;
        }
        // Check for a valid password, if the user entered one.
        else if (TextUtils.isEmpty(newPassword)) {
            mEtNewPassword.setError(getString(R.string.error_field_required));
            focusView = mEtNewPassword;
            cancel = true;
        } else if (TextUtils.isEmpty(confirmNewPassword)) {
            mEtConfirmNewPassword.setError(getString(R.string.error_field_required));
            focusView = mEtConfirmNewPassword;
            cancel = true;
        } else if (!ValidationUtil.INSTANCE.isPasswordMatch(newPassword, confirmNewPassword)) {
            mEtNewPassword.setError(getString(R.string.error_password_do_not_match));
            focusView = mEtNewPassword;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            new MaterialDialog.Builder(this)
                    .title(R.string.placeholder_konfirmasi)
                    .content(R.string.message_change_password_confirmation)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .negativeText(R.string.action_cancel)
                    .negativeColorRes(R.color.colorAccent)
                    .cancelable(false)
                    .onPositive((dialog, which) -> ws_changePassword(curPassword, newPassword))
                    .show();
        }
    }

    private void ws_changePassword(String curPassword, String newPassword) {
        if (SessionUtil.validateLoginSession()) {
            mLoading.show();

            ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest(SessionUtil.getUserId(), curPassword, newPassword);
            Call<GenericResponse> callChangePassword = ApiClient.INSTANCE.getClient().apiChangePassword(changePasswordRequest);
            callChangePassword.enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                    LogUtil.INSTANCE.log(LogUtil.INSTANCE.getON_RESPONSE());
                    mLoading.dismiss();

                    if (response.isSuccessful()) {
                        GenericResponse rps = response.body();
                        switch (rps.getCode()) {
                            case Config.CODE_200:
                                mSuccessDialog.show();
                                break;
                            case Config.CODE_99:
                                ToastUtil.INSTANCE.toastMessage(TAG, WardahApp.getInstance().getString(R.string.message_change_password_invalid));
                                break;
                            default:
                                ToastUtil.INSTANCE.toastMessage(TAG, rps.getStatus());
                                break;
                        }
                    } else {
                        ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    ToastUtil.INSTANCE.toastMessage(TAG, t.getMessage());
                    mLoading.dismiss();
                }
            });
        }
    }
}
