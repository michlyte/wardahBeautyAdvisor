package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.BAHome

class BAHomeResponse : GenericResponse() {
    var data: BAHome? = null
}
