package com.gghouse.wardah.wardahba.webservices.model

class OverallTestOverview {
    val totalQuestionsAnswered: Int
    val totalAccumulativeScore: Float

    constructor(totalQuestionsAnswered: Int, totalAccumulativeScore: Float) {
        this.totalQuestionsAnswered = totalQuestionsAnswered
        this.totalAccumulativeScore = totalAccumulativeScore
    }
}