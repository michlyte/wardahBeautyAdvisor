package com.gghouse.wardah.wardahba.webservices.model

class FCMonthlyVisit {
    val date: Long
    val visits: List<VisitCalendar>

    constructor(date: Long, visits: List<VisitCalendar>) {
        this.date = date
        this.visits = visits
    }
}