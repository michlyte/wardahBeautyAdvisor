package com.gghouse.wardah.wardahba.webservices.model;

import java.util.List;

public class StoreHomepageSales {
    private Long salesDate;
    private Integer salesPcs;
    private Integer productHighlightPcs;
    private Integer productFocusPcs;
    private Double salesAmount;
    private Double productHighlightAmount;
    private Double productFocusAmount;
    private List<SalesCategoryType> salesCategoryTypeList;
    private List<StoreHomepageItem> storeHomepageItemList;

    public StoreHomepageSales(Long salesDate, Integer salesPcs, Integer productHighlightPcs, Integer productFocusPcs, Double salesAmount, Double productHighlightAmount, Double productFocusAmount, List<SalesCategoryType> salesCategoryTypeList, List<StoreHomepageItem> storeHomepageItemList) {
        this.salesDate = salesDate;
        this.salesPcs = salesPcs;
        this.productHighlightPcs = productHighlightPcs;
        this.productFocusPcs = productFocusPcs;
        this.salesAmount = salesAmount;
        this.productHighlightAmount = productHighlightAmount;
        this.productFocusAmount = productFocusAmount;
        this.salesCategoryTypeList = salesCategoryTypeList;
        this.storeHomepageItemList = storeHomepageItemList;
    }

    public Long getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Long salesDate) {
        this.salesDate = salesDate;
    }

    public Integer getSalesPcs() {
        return salesPcs;
    }

    public void setSalesPcs(Integer salesPcs) {
        this.salesPcs = salesPcs;
    }

    public Integer getProductHighlightPcs() {
        return productHighlightPcs;
    }

    public void setProductHighlightPcs(Integer productHighlightPcs) {
        this.productHighlightPcs = productHighlightPcs;
    }

    public Integer getProductFocusPcs() {
        return productFocusPcs;
    }

    public void setProductFocusPcs(Integer productFocusPcs) {
        this.productFocusPcs = productFocusPcs;
    }

    public Double getSalesAmount() {
        return salesAmount;
    }

    public void setSalesAmount(Double salesAmount) {
        this.salesAmount = salesAmount;
    }

    public Double getProductHighlightAmount() {
        return productHighlightAmount;
    }

    public void setProductHighlightAmount(Double productHighlightAmount) {
        this.productHighlightAmount = productHighlightAmount;
    }

    public Double getProductFocusAmount() {
        return productFocusAmount;
    }

    public void setProductFocusAmount(Double productFocusAmount) {
        this.productFocusAmount = productFocusAmount;
    }

    public List<SalesCategoryType> getSalesCategoryTypeList() {
        return salesCategoryTypeList;
    }

    public void setSalesCategoryTypeList(List<SalesCategoryType> salesCategoryTypeList) {
        this.salesCategoryTypeList = salesCategoryTypeList;
    }

    public List<StoreHomepageItem> getStoreHomepageItemList() {
        return storeHomepageItemList;
    }

    public void setStoreHomepageItemList(List<StoreHomepageItem> storeHomepageItemList) {
        this.storeHomepageItemList = storeHomepageItemList;
    }
}
