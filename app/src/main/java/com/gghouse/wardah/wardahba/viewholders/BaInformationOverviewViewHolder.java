package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.models.BaInformation;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;

public class BaInformationOverviewViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    public final TextView mName;
    public final TextView mNip;
    public final TextView mJoinedAt;
    public final TextView mLongWork;

    public BaInformationOverviewViewHolder(View view) {
        super(view);

        this.view = view;
        mName = view.findViewById(R.id.tvOverviewName);
        mNip = view.findViewById(R.id.tvOverviewNip);
        mJoinedAt = view.findViewById(R.id.tvOverviewJoinedAt);
        mLongWork = view.findViewById(R.id.tvOverviewLongWork);
    }

    public void setData(BaInformation o) {
        mName.setText(o.getName());
        mNip.setText(o.getNip());
        mJoinedAt.setText(DateTimeUtil.INSTANCE.getSdfDate().format(o.getJoinedAt()));
        mLongWork.setText(o.getLongWork() + "Tahun");
    }
}