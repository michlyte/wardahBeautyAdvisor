package com.gghouse.wardah.wardahba.webservices.model

class BAHomeStoreSalesOverview {
    val daily: BAHomeDaily
    val monthly: BAHomeMonthly

    constructor(daily: BAHomeDaily, monthly: BAHomeMonthly) {
        this.daily = daily
        this.monthly = monthly
    }
}