package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.MonthlySales
import com.gghouse.wardah.wardahba.webservices.model.Pagination

class MonthlySalesResponse : GenericResponse() {
    var data: List<MonthlySales>? = null
    var pagination: Pagination? = null
}
