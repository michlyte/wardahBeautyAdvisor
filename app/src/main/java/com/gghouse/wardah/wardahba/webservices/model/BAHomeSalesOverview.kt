package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class BAHomeSalesOverview : Serializable {
    val salesDate: Long
    val sales: BAHomeSales
    val highlight: BAHomeSales
    val focus: BAHomeSales

    constructor(salesDate: Long, sales: BAHomeSales, highlight: BAHomeSales, focus: BAHomeSales) {
        this.salesDate = salesDate
        this.sales = sales
        this.highlight = highlight
        this.focus = focus
    }
}