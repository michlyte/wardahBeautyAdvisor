package com.gghouse.wardah.wardahba.activities.sales;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.adapters.SalesInputAdapter;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.SalesSummaryListener;
import com.gghouse.wardah.wardahba.models.IntentProductHighlight;
import com.gghouse.wardah.wardahba.models.SalesSummary;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.viewmodels.InputSalesViewModel;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;
import com.gghouse.wardah.wardahba.webservices.model.SalesEditProducts;
import com.gghouse.wardah.wardahba.webservices.request.ProductHighlightRequest;
import com.gghouse.wardah.wardahba.webservices.request.SalesRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InputSalesActivity extends AppCompatActivity implements SalesSummaryListener {

    public static final String TAG = InputSalesActivity.class.getSimpleName();

    private InputSalesViewModel mModel;

    // Personal Sales
    private EditText nominalEditText;
    private EditText pcsEditText;
    // Product Highlight
    private TextView mProductHighlightTextView;
    private TextView mProductHighlightTitleTextView;
    private SalesInputAdapter mProductHighlightAdapter;
    // Product Focus
    private TextView mProductFocusTextView;
    private TextView mProductFocusTitleTextView;
    private SalesInputAdapter mProductFocusAdapter;
    // Section Header
    private View mProductHighlightSectionHeader;
    private View mProductFocusSectionHeader;

    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_input);

        mModel = ViewModelProviders.of(this).get(InputSalesViewModel.class);

        Intent i = getIntent();
        if (i == null) {
            mModel.getProductHighlightLiveData().postValue(new ArrayList<>());
            mModel.setDate(new Date());
            mModel.setViewMode(ViewMode.VIEW);
        } else {
            mModel.getSalesEditProductsMutableLiveData().setValue((SalesEditProducts) i.getSerializableExtra(IntentUtil.Companion.getSALES()));
            IntentProductHighlight intentProductHighlight = (IntentProductHighlight) i.getSerializableExtra(IntentUtil.Companion.getPRODUCT_HIGHLIGHT());
            mModel.getProductHighlightLiveData().setValue(intentProductHighlight.getObjects());
            IntentProductHighlight intentProductFocus = (IntentProductHighlight) i.getSerializableExtra(IntentUtil.Companion.getPRODUCT_FOCUS());
            mModel.getProductFocusMutableLiveData().setValue(intentProductFocus.getObjects());
            mModel.setDate(DateTimeUtil.INSTANCE.getDateFromParam(i, IntentUtil.Companion.getDATE()));
            mModel.setViewMode((ViewMode) i.getSerializableExtra(IntentUtil.Companion.getVIEW_MODE()));
        }

        TextView dateTextView = findViewById(R.id.tv_date);
        nominalEditText = findViewById(R.id.et_ASI_sales);
        pcsEditText = findViewById(R.id.etPcs);

        mProductHighlightSectionHeader = findViewById(R.id.productHighlightSectionHeader);
        mProductFocusSectionHeader = findViewById(R.id.productFocusSectionHeader);

        // Product Highlight
        mProductHighlightTextView = findViewById(R.id.productHighlightTextView);
        mProductHighlightTitleTextView = findViewById(R.id.productHighlightTitleTextView);
        RecyclerView productHighlightRecyclerView = findViewById(R.id.productHighlightRecyclerView);
        productHighlightRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mProductHighlightLayoutManager = new LinearLayoutManager(this);
        productHighlightRecyclerView.setLayoutManager(mProductHighlightLayoutManager);
        mProductHighlightAdapter = new SalesInputAdapter(this, WardahItemType.PRODUCT_HIGHLIGHT, mModel.getProductHighlightLiveData().getValue(), this, mModel.getViewMode());
        productHighlightRecyclerView.setAdapter(mProductHighlightAdapter);
        // Product Focus
        mProductFocusTextView = findViewById(R.id.productFocusTextView);
        mProductFocusTitleTextView = findViewById(R.id.productFocusTitleTextView);
        RecyclerView productFocusRecyclerView = findViewById(R.id.productFocusRecyclerView);
        productFocusRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mProductFocusLayoutManager = new LinearLayoutManager(this);
        productFocusRecyclerView.setLayoutManager(mProductFocusLayoutManager);
        mProductFocusAdapter = new SalesInputAdapter(this, WardahItemType.PRODUCT_FOCUS, mModel.getProductFocusMutableLiveData().getValue(), this, mModel.getViewMode());
        productFocusRecyclerView.setAdapter(mProductFocusAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Material Loading
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(null)
                .content(R.string.message_sales_input_send)
                .cancelable(false)
                .progress(true, 0);
        materialDialog = builder.build();

        dateTextView.setText(DateTimeUtil.INSTANCE.getSdfDayCommaDateMonthYear().format(mModel.getDate()));

        // Observers
        final Observer<SalesSummary> salesSummaryObserver = salesSummary -> {
            mProductHighlightTextView.setText(WardahUtil.INSTANCE.getSummaryInputSales(salesSummary.getProductHighlightPcs(), salesSummary.getProductHighlightAmount()));
            mProductFocusTextView.setText(WardahUtil.INSTANCE.getSummaryInputSales(salesSummary.getProductFocusPcs(), salesSummary.getProductFocusAmount()));
        };
        mModel.getSalesSummary().observe(this, salesSummaryObserver);

        final Observer<Boolean> isSubmitSuccessObserver = isSuccess -> {
            materialDialog.dismiss();

            if (isSuccess != null && isSuccess) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        };
        mModel.isSubmitSuccess().observe(this, isSubmitSuccessObserver);

        final Observer<Boolean> isEditSuccessObserver = isSuccess -> {
            materialDialog.dismiss();

            if (isSuccess != null && isSuccess) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        };
        mModel.isEditSuccess().observe(this, isEditSuccessObserver);

        mModel.getSalesEditProductsMutableLiveData().observe(this, sales -> {
            switch (mModel.getViewMode()) {
                case VIEW:
                    nominalEditText.setEnabled(false);
                    pcsEditText.setEnabled(false);
                    // Prevent keyboard to show
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                case EDIT:
                    nominalEditText.setText(String.valueOf(sales.getSales().getAmount()));
                    pcsEditText.setText(String.valueOf(sales.getSales().getQuantity()));
                    break;
            }
        });

        mModel.getProductHighlightLiveData().observe(this, productHighlightList -> {
            if (productHighlightList != null && productHighlightList.size() > 0) {
                mProductHighlightSectionHeader.setVisibility(View.VISIBLE);
                mProductHighlightAdapter.notifyDataSetChanged();
                ProductRecap productRecap = calculateProductRecap(productHighlightList);
                mProductHighlightTextView.setText(WardahUtil.INSTANCE.getSummaryInputSales(productRecap.getPcs(), productRecap.getNominal()));
            } else {
                mProductHighlightSectionHeader.setVisibility(View.GONE);
            }
        });

        mModel.getProductFocusMutableLiveData().observe(this, productFocusList -> {
            if (productFocusList != null && productFocusList.size() > 0) {
                mProductFocusSectionHeader.setVisibility(View.VISIBLE);
                mProductFocusAdapter.notifyDataSetChanged();
                ProductRecap productRecap = calculateProductRecap(productFocusList);
                mProductFocusTextView.setText(WardahUtil.INSTANCE.getSummaryInputSales(productRecap.getPcs(), productRecap.getNominal()));
            } else {
                mProductFocusSectionHeader.setVisibility(View.GONE);
            }
        });

        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (mModel.getViewMode()) {
            case VIEW:
                break;
            case EDIT:
                getMenuInflater().inflate(R.menu.menu_edit, menu);
                break;
            case INPUT:
                getMenuInflater().inflate(R.menu.menu_input, menu);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                attemptCancel();
                return true;
            case R.id.action_save:
                attemptSubmit(ViewMode.INPUT);
                return true;
            case R.id.action_edit:
                attemptSubmit(ViewMode.EDIT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        attemptCancel();
    }

    @Override
    public void onAdd(WardahItemType type, ProductHighlight productHighlight) {
        switch (type) {
            case PRODUCT_HIGHLIGHT:
                mModel.addOrRemoveProductHighlightQty(productHighlight);
                break;
            case PRODUCT_FOCUS:
                mModel.addOrRemoveProductFocusQty(productHighlight);
                break;
        }
    }

    @Override
    public void onRemove(WardahItemType type, ProductHighlight productHighlight) {
        switch (type) {
            case PRODUCT_HIGHLIGHT:
                mModel.addOrRemoveProductHighlightQty(productHighlight);
                break;
            case PRODUCT_FOCUS:
                mModel.addOrRemoveProductFocusQty(productHighlight);
                break;
        }
    }

    private void attemptCancel() {
        switch (mModel.getViewMode()) {
            case EDIT:
            case VIEW:
                finish();
                break;
            default:
                new MaterialDialog.Builder(this)
                        .title(R.string.title_cancel)
                        .content(R.string.message_back_confirmation)
                        .positiveText(R.string.action_ok)
                        .positiveColorRes(R.color.colorPrimary)
                        .negativeText(R.string.action_cancel)
                        .negativeColorRes(R.color.colorAccent)
                        .onPositive((dialog, which) -> finish())
                        .show();
                break;
        }
    }

    private void attemptSubmit(ViewMode viewMode) {
        nominalEditText.setError(null);
        pcsEditText.setError(null);

        String nominalStr = nominalEditText.getText().toString();
        String pcsStr = pcsEditText.getText().toString();
        List<ProductHighlight> productHighlightList = mProductHighlightAdapter.getData();
        List<ProductHighlight> productFocusList = mProductFocusAdapter.getData();
        ProductRecap productHighlightRecap = calculateProductRecap(productHighlightList);
        ProductRecap productFocusRecap = calculateProductRecap(productFocusList);
        Double totalProductHighlightAndFocus = productHighlightRecap.getNominal() + productFocusRecap.getNominal();
        Integer pcsProductHighlightAndFocus = productHighlightRecap.getPcs() + productFocusRecap.getPcs();

        Double nominal = null;
        Integer pcs = null;

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(nominalStr)) {
            nominalEditText.setError(getString(R.string.error_field_required));
            focusView = nominalEditText;
            cancel = true;
        } else if (TextUtils.isEmpty(pcsStr)) {
            pcsEditText.setError(getString(R.string.error_field_required));
            focusView = pcsEditText;
            cancel = true;
        } else {
            try {
                nominal = Double.parseDouble(nominalStr);
            } catch (Exception e) {
                nominalEditText.setError(getString(R.string.error_field_invalid));
                focusView = nominalEditText;
                cancel = true;
            }

            try {
                pcs = Integer.parseInt(pcsStr);
            } catch (Exception e) {
                pcsEditText.setError(getString(R.string.error_field_invalid));
                focusView = pcsEditText;
                cancel = true;
            }
        }

        if (cancel) {
            focusView.requestFocus();
        } else if (!validate(nominal, pcs, totalProductHighlightAndFocus, pcsProductHighlightAndFocus)) {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_error)
                    .content(R.string.message_submit_sales_validation_error)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .show();
        } else {
            final Long salesDate = mModel.getDate().getTime();

            final SalesRequest salesRequest = new SalesRequest(pcs, nominal);
            final List<ProductHighlightRequest> highlight = new ArrayList<>();
            for (ProductHighlight ph : productHighlightList) {
                int qty = ph.getQuantity() == null ? 0 : ph.getQuantity();
                highlight.add(new ProductHighlightRequest(ph.getId(), qty));
            }

            final List<ProductHighlightRequest> focus = new ArrayList<>();
            for (ProductHighlight pf : productFocusList) {
                int qty = pf.getQuantity() == null ? 0 : pf.getQuantity();
                focus.add(new ProductHighlightRequest(pf.getId(), qty));
            }

            switch (viewMode) {
                case EDIT:
                    final Long salesId = mModel.getSalesEditProductsMutableLiveData().getValue().getSalesId();

                    new MaterialDialog.Builder(this)
                            .title(R.string.title_edit_sales)
                            .content(R.string.message_edit_sales_confirmation)
                            .positiveText(R.string.action_ok)
                            .positiveColorRes(R.color.colorPrimary)
                            .negativeText(R.string.action_cancel)
                            .negativeColorRes(R.color.colorAccent)
                            .onPositive((dialog, which) -> {
                                mModel.edit(materialDialog, salesId, salesRequest, highlight, focus, salesDate);
                            })
                            .show();
                    break;
                default:
                    new MaterialDialog.Builder(this)
                            .title(R.string.title_create_sales)
                            .content(R.string.message_create_sales_confirmation)
                            .positiveText(R.string.action_ok)
                            .positiveColorRes(R.color.colorPrimary)
                            .negativeText(R.string.action_cancel)
                            .negativeColorRes(R.color.colorAccent)
                            .onPositive((dialog, which) -> {
                                mModel.submit(materialDialog, salesRequest, highlight, focus);
                            })
                            .show();
                    break;
            }
        }
    }

    private boolean validate(Double nominal, Integer pcs, Double totalProductHighlightAndFocus, Integer pcsProductHighlightAndFocus) {
        Log.d(TAG, "nominal:" + nominal + ", pcs:" + pcs + ", totalProductHighlightAndFocus:" + totalProductHighlightAndFocus + ", pcsProductHighlightAndFocus:" + pcsProductHighlightAndFocus);
        boolean valid = true;

        if (nominal < totalProductHighlightAndFocus || pcs < pcsProductHighlightAndFocus) {
            valid = false;
        }

        return valid;
    }

    private ProductRecap calculateProductRecap(List<ProductHighlight> products) {
        ProductRecap productRecap = new ProductRecap(0.00, 0);
        for (ProductHighlight productHighlight : products) {
            if (productHighlight.getQuantity() != null && productHighlight.getQuantity() != null && productHighlight.getPrice() != null) {
                productRecap.setNominal(productRecap.getNominal() + (productHighlight.getQuantity() * productHighlight.getPrice()));
                productRecap.setPcs(productRecap.getPcs() + productHighlight.getQuantity());
            }
        }
        return productRecap;
    }

    private class ProductRecap {
        private Double nominal;
        private Integer pcs;

        public ProductRecap(Double nominal, Integer pcs) {
            this.nominal = nominal;
            this.pcs = pcs;
        }

        public Double getNominal() {
            return nominal;
        }

        public void setNominal(Double nominal) {
            this.nominal = nominal;
        }

        public Integer getPcs() {
            return pcs;
        }

        public void setPcs(Integer pcs) {
            this.pcs = pcs;
        }
    }
}
