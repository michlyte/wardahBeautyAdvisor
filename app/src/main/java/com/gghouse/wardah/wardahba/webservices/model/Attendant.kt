package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import java.io.Serializable


class Attendant : WardahHistoryRecyclerViewItem, Serializable {
    val attendantId: Long
    val name: String
    val mobileNumber: String
    val email: String
    val address: String

    constructor(attendantId: Long, name: String, mobileNumber: String, email: String, address: String) {
        this.attendantId = attendantId
        this.name = name
        this.mobileNumber = mobileNumber
        this.email = email
        this.address = address
    }
}
