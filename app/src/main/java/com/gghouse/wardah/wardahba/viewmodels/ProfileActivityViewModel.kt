package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.Profile
import com.gghouse.wardah.wardahba.webservices.response.ProfileResponse

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileActivityViewModel : ViewModel() {
    val profileMutableLiveData: MutableLiveData<Profile> by lazy {
        MutableLiveData<Profile>()
    }

    fun callProfileData() {
        if (SessionUtil.validateLoginSession()) {
            val callProfile = ApiClient.client.apiProfile(SessionUtil.getUserId()!!)
            callProfile.enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(call: Call<ProfileResponse>, response: Response<ProfileResponse>) {
                    if (response.isSuccessful) {
                        val profileResponse = response.body()
                        when (profileResponse!!.code) {
                            Config.CODE_200 -> profileMutableLiveData.postValue(profileResponse.data)
                            else -> ToastUtil.retrofitFailMessage(TAG, profileResponse.status!!)
                        }
                    } else {
                        ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                    }
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    ToastUtil.retrofitFailMessage(TAG, t.message.toString())
                }
            })
        }
    }

    companion object {

        private val TAG = ProfileActivityViewModel::class.java.simpleName
    }
}
