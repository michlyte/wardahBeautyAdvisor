package com.gghouse.wardah.wardahba.dummy

import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.models.BAHomepageItem
import com.gghouse.wardah.wardahba.utils.ImageUtil
import java.util.*

object HomePageDummy {
    var personalDailySales: List<BAHomepageItem> = object : ArrayList<BAHomepageItem>() {
        init {
            add(BAHomepageItem(ImageUtil.homepageSales, 5000000.00, 50))
            add(BAHomepageItem(ImageUtil.homepageProductHighlight, 2500000.00, 25))
            add(BAHomepageItem(ImageUtil.homepageProductFocus, 500000.00, 5))
        }
    }

    var salesRecap: List<BAHomepageItem> = object : ArrayList<BAHomepageItem>() {
        init {
            add(BAHomepageItem(ImageUtil.homepageSales, 200000000.00, 550))
            add(BAHomepageItem(ImageUtil.homepageProductHighlight, 100000000.00, 210))
            add(BAHomepageItem(ImageUtil.homepageProductFocus, 50000000.00, 110))
        }
    }

    var salesByCategory: List<BAHomepageItem> = object : ArrayList<BAHomepageItem>() {
        init {
            add(BAHomepageItem(R.mipmap.ic_launcher, 100000000.00, 100))
            add(BAHomepageItem(R.mipmap.ic_launcher, 100000000.00, 100))
            add(BAHomepageItem(R.mipmap.ic_launcher, 100000000.00, 100))
        }
    }

    var totalSalesHarianVsBulananList: List<BAHomepageItem> = object : ArrayList<BAHomepageItem>() {
        init {
            add(BAHomepageItem(R.drawable.ic_sales_turnover, 1000000.00, 100))
            add(BAHomepageItem(R.drawable.ic_sales_turnover, 200000000.00, 10000))
        }
    }

    var salesVsTargetVsCompetitor: List<BAHomepageItem> = object : ArrayList<BAHomepageItem>() {
        init {
            add(BAHomepageItem(R.drawable.ic_sales_turnover, 200000000.00, 10000))
            add(BAHomepageItem(R.drawable.ic_sales_turnover, 210000000.00, 10000))
            add(BAHomepageItem(R.drawable.ic_sales_turnover, 220000000.00, 10000))
        }
    }

    var buyerVsVisitor: List<BAHomepageItem> = object : ArrayList<BAHomepageItem>() {
        init {
            add(BAHomepageItem(R.drawable.ic_buyer, 0.00, 255))
            add(BAHomepageItem(R.drawable.ic_visitor, 0.00, 1000))
        }
    }

    var buyingPower: List<BAHomepageItem> = object : ArrayList<BAHomepageItem>() {
        init {
            add(BAHomepageItem(ImageUtil.buyingPower1, 0.00, 150))
            add(BAHomepageItem(ImageUtil.buyingPower2, 0.00, 150))
            add(BAHomepageItem(ImageUtil.buyingPower3, 0.00, 150))
            add(BAHomepageItem(ImageUtil.buyingPower4, 0.00, 150))
            add(BAHomepageItem(ImageUtil.buyingPower5, 0.00, 150))
        }
    }

}
