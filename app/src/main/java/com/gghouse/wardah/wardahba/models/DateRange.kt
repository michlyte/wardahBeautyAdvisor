package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.utils.DateTimeUtil

class DateRange {
    var startDate: Long
    var endDate: Long

    constructor(startDate: Long, endDate: Long) {
        this.startDate = startDate
        this.endDate = endDate
    }

    val startDateStr: String
        get() = DateTimeUtil.sdfFilter.format(startDate)

    val endDateStr: String
        get() = DateTimeUtil.sdfFilter.format(endDate)
}