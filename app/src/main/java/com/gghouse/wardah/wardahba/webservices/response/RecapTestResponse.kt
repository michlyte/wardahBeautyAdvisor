package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.models.TestHistoryHeader
import com.gghouse.wardah.wardahba.webservices.model.Pagination

/**
 * Created by michaelhalim on 5/6/17.
 */

class RecapTestResponse : GenericResponse() {
    var data: TestHistoryHeader? = null
    var pagination: Pagination? = null
}
