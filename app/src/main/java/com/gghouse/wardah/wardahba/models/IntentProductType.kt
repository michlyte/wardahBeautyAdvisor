package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.webservices.model.ProductType

import java.io.Serializable

class IntentProductType(var objects: List<ProductType>?) : Serializable
