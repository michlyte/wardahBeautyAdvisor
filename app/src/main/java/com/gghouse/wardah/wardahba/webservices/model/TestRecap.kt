package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem

class TestRecap : WardahHistoryRecyclerViewItem {
    val averageScore: Float
    val accuracy: Integer
    val totalQuestion: Integer

    constructor(averageScore: Float, accuracy: Integer, totalQuestion: Integer) : super() {
        this.averageScore = averageScore
        this.accuracy = accuracy
        this.totalQuestion = totalQuestion
    }
}