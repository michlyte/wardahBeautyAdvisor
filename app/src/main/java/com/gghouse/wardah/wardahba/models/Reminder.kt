package com.gghouse.wardah.wardahba.models

import java.io.Serializable

class Reminder : Serializable {
    var frequent: Int? = null
    var value: Int? = null
    var isChecked: Boolean
    var name: String

    constructor(frequent: Int?, value: Int?, isChecked: Boolean, name: String) {
        this.frequent = frequent
        this.value = value
        this.isChecked = isChecked
        this.name = name
    }
}