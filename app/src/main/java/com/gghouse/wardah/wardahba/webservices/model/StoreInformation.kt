package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import com.gghouse.wardah.wardahba.utils.WardahUtil
import java.io.Serializable

class StoreInformation : WardahHistoryRecyclerViewItem, Serializable {
    var storeId: Long? = null
    val dc: String
    val province: String
    val period: String
    val totalBA: Int
    val evaluationCompleted: Int
    val targetSales: Double
    val competitorTurnover: Double
    val productTypes: List<ProductType>
    val storeSalesDailyHistory: List<DailySales>
    val storeSalesMonthlyHistory: List<MonthlySales>
    val locationId: Long

    constructor(dc: String, province: String, period: String, totalBA: Int, evaluationCompleted: Int, targetSales: Double, competitorTurnover: Double, productTypes: List<ProductType>, storeSalesDailyHistory: List<DailySales>, storeSalesMonthlyHistory: List<MonthlySales>, locationId: Long) : super() {
        this.dc = dc
        this.province = province
        this.period = period
        this.totalBA = totalBA
        this.evaluationCompleted = evaluationCompleted
        this.targetSales = targetSales
        this.competitorTurnover = competitorTurnover
        this.productTypes = productTypes
        this.storeSalesDailyHistory = storeSalesDailyHistory
        this.storeSalesMonthlyHistory = storeSalesMonthlyHistory
        this.locationId = locationId
    }

    val targetSalesStr: String
        get() = WardahUtil.getAmount(targetSales)

    val competitorTurnoverStr: String
        get() = WardahUtil.getAmount(competitorTurnover)
}