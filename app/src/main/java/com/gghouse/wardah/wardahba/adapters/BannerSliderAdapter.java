package com.gghouse.wardah.wardahba.adapters;

import java.util.List;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class BannerSliderAdapter extends SliderAdapter {

    private List<String> mDataSet;

    public BannerSliderAdapter(List<String> dataSet) {
        mDataSet = dataSet;
    }

    @Override
    public int getItemCount() {
        return mDataSet == null ? 0 : mDataSet.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        viewHolder.bindImageSlide(mDataSet.get(position));
    }

    public String getUrl(int position) {
        return mDataSet.get(position);
    }

    public void setData(List<String> data) {
        mDataSet = data;
    }

    public List<String> getData() {
        return mDataSet;
    }
}