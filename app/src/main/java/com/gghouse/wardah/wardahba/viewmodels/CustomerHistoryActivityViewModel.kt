package com.gghouse.wardah.wardahba.viewmodels

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.adapters.WardahHistoryAdapter
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.dummy.CustomerDummy
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.Customer
import com.gghouse.wardah.wardahba.webservices.model.Pagination
import com.gghouse.wardah.wardahba.webservices.response.CustomerListResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class CustomerHistoryActivityViewModel : ViewModel() {

    val customerHistoryMutableLiveData: MutableLiveData<List<Customer>> by lazy {
        MutableLiveData<List<Customer>>()
    }
    val paginationMutableLiveData: MutableLiveData<Pagination> by lazy {
        MutableLiveData<Pagination>()
    }

    var mPage: Int = 0

    fun getData(adapter: WardahHistoryAdapter, page: Int, beginDate: Date?, endDate: Date?) {
        val userId = SessionUtil.getUserId()
        if (userId == null) {
            SessionUtil.loggingOut()
        } else {
            mPage = page
            when (Config.mode) {
                ModeEnum.DUMMY_DEVELOPMENT -> callCustomerHistoryDummyData(adapter, page)
                ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> callCustomerHistoryData(userId, page, beginDate, endDate)
            }
        }
    }

    private fun callCustomerHistoryDummyData(adapter: WardahHistoryAdapter, page: Int) {
        Handler().postDelayed({
            if (page == 0) {
                val pagination = Pagination()
                pagination.last = false
                paginationMutableLiveData.postValue(pagination)
                customerHistoryMutableLiveData.postValue(CustomerDummy.CUSTOMER_SIMPLE_LIST)
            } else {
                /// Remove loading item
                adapter.remove(adapter.itemCount - 1)

                val pelangganList = ArrayList<Customer>()
                //Load data
                val index = adapter.itemCount
                val end = index + 20
                for (i in index until end) {
                    val customer = Customer(
                            i.toLong(), "Name " + (i + 1), "081234566789",
                            "name" + (i + 1) + "@mmail.com", "Alamat " + (i + 1),
                            Date().time)
                    pelangganList.add(customer)
                }

                // Manipulate pagination for dummy data
                val pagination = Pagination()
                pagination.last = mPage == Config.MAX_LOAD_MORE_FOR_DUMMY_DATA
                paginationMutableLiveData.postValue(pagination)
                customerHistoryMutableLiveData.postValue(pelangganList)
            }
        }, Config.DELAY_LOAD_MORE.toLong())
    }

    private fun callCustomerHistoryData(userId: Long?, page: Int, beginDate: Date?, endDate: Date?) {
        var beginDateStr: String? = null
        var endDateStr: String? = null
        if (beginDate != null && endDate != null) {
            beginDateStr = DateTimeUtil.sdfFilter.format(beginDate)
            endDateStr = DateTimeUtil.sdfFilter.format(endDate)
        }

        val callSalesList = ApiClient.client.apiCustomer(userId!!, page, Config.HISTORY_ITEM_PER_PAGE, beginDateStr, endDateStr)
        callSalesList.enqueue(object : Callback<CustomerListResponse> {
            override fun onResponse(call: Call<CustomerListResponse>, response: Response<CustomerListResponse>) {
                if (response.isSuccessful) {
                    val customerListResponse = response.body()
                    when (customerListResponse!!.code) {
                        Config.CODE_200 -> {
                            paginationMutableLiveData.postValue(customerListResponse.pagination)
                            customerHistoryMutableLiveData.postValue(customerListResponse.data)
                        }
                        else -> {
                            customerHistoryMutableLiveData.postValue(null)
                            ToastUtil.retrofitFailMessage(TAG, customerListResponse.status!!)
                        }
                    }
                } else {
                    customerHistoryMutableLiveData.postValue(null)
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<CustomerListResponse>, t: Throwable) {
                customerHistoryMutableLiveData.postValue(null)
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    companion object {

        private val TAG = CustomerHistoryActivityViewModel::class.java.simpleName
    }
}
