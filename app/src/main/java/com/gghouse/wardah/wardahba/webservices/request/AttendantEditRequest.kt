package com.gghouse.wardah.wardahba.webservices.request

class AttendantEditRequest {
    val attendantId: Long
    val userId: Long
    val name: String
    val mobileNumber: String
    val email: String
    val address: String

    constructor(attendantId: Long, userId: Long, name: String, mobileNumber: String, email: String, address: String) {
        this.attendantId = attendantId
        this.userId = userId
        this.name = name
        this.mobileNumber = mobileNumber
        this.email = email
        this.address = address
    }
}