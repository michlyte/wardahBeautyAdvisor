package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Pagination
import com.gghouse.wardah.wardahba.webservices.model.Test

/**
 * Created by michaelhalim on 5/6/17.
 */

class HistoryTestResponse : GenericResponse() {
    var data: List<Test>? = null
    var pagination: Pagination? = null
}
