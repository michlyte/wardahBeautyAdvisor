package com.gghouse.wardah.wardahba.enumerations

import com.gghouse.wardah.wardahba.R

enum class HomePageMenuEnum(var resourceId: Int) {
    SALES(R.string.title_sales),
    TEST(R.string.title_tes),
    CUSTOMER(R.string.title_pelanggan),
    STORE(R.string.title_store),
    EVENT(R.string.title_event),
    VISIT(R.string.title_kunjungan),
    INPUT_TARGET(R.string.text_input_target);

    companion object {
        fun getHomePageMenuEnum(id: Int): HomePageMenuEnum? {
            for (homePageMenuEnum in values()) {
                if (homePageMenuEnum.resourceId == id) {
                    return homePageMenuEnum
                }
            }
            return null
        }
    }
}
