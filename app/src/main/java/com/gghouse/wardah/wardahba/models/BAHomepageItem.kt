package com.gghouse.wardah.wardahba.models

import com.amulyakhare.textdrawable.TextDrawable
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.WardahApp

class BAHomepageItem {
    var textDrawable: TextDrawable? = null
    var imageUrl: String? = null
    var imageResId: Int? = null
        private set
    var title: String? = null
    var value: Double? = null
    var pcs: Int? = null

    val pcsStr: String
        get() = if (pcs == null) {
            WardahApp.getInstance().getString(R.string.text_empty_value)
        } else {
            pcs.toString()
        }

    constructor(imageResId: Int, value: Double?, pcs: Int?) {
        this.imageResId = imageResId
        this.value = value
        this.pcs = pcs
    }

    constructor(textDrawable: TextDrawable, value: Double?, pcs: Int?) {
        this.textDrawable = textDrawable
        this.value = value
        this.pcs = pcs
    }

    constructor(imageUrl: String, value: Double?, pcs: Int?) {
        this.imageUrl = imageUrl
        this.value = value
        this.pcs = pcs
    }

    constructor(imageResId: Int?, pcs: Int?) {
        this.imageResId = imageResId
        this.pcs = pcs
    }

    fun setImageResId(imageResId: Int) {
        this.imageResId = imageResId
    }
}
