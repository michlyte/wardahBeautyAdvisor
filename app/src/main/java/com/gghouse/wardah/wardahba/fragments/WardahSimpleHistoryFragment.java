package com.gghouse.wardah.wardahba.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.attendant.InputAttendantActivity;
import com.gghouse.wardah.wardahba.activities.sales.InputSalesTargetActivity;
import com.gghouse.wardah.wardahba.activities.visit.BaInformationActivity;
import com.gghouse.wardah.wardahba.activities.visit.EvaluationBAActivity;
import com.gghouse.wardah.wardahba.adapters.WardahHistoryAdapter;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.ChecklistBAListener;
import com.gghouse.wardah.wardahba.interfaces.OnLoadMoreListListener;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.viewmodels.WardahSimpleHistoryFragmentViewModel;
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA;
import com.gghouse.wardah.wardahba.webservices.model.Customer;
import com.gghouse.wardah.wardahba.webservices.model.Event;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;
import com.gghouse.wardah.wardahba.webservices.model.SalesTarget;

import java.util.ArrayList;

import mehdi.sakout.dynamicbox.DynamicBox;

public class WardahSimpleHistoryFragment extends Fragment implements WardahListener, ChecklistBAListener {

    private static final String TAG = WardahSimpleHistoryFragment.class.getSimpleName();
    private static final String ARG_TYPE = "ARG_TYPE";
    private static final String ARG_EVENT = "ARG_EVENT";
    private static final String ARG_CALENDAR_ID = "ARG_CALENDAR_ID";
    private static final String ARG_LOCATION_ID = "ARG_LOCATION_ID";
    private static final int ON_ACTIVITY_RESULT = 1;

    private WardahSimpleHistoryFragmentViewModel mModel;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private OnLoadMoreListListener mOnLoadMoreListListener;
    private DynamicBox mDynamicBox;
    private WardahItemType mType;

    private RecyclerView mRecyclerView;
    private WardahHistoryAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView mTvMessage;

    public WardahSimpleHistoryFragment() {

    }

    public static WardahSimpleHistoryFragment newInstance(WardahItemType type) {
        WardahSimpleHistoryFragment fragment = new WardahSimpleHistoryFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    public static WardahSimpleHistoryFragment newInstance(WardahItemType type, Long calendarId, Long locationId) {
        WardahSimpleHistoryFragment fragment = new WardahSimpleHistoryFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        args.putSerializable(ARG_CALENDAR_ID, calendarId);
        args.putSerializable(ARG_LOCATION_ID, locationId);
        fragment.setArguments(args);
        return fragment;
    }

    public static WardahSimpleHistoryFragment newInstance(WardahItemType type, Long calendarId) {
        WardahSimpleHistoryFragment fragment = new WardahSimpleHistoryFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        args.putSerializable(ARG_CALENDAR_ID, calendarId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_history, container, false);

        mModel = ViewModelProviders.of(this).get(WardahSimpleHistoryFragmentViewModel.class);

        try {
            mModel.setEvent((Event) getArguments().get(ARG_EVENT));
            mModel.setCalendarId(getArguments().getLong(ARG_CALENDAR_ID, 0L));
            mModel.setLocationId(getArguments().getLong(ARG_LOCATION_ID, 0L));
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

        mType = (WardahItemType) getArguments().get(ARG_TYPE);

        mSwipeRefreshLayout = view.findViewById(R.id.srl_swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(() -> onSwipeToRefresh());

        mRecyclerView = view.findViewById(R.id.rv_list);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new WardahHistoryAdapter(getContext(),
                mType, mRecyclerView, new ArrayList<>(), this, this);
        mRecyclerView.setAdapter(mAdapter);

        mOnLoadMoreListListener = () -> {
            new Handler().post(() -> {
                mAdapter.add(null);
                mAdapter.notifyItemInserted(mAdapter.getItemCount() - 1);
            });
            mModel.getData(mType, mAdapter, mModel.mPage + 1);
        };

        /*
         * Empty Message
         */
        mTvMessage = view.findViewById(R.id.tv_message);

        mDynamicBox = new DynamicBox(getActivity(), mRecyclerView);

        mModel.getWardahHistoryMutableLiveData().observe(this, list -> {
            mDynamicBox.hideAll();
            mSwipeRefreshLayout.setRefreshing(false);

            if (mModel.mPage == 0) {
                if (list != null) {
                    mAdapter.clear();
                    mAdapter.addAll(list);
                    mRecyclerView.setAdapter(mAdapter);

                    // Empty Message
                    if (list.size() <= 0) {
                        mTvMessage.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (list != null) {
                    mAdapter.addAll(list);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });

        mModel.getPaginationMutableLiveData().observe(this, pagination -> {
            if (pagination != null) {
                if (pagination.getLast()) {
                    mAdapter.removeOnLoadMoreListener();
                } else {
                    mAdapter.setLoaded();
                    mAdapter.setOnLoadMoreListener(mOnLoadMoreListListener);
                }
            }
        });

        mModel.getEvaluationQuestionsMutableLiveData().observe(this, evaluationQuestions -> {
            if (evaluationQuestions != null) {
                Intent iEvaluation = new Intent(getContext(), EvaluationBAActivity.class);
                iEvaluation.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
                iEvaluation.putExtra(IntentUtil.Companion.getEVALUTION_QUESTIONS(), evaluationQuestions);
                startActivityForResult(iEvaluation, ON_ACTIVITY_RESULT);
            }
        });

        mDynamicBox.showLoadingLayout();
        mModel.getData(mType, mAdapter, 0);

        return view;
    }

    @Override
    public void onClick(WardahHistoryRecyclerViewItem item, ViewMode viewMode) {
        switch (mType) {
            case CATEGORY:
                ProductType productType = (ProductType) item;
                break;
            case ATTENDANT:
                Intent intent = new Intent(getContext(), InputAttendantActivity.class);
                intent.putExtra(IntentUtil.Companion.getCUSTOMER(), (Customer) item);
                intent.putExtra(IntentUtil.Companion.getVIEW_MODE(), viewMode);

                switch (viewMode) {
                    case EDIT:
                        startActivityForResult(intent, ON_ACTIVITY_RESULT);
                        break;
                    case VIEW:
                        startActivity(intent);
                        break;
                }
                break;
            case INPUT_TARGET:
                Intent iInputSalesTarget = new Intent(getContext(), InputSalesTargetActivity.class);
                iInputSalesTarget.putExtra(IntentUtil.Companion.getINPUT_TARGET(), (SalesTarget) item);
                iInputSalesTarget.putExtra(IntentUtil.Companion.getVIEW_MODE(), viewMode);

                switch (viewMode) {
                    case EDIT:
                        startActivityForResult(iInputSalesTarget, ON_ACTIVITY_RESULT);
                        break;
                    case VIEW:
                        startActivity(iInputSalesTarget);
                        break;
                }
                break;
        }
    }

    @Override
    public void onClick(ChecklistBA checklistBA) {
        Intent iBAInformation = new Intent(getContext(), BaInformationActivity.class);
        iBAInformation.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
        iBAInformation.putExtra(IntentUtil.Companion.getUSER_ID(), checklistBA.getUserId());
        startActivity(iBAInformation);
    }

    @Override
    public void onEvaluationButtonClicked(ChecklistBA checklistBA) {
        mModel.callEvaluationQuestions(checklistBA.getUserId());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ON_ACTIVITY_RESULT && resultCode == Activity.RESULT_OK) {
            mModel.getData(mType, mAdapter, 0);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onSwipeToRefresh() {
        mModel.getData(mType, mAdapter, 0);
    }
}
