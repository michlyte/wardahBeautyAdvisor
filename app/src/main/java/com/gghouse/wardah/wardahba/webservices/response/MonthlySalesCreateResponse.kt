package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.ProductType

class MonthlySalesCreateResponse : GenericResponse() {
    var data: List<ProductType>? = null
}
