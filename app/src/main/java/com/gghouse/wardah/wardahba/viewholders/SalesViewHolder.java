package com.gghouse.wardah.wardahba.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.webservices.model.Sales;

/**
 * Created by michaelhalim on 5/9/17.
 */

public class SalesViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    private final CardView mCvContainer;
    private final LinearLayout mLlContainer;
    private final TextView mTVTitle;
    private final TextView mTVDate;
    private final TextView mTVValue;
    private final TextView mTVValuePieceSeparator;
    private final TextView mTVPiece;
    private final ImageView mIVEdit;
    private final ImageView mIVProductHighlight;
    private final ImageView mIVProductFocus;
    private final TextView mTVNumOfProductHighlight;
    private final TextView mTVNumOfProductFocus;
    private final TextView mTVProductHighlightSeparator;
    private final TextView mTVProductHighlightFocusSeparator;
    private final TextView mTVProductFocusSeparator;
    public Sales mItem;

    public SalesViewHolder(View view) {
        super(view);
        mView = view;
        mCvContainer = mView.findViewById(R.id.cv);
        mLlContainer = mView.findViewById(R.id.ll_container);
        mTVTitle = mView.findViewById(R.id.tv_title);
        mTVDate = mView.findViewById(R.id.tv_date);
        mTVValue = mView.findViewById(R.id.tv_value);
        mTVValuePieceSeparator = mView.findViewById(R.id.tvAmountPcsSeparator);
        mTVPiece = mView.findViewById(R.id.tv_pcs);
        mIVEdit = mView.findViewById(R.id.iv_edit);
        mIVProductHighlight = mView.findViewById(R.id.ivProductHighlightIcon);
        mIVProductFocus = mView.findViewById(R.id.ivProductFocusIcon);
        mTVNumOfProductHighlight = mView.findViewById(R.id.tvProductHighlightValue);
        mTVNumOfProductFocus = mView.findViewById(R.id.tvProductFocusValue);
        mTVProductHighlightSeparator = mView.findViewById(R.id.tvProductHighlightSeparator);
        mTVProductHighlightFocusSeparator = mView.findViewById(R.id.tvProductHighlightFocusSeparator);
        mTVProductFocusSeparator = mView.findViewById(R.id.tvProductFocusSeparator);

        mIVProductHighlight.setImageDrawable(ImageUtil.INSTANCE.getDrawableProductHighlight());
        mIVProductFocus.setImageDrawable(ImageUtil.INSTANCE.getDrawableProductFocus());
    }

    private void setupWeekendView(Context context) {
        if (DateTimeUtil.INSTANCE.isWeekend(mItem.getSalesDate())) {
            mTVTitle.setTextColor(ContextCompat.getColor(context, R.color.colorHistoryHeader6));
        } else {
            mTVTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }
    }

    private void setupRowView(Context context) {
        if (mItem.getEditable() != null && mItem.getEditable()) {
            mCvContainer.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

            int color = ContextCompat.getColor(context, android.R.color.white);
            mTVTitle.setTextColor(color);
            mTVDate.setTextColor(color);
            mTVPiece.setTextColor(color);
            mTVValue.setTextColor(color);
            mTVNumOfProductHighlight.setTextColor(color);
            mTVNumOfProductFocus.setTextColor(color);
            mTVValuePieceSeparator.setTextColor(color);
            mTVProductHighlightSeparator.setTextColor(color);
            mTVProductHighlightFocusSeparator.setTextColor(color);
            mTVProductFocusSeparator.setTextColor(color);
        } else {
            mCvContainer.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.white));

            int accentColor = ContextCompat.getColor(context, R.color.colorAccent);
            int blackColor = ContextCompat.getColor(context, android.R.color.black);
            mTVDate.setTextColor(blackColor);
            mTVPiece.setTextColor(accentColor);
            mTVValue.setTextColor(accentColor);
            mTVNumOfProductHighlight.setTextColor(accentColor);
            mTVNumOfProductFocus.setTextColor(accentColor);
            mTVValuePieceSeparator.setTextColor(accentColor);
            mTVProductHighlightSeparator.setTextColor(accentColor);
            mTVProductHighlightFocusSeparator.setTextColor(accentColor);
            mTVProductFocusSeparator.setTextColor(accentColor);
        }
    }

    private void setupActionEvent(WardahListener wardahListener) {
        ViewMode viewMode;
        if (mItem.getEditable() != null && mItem.getEditable()) {
            viewMode = ViewMode.EDIT;
        } else {
            viewMode = ViewMode.VIEW;
        }

        mLlContainer.setOnClickListener(v -> wardahListener.onClick(mItem, viewMode));
    }

    public void setData(Context context, Sales sales, WardahListener wardahListener) {
        mItem = sales;
        String day = DateTimeUtil.INSTANCE.getSdfDay().format(sales.getSalesDate());
        String date = DateTimeUtil.INSTANCE.getSdfDateMonthYear().format(sales.getSalesDate());
        mTVTitle.setText(day);
        mTVDate.setText(date);
        mTVValue.setText(WardahUtil.INSTANCE.getAmount(sales.getAmount()));
        mTVPiece.setText(String.valueOf(sales.getQuantity()));
        mTVNumOfProductHighlight.setText(String.valueOf(sales.getProductHighlight()));
        mTVNumOfProductFocus.setText(String.valueOf(sales.getProductFocus()));

        setupWeekendView(context);
        setupRowView(context);
        setupActionEvent(wardahListener);
    }
}
