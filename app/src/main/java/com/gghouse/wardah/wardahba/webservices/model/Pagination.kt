package com.gghouse.wardah.wardahba.webservices.model

/**
 * Created by michael on 3/13/2017.
 */

class Pagination {
    var last: Boolean = false
    var totalPages: Int? = null
    var totalElements: Int? = null
    var isFirst: Boolean = false
    var numberOfElements: Int? = null
    var size: Int? = null
    var number: Int? = null
}
