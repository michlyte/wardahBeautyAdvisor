package com.gghouse.wardah.wardahba.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.models.EvaluationQuestionInner;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.BAInformationOverview;
import com.gghouse.wardah.wardahba.webservices.response.BAInformationResponse;
import com.gghouse.wardah.wardahba.webservices.response.EvaluationQuestionListResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BAInformationOverviewFragmentViewModel extends ViewModel {
    private static final String TAG = BAInformationOverviewFragmentViewModel.class.getSimpleName();

    private MutableLiveData<BAInformationOverview> baInformationOverviewMutableLiveData;
    private MutableLiveData<EvaluationQuestionInner> evaluationQuestionsMutableLiveData;
    private Long userId;

    public MutableLiveData<BAInformationOverview> getBaInformationOverviewMutableLiveData() {
        if (baInformationOverviewMutableLiveData == null) {
            baInformationOverviewMutableLiveData = new MutableLiveData<>();
        }
        return baInformationOverviewMutableLiveData;
    }

    public MutableLiveData<EvaluationQuestionInner> getEvaluationQuestionsMutableLiveData() {
        if (evaluationQuestionsMutableLiveData == null) {
            evaluationQuestionsMutableLiveData = new MutableLiveData<>();
        }
        return evaluationQuestionsMutableLiveData;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void getData() {
        switch (Config.Companion.getMode()) {
            case DEVELOPMENT:
            case PRODUCTION:
                callBAInformationData(userId);
                break;
        }
    }

    public void setEvaluations() {
//        getOverview().getValue().setEvaluations(ChecklistDummy.INSTANCE.getEvaluationBAList());
    }

    private void callBAInformationData(long userId) {
        Call<BAInformationResponse> callBAInformationOverview = ApiClient.INSTANCE.getClient().apiBAInformation(userId);
        callBAInformationOverview.enqueue(new Callback<BAInformationResponse>() {
            @Override
            public void onResponse(Call<BAInformationResponse> call, Response<BAInformationResponse> response) {
                if (response.isSuccessful()) {
                    BAInformationResponse baInformationResponse = response.body();
                    switch (baInformationResponse.getCode()) {
                        case Config.CODE_200:
                            getBaInformationOverviewMutableLiveData().postValue(baInformationResponse.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, baInformationResponse.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<BAInformationResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callEvaluationQuestions(long userId) {
        Call<EvaluationQuestionListResponse> callEvalutionQuestions = ApiClient.INSTANCE.getClient().apiEvaluationQuestions(userId);
        callEvalutionQuestions.enqueue(new Callback<EvaluationQuestionListResponse>() {
            @Override
            public void onResponse(Call<EvaluationQuestionListResponse> call, Response<EvaluationQuestionListResponse> response) {
                if (response.isSuccessful()) {
                    EvaluationQuestionListResponse evaluationQuestionListResponse = response.body();
                    switch (evaluationQuestionListResponse.getCode()) {
                        case Config.CODE_200:
                            EvaluationQuestionInner evaluationQuestionInner = new EvaluationQuestionInner(userId, 0L, evaluationQuestionListResponse.getData());
                            getEvaluationQuestionsMutableLiveData().postValue(evaluationQuestionInner);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, evaluationQuestionListResponse.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<EvaluationQuestionListResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }
}
