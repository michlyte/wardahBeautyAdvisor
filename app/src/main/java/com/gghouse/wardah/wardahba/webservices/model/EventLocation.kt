package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class EventLocation : Serializable {
    var id: Long
    val formattedAddress: String
    val latitude: String
    val longitude: String

    constructor(id: Long, formattedAddress: String, latitude: String, longitude: String) {
        this.id = id
        this.formattedAddress = formattedAddress
        this.latitude = latitude
        this.longitude = longitude
    }
}