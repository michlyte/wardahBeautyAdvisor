package com.gghouse.wardah.wardahba.interfaces

interface OnLoadMoreListListener {
    fun onLoadMore()
}
