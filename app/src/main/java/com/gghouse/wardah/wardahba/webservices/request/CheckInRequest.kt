package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.CheckInLocation

class CheckInRequest {
    val calendarId: Long
    val checkInLocation: CheckInLocation
    val bpImage: String
    val boothImage: String

    constructor(calendarId: Long, checkInLocation: CheckInLocation, bpImage: String, boothImage: String) {
        this.calendarId = calendarId
        this.checkInLocation = checkInLocation
        this.bpImage = bpImage
        this.boothImage = boothImage
    }
}