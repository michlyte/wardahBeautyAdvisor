package com.gghouse.wardah.wardahba.activities.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.customer.CustomerActivity;
import com.gghouse.wardah.wardahba.activities.profile.ProfileActivity;
import com.gghouse.wardah.wardahba.activities.sales.SalesActivity;
import com.gghouse.wardah.wardahba.activities.store.StoreActivity;
import com.gghouse.wardah.wardahba.activities.test.TestActivity;
import com.gghouse.wardah.wardahba.adapters.HomeStoreAdapter;
import com.gghouse.wardah.wardahba.adapters.HomepageMenuItemAdapter;
import com.gghouse.wardah.wardahba.enumerations.HomePageMenuEnum;
import com.gghouse.wardah.wardahba.enumerations.HomepageStoreSection;
import com.gghouse.wardah.wardahba.enumerations.StoreSalesType;
import com.gghouse.wardah.wardahba.models.BAHomepageItem;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.viewmodels.HomeBAActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.BAHomeDaily;
import com.gghouse.wardah.wardahba.webservices.model.BAHomeMonthly;
import com.gghouse.wardah.wardahba.webservices.model.BAHomeSalesOverview;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;

import java.util.ArrayList;
import java.util.List;

public class HomeBAActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private static final String TAG = HomeBAActivity.class.getSimpleName();

    private HomeBAActivityViewModel mModel;

    private SwipeRefreshLayout mSwipeRefresh;

    private TextView mUsername;
    private GridView mMenuGridView;
    private HomepageMenuItemAdapter mMenuAdapter;

    // Personal Sales
    private View mPersonalSalesLayout;
    private TextView mPersonalSalesTimestampTextView;
    private HomeStoreAdapter mPersonalSalesAdapter;

    // Store Sales
    private View mStoreSalesLayout;
    private LinearLayout mStoreDailySalesLinearLayout;
    private Button mDailyStoreSalesButton;
    private Button mMonthlyStoreSalesButton;
    private TextView mStoreSalesTimestampTextView;

    private HomeStoreAdapter mSalesRecapAdapter;
    private HomeStoreAdapter mSalesByCategoryAdapter;
    private HomeStoreAdapter mDailyVsMonthlySalesAdapter;
    private HomeStoreAdapter mSalesVsTargetVsCompetitorAdapter;
    private HomeStoreAdapter mBuyerVsVisitorAdapter;
    private HomeStoreAdapter mBuyingPowerAdapter;

    private View mSalesByCategoryLayout;
    private View mSalesVsTargetVsCompetitorLayout;
    private View mDailyVsMonthlySalesLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_ba);

        mModel = ViewModelProviders.of(this).get(HomeBAActivityViewModel.class);

        mSwipeRefresh = findViewById(R.id.swipe_container);
        mSwipeRefresh.setOnRefreshListener(this);

        mUsername = findViewById(R.id.etHeaderUsername);
        TextView profileTextView = findViewById(R.id.etProfile);
        profileTextView.setOnClickListener(this);

        mMenuGridView = findViewById(R.id.gridview);
        mMenuAdapter = new HomepageMenuItemAdapter(new ArrayList<>());
        mMenuGridView.setAdapter(mMenuAdapter);

        // Personal Sales
        mPersonalSalesLayout = findViewById(R.id.dailySalesLayout);
        findViewById(R.id.detailDailySalesTextView).setOnClickListener(this);
        mPersonalSalesTimestampTextView = findViewById(R.id.dailySalesTimestamp);
        GridView dailySalesGridView = findViewById(R.id.dailySalesGridView);
        mPersonalSalesAdapter = new HomeStoreAdapter(this, HomepageStoreSection.DAILY_SALES_PERSONAL, new ArrayList<>());
        dailySalesGridView.setAdapter(mPersonalSalesAdapter);

        // Store Sales
        mStoreSalesLayout = findViewById(R.id.storeSalesLayout);
        findViewById(R.id.detailStoreSalesTextView).setOnClickListener(this);
        mDailyStoreSalesButton = findViewById(R.id.dailyStoreSalesButton);
        mDailyStoreSalesButton.setOnClickListener(this);
        mMonthlyStoreSalesButton = findViewById(R.id.monthlyStoreSalesButton);
        mMonthlyStoreSalesButton.setOnClickListener(this);
        mStoreSalesTimestampTextView = findViewById(R.id.tvStoreTimestamp);
        mStoreDailySalesLinearLayout = findViewById(R.id.llStoreDailySales);

        mSalesByCategoryLayout = findViewById(R.id.subviewSalesByCateogory);
        mDailyVsMonthlySalesLayout = findViewById(R.id.subviewDailyVsMonthySales);
        mSalesVsTargetVsCompetitorLayout = findViewById(R.id.subviewSalesTargetVsCompetitorLayout);

        // Sales Recap
        GridView salesRecapGridView = findViewById(R.id.salesRecapGridView);
        mSalesRecapAdapter = new HomeStoreAdapter(this, HomepageStoreSection.SALES_RECAP, new ArrayList<>());
        salesRecapGridView.setAdapter(mSalesRecapAdapter);

        // Sales By Category
        GridView salesByKategoriGridView = findViewById(R.id.salesByKategoriGridView);
        mSalesByCategoryAdapter = new HomeStoreAdapter(this, HomepageStoreSection.SALES_BY_CATEGORY, new ArrayList<>());
        salesByKategoriGridView.setAdapter(mSalesByCategoryAdapter);

        // Total Sales Harian vs Bulanan
        GridView totalSalesHarianVsBulananGridView = findViewById(R.id.totalSalesHarianVsBulananGridView);
        mDailyVsMonthlySalesAdapter = new HomeStoreAdapter(this, HomepageStoreSection.TOTAL_SALES_HARIAN_VS_BULANAN, new ArrayList<>());
        totalSalesHarianVsBulananGridView.setAdapter(mDailyVsMonthlySalesAdapter);

        // Sales vs target sales vs omset kompetitor
        GridView salesVsTargetVsCompetitorGridView = findViewById(R.id.salesVsTargetVsCompetitorGridView);
        mSalesVsTargetVsCompetitorAdapter = new HomeStoreAdapter(this, HomepageStoreSection.SALES_VS_SALES_TARGET_VS_OMSET_COMPETITOR, new ArrayList<>());
        salesVsTargetVsCompetitorGridView.setAdapter(mSalesVsTargetVsCompetitorAdapter);

        // Homepage pemebeli vs pengunjung
        GridView buyerVsVisitorGridView = findViewById(R.id.gridviewBuyerVsVisitor);
        mBuyerVsVisitorAdapter = new HomeStoreAdapter(this, HomepageStoreSection.BUYER_VS_VISITOR, new ArrayList<>());
        buyerVsVisitorGridView.setAdapter(mBuyerVsVisitorAdapter);

        // Homepage buying power
        GridView buyingPowerGridView = findViewById(R.id.buyingPowerGridView);
        mBuyingPowerAdapter = new HomeStoreAdapter(this, HomepageStoreSection.BUYING_POWER, new ArrayList<>());
        buyingPowerGridView.setAdapter(mBuyingPowerAdapter);

        mModel.getUserMutableLiveData().observe(this, user -> {
            if (user != null) {
                mUsername.setText(user.getFullname());
            } else {
                mUsername.setText(R.string.placeholder_user);
            }

            mModel.getMenuMutableLiveData().postValue(new ArrayList<Integer>() {{
                add(HomePageMenuEnum.SALES.getResourceId());
                add(HomePageMenuEnum.TEST.getResourceId());
                add(HomePageMenuEnum.CUSTOMER.getResourceId());
                add(HomePageMenuEnum.STORE.getResourceId());
            }});
        });

        mModel.getMenuMutableLiveData().observe(this, menuList -> {
            if (menuList != null) {
                mMenuAdapter.setData(menuList);
                mMenuAdapter.notifyDataSetChanged();

                mMenuGridView.setOnItemClickListener((parent, v, position, id) -> {

                    HomePageMenuEnum homePageMenuEnum = HomePageMenuEnum.Companion.getHomePageMenuEnum(menuList.get(position));
                    switch (homePageMenuEnum) {
                        case SALES:
                            Intent iSales = new Intent(getBaseContext(), SalesActivity.class);
                            startActivity(iSales);
                            break;
                        case TEST:
                            Intent iTest = new Intent(getBaseContext(), TestActivity.class);
                            startActivity(iTest);
                            break;
                        case CUSTOMER:
                            Intent iCustomer = new Intent(getBaseContext(), CustomerActivity.class);
                            startActivity(iCustomer);
                            break;
                        case STORE:
                            Intent iStore = new Intent(getBaseContext(), StoreActivity.class);
                            startActivity(iStore);
                            break;
                    }
                });
            }
        });

        mModel.getBaHomeMutableLiveData().observe(this, baHome -> {
            mSwipeRefresh.setRefreshing(false);

            if (baHome != null) {
                // Personal Sales
                BAHomeSalesOverview baHomeSalesOverview = baHome.getSalesOverview();

                List<BAHomepageItem> personalSales = new ArrayList<BAHomepageItem>() {{
                    add(new BAHomepageItem(R.drawable.ic_ba_sales, baHomeSalesOverview.getSales().getAmount(), baHomeSalesOverview.getSales().getQuantity()));
                    add(new BAHomepageItem(R.drawable.ic_ba_product_highlight, baHomeSalesOverview.getHighlight().getAmount(), baHomeSalesOverview.getHighlight().getQuantity()));
                    add(new BAHomepageItem(R.drawable.ic_ba_product_focus, baHomeSalesOverview.getFocus().getAmount(), baHomeSalesOverview.getFocus().getQuantity()));
                }};

                // Store Sales
                mPersonalSalesLayout.setVisibility(View.VISIBLE);
                mPersonalSalesTimestampTextView.setText(DateTimeUtil.INSTANCE.getSdfDayCommaDateMonthYear().format(baHomeSalesOverview.getSalesDate()));

                mPersonalSalesAdapter.clear();
                mPersonalSalesAdapter.addAll(personalSales);
                mPersonalSalesAdapter.notifyDataSetChanged();

                mStoreSalesLayout.setVisibility(View.VISIBLE);

                StoreSalesType storeSalesType = mModel.getStoreSalesTypeMutableLiveData().getValue();
                if (storeSalesType != null) {
                    mModel.getStoreSalesTypeMutableLiveData().postValue(storeSalesType);
                } else {
                    mModel.getStoreSalesTypeMutableLiveData().postValue(StoreSalesType.DAILY);
                }
            }
        });

        mModel.getStoreSalesTypeMutableLiveData().observe(this, storeSalesType -> {
            if (storeSalesType != null) {
                switch (storeSalesType) {
                    case DAILY:
                        BAHomeDaily baHomeDaily = mModel.getBaHomeMutableLiveData().getValue().getLocation().getSalesOverview().getDaily();

                        mStoreSalesTimestampTextView.setText(DateTimeUtil.INSTANCE.getSdfDayCommaDateMonthYear().format(
                                baHomeDaily.getSalesDate()));

                        mStoreDailySalesLinearLayout.setVisibility(View.VISIBLE);

                        mSalesByCategoryLayout.setVisibility(View.GONE);
                        mDailyVsMonthlySalesLayout.setVisibility(View.GONE);
                        mSalesVsTargetVsCompetitorLayout.setVisibility(View.GONE);

                        // Sales Recap
                        List<BAHomepageItem> salesRecap = new ArrayList<BAHomepageItem>() {{
                            add(new BAHomepageItem(R.drawable.ic_sales_turnover, baHomeDaily.getSales().getAmount(), baHomeDaily.getSales().getQuantity()));
                            add(new BAHomepageItem(R.drawable.ic_ba_product_highlight, baHomeDaily.getHighlight().getAmount(), baHomeDaily.getHighlight().getQuantity()));
                            add(new BAHomepageItem(R.drawable.ic_ba_product_focus, baHomeDaily.getFocus().getAmount(), baHomeDaily.getFocus().getQuantity()));
                        }};

                        mSalesRecapAdapter.clear();
                        mSalesRecapAdapter.addAll(salesRecap);
                        mSalesRecapAdapter.notifyDataSetChanged();

                        // Buyer Vs Visitor
                        List<BAHomepageItem> buyerVsVisitor = new ArrayList<BAHomepageItem>() {{
                            add(new BAHomepageItem(R.drawable.ic_buyer, 0.00, baHomeDaily.getBuyer()));
                            add(new BAHomepageItem(R.drawable.ic_visitor, 0.00, baHomeDaily.getVisitor()));
                        }};

                        mBuyerVsVisitorAdapter.clear();
                        mBuyerVsVisitorAdapter.addAll(buyerVsVisitor);
                        mBuyerVsVisitorAdapter.notifyDataSetChanged();

                        // Buying Power
                        List<BAHomepageItem> buyingPower = new ArrayList<BAHomepageItem>() {{
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower1(), 0.00, baHomeDaily.getBuyingPower().getLess50()));
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower2(), 0.00, baHomeDaily.getBuyingPower().getLess100()));
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower3(), 0.00, baHomeDaily.getBuyingPower().getLess150()));
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower4(), 0.00, baHomeDaily.getBuyingPower().getLess300()));
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower5(), 0.00, baHomeDaily.getBuyingPower().getMore300()));
                        }};

                        mBuyingPowerAdapter.clear();
                        mBuyingPowerAdapter.addAll(buyingPower);
                        mBuyingPowerAdapter.notifyDataSetChanged();
                        break;
                    case MONTHLY:
                        BAHomeMonthly baHomeMonthly = mModel.getBaHomeMutableLiveData().getValue().getLocation().getSalesOverview().getMonthly();

                        mStoreSalesTimestampTextView.setText(DateTimeUtil.INSTANCE.getSdfMonthYear().format(
                                DateTimeUtil.INSTANCE.getDateFromMonthYear(
                                        baHomeMonthly.getMonth(),
                                        baHomeMonthly.getYear())));

                        mStoreDailySalesLinearLayout.setVisibility(View.VISIBLE);

                        mSalesByCategoryLayout.setVisibility(View.VISIBLE);
                        mDailyVsMonthlySalesLayout.setVisibility(View.VISIBLE);
                        mSalesVsTargetVsCompetitorLayout.setVisibility(View.VISIBLE);

                        // Sales Recap
                        salesRecap = new ArrayList<BAHomepageItem>() {{
                            add(new BAHomepageItem(R.drawable.ic_sales_turnover, baHomeMonthly.getSales().getAmount(), baHomeMonthly.getSales().getQuantity()));
                            add(new BAHomepageItem(R.drawable.ic_ba_product_highlight, baHomeMonthly.getHighlight().getAmount(), baHomeMonthly.getHighlight().getQuantity()));
                            add(new BAHomepageItem(R.drawable.ic_ba_product_focus, baHomeMonthly.getFocus().getAmount(), baHomeMonthly.getFocus().getQuantity()));
                        }};

                        mSalesRecapAdapter.clear();
                        mSalesRecapAdapter.addAll(salesRecap);
                        mSalesRecapAdapter.notifyDataSetChanged();

                        // Sales per product
                        List<BAHomepageItem> salesByCategory = new ArrayList<>();
                        for (ProductType productType : baHomeMonthly.getSales().getInputPerProductTypes().getProductTypes()) {
                            salesByCategory.add(new BAHomepageItem(productType.getImageUrl(), productType.getAmount(), productType.getQuantity()));
                        }

                        mSalesByCategoryAdapter.clear();
                        mSalesByCategoryAdapter.addAll(salesByCategory);
                        mSalesByCategoryAdapter.notifyDataSetChanged();

                        // Daily Vs Monthly Sales
                        List<BAHomepageItem> dailyVsMonthlySalesList = new ArrayList<BAHomepageItem>() {{
                            add(new BAHomepageItem(R.drawable.ic_sales_turnover, baHomeMonthly.getSales().getAmount(), baHomeMonthly.getSales().getQuantity()));
                            add(new BAHomepageItem(R.drawable.ic_sales_turnover, baHomeMonthly.getSales().getInputPerProductTypes().getAmount(), baHomeMonthly.getSales().getInputPerProductTypes().getQuantity()));
                        }};

                        mDailyVsMonthlySalesAdapter.clear();
                        mDailyVsMonthlySalesAdapter.addAll(dailyVsMonthlySalesList);
                        mDailyVsMonthlySalesAdapter.notifyDataSetChanged();

                        // Sales Vs Target Vs Competitor
                        List<BAHomepageItem> salesVsTargetVsCompetitor = new ArrayList<BAHomepageItem>() {{
                            add(new BAHomepageItem(R.drawable.ic_sales_turnover, baHomeMonthly.getSales().getAmount(), baHomeMonthly.getSales().getQuantity()));
                            add(new BAHomepageItem(R.drawable.ic_ba_target, baHomeMonthly.getSales().getTarget(), 0));
                            add(new BAHomepageItem(R.drawable.ic_ba_competitor, baHomeMonthly.getSales().getCompetitorTurnover(), 0));
                        }};

                        mSalesVsTargetVsCompetitorAdapter.clear();
                        mSalesVsTargetVsCompetitorAdapter.addAll(salesVsTargetVsCompetitor);
                        mSalesVsTargetVsCompetitorAdapter.notifyDataSetChanged();

                        // Buyer Vs Visitor
                        buyerVsVisitor = new ArrayList<BAHomepageItem>() {{
                            add(new BAHomepageItem(R.drawable.ic_buyer, 0.00, baHomeMonthly.getBuyer()));
                            add(new BAHomepageItem(R.drawable.ic_visitor, 0.00, baHomeMonthly.getVisitor()));
                        }};

                        mBuyerVsVisitorAdapter.clear();
                        mBuyerVsVisitorAdapter.addAll(buyerVsVisitor);
                        mBuyerVsVisitorAdapter.notifyDataSetChanged();

                        // Buying Power
                        buyingPower = new ArrayList<BAHomepageItem>() {{
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower1(), 0.00, baHomeMonthly.getBuyingPower().getLess50()));
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower2(), 0.00, baHomeMonthly.getBuyingPower().getLess100()));
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower3(), 0.00, baHomeMonthly.getBuyingPower().getLess150()));
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower4(), 0.00, baHomeMonthly.getBuyingPower().getLess300()));
                            add(new BAHomepageItem(ImageUtil.INSTANCE.getBuyingPower5(), 0.00, baHomeMonthly.getBuyingPower().getMore300()));
                        }};

                        mBuyingPowerAdapter.clear();
                        mBuyingPowerAdapter.addAll(buyingPower);
                        mBuyingPowerAdapter.notifyDataSetChanged();
                        break;
                }
            }
        });

        mModel.getProfileMutableLiveData().observe(this, profile -> {
            if (profile != null) {
                Intent iProfile = new Intent(this, ProfileActivity.class);
                iProfile.putExtra(IntentUtil.Companion.getDATA(), profile);
                startActivity(iProfile);
            }
        });

        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_homepage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notification:
                Intent iNotification = new Intent(this, NotificationActivity.class);
                startActivity(iNotification);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        mModel.getData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etProfile:
                mModel.callProfileData();
                break;
            case R.id.detailDailySalesTextView:
                Intent iSales = new Intent(getBaseContext(), SalesActivity.class);
                startActivity(iSales);
                break;
            case R.id.detailStoreSalesTextView:
                Intent iStore = new Intent(getBaseContext(), StoreActivity.class);
                startActivity(iStore);
                break;
            case R.id.dailyStoreSalesButton:
                WardahUtil.INSTANCE.setTabButton(getBaseContext(), mDailyStoreSalesButton, true);
                WardahUtil.INSTANCE.setTabButton(getBaseContext(), mMonthlyStoreSalesButton, false);
                mModel.getStoreSalesTypeMutableLiveData().postValue(StoreSalesType.DAILY);
                break;
            case R.id.monthlyStoreSalesButton:
                WardahUtil.INSTANCE.setTabButton(getBaseContext(), mDailyStoreSalesButton, false);
                WardahUtil.INSTANCE.setTabButton(getBaseContext(), mMonthlyStoreSalesButton, true);
                mModel.getStoreSalesTypeMutableLiveData().postValue(StoreSalesType.MONTHLY);
                break;
        }
    }
}
