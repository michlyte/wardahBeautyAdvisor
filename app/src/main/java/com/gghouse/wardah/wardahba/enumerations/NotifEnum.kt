package com.gghouse.wardah.wardahba.enumerations

import android.R.attr.id
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.utils.LogUtil

enum class NotifEnum(var type: String?, var iconResId: Int?, var remarks: String?) {
    PRODUCT_LAUNCH("PRODUCT_LAUNCH", R.drawable.ic_new_product_launch, "PRODUCT LAUNCH"),
    QUOTE("QUOTES", R.drawable.ic_quote, "QUOTES"),
    PROMOTION("PROMO", R.drawable.ic_promotion, "PROMO");

    companion object {
        fun getNotifEnumById(type: String): NotifEnum? {
            for (notifEnum in values()) {
                if (notifEnum.type == type) {
                    return notifEnum
                }
            }
            LogUtil.log("id [$id] is not supported.")
            return null
        }
    }
}
