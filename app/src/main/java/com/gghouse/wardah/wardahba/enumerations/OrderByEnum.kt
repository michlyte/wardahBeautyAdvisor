package com.gghouse.wardah.wardahba.enumerations

import com.gghouse.wardah.wardahba.utils.LogUtil

enum class OrderByEnum(var title: String?, var value: String?) {
    ASC("Terkecil / A -> Z", "ASC"),
    DESC("Terbesar / Z -> A", "DESC");

    companion object {
        fun getSortByEnum(title: String): OrderByEnum {
            for (orderByEnum in values()) {
                if (orderByEnum.title == title) {
                    return orderByEnum
                }
            }
            LogUtil.log("value [$title] is not supported.")
            return DESC
        }
    }
}
