package com.gghouse.wardah.wardahba.dummy

import com.gghouse.wardah.wardahba.webservices.model.User

object UserDummy {
    val BAUser = User(1L, 1L, "NOVITA DEWI", "BEAUTY_ADVISOR", "MDS HERMES MALL ACEH")
    val BPUser = User(1L, 1L, "NOVITA DEWI", "BEAUTY_PROMOTER", "MDS HERMES MALL ACEH")
    val BPLUser = User(1L, 1L, "NOVITA DEWI", "BEAUTY_PROMOTER_LEADER", "MDS HERMES MALL ACEH")
    val FCUser = User(1L, 1L, "NOVITA DEWI", "FIELD_CONTROLLER", "MDS HERMES MALL ACEH")
}
