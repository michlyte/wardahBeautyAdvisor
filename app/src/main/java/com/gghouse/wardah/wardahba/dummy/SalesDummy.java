package com.gghouse.wardah.wardahba.dummy;

import com.gghouse.wardah.wardahba.models.ProductHighlightHistoryHeader;
import com.gghouse.wardah.wardahba.models.SalesHistoryHeaderP2;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;
import com.gghouse.wardah.wardahba.webservices.model.Sales;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class SalesDummy {
    /**
     * Product Highlight
     */
    public static ProductHighlightHistoryHeader productHighlightHistoryHeader = new ProductHighlightHistoryHeader(
            40000.00, 20, 20000.00, 10);

    public static List<ProductHighlight> productHighlightCleanZeroQtyList =
            new ArrayList<ProductHighlight>() {{
                add(new ProductHighlight(0L, "item 1", "", 10000.00, 0));
                add(new ProductHighlight(1L, "item 2", "", 25000.00, 0));
                add(new ProductHighlight(2L, "item 3", "", 5000.00, 0));
                add(new ProductHighlight(3L, "item 4", "", 7500.00, 0));
                add(new ProductHighlight(4L, "item 5", "", 9000.00, 0));
                add(new ProductHighlight(5L, "item 6", "", 200000.00, 0));
            }};

    public static List<ProductHighlight> productHighlightHistorySortByPrice =
            new ArrayList<ProductHighlight>() {{
                add(new ProductHighlight(1L, "item 6", "", 200000.00, 1));
                add(new ProductHighlight(3L, "item 2", "", 25000.00, 1));
                add(new ProductHighlight(2L, "item 1", "", 10000.00, 1));
                add(new ProductHighlight(4L, "item 5", "", 9000.00, 1));
                add(new ProductHighlight(5L, "item 3", "", 5000.00, 1));
                add(new ProductHighlight(6L, "item 4", "", 7500.00, 1));
            }};

    public static List<ProductHighlight> productHighlightZeroQtyList =
            new ArrayList<ProductHighlight>() {{
                add(new ProductHighlight(0L, "item 1", "", 10000.00, 0));
                add(new ProductHighlight(1L, "item 2", "", 25000.00, 0));
                add(new ProductHighlight(2L, "item 3", "", 5000.00, 0));
                add(new ProductHighlight(3L, "item 4", "", 7500.00, 0));
                add(new ProductHighlight(4L, "item 5", "", 9000.00, 0));
                add(new ProductHighlight(5L, "item 6", "", 200000.00, 0));
            }};

    public static List<ProductHighlight> productHighlightList =
            new ArrayList<ProductHighlight>() {{
                add(new ProductHighlight(0L, "item 1", "", 10000.00, 1));
                add(new ProductHighlight(1L, "item 2", "", 25000.00, 1));
                add(new ProductHighlight(2L, "item 3", "", 5000.00, 1));
                add(new ProductHighlight(3L, "item 4", "", 7500.00, 1));
                add(new ProductHighlight(4L, "item 5", "", 9000.00, 1));
                add(new ProductHighlight(5L, "item 6", "", 200000.00, 1));
            }};

    /**
     * Product Focus
     */
    public static List<ProductHighlight> productFocusCleanZeroQtyList =
            new ArrayList<ProductHighlight>() {{
                add(new ProductHighlight(0L, "item 1", "", 10000.00, 0));
                add(new ProductHighlight(1L, "item 2", "", 25000.00, 0));
                add(new ProductHighlight(2L, "item 3", "", 5000.00, 0));
                add(new ProductHighlight(3L, "item 4", "", 7500.00, 0));
                add(new ProductHighlight(4L, "item 5", "", 9000.00, 0));
                add(new ProductHighlight(5L, "item 6", "", 200000.00, 0));
            }};

    public static List<ProductHighlight> productFocusList =
            new ArrayList<ProductHighlight>() {{
                add(new ProductHighlight(1L, "item 1", "", 10000.00, 1));
                add(new ProductHighlight(2L, "item 2", "", 25000.00, 2));
                add(new ProductHighlight(3L, "item 3", "", 5000.00, 3));
                add(new ProductHighlight(4L, "item 4", "", 7500.00, 4));
                add(new ProductHighlight(5L, "item 5", "", 9000.00, 5));
                add(new ProductHighlight(6L, "item 6", "", 200000.00, 6));
            }};

    public static List<ProductHighlight> productFocusHistorySortByPrice =
            new ArrayList<ProductHighlight>() {{
                add(new ProductHighlight(5L, "item 6", "", 200000.00, 6));
                add(new ProductHighlight(1L, "item 2", "", 25000.00, 2));
                add(new ProductHighlight(0L, "item 1", "", 10000.00, 1));
                add(new ProductHighlight(4L, "item 5", "", 9000.00, 5));
                add(new ProductHighlight(3L, "item 4", "", 7500.00, 4));
                add(new ProductHighlight(2L, "item 3", "", 5000.00, 3));
            }};


    public static List<WardahHistoryRecyclerViewItem> productHighlightHistoryRecyclerViewItemList =
            new ArrayList<WardahHistoryRecyclerViewItem>() {{
                add(new ProductHighlightHistoryHeader(40000.00, 20, 20000.00, 10));
                add(new ProductHighlight(0L, "item 1", "", 10000.00, 5));
                add(new ProductHighlight(1L, "item 2", "", 23000.00, 1));
                add(new ProductHighlight(2L, "item 3", "", 10000.00, 0));
                add(new ProductHighlight(3L, "item 4", "", 10000.00, 0));
                add(new ProductHighlight(4L, "item 5", "", 10000.00, 0));
                add(new ProductHighlight(5L, "item 6", "", 10000.00, 0));
            }};

    public static List<WardahHistoryRecyclerViewItem> productHighlightFocusRecyclerViewItemList =
            new ArrayList<WardahHistoryRecyclerViewItem>() {{
                add(new ProductHighlightHistoryHeader(30000.00, 15, 10000.00, 5));
                add(new ProductHighlight(0L, "item A", "", 10000.00, 1));
                add(new ProductHighlight(1L, "item B", "", 10000.00, 2));
                add(new ProductHighlight(2L, "item C", "", 10000.00, 3));
                add(new ProductHighlight(3L, "item D", "", 10000.00, 4));
                add(new ProductHighlight(4L, "item E", "", 10000.00, 5));
                add(new ProductHighlight(5L, "item F", "", 10000.00, 6));
            }};

    public static List<WardahHistoryRecyclerViewItem> salesRecyclerViewItemList = new ArrayList<WardahHistoryRecyclerViewItem>();

    public static List<Sales> salesList = new ArrayList<>();

    public static List<Sales> salesNonEditableList = new ArrayList<>();

//    public static List<SalesTarget> salesTargetSimpleList = new ArrayList<SalesTarget>() {{
//        add(new SalesTarget(0L, new Date().getTime(), "Bandung", "ABC Store", 4000000.00, true));
//        add(new SalesTarget(1L, DateTimeUtil.INSTANCE.getPreviousMonth(1).getTime(), "Bandung", "ABC Store", 4000000.00, false));
//        add(new SalesTarget(2L, DateTimeUtil.INSTANCE.getPreviousMonth(2).getTime(), "Bandung", "ABC Store", 4000000.00, false));
//    }};

    static {
        /**
         * salesRecyclerViewItemList
         */
        salesRecyclerViewItemList.add(new SalesHistoryHeaderP2(
                40000.00, 20, 20000.00, 10));
        for (long i = 0; i < 15; i++) {
            salesRecyclerViewItemList.add(
                    new Sales(
                            i,
                            (Double.parseDouble(i + "") + 1000000),
                            new Date().getTime() - (i * (1000 * 60 * 60 * 24)), false,
                            (int) i,
                            3,
                            2,
                            1L)
            );
        }

        /**
         * salesList
         */
        for (long i = 0; i < 5; i++) {
            salesList.add(
                    new Sales(
                            i,
                            (Double.parseDouble(i + "") + 1000000),
                            new Date().getTime() - (i * (1000 * 60 * 60 * 24)),false,
                            (int) i,
                            3,
                            2,
                            1L)
            );

            salesNonEditableList.add(
                    new Sales(
                            i,
                            (Double.parseDouble(i + "") + 1000000),
                            new Date().getTime() - (i * (1000 * 60 * 60 * 24)),false,
                            (int) i,
                            3,
                            2,
                            1L)
            );
        }
    }
}
