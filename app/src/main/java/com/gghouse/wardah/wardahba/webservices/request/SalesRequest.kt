package com.gghouse.wardah.wardahba.webservices.request

import java.io.Serializable

class SalesRequest(var quantity: Long, var amount: Double): Serializable