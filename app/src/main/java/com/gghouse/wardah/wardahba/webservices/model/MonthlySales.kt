package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import java.io.Serializable

class MonthlySales : WardahHistoryRecyclerViewItem, Serializable {
    var buyer: Integer? = null
    var visitor: Integer? = null
    var buyingPower: BuyingPower? = null
    var quantity: Integer? = null
    var amount: Double? = null
    var month: Integer? = null
    var year: Integer? = null
    var monthlyReportId: Long? = null
    var editable: Boolean? = null
    var target: Double? = null

    constructor(buyer: Integer?, visitor: Integer?, buyingPower: BuyingPower?, quantity: Integer?, amount: Double?, month: Integer?, year: Integer?, monthlyReportId: Long?, editable: Boolean?, target: Double?) : super() {
        this.buyer = buyer
        this.visitor = visitor
        this.buyingPower = buyingPower
        this.quantity = quantity
        this.amount = amount
        this.month = month
        this.year = year
        this.monthlyReportId = monthlyReportId
        this.editable = editable
        this.target = target
    }
}