package com.gghouse.wardah.wardahba.webservices.model

class StoreSalesBulanan(createdTime: Long?, id: Long?, salesAmount: Double?, userId: Long?, editable: Boolean, pcs: Int?, numOfVisitor: Int?, numOfBuyer: Int?, buyingPowerList: List<BuyingPower>, var monthlySalesAmount: Double?, var omsetKompetitor: Double?) : StoreSales(createdTime, id, salesAmount, userId, editable, pcs, numOfVisitor, numOfBuyer, buyingPowerList)
