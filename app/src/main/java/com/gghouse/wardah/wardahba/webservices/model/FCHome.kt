package com.gghouse.wardah.wardahba.webservices.model

class FCHome {
    val userId: Long
    val userType: String
    var latestVisit: LatestVisit? = null
    var nearestVisit: NearestVisit? = null

    constructor(userId: Long, userType: String, latestVisit: LatestVisit?, nearestVisit: NearestVisit?) {
        this.userId = userId
        this.userType = userType
        this.latestVisit = latestVisit
        this.nearestVisit = nearestVisit
    }
}