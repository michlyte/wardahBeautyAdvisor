package com.gghouse.wardah.wardahba.utils

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics

/**
 * Created by michaelhalim on 2/19/17.
 */

object ScreenUtil {
    fun getWidth(activity: Activity): Float {
        LogUtil.log(LogUtil.LOG_BEGIN + "getWidth")
        val metrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(metrics)
        LogUtil.log(LogUtil.LOG_END + "getWidth " + metrics.widthPixels)
        return metrics.widthPixels.toFloat()
    }

    fun getWidth(context: Context): Float {
        //        WOILogger.log(LOG_BEGIN + "getWidth");
        val metrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(metrics)
        //        WOILogger.log(LOG_END + "getWidth " + metrics.widthPixels);
        return metrics.widthPixels.toFloat()
    }

    fun getHeight(activity: Activity): Float {
        LogUtil.log(LogUtil.LOG_BEGIN + "getHeight")
        val metrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(metrics)
        LogUtil.log(LogUtil.LOG_END + "getHeight " + metrics.widthPixels)
        return metrics.heightPixels.toFloat()
    }

    fun getHeight(context: Context): Float {
        LogUtil.log(LogUtil.LOG_BEGIN + "getHeight")
        val metrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(metrics)
        LogUtil.log(LogUtil.LOG_END + "getHeight " + metrics.widthPixels)
        return metrics.heightPixels.toFloat()
    }
}
