package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.Questioner

/**
 * Created by ecquaria-macmini on 5/26/17.
 */

class QuestionerRequest(private val userId: Long?, private val questionnaireDate: Long?, private val answers: List<Questioner>)
