package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.webservices.model.TestRecap;

/**
 * Created by michael on 5/4/2017.
 */

public class TestHistoryHeaderViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final TextView averageTextView;
    private final TextView rankTextView;
    private final TextView totalQuestionTextView;
    public TestRecap mItem;

    public TestHistoryHeaderViewHolder(View view) {
        super(view);

        this.view = view;
        averageTextView = view.findViewById(R.id.tv_average);
        rankTextView = view.findViewById(R.id.tv_rank);
        totalQuestionTextView = view.findViewById(R.id.tv_sum_test_taken);
    }

    public void setData(TestRecap testRecap) {
        mItem = testRecap;

        averageTextView.setText(String.valueOf(mItem.getAverageScore()));
        rankTextView.setText(String.valueOf(mItem.getAccuracy()));
        totalQuestionTextView.setText(String.valueOf(mItem.getTotalQuestion()));
    }
}
