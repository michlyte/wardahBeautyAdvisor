package com.gghouse.wardah.wardahba.webservices.request

class CustomerEditRequest(name: String, mobileNumber: String, email: String, address: String, byUserId: Long?, private val customerId: Long?) : CustomerRequest(name, mobileNumber, email, address, byUserId)
