package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class EvaluationChecklist : Serializable {
    val id: Long
    val content: String
    var rating: Float? = null

    constructor(id: Long, content: String) {
        this.id = id
        this.content = content
    }
}