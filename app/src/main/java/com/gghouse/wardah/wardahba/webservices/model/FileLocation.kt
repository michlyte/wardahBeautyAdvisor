package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

/**
 * Created by michael on 3/13/2017.
 */

class FileLocation : Serializable {
    var createdBy: String? = null
    var createdTime: Long? = null
    var id: Long? = null
    var fileName: String? = null
    var imgUrl: String? = null
    var thumbnailUrl: String? = null
}
