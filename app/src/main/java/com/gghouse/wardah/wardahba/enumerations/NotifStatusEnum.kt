package com.gghouse.wardah.wardahba.enumerations

import com.gghouse.wardah.wardahba.utils.LogUtil

enum class NotifStatusEnum(var value: String?) {
    ACTIVE("active"),
    NONACTIVE("non-active"),
    UNDEFINE("undefine");

    companion object {
        fun getByValue(value: String): NotifStatusEnum {
            for (notifStatusEnum in values()) {
                if (notifStatusEnum.value == value) {
                    return notifStatusEnum
                }
            }
            LogUtil.log("value [$value] is not supported.")
            return UNDEFINE
        }
    }
}
