package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Notification
import com.gghouse.wardah.wardahba.webservices.model.Pagination

/**
 * Created by michael on 3/13/2017.
 */

class NotificationsResponse : GenericResponse() {
    var data: List<Notification>? = null
    var pagination: Pagination? = null
}
