package com.gghouse.wardah.wardahba.dummy

import com.gghouse.wardah.wardahba.models.TestHistoryHeader
import com.gghouse.wardah.wardahba.webservices.model.Answer
import com.gghouse.wardah.wardahba.webservices.model.Question
import com.gghouse.wardah.wardahba.webservices.model.Test
import com.gghouse.wardah.wardahba.webservices.model.TestQuestion
import java.util.*

object TestDummy {
    val testHeader = TestHistoryHeader(70.7, 7.5f, 8)

    val testList: List<Test> = object : ArrayList<Test>() {
        init {
            add(Test(100, 5, Date().time))
            add(Test(80, 5, Date().time - 24 * 60 * 60 * 1000))
            add(Test(80, 5, Date().time - 48 * 60 * 60 * 1000))
            add(Test(100, 5, Date().time - 72 * 60 * 60 * 1000))
            add(Test(80, 5, Date().time - 96 * 60 * 60 * 1000))
        }
    }

    val answerList: List<Answer> = object : ArrayList<Answer>() {
        init {
            add(Answer(0L, "Jawaban 1"))
            add(Answer(1L, "Jawaban 2"))
            add(Answer(2L, "Jawaban 3"))
            add(Answer(3L, "Jawaban 4"))
        }
    }

    val questionList = TestQuestion(Date().time, object : ArrayList<Question>() {
        init {
            add(Question(0L, "Pertanyaan ke 1", answerList))
            add(Question(1L, "Pertanyaan ke 2", answerList))
            add(Question(2L, "Pertanyaan ke 3", answerList))
            add(Question(3L, "Pertanyaan ke 4", answerList))
            add(Question(4L, "Pertanyaan ke 5", answerList))
        }
    })
}
