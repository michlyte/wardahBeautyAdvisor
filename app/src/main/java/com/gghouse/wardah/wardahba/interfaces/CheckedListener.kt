package com.gghouse.wardah.wardahba.interfaces

import com.gghouse.wardah.wardahba.models.Reminder

interface CheckedListener {
    fun onChecked(reminder: Reminder)
}
