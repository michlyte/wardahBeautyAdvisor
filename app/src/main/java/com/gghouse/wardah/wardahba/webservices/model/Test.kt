package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import com.gghouse.wardah.wardahba.utils.DateTimeUtil

class Test : WardahHistoryRecyclerViewItem {
    var score: Int? = null
    var totalQuestion: Int? = null
    var testDate: Long? = null

    constructor(score: Int?, totalQuestion: Int?, testDate: Long?) {
        this.score = score
        this.totalQuestion = totalQuestion
        this.testDate = testDate
    }

    val scoreStr: String
        get() = score.toString()

    val dayStr: String
        get() = DateTimeUtil.sdfDay.format(testDate)

    val testDateStr: String
        get() = DateTimeUtil.sdfDate.format(testDate)

    val monthYearStr: String
        get() = DateTimeUtil.sdfMonthCommaYear.format(testDate)
}