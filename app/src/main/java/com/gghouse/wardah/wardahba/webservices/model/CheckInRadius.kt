package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class CheckInRadius : Serializable {
    var calendarId: Long? = null
    val radius: Int
    val timeout: Int
    var location: EventLocation? = null

    constructor(radius: Int, timeout: Int, location: EventLocation) {
        this.radius = radius
        this.timeout = timeout
        this.location = location
    }
}