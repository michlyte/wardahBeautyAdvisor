package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.DCDropdown
import com.gghouse.wardah.wardahba.webservices.model.DCLocationDropdown

class VisitEditRequest : VisitCreateRequest {
    val calendarId: Long

    constructor(userId: Long, location: DCLocationDropdown, dc: DCDropdown, category: String, startTime: Long, endTime: Long, alarmTime: Long, alarmTimeStr: String?, notes: String, calendarId: Long) : super(userId, location, dc, category, startTime, endTime, alarmTime, alarmTimeStr, notes) {
        this.calendarId = calendarId
    }
}