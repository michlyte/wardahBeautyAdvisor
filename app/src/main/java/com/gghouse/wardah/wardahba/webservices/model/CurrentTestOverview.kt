package com.gghouse.wardah.wardahba.webservices.model

class CurrentTestOverview {
    val month: Int
    val year: Int
    val sharedPercentage: Float
    val totalQuestionsAnswered: Int
    val totalAccumulativeScore: Float

    constructor(month: Int, year: Int, sharedPercentage: Float, totalQuestionsAnswered: Int, totalAccumulativeScore: Float) {
        this.month = month
        this.year = year
        this.sharedPercentage = sharedPercentage
        this.totalQuestionsAnswered = totalQuestionsAnswered
        this.totalAccumulativeScore = totalAccumulativeScore
    }
}