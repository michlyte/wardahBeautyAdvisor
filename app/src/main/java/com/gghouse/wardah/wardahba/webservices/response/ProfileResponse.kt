package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Profile

class ProfileResponse : GenericResponse {
    var data: Profile? = null

    constructor()
}