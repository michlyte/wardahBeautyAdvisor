package com.gghouse.wardah.wardahba.activities.attendant;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.fragments.WardahSimpleHistoryFragment;
import com.gghouse.wardah.wardahba.utils.IntentUtil;

public class AttendantHistoryActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peserta_history);

        Long calendarId = null;

        Intent i = getIntent();
        if (i != null) {
            calendarId = i.getLongExtra(IntentUtil.Companion.getCALENDAR_ID(), 0L);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), calendarId);

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private Long calendarId;

        public SectionsPagerAdapter(FragmentManager fm, Long calendarId) {
            super(fm);
            this.calendarId = calendarId;
        }

        @Override
        public Fragment getItem(int position) {
            return WardahSimpleHistoryFragment.newInstance(WardahItemType.ATTENDANT, calendarId);
        }

        @Override
        public int getCount() {
            return 1;
        }
    }
}
