package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA
import java.util.*

class BaInformation(var name: String?, var nip: String?, var joinedAt: Date?, var longWork: Int?, var monthlySales: List<BAHomepageItem>?, var allSales: List<BAHomepageItem>?, var monthlyTest: List<BAHomepageItem>?, var allTest: List<BAHomepageItem>?, var evaluations: List<ChecklistBA>?) : WardahHistoryRecyclerViewItem() {

    val longWorkStr: String
        get() = longWork!!.toString() + " Tahun"

    val joinedAtStr: String
        get() = DateTimeUtil.sdfFilter.format(joinedAt)
}
