package com.gghouse.wardah.wardahba.dummy

import com.gghouse.wardah.wardahba.webservices.model.Checklist
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA
import java.util.*

object ChecklistDummy {
    val checklistList: ArrayList<Checklist> = object : ArrayList<Checklist>() {
        init {
            add(Checklist(0L, "Checklist 1", false))
            add(Checklist(1L, "Checklist 2", false))
            add(Checklist(2L, "Checklist 3", false))
        }
    }

    /**
     * Checklist BA
     */
    val checklistBAPastEvent: ArrayList<ChecklistBA> = object : ArrayList<ChecklistBA>() {
        init {
            add(ChecklistBA(0L, "Michael", "4.5", true))
            add(ChecklistBA(0L, "Adrianto", "4", true))
            add(ChecklistBA(0L, "Gunawan", "5", true))
        }
    }

    val checklistBATodayEvent: ArrayList<ChecklistBA> = object : ArrayList<ChecklistBA>() {
        init {
            add(ChecklistBA(0L, "Michael", "4.5", true))
            add(ChecklistBA(0L, "Adrianto", null, true))
            add(ChecklistBA(0L, "Gunawan", null, true))
        }
    }

    val checklistBAFutureEvent: ArrayList<ChecklistBA> = object : ArrayList<ChecklistBA>() {
        init {
            add(ChecklistBA(0L, "Michael", null, true))
            add(ChecklistBA(0L, "Adrianto", null, true))
            add(ChecklistBA(0L, "Gunawan", null, true))
        }
    }
}