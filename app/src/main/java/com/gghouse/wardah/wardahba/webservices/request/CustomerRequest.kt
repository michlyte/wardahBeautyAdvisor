package com.gghouse.wardah.wardahba.webservices.request

open class CustomerRequest(private val name: String, private val mobileNumber: String, private val email: String, private val address: String, private val byUserId: Long?)
