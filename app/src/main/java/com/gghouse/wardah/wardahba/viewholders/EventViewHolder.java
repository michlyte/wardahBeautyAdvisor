package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum;
import com.gghouse.wardah.wardahba.interfaces.EventBPListener;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.webservices.model.EventCalendar;
import com.squareup.picasso.Picasso;

public class EventViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final CardView mCardView;
    private final TextView mTitleTextView;
    private final TextView mTimeTextView;
    private final TextView mLocationTextView;
    private final ImageView mTimeImageView;
    private final ImageView mLocationImageView;
    private final ImageView mCreatedByImageView;
    private final Button mChecklistButton;
    private final Button mCheckInButton;
    private final Button mAttendantButton;
    private final LinearLayout mEventButtonLinearLayout;
    private final LinearLayout mEventCreatedByLinearLayout;
    private final TextView mCreatedByTextView;

    public EventCalendar mItem;

    public EventViewHolder(View view) {
        super(view);

        this.view = view;
        mCardView = view.findViewById(R.id.cardView);
        mTitleTextView = view.findViewById(R.id.titleEditText);
        mTimeTextView = view.findViewById(R.id.timeTextView);
        mLocationTextView = view.findViewById(R.id.locationTextView);
        mTimeImageView = view.findViewById(R.id.timeImageView);
        mLocationImageView = view.findViewById(R.id.locationImageView);
        mCreatedByImageView = view.findViewById(R.id.createdImageView);
        mChecklistButton = view.findViewById(R.id.checklistButton);
        mCheckInButton = view.findViewById(R.id.checkInButton);
        mAttendantButton = view.findViewById(R.id.pesertaButton);
        mEventButtonLinearLayout = view.findViewById(R.id.eventButtonLinearLayout);
        mEventCreatedByLinearLayout = view.findViewById(R.id.eventCreatedByLinearLayout);
        mCreatedByTextView = view.findViewById(R.id.createdByTextView);

        Picasso.get().load(R.drawable.ic_event_datetime).into(mTimeImageView);
        Picasso.get().load(R.drawable.ic_event_location).into(mLocationImageView);
        Picasso.get().load(R.drawable.ic_event_bp_user).into(mCreatedByImageView);
    }

    private void setupRowView(UserTypeEnum userType) {
        switch (userType) {
            case BEAUTY_PROMOTER_LEADER:
                mEventButtonLinearLayout.setVisibility(View.GONE);
                break;
            default:
                mEventCreatedByLinearLayout.setVisibility(View.GONE);
                WardahUtil.INSTANCE.setEventButtons(mItem, mChecklistButton, mCheckInButton, mAttendantButton);
                break;
        }
    }

    private void setupActionEvent(EventBPListener eventBPListener) {
        mCardView.setOnClickListener(view -> eventBPListener.onEventClicked(mItem));
        mChecklistButton.setOnClickListener(view -> eventBPListener.onChecklistClicked(mItem));
        mCheckInButton.setOnClickListener(view -> eventBPListener.onCheckInClicked(mItem));
        mAttendantButton.setOnClickListener(view -> eventBPListener.onAttendantClicked(mItem));
    }

    public void setData(UserTypeEnum userType, EventCalendar eventCalendar, EventBPListener eventBPListener) {
        mItem = eventCalendar;

        mTitleTextView.setText(mItem.getTitle());

        mLocationTextView.setText(mItem.getLocationAddress());
        mTimeTextView.setText(DateTimeUtil.INSTANCE.getEventTimeStr(mItem.getStartTime(), mItem.getEndTime()));
        if (mItem.getAssignedBp() != null)
            mCreatedByTextView.setText(mItem.getAssignedBp().getName());

        setupRowView(userType);
        setupActionEvent(eventBPListener);
    }
}