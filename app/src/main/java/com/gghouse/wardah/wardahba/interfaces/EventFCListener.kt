package com.gghouse.wardah.wardahba.interfaces

import com.gghouse.wardah.wardahba.webservices.model.VisitCalendar

interface EventFCListener : EventListener {
    fun onEventClicked(visitCalendar: VisitCalendar)

    fun onChecklistBAClicked(visitCalendar: VisitCalendar)

    fun onCheckInOutClicked(visitCalendar: VisitCalendar)

    fun onStoreInformationClicked(visitCalendar: VisitCalendar)
}
