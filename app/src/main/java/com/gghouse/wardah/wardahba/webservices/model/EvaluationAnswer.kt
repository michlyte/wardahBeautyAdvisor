package com.gghouse.wardah.wardahba.webservices.model

class EvaluationAnswer {
    val id: Long
    val value: Float

    constructor(id: Long, value: Float) {
        this.id = id
        this.value = value
    }
}