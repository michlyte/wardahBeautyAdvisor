package com.gghouse.wardah.wardahba.interfaces

import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA

interface ChecklistBAListener {
    fun onClick(checklistBA: ChecklistBA)

    fun onEvaluationButtonClicked(checklistBA: ChecklistBA)
}
