package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.ProductType

class StoreMonthlyEditRequest {
    val monthlyReportId: Long
    val competitorTurnover: Double
    val productTypes: List<ProductType>

    constructor(monthlyReportId: Long, competitorTurnover: Double, productTypes: List<ProductType>) {
        this.monthlyReportId = monthlyReportId
        this.competitorTurnover = competitorTurnover
        this.productTypes = productTypes
    }
}