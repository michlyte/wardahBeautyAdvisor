package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import java.io.Serializable

class EventCalendar : WardahHistoryRecyclerViewItem, Serializable {
    var calendarId: Long
    val title: String
    val startTime: Long
    val endTime: Long
    val locationAddress: String
    val checkInButtonEnable: Boolean
    val attendantButtonEnable: Boolean
    var assignedBp: AssignedBp? = null

    constructor(calendarId: Long, title: String, startTime: Long, endTime: Long, locationAddress: String, checkInButtonEnable: Boolean, attendantButtonEnable: Boolean, assignedBp: AssignedBp?) : super() {
        this.calendarId = calendarId
        this.title = title
        this.startTime = startTime
        this.endTime = endTime
        this.locationAddress = locationAddress
        this.checkInButtonEnable = checkInButtonEnable
        this.attendantButtonEnable = attendantButtonEnable
        this.assignedBp = assignedBp
    }
}