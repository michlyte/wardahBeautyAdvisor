package com.gghouse.wardah.wardahba.viewmodels

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.enumerations.ViewMode
import com.gghouse.wardah.wardahba.utils.SessionUtil
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.Customer
import com.gghouse.wardah.wardahba.webservices.request.CustomerEditRequest
import com.gghouse.wardah.wardahba.webservices.request.CustomerRequest
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InputCustomerActivityViewModel : ViewModel() {

    val customerMutableLiveData: MutableLiveData<Customer> by lazy {
        MutableLiveData<Customer>()
    }
    val viewModeMutableLiveData: MutableLiveData<ViewMode> by lazy {
        MutableLiveData<ViewMode>()
    }
    val isSubmitSuccessMutableLiveData: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    fun submit(name: String, mobileNumber: String, email: String, address: String) {
        if (SessionUtil.validateLoginSession()) {
            when (Config.mode) {
                ModeEnum.DUMMY_DEVELOPMENT -> Handler().postDelayed({ isSubmitSuccessMutableLiveData.postValue(true) }, Config.DELAY_LOAD_MORE.toLong())
                ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> {
                    val customerRequest = CustomerRequest(name, mobileNumber, email, address, SessionUtil.getUserId())

                    val callCustomerSubmit = ApiClient.client.apiCustomerCreate(customerRequest)
                    callCustomerSubmit.enqueue(object : Callback<GenericResponse> {
                        override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                            if (response.isSuccessful) {
                                val genericResponse = response.body()
                                when (genericResponse!!.code) {
                                    Config.CODE_200 -> isSubmitSuccessMutableLiveData.postValue(true)
                                    else -> {
                                        isSubmitSuccessMutableLiveData.postValue(false)
                                        ToastUtil.toastMessage(TAG, genericResponse.status!!)
                                    }
                                }
                            } else {
                                isSubmitSuccessMutableLiveData.postValue(false)
                                ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                            }
                        }

                        override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                            isSubmitSuccessMutableLiveData.postValue(false)
                            ToastUtil.toastMessage(TAG, t.message.toString())
                        }
                    })
                }
            }
        }
    }

    fun edit(customerId: Long?, name: String, mobileNumber: String, email: String, address: String) {
        if (SessionUtil.validateLoginSession()) {
            when (Config.mode) {
                ModeEnum.DUMMY_DEVELOPMENT -> Handler().postDelayed({ isSubmitSuccessMutableLiveData.postValue(true) }, Config.DELAY_LOAD_MORE.toLong())
                ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> {
                    val customerEditRequest = CustomerEditRequest(name, mobileNumber, email, address, SessionUtil.getUserId(), customerId)

                    val callCustomerEdit = ApiClient.client.apiCustomerEdit(customerEditRequest)
                    callCustomerEdit.enqueue(object : Callback<GenericResponse> {
                        override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                            if (response.isSuccessful) {
                                val genericResponse = response.body()
                                when (genericResponse!!.code) {
                                    Config.CODE_200 -> isSubmitSuccessMutableLiveData.postValue(true)
                                    else -> {
                                        isSubmitSuccessMutableLiveData.postValue(false)
                                        ToastUtil.toastMessage(TAG, genericResponse.status!!)
                                    }
                                }
                            } else {
                                isSubmitSuccessMutableLiveData.postValue(false)
                                ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                            }
                        }

                        override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                            isSubmitSuccessMutableLiveData.postValue(false)
                            ToastUtil.toastMessage(TAG, t.message.toString())
                        }
                    })
                }
            }
        }
    }

    companion object {
        private val TAG = InputCustomerActivityViewModel::class.java.simpleName
    }
}
