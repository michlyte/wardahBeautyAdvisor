package com.gghouse.wardah.wardahba.webservices

import com.gghouse.wardah.wardahba.webservices.request.*
import com.gghouse.wardah.wardahba.webservices.response.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by michael on 9/18/2016.
 * http://square.github.io/retrofit/
 */
interface ApiService {

    // Login
    @POST("/users/login")
    fun apiLogin(@Body loginRequest: LoginRequest): Call<LoginResponse>

    @GET("/user/profile")
    fun apiProfile(@Query("userId") userId: Long): Call<ProfileResponse>

    @POST("/user/change-password")
    fun apiChangePassword(@Body changePasswordRequest: ChangePasswordRequest): Call<GenericResponse>

    // Notification
    @GET("/notifications")
    fun apiNotifications(@Query("locationId") locationId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("sort") sort: String): Call<NotificationsResponse>


    // Home
    @GET("/home")
    fun apiBAHome(@Query("userId") userId: Long): Call<BAHomeResponse>

    @GET("/home")
    fun apiBPHome(@Query("userId") userId: Long): Call<BPHomeResponse>

    @GET("/home")
    fun apiFCHome(@Query("userId") userId: Long): Call<FCHomeResponse>


    // GPS
    @POST("/store")
    fun apiStoreGPS(@Body storeGPSRequest: StoreGPSRequest): Call<GenericResponse>

    // Test
    @GET("/test/latestDate")
    fun apiTestLatestDate(@Query("userId") userId: Long): Call<TestLatestDateResponse>

    @GET("/test/recap")
    fun apiTestRecap(@Query("userId") userId: Long): Call<TestRecapResponse>

    @GET("/test/score")
    fun apiTest(@Query("userId") userId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<HistoryTestResponse>

    @GET("/test/questions")
    fun apiTestQuestions(@Query("userId") userId: Long): Call<TestQuestionsResponse>

    @POST("/test/answers")
    fun apiSubmitTest(@Body answersRequest: AnswersRequest): Call<GenericResponse>


    // Product Highlight
    @GET("/product-highlight/recap")
    fun apiProductHighlightRecap(@Query("userId") userId: Long): Call<SalesRecapResponse>

    @GET("/product-highlight")
    fun apiProductHighlight(@Query("userId") userId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("sort") sort: String, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<ProductHighlightResponse>


    // Product Focus
    @GET("/product-focus/recap")
    fun apiProductFocusRecap(@Query("userId") userId: Long): Call<SalesRecapResponse>

    @GET("/product-focus")
    fun apiProductFocus(@Query("userId") userId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("sort") sort: String, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<ProductHighlightResponse>


    // Sales
    @GET("/sales/latestDate")
    fun apiSalesLatestDate(@Query("userId") userId: Long): Call<SalesLatestDateResponse>

    @GET("/sales/product")
    fun apiSalesInputProduct(@Query("locationId") locationId: Long): Call<SalesInputProductResponse>

    @GET("sales/product")
    fun apiSalesEditProduct(@Query("salesId") salesId: Long): Call<SalesEditProductResponse>

    @GET("/sales/recap")
    fun apiSalesRecap(@Query("userId") userId: Long): Call<SalesRecapResponse>

    @GET("/sales")
    fun apiSales(@Query("userId") userId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<SalesListResponse>

    @POST("/sales/create")
    fun apiSalesCreate(@Body salesInputRequest: SalesInputRequest): Call<GenericResponse>

    @POST("/sales/edit")
    fun apiSalesEdit(@Body salesEditRequest: SalesEditRequest): Call<GenericResponse>

    // Store Sales
    @GET("/store/sales/recap")
    fun apiStoreSalesRecap(@Query("locationId") locationId: Long): Call<StoreSalesRecapResponse>

    @GET("/store/daily/latestDate")
    fun apiDailySalesLatestDate(@Query("locationId") locationId: Long): Call<DailySalesLatestDateResponse>

    @GET("/store/monthly/latestMonth")
    fun apiMonthlySalesMonth(@Query("locationId") locationId: Long): Call<MonthlyLatestMonthResponse>

    @GET("/store/daily/sales")
    fun apiDailySales(@Query("locationId") locationId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("sort") sort: String, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<DailySalesResponse>

    @GET("/store/monthly/sales")
    fun apiMonthlySales(@Query("locationId") locationId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("sort") sort: String, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<MonthlySalesResponse>

    @GET("/store/product-highlight/recap")
    fun apiStoreProductHighlightRecap(@Query("locationId") locationId: Long): Call<SalesRecapResponse>

    @GET("/store/product-focus/recap")
    fun apiStoreProductFocusRecap(@Query("locationId") locationId: Long): Call<SalesRecapResponse>

    @GET("/store/daily/product-highlight")
    fun apiDailyProductHighlight(@Query("locationId") locationId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("sort") sort: String, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<ProductHighlightResponse>

    @GET("/store/monthly/product-highlight")
    fun apiMonthlyProductHighlight(@Query("locationId") locationId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("sort") sort: String, @Query("startMonth") startMonth: String, @Query("endMonth") endMonth: String): Call<ProductHighlightResponse>

    @GET("/store/daily/product-focus")
    fun apiDailyProductFocus(@Query("locationId") locationId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("sort") sort: String, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<ProductHighlightResponse>

    @GET("/store/monthly/product-focus")
    fun apiMonthlyProductFocus(@Query("locationId") locationId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("sort") sort: String, @Query("startMonth") startMonth: String, @Query("endMonth") endMonth: String): Call<ProductHighlightResponse>

    @GET("/store/monthly/category")
    fun apiMonthlyCategory(@Query("locationId") locationId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<CategoryResponse>

    @POST("/store/daily/create")
    fun apiDailyCreate(@Body storeDailyCreateRequest: StoreDailyCreateRequest): Call<GenericResponse>

    @GET("/store/daily/view")
    fun apiDailySalesView(@Query("dailyReportId") dailyReportId: Long): Call<DailySalesViewResponse>

    @POST("/store/daily/edit")
    fun apiDailyEdit(@Body storeDailyEditRequest: StoreDailyEditRequest): Call<GenericResponse>

    @GET("/store/product-type")
    fun apiMonthlySalesCreate(@Query("locationId") locationId: Long): Call<MonthlySalesCreateResponse>

    @POST("/store/monthly/create")
    fun apiMonthlyCreate(@Body storeMonthlyCreateRequest: StoreMonthlyCreateRequest): Call<GenericResponse>

    @GET("/store/monthly/view")
    fun apiMonthlySalesView(@Query("monthlyReportId") monthlyReportId: Long): Call<MonthlySalesViewResponse>

    @POST("/store/monthly/edit")
    fun apiMonthlyEdit(@Body storeMonthlyEditRequest: StoreMonthlyEditRequest): Call<GenericResponse>


    // Questioner
    @GET("/questionnaire/latestDate")
    fun apiQuestionerLatestDate(@Query("userId") userId: Long): Call<QuestionerLatestDateResponse>

    @GET("/questionnaire/questions")
    fun apiQuestioner(@Query("userId") userId: Long): Call<QuestionerQuestionsResponse>

    @POST("/questionnaire/answers")
    fun apiPostQuestioner(@Body questionerRequest: QuestionerRequest): Call<GenericResponse>


    // Customer
    @GET("/customer")
    fun apiCustomer(@Query("userId") locationId: Long, @Query("page") page: Int, @Query("size") size: Int, @Query("startDate") startDate: String?, @Query("endDate") endDate: String?): Call<CustomerListResponse>

    @POST("/customer/create")
    fun apiCustomerCreate(@Body customerRequest: CustomerRequest): Call<GenericResponse>

    @POST("/customer/edit")
    fun apiCustomerEdit(@Body customerEditRequest: CustomerEditRequest): Call<GenericResponse>


    // Event
    @GET("/event")
    fun apiEventListByDateRange(@Query("userId") userId: Long, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<MonthlyEventListResponse>

    @GET("/event")
    fun apiEventListByMonth(@Query("userId") userId: Long, @Query("month") month: String): Call<MonthlyEventListResponse>

    @GET("/event/view")
    fun apiEventView(@Query("calendarId") calendarId: Long, @Query("userId") userId: Long): Call<EventViewResponse>

    @POST("/event/create")
    fun apiEventCreate(@Body eventCreateRequest: EventCreateRequest): Call<GenericResponse>

    @POST("/event/edit")
    fun apiEventEdit(@Body eventEditRequest: EventEditRequest): Call<GenericResponse>

    @GET("/event/category")
    fun apiEventCategory(@Query("userId") userId: Long): Call<EventCategoryResponse>


    // Checklist
    @GET("/checklist")
    fun apiChecklistEvent(@Query("calendarId") calendarId: Long): Call<ChecklistEventResponse>

    @POST("/checklist/edit")
    fun apiChecklistEventEdit(@Body checklistEventEditRequest: ChecklistEventEditRequest): Call<GenericResponse>


    // Attendant
    @GET("/attendant/button")
    fun apiAttendantButton(@Query("calendarId") calendarId: Long): Call<AttendantButtonResponse>

    @GET("/attendant")
    fun apiAttendantEvent(@Query("calendarId") calendarId: Long, @Query("page") page: Int, @Query("size") size: Int): Call<AttendantEventResponse>

    @POST("/attendant/create")
    fun apiAttendantEventCreate(@Body attendantCreateRequest: AttendantCreateRequest): Call<GenericResponse>

    @GET("/attendant/view")
    fun apiAttendantView(@Query("attendantId") attendantId: Long): Call<AttendantViewResponse>

    @POST("/attendant/edit")
    fun apiAttendantEdit(@Body attendantEditRequest: AttendantEditRequest): Call<GenericResponse>


    // Check In
    @POST("/checkin")
    fun apiCheckIn(@Body checkInRequest: CheckInRequest): Call<GenericResponse>

    @POST("/checkin")
    fun apiFCCheckIn(@Body checkInFCRequest: CheckInFCRequest): Call<GenericResponse>

    @POST("/radius")
    fun apiCheckInRadius(@Query("userId") userId: Long, @Query("calendarId") calendarId: Long): Call<CheckInRadiusResponse>


    // Visit
    @GET("/visit")
    fun apiVisitListByDateRange(@Query("userId") userId: Long, @Query("startDate") startDate: String, @Query("endDate") endDate: String): Call<MonthlyVisitListResponse>

    @GET("/visit")
    fun apiVisitListByMonth(@Query("userId") userId: Long, @Query("month") month: String): Call<MonthlyVisitListResponse>

    @GET("/visit/view")
    fun apiVisitView(@Query("calendarId") calendarId: Long): Call<VisitViewResponse>

    @GET("/visit/category")
    fun apiVisitCategory(@Query("userId") userId: Long): Call<VisitCategoryResponse>

    @POST("/visit/create")
    fun apiVisitCreate(@Body visitCreateRequest: VisitCreateRequest): Call<GenericResponse>

    @POST("/visit/edit")
    fun apiVisitEdit(@Body visitEditRequest: VisitEditRequest): Call<GenericResponse>

    @POST("/checkout")
    fun apiVisitCheckOut(@Body visitCheckOutRequest: VisitCheckOutRequest): Call<GenericResponse>

    @GET("/dc/search")
    fun apiDCDropdown(@Query("q") q: String): Call<DCDropdownResponse>

    @GET("/location/search")
    fun apiDCLocationDropdown(@Query("dc") dc: String, @Query("q") q: String): Call<DCLocationDropdownResponse>


    // Checklist BA
    @GET("/ba-checklist")
    fun apiChecklistBAList(@Query("locationId") locationId: Long, @Query("calendarId") calendarId: Long): Call<ChecklistBAListResponse>

    @GET("/ba/information")
    fun apiBAInformation(@Query("userId") userId: Long): Call<BAInformationResponse>

    @GET("/evaluation/questions")
    fun apiEvaluationQuestions(@Query("userId") userId: Long): Call<EvaluationQuestionListResponse>

    @POST("/evaluation/answers")
    fun apiEvaluationSubmit(@Body evaluationAnswerRequest: EvaluationAnswerRequest): Call<GenericResponse>


    // Store Information
    @GET("/store/information")
    fun apiStoreInformation(@Query("storeId") storeId: Long): Call<StoreInformationResponse>


    // Sales Target
    @GET("/sales-target")
    fun apiSalesTargetHistory(@Query("userId") userId: Long, @Query("page") page: Int, @Query("size") size: Int): Call<SalesTargetHistoryResponse>

    @GET("/sales-target/view")
    fun apiSalesTargetView(@Query("salesTargetId") salesTargetId: Long): Call<SalesTargetViewResponse>

    @POST("/sales-target/create")
    fun apiSalesTargetCreate(@Body salesTargetCreateRequest: SalesTargetCreateRequest): Call<GenericResponse>

    @POST("/sales-target/edit")
    fun apiSalesTargetEdit(@Body salesTargetEditRequest: SalesTargetEditRequest): Call<GenericResponse>
}
