package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.webservices.model.Attendant;

public class AttendantViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final LinearLayout mLlContainer;
    public final CardView mCardView;
    public final TextView mTVName;
    public final TextView mTVNoHp;
    public Attendant mItem;

    public AttendantViewHolder(View view) {
        super(view);
        mView = view;
        mLlContainer = mView.findViewById(R.id.ll_container);
        mCardView = mView.findViewById(R.id.cv);
        mTVName = mView.findViewById(R.id.tv_name);
        mTVNoHp = mView.findViewById(R.id.noHpTextView);
    }

    private void setupActionEvent(WardahListener wardahListener) {
//        ViewMode viewMode;
//        if (mItem.getEditable()) {
//            viewMode = ViewMode.EDIT;
//        } else {
//            viewMode = ViewMode.VIEW;
//        }

        mLlContainer.setOnClickListener(v -> wardahListener.onClick(mItem, ViewMode.VIEW));
    }

    public void setData(Attendant attendant, WardahListener wardahListener) {
        mItem = attendant;
        mTVName.setText(mItem.getName());
        mTVNoHp.setText(mItem.getMobileNumber());

        setupActionEvent(wardahListener);
    }
}
