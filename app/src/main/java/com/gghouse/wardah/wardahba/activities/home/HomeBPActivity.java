package com.gghouse.wardah.wardahba.activities.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.attendant.AttendantActivity;
import com.gghouse.wardah.wardahba.activities.attendant.AttendantHistoryActivity;
import com.gghouse.wardah.wardahba.activities.event.CalendarMonthlyViewActivity;
import com.gghouse.wardah.wardahba.activities.event.CheckInActivity;
import com.gghouse.wardah.wardahba.activities.event.ChecklistActivity;
import com.gghouse.wardah.wardahba.activities.event.CreateEventActivity;
import com.gghouse.wardah.wardahba.activities.profile.ProfileActivity;
import com.gghouse.wardah.wardahba.activities.test.TestActivity;
import com.gghouse.wardah.wardahba.adapters.BannerSliderAdapter;
import com.gghouse.wardah.wardahba.adapters.HomepageMenuItemAdapter;
import com.gghouse.wardah.wardahba.enumerations.HomePageMenuEnum;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.models.IntentEventCategory;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.viewmodels.HomeBPActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.BPHome;
import com.gghouse.wardah.wardahba.webservices.model.LatestEvent;
import com.gghouse.wardah.wardahba.webservices.model.NearestEvent;

import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.Slider;

public class HomeBPActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private final static String TAG = HomeBPActivity.class.getSimpleName();
    private final static int ARG_RESULT_ACTIVITY = 1;

    private HomeBPActivityViewModel mModel;

    private SwipeRefreshLayout mSwipeRefresh;

    private TextView mUsername;
    private GridView mMenuGridView;
    private HomepageMenuItemAdapter mMenuAdapter;

    // Nearest Event
    private View mNearestEventLayout;
    private TextView mNearestEventTitleTextView;
    private TextView mNearestEventDateTextView;
    private TextView mNearestEventTimeTextView;
    private TextView mNearestEventLocationTextView;
    private TextView mNearestEventAlarmTextView;
    private TextView mNearestEventCategoryTextView;
    private TextView mNearestEventPersonTextView;
    private TextView mNearestEventCheckInTextView;
    private TextView mNearestEventCheckTextView;
    private Button mNearestEventChecklistButton;
    private Button mNearestEventCheckInButton;
    private Button mNearestEventParticipantButton;

    // Latest Event
    private View mLatestEventLayout;
    private Slider mLatestEventSlider;
    private BannerSliderAdapter mSliderAdapter;
    private TextView mLatestEventTitleTextView;
    private TextView mLatestEventDateTextView;
    private TextView mLatestEventTimeTextView;
    private TextView mLatestEventLocationTextView;
    private TextView mLatestEventAlarmTextView;
    private TextView mLatestEventCategoryTextView;
    private TextView mLatestEventPersonTextView;
    private TextView mLatestEventCheckInTextView;
    private TextView mLatestEventCheckTextView;
    private Button mLatestEventChecklistButton;
    private Button mLatestEventCheckInButton;
    private Button mLatestEventParticipantButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_bp);

        mModel = ViewModelProviders.of(this).get(HomeBPActivityViewModel.class);

        mSwipeRefresh = findViewById(R.id.swipe_container);
        mSwipeRefresh.setOnRefreshListener(this);

        mUsername = findViewById(R.id.etHeaderUsername);
        TextView profileTextView = findViewById(R.id.etProfile);
        profileTextView.setOnClickListener(this);

        mMenuGridView = findViewById(R.id.gridview);
        mMenuAdapter = new HomepageMenuItemAdapter(new ArrayList<>());
        mMenuGridView.setAdapter(mMenuAdapter);

        bindNearestEventViews();
        bindLatestEventViews();

        mModel.getUserMutableLiveData().observe(this, user -> {
            if (user != null) {
                mUsername.setText(user.getFullname());
            } else {
                mUsername.setText(R.string.placeholder_user);
            }

            mModel.getMenuMutableLiveData().postValue(new ArrayList<Integer>() {{
                add(HomePageMenuEnum.EVENT.getResourceId());
                add(HomePageMenuEnum.TEST.getResourceId());
            }});
        });

        mModel.getMenuMutableLiveData().observe(this, menuList -> {
            if (menuList != null) {
                mMenuAdapter.setData(menuList);
                mMenuAdapter.notifyDataSetChanged();

                mMenuGridView.setOnItemClickListener((parent, v, position, id) -> {

                    HomePageMenuEnum homePageMenuEnum = HomePageMenuEnum.Companion.getHomePageMenuEnum(menuList.get(position));
                    switch (homePageMenuEnum) {
                        case TEST:
                            Intent iTest = new Intent(getBaseContext(), TestActivity.class);
                            startActivity(iTest);
                            break;
                        case EVENT:
                            Intent iEvent = new Intent(getBaseContext(), CalendarMonthlyViewActivity.class);
                            startActivity(iEvent);
                            break;
                    }
                });
            }
        });

        mModel.getProfileMutableLiveData().observe(this, profile -> {
            if (profile != null) {
                Intent iProfile = new Intent(this, ProfileActivity.class);
                iProfile.putExtra(IntentUtil.Companion.getDATA(), profile);
                startActivity(iProfile);
            }
        });

        mModel.getBpHomeMutableLiveData().observe(this, bpHome -> {
            mSwipeRefresh.setRefreshing(false);

            if (bpHome != null) {
                setBPHomepageData(bpHome);
            }
        });

        mModel.getEventMutableLiveData().observe(this, event -> {
            if (event != null) {
                Intent iCreateEvent = new Intent(getBaseContext(), CreateEventActivity.class);
                iCreateEvent.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.valueOf(event.getViewType()));
                iCreateEvent.putExtra(IntentUtil.Companion.getEVENT(), event);
                iCreateEvent.putExtra(IntentUtil.Companion.getEVENT_CATEGORY(), new IntentEventCategory(event.getCategories()));
                startActivity(iCreateEvent);
            }
        });

        mModel.getChecklistEventMutableLiveData().observe(this, checklistEvent -> {
            if (checklistEvent != null) {
                Intent iChecklist = new Intent(getBaseContext(), ChecklistActivity.class);
                if (checklistEvent.getEditButtonEnable()) {
                    iChecklist.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.EDIT);
                } else {
                    iChecklist.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.VIEW);
                }
                iChecklist.putExtra(IntentUtil.Companion.getCHECKLIST(), checklistEvent);
                startActivityForResult(iChecklist, ARG_RESULT_ACTIVITY);
            }
        });

        mModel.getAttendantButtonMutableLiveData().observe(this, attendantButton -> {
            if (attendantButton != null) {
                if (attendantButton.getInputButtonEnable()) {
                    Intent iAttendant = new Intent(getBaseContext(), AttendantActivity.class);
                    iAttendant.putExtra(IntentUtil.Companion.getATTENDANT_BUTTON(), attendantButton);
                    startActivityForResult(iAttendant, ARG_RESULT_ACTIVITY);
                } else {
                    Intent iAttendantHistory = new Intent(getBaseContext(), AttendantHistoryActivity.class);
                    iAttendantHistory.putExtra(IntentUtil.Companion.getCALENDAR_ID(), attendantButton.getCalendarId());
                    startActivity(iAttendantHistory);
                }
            }
        });

        mModel.getCheckInRadiusBPMutableLiveData().observe(this, checkInRadius -> {
            if (checkInRadius != null) {
                Intent iCheckIn = new Intent(getBaseContext(), CheckInActivity.class);
                iCheckIn.putExtra(IntentUtil.Companion.getCHECK_IN_RADIUS(), checkInRadius);
                startActivityForResult(iCheckIn, ARG_RESULT_ACTIVITY);
            }
        });

        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_homepage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notification:
                Intent iNotification = new Intent(this, NotificationActivity.class);
                startActivity(iNotification);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        mModel.getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ARG_RESULT_ACTIVITY && resultCode == Activity.RESULT_OK) {
            mModel.getData();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etProfile:
                mModel.callProfileData();
                break;
            // Nearest Event
            case R.id.nearestEventLayout:
                if (mModel.getBpHomeMutableLiveData().getValue() != null) {
                    NearestEvent nearestEvent = mModel.getBpHomeMutableLiveData().getValue().getNearestEvent();
                    if (nearestEvent != null) {
                        mModel.callEventView(nearestEvent.getCalendarId(), SessionUtil.getUserId());
                    }
                }
                break;
            case R.id.nearestEventChecklistButton:
                NearestEvent nearestEvent = mModel.getBpHomeMutableLiveData().getValue().getNearestEvent();
                if (nearestEvent != null) {
                    mModel.callChecklistEventData(nearestEvent.getCalendarId());
                }
                break;
            case R.id.nearestEventCheckInButton:
                nearestEvent = mModel.getBpHomeMutableLiveData().getValue().getNearestEvent();
                if (nearestEvent != null) {
                    mModel.callCheckInRadius(SessionUtil.getUserId(), nearestEvent.getCalendarId());
                }
                break;
            case R.id.nearestEventParticipantButton:
                nearestEvent = mModel.getBpHomeMutableLiveData().getValue().getNearestEvent();
                if (nearestEvent != null) {
                    mModel.callAttendantEventData(nearestEvent.getCalendarId());
                }
                break;
            // Latest Event
            case R.id.latestEventLayout:
                if (mModel.getBpHomeMutableLiveData().getValue() != null) {
                    LatestEvent latestEvent = mModel.getBpHomeMutableLiveData().getValue().getLatestEvent();
                    if (latestEvent != null) {
                        mModel.callEventView(latestEvent.getCalendarId(), SessionUtil.getUserId());
                    }
                }
                break;
            case R.id.latestEventChecklistButton:
                LatestEvent latestEvent = mModel.getBpHomeMutableLiveData().getValue().getLatestEvent();
                if (latestEvent != null) {
                    mModel.callChecklistEventData(latestEvent.getCalendarId());
                }
                break;
            case R.id.latestEventCheckInButton:
                latestEvent = mModel.getBpHomeMutableLiveData().getValue().getLatestEvent();
                if (latestEvent != null) {
                    mModel.callCheckInRadius(SessionUtil.getUserId(), latestEvent.getCalendarId());
                }
                break;
            case R.id.latestEventParticipantButton:
                latestEvent = mModel.getBpHomeMutableLiveData().getValue().getLatestEvent();
                if (latestEvent != null) {
                    mModel.callAttendantEventData(latestEvent.getCalendarId());
                }
                break;
        }
    }

    private void bindNearestEventViews() {
        mNearestEventLayout = findViewById(R.id.nearestEventLayout);
        mNearestEventLayout.setOnClickListener(this);
        mNearestEventTitleTextView = findViewById(R.id.nearestEventTitleTextView);
        mNearestEventDateTextView = findViewById(R.id.nearestEventDateTextView);
        mNearestEventTimeTextView = findViewById(R.id.nearestEventTimeTextView);
        mNearestEventLocationTextView = findViewById(R.id.nearestEventLocationTextView);
        mNearestEventAlarmTextView = findViewById(R.id.nearestEventAlarmTextView);
        mNearestEventCategoryTextView = findViewById(R.id.nearestEventCategoryTextView);
        mNearestEventPersonTextView = findViewById(R.id.nearestEventPersonTextView);
        mNearestEventCheckInTextView = findViewById(R.id.nearestEventCheckInTextView);
        mNearestEventCheckTextView = findViewById(R.id.nearestEventCheckTextView);
        findViewById(R.id.nearestEventCreatedByLinearLayout).setVisibility(View.GONE);

        mNearestEventChecklistButton = findViewById(R.id.nearestEventChecklistButton);
        mNearestEventChecklistButton.setOnClickListener(this);
        mNearestEventCheckInButton = findViewById(R.id.nearestEventCheckInButton);
        mNearestEventCheckInButton.setOnClickListener(this);
        mNearestEventParticipantButton = findViewById(R.id.nearestEventParticipantButton);
        mNearestEventParticipantButton.setOnClickListener(this);
    }

    private void bindLatestEventViews() {
        mLatestEventLayout = findViewById(R.id.latestEventLayout);
        mLatestEventSlider = findViewById(R.id.bannerSlider);
        mSliderAdapter = new BannerSliderAdapter(ImageUtil.INSTANCE.getBannerImages());
        mLatestEventSlider.setAdapter(mSliderAdapter);
        mLatestEventSlider.setOnSlideClickListener(position -> {
            Intent iImagePreview = new Intent(this, ImagePreviewActivity.class);
            iImagePreview.putExtra(IntentUtil.Companion.getDATA(), mSliderAdapter.getUrl(position));
            startActivity(iImagePreview);
        });
        mLatestEventTitleTextView = findViewById(R.id.latestEventTitleTextView);
        mLatestEventDateTextView = findViewById(R.id.latestEventDateTextView);
        mLatestEventTimeTextView = findViewById(R.id.latestEventTimeTextView);
        mLatestEventLocationTextView = findViewById(R.id.latestEventLocationTextView);
        mLatestEventAlarmTextView = findViewById(R.id.latestEventAlarmTextView);
        mLatestEventCategoryTextView = findViewById(R.id.latestEventCategoryTextView);
        mLatestEventPersonTextView = findViewById(R.id.latestEventPersonTextView);
        mLatestEventCheckInTextView = findViewById(R.id.latestEventCheckInTextView);
        mLatestEventCheckTextView = findViewById(R.id.latestEventCheckTextView);
        findViewById(R.id.latestEventCreatedByLinearLayout).setVisibility(View.GONE);

        mLatestEventChecklistButton = findViewById(R.id.latestEventChecklistButton);
        mLatestEventChecklistButton.setOnClickListener(this);
        mLatestEventCheckInButton = findViewById(R.id.latestEventCheckInButton);
        mLatestEventCheckInButton.setOnClickListener(this);
        mLatestEventParticipantButton = findViewById(R.id.latestEventParticipantButton);
        mLatestEventParticipantButton.setOnClickListener(this);
    }

    private void setBPHomepageData(BPHome bpHome) {
        NearestEvent nearestEvent = bpHome.getNearestEvent();
        if (nearestEvent != null) {
            mNearestEventLayout.setVisibility(View.VISIBLE);
            mNearestEventTitleTextView.setText(nearestEvent.getTitle());
            mNearestEventDateTextView.setText(DateTimeUtil.INSTANCE.getEventDateStr(nearestEvent.getStartTime()));
            mNearestEventTimeTextView.setText(DateTimeUtil.INSTANCE.getEventTimeStr(nearestEvent.getStartTime(), nearestEvent.getEndTime()));
            mNearestEventAlarmTextView.setText(DateTimeUtil.INSTANCE.getEventAlarmStr(nearestEvent.getAlarmTime()));
            mNearestEventLocationTextView.setText(nearestEvent.getLocation().getFormattedAddress());
            mNearestEventCategoryTextView.setText(nearestEvent.getCategory());
            mNearestEventCheckTextView.setText(WardahUtil.INSTANCE.getEventChecklistStr(nearestEvent.getChecklistCompleted(), nearestEvent.getTotalChecklist()));

            WardahUtil.INSTANCE.setEventButtons(nearestEvent.getCheckInButtonEnable(), nearestEvent.getAttendantButtonEnable(),
                    mNearestEventChecklistButton, mNearestEventCheckInButton, mNearestEventParticipantButton);
        }

        LatestEvent latestEvent = bpHome.getLatestEvent();
        if (latestEvent != null) {
            mLatestEventLayout.setVisibility(View.VISIBLE);
            mLatestEventLayout.setOnClickListener(this);
            if (latestEvent.getBpImageURL() != null && latestEvent.getBoothImageURL() != null) {
                List<String> latestEventImages = new ArrayList<String>() {{
                    add(ImageUtil.INSTANCE.getImageUrl(latestEvent.getBpImageURL()));
                    add(ImageUtil.INSTANCE.getImageUrl(latestEvent.getBoothImageURL()));
                }};
                mSliderAdapter.setData(latestEventImages);
                mLatestEventSlider.setAdapter(mSliderAdapter);
                mLatestEventSlider.setVisibility(View.VISIBLE);
            } else {
                mLatestEventSlider.setVisibility(View.GONE);
            }

            mLatestEventTitleTextView.setText(latestEvent.getTitle());
            mLatestEventDateTextView.setText(DateTimeUtil.INSTANCE.getEventDateStr(latestEvent.getStartTime()));
            mLatestEventTimeTextView.setText(DateTimeUtil.INSTANCE.getEventTimeStr(latestEvent.getStartTime(), latestEvent.getEndTime()));
            mLatestEventAlarmTextView.setText(DateTimeUtil.INSTANCE.getEventAlarmStr(latestEvent.getAlarmTime()));
            mLatestEventLocationTextView.setText(latestEvent.getLocation().getFormattedAddress());
            mLatestEventCategoryTextView.setText(latestEvent.getCategory());
            mLatestEventPersonTextView.setText(WardahUtil.INSTANCE.getEventNumberOfAttendantStr(latestEvent.getTotalAttendant()));
            mLatestEventCheckInTextView.setText(DateTimeUtil.INSTANCE.getEventCheckInStr(latestEvent.getCheckInTime()));
            mLatestEventCheckTextView.setText(WardahUtil.INSTANCE.getEventChecklistStr(latestEvent.getChecklistCompleted(), latestEvent.getTotalChecklist()));

            WardahUtil.INSTANCE.setEventButtons(latestEvent.getCheckInButtonEnable(), latestEvent.getAttendantButtonEnable(),
                    mLatestEventChecklistButton, mLatestEventCheckInButton, mLatestEventParticipantButton);
        } else {
            mLatestEventLayout.setOnClickListener(null);
        }
    }
}
