package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.SalesEditProducts

class SalesEditProductResponse : GenericResponse() {
    var data: SalesEditProducts? = null
}
