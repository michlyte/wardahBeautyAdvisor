package com.gghouse.wardah.wardahba.dummy

import com.gghouse.wardah.wardahba.webservices.model.Customer
import java.util.*

object CustomerDummy {
    val CUSTOMER_SIMPLE_LIST: List<Customer> = object : ArrayList<Customer>() {
        init {
            add(Customer(0L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri", Date().time))
            add(Customer(1L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri", Date().time - 24 * 60 * 60 * 1000))
            add(Customer(2L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri", Date().time - 48 * 60 * 60 * 1000))
            add(Customer(3L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri", Date().time - 72 * 60 * 60 * 1000))
            add(Customer(4L, "Michael Halim", "08123456789", "mikefla10@gmail.com", "Jl. Ciganitri", Date().time - 96 * 60 * 60 * 1000))
        }
    }
}