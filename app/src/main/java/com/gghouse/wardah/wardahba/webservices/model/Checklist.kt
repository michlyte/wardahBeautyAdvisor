package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class Checklist : Serializable {
    val checklistId: Long
    val content: String
    var status: Boolean

    constructor(checklistId: Long, content: String, status: Boolean) {
        this.checklistId = checklistId
        this.content = content
        this.status = status
    }
}