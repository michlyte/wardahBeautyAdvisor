package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.DCDropdown

class DCDropdownResponse : GenericResponse() {
    var data: List<DCDropdown>? = null
}
