package com.gghouse.wardah.wardahba.activities.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.WelcomeActivity;
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.LocationUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.viewmodels.ProfileActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.Profile;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ProfileActivityViewModel mModel;
    private static final int RESULT_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mModel = ViewModelProviders.of(this).get(ProfileActivityViewModel.class);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView nipTextView = findViewById(R.id.nipTextView);
        TextView fullNameTextView = findViewById(R.id.fullNameTextView);
        TextView brandTextView = findViewById(R.id.brandTextView);
        TextView firstDateWorkTextView = findViewById(R.id.firstDateWorkTextView);
        TextView positionTextView = findViewById(R.id.positionTextView);
        TextView locationTextView = findViewById(R.id.locationTextView);
        TextView cityTextView = findViewById(R.id.cityTextView);
        TextView provinceTextView = findViewById(R.id.provinceTextView);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.getDrawable().mutate().setTint(ContextCompat.getColor(this, android.R.color.white));
        fab.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent != null && intent.getSerializableExtra(IntentUtil.Companion.getDATA()) != null) {
            mModel.getProfileMutableLiveData().postValue((Profile) intent.getSerializableExtra(IntentUtil.Companion.getDATA()));
        }

        mModel.getProfileMutableLiveData().observe(this, profile -> {
            if (profile != null) {

                UserTypeEnum userTypeEnum = SessionUtil.getUserTypeEnum();
                if (userTypeEnum != null) {
                    switch (SessionUtil.getUserTypeEnum()) {
                        case BEAUTY_ADVISOR:
                            if (profile.isButtonEnable() != null && profile.isButtonEnable()) {
                                fab.setVisibility(View.VISIBLE);
                            } else {
                                fab.setVisibility(View.GONE);
                            }
                            break;
                    }
                }

                nipTextView.setText(String.valueOf(profile.getNip()));
                fullNameTextView.setText(profile.getFullname());
                brandTextView.setText(profile.getBrand());
                firstDateWorkTextView.setText(DateTimeUtil.INSTANCE.getSdfDayCommaDateMonthYear().format(profile.getFirstDateWork()));
                positionTextView.setText(profile.getPosition());
                locationTextView.setText(profile.getLocation());
                cityTextView.setText(profile.getCity());
                provinceTextView.setText(profile.getProvince());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_change_password:
                Intent iChangePassword = new Intent(this, ChangePasswordActivity.class);
                startActivity(iChangePassword);
                return true;
            case R.id.action_logout:
                SessionUtil.loggingOut();

                Intent iLogin = new Intent(this, WelcomeActivity.class);
                iLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(iLogin);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                if (LocationUtil.INSTANCE.isGPSEnabled()) {
                    Profile profile = mModel.getProfileMutableLiveData().getValue();

                    if (profile != null) {
                        Intent iStoreGPS = new Intent(this, SelectStoreGPSActivity.class);
                        iStoreGPS.putExtra(IntentUtil.Companion.getSTORE_ID(), profile.getStoreId());
                        startActivityForResult(iStoreGPS, RESULT_ACTIVITY);
                    }
                } else {
                    LocationUtil.INSTANCE.popupEnableGPS(this);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case RESULT_ACTIVITY:
                mModel.callProfileData();
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
