package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.StoreSalesRecap

class StoreSalesRecapResponse {
    var data: StoreSalesRecap? = null
}
