package com.gghouse.wardah.wardahba.models

import java.io.Serializable

open class WardahVisit : Serializable {
    val calendarId: Long? = null
    val startTime: Long
    val endTime: Long

    constructor(startTime: Long, endTime: Long) {
        this.startTime = startTime
        this.endTime = endTime
    }
}