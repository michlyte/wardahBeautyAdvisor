package com.gghouse.wardah.wardahba.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ViewMode
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.ChecklistEvent
import com.gghouse.wardah.wardahba.webservices.model.Event
import com.gghouse.wardah.wardahba.webservices.request.ChecklistEventEditRequest
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChecklistActivityViewModel : ViewModel() {
    val checklistEventMutableLiveData: MutableLiveData<ChecklistEvent> by lazy {
        MutableLiveData<ChecklistEvent>()
    }
    val viewModeMutableLiveData: MutableLiveData<ViewMode> by lazy {
        MutableLiveData<ViewMode>()
    }
    val eventMutableLiveData: MutableLiveData<Event> by lazy {
        MutableLiveData<Event>()
    }
    val isSubmitSucceedMutableLiveData: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    fun getData() {

    }

    fun editChecklistEvent(checklistEventEditRequest: ChecklistEventEditRequest) {
        val callMonthlySalesEdit = ApiClient.client.apiChecklistEventEdit(checklistEventEditRequest)
        callMonthlySalesEdit.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                        else -> {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.retrofitFailMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSucceedMutableLiveData.postValue(false)
                    ToastUtil.retrofitFailMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSucceedMutableLiveData.postValue(false)
                ToastUtil.retrofitFailMessage(TAG, t.message.toString())
            }
        })
    }

    companion object {
        private val TAG = ChecklistActivityViewModel::class.java.simpleName
    }
}
