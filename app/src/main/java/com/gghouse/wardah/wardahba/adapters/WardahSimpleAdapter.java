package com.gghouse.wardah.wardahba.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.ChecklistBAListener;
import com.gghouse.wardah.wardahba.interfaces.ChecklistListener;
import com.gghouse.wardah.wardahba.interfaces.EventBPListener;
import com.gghouse.wardah.wardahba.interfaces.EventFCListener;
import com.gghouse.wardah.wardahba.interfaces.EventListener;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.viewholders.AttendantViewHolder;
import com.gghouse.wardah.wardahba.viewholders.CategoryInputViewHolder;
import com.gghouse.wardah.wardahba.viewholders.CategoryViewHolder;
import com.gghouse.wardah.wardahba.viewholders.ChecklistBAViewHolder;
import com.gghouse.wardah.wardahba.viewholders.ChecklistViewHolder;
import com.gghouse.wardah.wardahba.viewholders.CustomerViewHolder;
import com.gghouse.wardah.wardahba.viewholders.EvaluationBAViewHolder;
import com.gghouse.wardah.wardahba.viewholders.EventFCViewHolder;
import com.gghouse.wardah.wardahba.viewholders.EventViewHolder;
import com.gghouse.wardah.wardahba.viewholders.ProductHighlightStoreDetailHistoryViewHolder;
import com.gghouse.wardah.wardahba.viewholders.ProductHighlightViewHolder;
import com.gghouse.wardah.wardahba.viewholders.SalesTargetViewHolder;
import com.gghouse.wardah.wardahba.viewholders.SalesViewHolder;
import com.gghouse.wardah.wardahba.viewholders.StoreDailySalesViewHolder;
import com.gghouse.wardah.wardahba.viewholders.StoreMonthlySalesViewHolder;
import com.gghouse.wardah.wardahba.viewholders.TestViewHolder;
import com.gghouse.wardah.wardahba.webservices.model.Attendant;
import com.gghouse.wardah.wardahba.webservices.model.Checklist;
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA;
import com.gghouse.wardah.wardahba.webservices.model.Customer;
import com.gghouse.wardah.wardahba.webservices.model.DailySales;
import com.gghouse.wardah.wardahba.webservices.model.EventCalendar;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySales;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;
import com.gghouse.wardah.wardahba.webservices.model.Sales;
import com.gghouse.wardah.wardahba.webservices.model.SalesTarget;
import com.gghouse.wardah.wardahba.webservices.model.Test;
import com.gghouse.wardah.wardahba.webservices.model.VisitCalendar;

import java.util.List;

public class WardahSimpleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private final WardahItemType mWardahItemType;
    private WardahListener mWardahListener;
    private UserTypeEnum mUserType;
    private EventListener mEventListener;
    private ChecklistListener mChecklistListener;
    private ChecklistBAListener mChecklistBAListener;
    private ViewMode mViewMode;
    private List<?> mValues;

    public WardahSimpleAdapter(Context context, WardahItemType wardahItemType, List<?> items) {
        mContext = context;
        mWardahItemType = wardahItemType;
        mValues = items;
    }

    public WardahSimpleAdapter(Context context, WardahItemType wardahItemType, List<?> items, WardahListener wardahSimpleListener) {
        this(context, wardahItemType, items);
        mWardahListener = wardahSimpleListener;
    }

    public WardahSimpleAdapter(Context context, WardahItemType wardahItemType, UserTypeEnum userType, List<?> items, EventListener eventListener) {
        this(context, wardahItemType, items);
        mUserType = userType;
        mEventListener = eventListener;
    }

    public WardahSimpleAdapter(Context context, WardahItemType wardahItemType, List<?> items, EventListener eventListener) {
        this(context, wardahItemType, items);
        mEventListener = eventListener;
    }

    public WardahSimpleAdapter(Context context, WardahItemType wardahItemType, ViewMode viewMode, List<?> items, ChecklistListener checklistListener) {
        this(context, wardahItemType, items);
        mViewMode = viewMode;
        mChecklistListener = checklistListener;
    }

    public WardahSimpleAdapter(Context context, WardahItemType wardahItemType, ViewMode viewMode, List<?> items, ChecklistBAListener checklistBAListener) {
        this(context, wardahItemType, items);
        mViewMode = viewMode;
        mChecklistBAListener = checklistBAListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (mWardahItemType) {
            case CUSTOMER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_pelanggan, parent, false);
                return new CustomerViewHolder(view);
            case ATTENDANT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_peserta, parent, false);
                return new AttendantViewHolder(view);
            case TEST:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_test, parent, false);
                return new TestViewHolder(view);
            case SALES:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_sales, parent, false);
                return new SalesViewHolder(view);
            case PRODUCT_HIGHLIGHT:
            case PRODUCT_FOCUS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_product_highlight, parent, false);
                return new ProductHighlightViewHolder(view);
            case STORE_DAILY_SALES:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_store_sales_harian, parent, false);
                return new StoreDailySalesViewHolder(view);
            case STORE_MONTHLY_SALES:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_store_sales_bulanan, parent, false);
                return new StoreMonthlySalesViewHolder(view);
            case CATEGORY:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_category, parent, false);
                return new CategoryViewHolder(view);
            case CATEGORY_INPUT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_input_category, parent, false);
                return new CategoryInputViewHolder(view);
            case PRODUCT_HIGHLIGHT_STORE_DETAIL_HISTORY:
            case PRODUCT_FOCUS_STORE_DETAIL_HISTORY:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_product_highlight_store_history, parent, false);
                return new ProductHighlightStoreDetailHistoryViewHolder(view);
            case EVENT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_event, parent, false);
                return new EventViewHolder(view);
            case EVENT_FC:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_event_fc, parent, false);
                return new EventFCViewHolder(view);
            case CHECKLIST:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_checklist, parent, false);
                return new ChecklistViewHolder(view);
            case EVENT_CHECKLIST:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_event_checklist, parent, false);
                return new ChecklistViewHolder(view);
            case EVENT_CHECKLIST_BA:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_checklist_ba, parent, false);
                return new ChecklistBAViewHolder(view);
            case INPUT_TARGET:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_sales_target, parent, false);
                return new SalesTargetViewHolder(view);
            case BA_EVALUATION:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ba_evaluation, parent, false);
                return new EvaluationBAViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (mWardahItemType) {
            case CUSTOMER:
                final CustomerViewHolder customerViewHolder = (CustomerViewHolder) holder;
                customerViewHolder.setData(mContext, (Customer) mValues.get(position), mWardahListener);
                break;
            case ATTENDANT:
                final AttendantViewHolder attendantViewHolder = (AttendantViewHolder) holder;
                attendantViewHolder.setData((Attendant) mValues.get(position), mWardahListener);
                break;
            case TEST:
                final TestViewHolder testViewHolder = (TestViewHolder) holder;
                testViewHolder.setData(mContext, (Test) mValues.get(position));
                break;
            case SALES:
                final SalesViewHolder salesViewHolder = (SalesViewHolder) holder;
                salesViewHolder.setData(mContext, (Sales) mValues.get(position), mWardahListener);
                break;
            case PRODUCT_HIGHLIGHT:
            case PRODUCT_FOCUS:
                ProductHighlightViewHolder productHighlightViewHolder = (ProductHighlightViewHolder) holder;
                productHighlightViewHolder.setData(mContext, (ProductHighlight) mValues.get(position), ViewMode.VIEW);
                break;
            case STORE_DAILY_SALES:
                final StoreDailySalesViewHolder storeDailySalesViewHolder = (StoreDailySalesViewHolder) holder;
                storeDailySalesViewHolder.setData(mContext, (DailySales) mValues.get(position), mWardahListener);
                break;
            case STORE_MONTHLY_SALES:
                final StoreMonthlySalesViewHolder storeMonthlySalesViewHolder = (StoreMonthlySalesViewHolder) holder;
                storeMonthlySalesViewHolder.setData(mContext, (MonthlySales) mValues.get(position), mWardahListener);
                break;
            case CATEGORY:
                final CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
                categoryViewHolder.setData((ProductType) mValues.get(position));
                break;
            case CATEGORY_INPUT:
                final CategoryInputViewHolder categoryInputViewHolder = (CategoryInputViewHolder) holder;
                categoryInputViewHolder.setData((ProductType) mValues.get(position), mWardahListener);
                break;
            case PRODUCT_HIGHLIGHT_STORE_DETAIL_HISTORY:
            case PRODUCT_FOCUS_STORE_DETAIL_HISTORY:
                final ProductHighlightStoreDetailHistoryViewHolder productHighlightStoreDetailHistoryViewHolder = (ProductHighlightStoreDetailHistoryViewHolder) holder;
                productHighlightStoreDetailHistoryViewHolder.setData((ProductHighlight) mValues.get(position));
                break;
            case EVENT:
                final EventViewHolder eventViewHolder = (EventViewHolder) holder;
                eventViewHolder.setData(mUserType, (EventCalendar) mValues.get(position), (EventBPListener) mEventListener);
                break;
            case EVENT_FC:
                final EventFCViewHolder eventFCViewHolder = (EventFCViewHolder) holder;
                eventFCViewHolder.setData(mContext, (VisitCalendar) mValues.get(position), (EventFCListener) mEventListener);
                break;
            case CHECKLIST:
                final ChecklistViewHolder checklistViewHolder = (ChecklistViewHolder) holder;
                checklistViewHolder.setData(mViewMode, (Checklist) mValues.get(position), mChecklistListener);
                break;
            case EVENT_CHECKLIST:
                final ChecklistViewHolder checklistViewHolder2 = (ChecklistViewHolder) holder;
                checklistViewHolder2.setData(mViewMode, (Checklist) mValues.get(position), mChecklistListener);
                break;
            case EVENT_CHECKLIST_BA:
                final ChecklistBAViewHolder checklistBAViewHolder = (ChecklistBAViewHolder) holder;
                checklistBAViewHolder.setData(mViewMode, (ChecklistBA) mValues.get(position), mChecklistBAListener);
                break;
            case INPUT_TARGET:
                final SalesTargetViewHolder salesTargetViewHolder = (SalesTargetViewHolder) holder;
                salesTargetViewHolder.setData(mContext, (SalesTarget) mValues.get(position), mWardahListener);
                break;
            case BA_EVALUATION:
                final EvaluationBAViewHolder evaluationBAViewHolder = (EvaluationBAViewHolder) holder;
                evaluationBAViewHolder.setData((ChecklistBA) mValues.get(position), mChecklistBAListener);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mValues == null ? 0 : mValues.size();
    }

    public void setData(List<?> mValues) {
        this.mValues = mValues;
    }

    public List<?> getData() {
        return mValues;
    }

    public void setViewMode(ViewMode viewMode) {
        mViewMode = viewMode;
    }
}
