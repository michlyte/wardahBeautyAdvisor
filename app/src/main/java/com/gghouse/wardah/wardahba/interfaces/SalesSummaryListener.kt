package com.gghouse.wardah.wardahba.interfaces

import com.gghouse.wardah.wardahba.enumerations.WardahItemType
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight

interface SalesSummaryListener {
    fun onAdd(type: WardahItemType, productHighlight: ProductHighlight)

    fun onRemove(type: WardahItemType, productHighlight: ProductHighlight)
}
