package com.gghouse.wardah.wardahba.activities.customer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.WardahHistoryActivity;
import com.gghouse.wardah.wardahba.adapters.WardahHistoryAdapter;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.OnLoadMoreListListener;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.viewmodels.CustomerHistoryActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.Customer;
import com.gghouse.wardah.wardahba.webservices.model.Pagination;

import java.util.ArrayList;
import java.util.List;

import mehdi.sakout.dynamicbox.DynamicBox;

public class CustomerHistoryActivity extends WardahHistoryActivity implements WardahListener {

    public static final String TAG = CustomerHistoryActivity.class.getSimpleName();

    static final int CUSTOMER_EDIT_ACTIVITY = 15;

    private CustomerHistoryActivityViewModel mModel;

    private RecyclerView mRecyclerView;
    private WardahHistoryAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView mTvMessage;

    /*
     * Data Source and Pagination
     */
    private OnLoadMoreListListener mOnLoadMoreListListener;

    /*
     * Loading view
     */
    private DynamicBox mDynamicBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mModel = ViewModelProviders.of(this).get(CustomerHistoryActivityViewModel.class);

        mOnLoadMoreListListener = () -> {
            new Handler().post(() -> {
                mAdapter.add(null);
                mAdapter.notifyItemInserted(mAdapter.getItemCount() - 1);
            });
            mModel.getData(mAdapter, mModel.getMPage() + 1, mDBegin, mDEnd);
        };

        mRecyclerView = findViewById(R.id.rv_list);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new WardahHistoryAdapter(this,
                WardahItemType.CUSTOMER, mRecyclerView, new ArrayList<>(), this);
        mRecyclerView.setAdapter(mAdapter);

        /*
         * Empty Message
         */
        mTvMessage = findViewById(R.id.tv_message);

        mDynamicBox = new DynamicBox(this, mRecyclerView);

        final Observer<List<Customer>> pelangganHistoryMutableLiveDataObserver = pelangganList -> {
            mDynamicBox.hideAll();
            mSwipeRefreshLayout.setRefreshing(false);

            if (mModel.getMPage() == 0) {
                // Refresh
                try {
                    Log.d(TAG, "onChanged: pelangganHistoryMutableLiveDataObserver [Refresh]");
                    mAdapter.clear();
                    mAdapter.addAll(pelangganList);
                    mRecyclerView.setAdapter(mAdapter);

                    // Empty Message
                    if (pelangganList.size() <= 0) {
                        mTvMessage.setVisibility(View.VISIBLE);
                    }
                } catch (NullPointerException npe) {
                    Log.e(TAG, npe.getMessage());
                }
            } else {
                // Load More
                try {
                    Log.d(TAG, "onChanged: pelangganHistoryMutableLiveDataObserver [Load More]");
                    mAdapter.addAll(pelangganList);
                    mAdapter.notifyDataSetChanged();
                } catch (NullPointerException npe) {
                    Log.e(TAG, npe.getMessage());
                }
            }
        };
        mModel.getCustomerHistoryMutableLiveData().observe(this, pelangganHistoryMutableLiveDataObserver);

        final Observer<Pagination> paginationMutableLiveDataObserver = pagination -> {
            try {
                if (pagination.getLast()) {
                    mAdapter.removeOnLoadMoreListener();
                } else {
                    mAdapter.setLoaded();
                    mAdapter.setOnLoadMoreListener(mOnLoadMoreListListener);
                }
            } catch (NullPointerException npe) {
                Log.e(TAG, npe.getMessage());
            }
        };
        mModel.getPaginationMutableLiveData().observe(this, paginationMutableLiveDataObserver);

        mDynamicBox.showLoadingLayout();
        mModel.getData(mAdapter, 0, mDBegin, mDEnd);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Todo: SnackBar
        if (requestCode == CUSTOMER_EDIT_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                mModel.getData(mAdapter, 0, mDBegin, mDEnd);
                setResult(Activity.RESULT_OK);
            }
        }
    }

    @Override
    protected void onSwipeToRefresh() {
        mModel.getData(mAdapter, 0, mDBegin, mDEnd);
    }

    @Override
    protected void onClickResetButton() {
        mDynamicBox.showLoadingLayout();
        mModel.getData(mAdapter, 0, mDBegin, mDEnd);
    }

    @Override
    public void onClick(WardahHistoryRecyclerViewItem item, ViewMode viewMode) {
        Intent intent = new Intent(getBaseContext(), InputCustomerActivity.class);
        intent.putExtra(IntentUtil.Companion.getCUSTOMER(), (Customer) item);
        intent.putExtra(IntentUtil.Companion.getVIEW_MODE(), viewMode);

        switch (viewMode) {
            case EDIT:
                startActivityForResult(intent, CUSTOMER_EDIT_ACTIVITY);
                break;
            case VIEW:
                startActivity(intent);
                break;
        }
    }
}
