package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.SalesLatestDate

class SalesLatestDateResponse : GenericResponse() {
    var data: SalesLatestDate? = null
}
