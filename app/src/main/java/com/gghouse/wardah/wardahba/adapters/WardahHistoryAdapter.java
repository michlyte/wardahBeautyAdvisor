package com.gghouse.wardah.wardahba.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.ChecklistBAListener;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.viewholders.AttendantViewHolder;
import com.gghouse.wardah.wardahba.viewholders.CategoryViewHolder;
import com.gghouse.wardah.wardahba.viewholders.ChecklistBAViewHolder;
import com.gghouse.wardah.wardahba.viewholders.CustomerViewHolder;
import com.gghouse.wardah.wardahba.viewholders.LoadingViewHolder;
import com.gghouse.wardah.wardahba.viewholders.SalesTargetViewHolder;
import com.gghouse.wardah.wardahba.webservices.model.Attendant;
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA;
import com.gghouse.wardah.wardahba.webservices.model.Customer;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;
import com.gghouse.wardah.wardahba.webservices.model.SalesTarget;

import java.util.List;

public class WardahHistoryAdapter extends WardahLoadMoreAdapter {

    private static final String TAG = WardahHistoryAdapter.class.getSimpleName();

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private final WardahItemType mWardahItemType;
    private final Context mContext;
    private WardahListener mWardahListener;
    private ChecklistBAListener mChecklistBAListener;
    private List<WardahHistoryRecyclerViewItem> mValues;

    public WardahHistoryAdapter(Context context, WardahItemType wardahItemType,
                                RecyclerView recyclerView, List<WardahHistoryRecyclerViewItem> items, WardahListener wardahListener) {
        super(context, recyclerView);
        mContext = context;
        mWardahItemType = wardahItemType;
        mValues = items;
        mWardahListener = wardahListener;
    }

    public WardahHistoryAdapter(Context context, WardahItemType wardahItemType,
                                RecyclerView recyclerView, List<WardahHistoryRecyclerViewItem> items, WardahListener wardahListener, ChecklistBAListener checklistBAListener) {
        super(context, recyclerView);
        mContext = context;
        mWardahItemType = wardahItemType;
        mValues = items;
        mWardahListener = wardahListener;
        mChecklistBAListener = checklistBAListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEW_TYPE_LOADING:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_loading_item, parent, false);
                return new LoadingViewHolder(view);
            case VIEW_TYPE_ITEM:
                switch (mWardahItemType) {
                    case CUSTOMER:
                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_pelanggan, parent, false);
                        return new CustomerViewHolder(view);
                    case ATTENDANT:
                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_peserta, parent, false);
                        return new AttendantViewHolder(view);
                    case CATEGORY:
                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_category, parent, false);
                        return new CategoryViewHolder(view);
                    case INPUT_TARGET:
                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_sales_target, parent, false);
                        return new SalesTargetViewHolder(view);
                    case EVENT_CHECKLIST_BA:
                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_checklist_ba, parent, false);
                        return new ChecklistBAViewHolder(view);

                }
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
                break;
            case VIEW_TYPE_ITEM:
                switch (mWardahItemType) {
                    case CUSTOMER:
                        final CustomerViewHolder customerViewHolder = (CustomerViewHolder) holder;
                        customerViewHolder.setData(mContext, (Customer) mValues.get(position), mWardahListener);
                        break;
                    case ATTENDANT:
                        final AttendantViewHolder attendantViewHolder = (AttendantViewHolder) holder;
                        attendantViewHolder.setData((Attendant) mValues.get(position), mWardahListener);
                        break;
                    case CATEGORY:
                        final CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
                        categoryViewHolder.setData((ProductType) mValues.get(position));
                        break;
                    case INPUT_TARGET:
                        final SalesTargetViewHolder salesTargetViewHolder = (SalesTargetViewHolder) holder;
                        salesTargetViewHolder.setData(mContext, (SalesTarget) mValues.get(position), mWardahListener);
                        break;
                    case EVENT_CHECKLIST_BA:
                        final ChecklistBAViewHolder checklistBAViewHolder = (ChecklistBAViewHolder) holder;
                        checklistBAViewHolder.setData((ChecklistBA) mValues.get(position), mChecklistBAListener);
                        break;
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mValues.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return mValues == null ? 0 : mValues.size();
    }

    public void setData(List<?> values) {
        mValues = (List<WardahHistoryRecyclerViewItem>) values;
    }

    public void add(WardahHistoryRecyclerViewItem item) {
        mValues.add(item);
    }

    public void remove(int position) {
        mValues.remove(position);
    }

    public void addAll(List<?> values) {
        mValues.addAll((List<WardahHistoryRecyclerViewItem>) values);
    }

    public void clear() {
        mValues.clear();
    }
}
