package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.MonthlyLatestMonth

class MonthlyLatestMonthResponse : GenericResponse {
    var data: MonthlyLatestMonth? = null

    constructor(data: MonthlyLatestMonth?) : super() {
        this.data = data
    }
}