package com.gghouse.wardah.wardahba.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.webservices.model.SalesTarget;

/**
 * Created by michaelhalim on 5/9/17.
 */

public class SalesTargetViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public SalesTarget mItem;

    private final LinearLayout mContainerLinearLayout;
    private final CardView mCardView;
    private final TextView mDateTextView;
    private final TextView mNameTextView;
    private final TextView mAmountTextView;

    public SalesTargetViewHolder(View view) {
        super(view);
        mView = view;
        mContainerLinearLayout = view.findViewById(R.id.containerLinearLayout);
        mCardView = view.findViewById(R.id.cardView);
        mDateTextView = view.findViewById(R.id.dateTextView);
        mNameTextView = view.findViewById(R.id.nameTextView);
        mAmountTextView = view.findViewById(R.id.amounTextView);
    }

    private void setupRowView(Context context) {
        if (mItem.getEditable()) {
            mCardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

            int color = ContextCompat.getColor(context, android.R.color.white);
            mDateTextView.setTextColor(color);
            mNameTextView.setTextColor(color);
            mAmountTextView.setTextColor(color);
        } else {
            mCardView.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.white));

            int primaryColor = ContextCompat.getColor(context, R.color.colorPrimary);
            int accentColor = ContextCompat.getColor(context, R.color.colorAccent);
            int blackColor = ContextCompat.getColor(context, android.R.color.black);
            mNameTextView.setTextColor(primaryColor);
            mAmountTextView.setTextColor(accentColor);
            mDateTextView.setTextColor(blackColor);
        }
    }

    private void setupActionEvent(WardahListener wardahListener) {
        ViewMode viewMode;
        if (mItem.getEditable()) {
            viewMode = ViewMode.EDIT;
        } else {
            viewMode = ViewMode.VIEW;
        }
        mContainerLinearLayout.setOnClickListener(v -> wardahListener.onClick(mItem, viewMode));
    }

    public void setData(Context context, SalesTarget salesTarget, WardahListener wardahListener) {
        mItem = salesTarget;

        mDateTextView.setText(mItem.getDateStr());
//        mNameTextView.setText(mItem.getName());
        mAmountTextView.setText(mItem.getSalesTargetStr());

        setupRowView(context);
        setupActionEvent(wardahListener);
    }
}
