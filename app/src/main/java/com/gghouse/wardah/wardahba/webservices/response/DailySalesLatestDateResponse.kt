package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.DailySalesLatestDate

class DailySalesLatestDateResponse : GenericResponse() {
    var data: DailySalesLatestDate? = null
}
