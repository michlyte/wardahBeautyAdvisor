package com.gghouse.wardah.wardahba.activities.visit;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.fragments.BaInformationOverviewFragment;
import com.gghouse.wardah.wardahba.fragments.WardahHistoryFragment;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.google.android.material.tabs.TabLayout;

public class BaInformationActivity extends AppCompatActivity {
    private static final String TAG = BaInformationActivity.class.getSimpleName();

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi_ba);

        Intent i = getIntent();
        ViewMode viewMode;
        Long userId;
        if (i != null) {
            viewMode = (ViewMode) i.getSerializableExtra(IntentUtil.Companion.getVIEW_MODE());
            userId = i.getLongExtra(IntentUtil.Companion.getUSER_ID(), 0L);
        } else {
            viewMode = ViewMode.INPUT;
            userId = 0L;
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), viewMode, userId);

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sales_history_p2, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private ViewMode mViewMode;
        private Long mUserId;

        public SectionsPagerAdapter(FragmentManager fm, ViewMode viewMode, Long userId) {
            super(fm);
            mViewMode = viewMode;
            mUserId = userId;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return BaInformationOverviewFragment.Companion.newInstance(mViewMode, mUserId);
                case 1:
                    return WardahHistoryFragment.newInstance(WardahItemType.SALES_FC, mUserId);
                case 2:
                    return WardahHistoryFragment.newInstance(WardahItemType.PRODUCT_HIGHLIGHT_FC, mUserId);
                case 3:
                    return WardahHistoryFragment.newInstance(WardahItemType.PRODUCT_FOCUS_FC, mUserId);
                default:
                    return PlaceholderFragment.newInstance(position);
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
