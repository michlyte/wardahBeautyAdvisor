package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class Profile : Serializable {
    val userId: Long
    val nip: Long
    val fullname: String
    val firstDateWork: Long
    val position: String
    val brand: String
    val location: String
    val dc: String
    val city: String
    val province: String
    val storeId: Long
    var isButtonEnable: Boolean?

    constructor(userId: Long, nip: Long, fullname: String, firstDateWork: Long, position: String, brand: String, location: String, dc: String, city: String, province: String, storeId: Long, isButtonEnable: Boolean) {
        this.userId = userId
        this.nip = nip
        this.fullname = fullname
        this.firstDateWork = firstDateWork
        this.position = position
        this.brand = brand
        this.location = location
        this.dc = dc
        this.city = city
        this.province = province
        this.storeId = storeId
        this.isButtonEnable = isButtonEnable
    }
}