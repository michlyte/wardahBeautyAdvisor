package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem
import java.io.Serializable

class ProductType : WardahHistoryRecyclerViewItem, Serializable {
    var id: Long? = null
    var amount: Double? = null
    var quantity: Integer? = null
    var name: String? = null
    var imageUrl: String? = null

    constructor(id: Long?, amount: Double?, quantity: Integer?, name: String?, imageUrl: String?) : super() {
        this.id = id
        this.amount = amount
        this.quantity = quantity
        this.name = name
        this.imageUrl = imageUrl
    }

    constructor(quantity: Integer?, name: String?, amount: Double?, imageUrl: String?) : super() {
        this.quantity = quantity
        this.name = name
        this.amount = amount
        this.imageUrl = imageUrl
    }
}