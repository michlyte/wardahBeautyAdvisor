package com.gghouse.wardah.wardahba.dummy

import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum
import com.gghouse.wardah.wardahba.webservices.model.*
import java.util.*

object EventDummy {

    val eventList: List<Event> = object : ArrayList<Event>() {
        init {
            add(Event("EDIT", 8L, "Event Meet n' Greet Wardah",
                    "Performance", Date().time, Date().time, "notes",
                    EventLocation(11L, "Jl. Asia Afrika", "-6.921894", "107.615599"),
                    CheckInEvent(Date().time, "A", "img/bp-image-1547960263329.jpg", "img/booth-image-1547960263381.jpg", false),
                    ChecklistDummy.checklistList,
                    AttendantDummy.ATTENDANT_EVENT_LIST))
        }
    }

    private val nearestEvent = NearestEvent(0L, "Dummy Event", "Performance", 1549439406000L,
            1549439406000L, 1549439406000L, "",
            EventLocation(0L, "Wisma Bumiputera", "-6.9220405", "107.61670099999999"),
            "Wisma Bumiputera", 5, 3, 1, true, true)
    private val latestEvent = LatestEvent(0L, "Dummy Event", "Performance", 1549439406000L,
            1549439406000L, 1549439406000L, "",
            EventLocation(0L, "Wisma Bumiputera", "-6.9220405", "107.61670099999999"),
            "Wisma Bumiputera", "img/bp-image-1549524554523.jpg", "img/booth-image-1549524554559.jpg",
            5, 3, 1, 1549524554000L, true, true, "")

    val bpHome = BPHome(4514L, 0, UserTypeEnum.BEAUTY_PROMOTOR.name, nearestEvent, latestEvent)

    val eventDc: List<String> = object : ArrayList<String>() {
        init {
            add("-- Pilih DC --")
            add("Aceh")
            add("Bandung")
        }
    }

    val eventCategory: List<String> = object : ArrayList<String>() {
        init {
            add("-- Pilih Kategori --")
            add("Marketing")
            add("Kategori 2")
            add("Kategori 3")
        }
    }

    val eventLocation: List<String> = object : ArrayList<String>() {
        init {
            add("-- Pilih Lokasi --")
            add("ABC Store")
        }
    }

    val eventReminderType: List<String> = object : ArrayList<String>() {
        init {
            add("menit sebelumnya")
            add("jam sebelumnya")
            add("hari sebelumnya")
        }
    }

}