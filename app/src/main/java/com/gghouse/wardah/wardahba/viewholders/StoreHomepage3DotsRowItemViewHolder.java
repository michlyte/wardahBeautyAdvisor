package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.models.StoreHomepage3DotsRowItem;
import com.gghouse.wardah.wardahba.utils.SystemUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.squareup.picasso.Picasso;

public class StoreHomepage3DotsRowItemViewHolder extends RecyclerView.ViewHolder {
    private final TextView mLeftTitleTextView;
    private final TextView mCenterTitleTextView;
    private final TextView mRightTitleTextView;
    private final TextView mLeftValueTextView;
    private final TextView mCenterValueTextView;
    private final TextView mRightValueTextView;
    private final ImageView mLeftImageView;
    private final ImageView mCenterImageView;
    private final ImageView mRightImageView;
    public StoreHomepage3DotsRowItem mItem;

    public StoreHomepage3DotsRowItemViewHolder(View v) {
        super(v);
        mLeftTitleTextView = (TextView) v.findViewById(R.id.tvLeftTitle);
        mCenterTitleTextView = (TextView) v.findViewById(R.id.tvCenterTitle);
        mRightTitleTextView = (TextView) v.findViewById(R.id.tvRightTitle);

        mLeftValueTextView = (TextView) v.findViewById(R.id.tvLeftValue);
        mCenterValueTextView = (TextView) v.findViewById(R.id.tvCenterValue);
        mRightValueTextView = (TextView) v.findViewById(R.id.tvRightValue);

        mLeftImageView = (ImageView) v.findViewById(R.id.ivLeftIcon);
        mCenterImageView = (ImageView) v.findViewById(R.id.ivCenterIcon);
        mRightImageView = (ImageView) v.findViewById(R.id.ivRightIcon);
    }

    public void setData(StoreHomepage3DotsRowItem storeHomepageRowItem) {
        mItem = storeHomepageRowItem;

        mLeftTitleTextView.setText(mItem.getLeftTitle());
        mCenterTitleTextView.setText(mItem.getCenterTitle());
        mRightTitleTextView.setText(mItem.getRightTitle());

        mLeftValueTextView.setText(SystemUtil.withSuffix(
                mItem.getLeftValue() != null ? Double.parseDouble(mItem.getLeftValue()) : 0.00, 0));
        mCenterValueTextView.setText(SystemUtil.withSuffix(
                mItem.getCenterValue() != null ? Double.parseDouble(mItem.getCenterValue()) : 0.00, 0));
        mRightValueTextView.setText(SystemUtil.withSuffix(
                mItem.getRightValue() != null ? Double.parseDouble(mItem.getRightValue()) : 0.00, 0));

        Picasso.get().load(WardahUtil.INSTANCE.validateResId(mItem.getLeftDrawableResId())).into(mLeftImageView);
        Picasso.get().load(WardahUtil.INSTANCE.validateResId(mItem.getCenterDrawableResId())).into(mCenterImageView);
        Picasso.get().load(WardahUtil.INSTANCE.validateResId(mItem.getRightDrawableResId())).into(mRightImageView);
    }
}
