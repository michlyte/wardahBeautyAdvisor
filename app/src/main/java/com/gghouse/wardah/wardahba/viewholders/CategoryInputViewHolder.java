package com.gghouse.wardah.wardahba.viewholders;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;
import com.squareup.picasso.Picasso;

public class CategoryInputViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final ImageView mIconImageView;
    private final TextView mTitleTextView;
    private final EditText mPcsEditText;
    private final EditText mAmountEditText;
    private ProductType mItem;

    public CategoryInputViewHolder(View view) {
        super(view);

        this.view = view;
        mIconImageView = view.findViewById(R.id.iconImageView);
        mTitleTextView = view.findViewById(R.id.titleEditText);
        mAmountEditText = view.findViewById(R.id.amountEditText);
        mPcsEditText = view.findViewById(R.id.pcsEditText);
    }

    private void setupActionEvent(WardahListener wardahListener) {
        mAmountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!mAmountEditText.getText().toString().isEmpty()) {
                    Double amount = Double.parseDouble(mAmountEditText.getText().toString());
                    mItem.setAmount(amount);
                    wardahListener.onClick(mItem, ViewMode.INPUT);
                }
            }
        });

        mPcsEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!mPcsEditText.getText().toString().isEmpty()) {
                    mItem.setQuantity(Integer.parseInt(mPcsEditText.getText().toString()));
                    wardahListener.onClick(mItem, ViewMode.INPUT);
                }
            }
        });
    }

    public void setData(ProductType productType, WardahListener listener) {
        mItem = productType;

        mTitleTextView.setText(mItem.getName());

        if (mItem.getQuantity() == null || mItem.getQuantity() <= 0) {
            mPcsEditText.setText("");
        } else {
            mPcsEditText.setText(String.valueOf(mItem.getQuantity()));
        }

        if (mItem.getAmount() == null || mItem.getAmount() <= 0.00) {
            mAmountEditText.setText("");
        } else {
            mAmountEditText.setText(String.valueOf(mItem.getAmount()));
        }

        Picasso.get().load(mItem.getImageUrl()).into(mIconImageView);

        setupActionEvent(listener);
    }
}