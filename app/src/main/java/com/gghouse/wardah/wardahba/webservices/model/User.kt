package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum

class User {
    var userId: Long? = null
    var locationId: Long? = null
    var fullname: String? = null
    var position: String? = null
    var location: String? = null

    constructor() {
        this.userId = 0L
        this.locationId = 0L
        this.fullname = ""
        this.position = UserTypeEnum.BEAUTY_ADVISOR.name
        this.location = ""
    }

    constructor(userId: Long?, locationId: Long?, fullname: String?, position: String?, location: String?) {
        this.userId = userId
        this.locationId = locationId
        this.fullname = fullname
        this.position = position
        this.location = location
    }
}