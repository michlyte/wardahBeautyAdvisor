package com.gghouse.wardah.wardahba.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;
import com.squareup.picasso.Picasso;

public class ProductHighlightViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final ImageView mIVImage;
    public final TextView mTVTitle;
    public final ImageView mIVProductType;
    public final TextView mTVPrice;
    public final EditText mETQty;
    public final Button mBRemove;
    public final Button mBAdd;
    public ProductHighlight mItem;

    public ProductHighlightViewHolder(View view) {
        super(view);
        mView = view;
        mIVImage = view.findViewById(R.id.iv_image);
        mTVTitle = view.findViewById(R.id.tv_title);
        mIVProductType = view.findViewById(R.id.ivProductType);
        mTVPrice = view.findViewById(R.id.tvPrice);
        mETQty = view.findViewById(R.id.et_qty);
        mBRemove = view.findViewById(R.id.b_remove);
        mBAdd = view.findViewById(R.id.b_add);
    }

    private void setupRowView(Context context, ViewMode viewMode) {
        switch (viewMode) {
            case EDIT:
            case INPUT:
                mBAdd.setVisibility(View.VISIBLE);
                mBRemove.setVisibility(View.VISIBLE);
                mETQty.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                break;
            case VIEW:
                mBAdd.setVisibility(View.GONE);
                mBRemove.setVisibility(View.GONE);
                mETQty.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                break;
        }
    }

    public void setData(Context context, ProductHighlight productHighlight, ViewMode viewMode) {
        mItem = productHighlight;

        mTVTitle.setText(mItem.getName());
        if (mItem.getImgURL() != null && !mItem.getImgURL().isEmpty()) {
            Picasso.get()
                    .load(ImageUtil.INSTANCE.getImageUrl(mItem.getImgURL()))
                    .fit()
                    .centerCrop()
                    .into(mIVImage);
        } else {
            Picasso.get()
                    .load(R.drawable.pic_image_not_found)
                    .fit()
                    .centerCrop()
                    .into(mIVImage);
        }
        Integer qty = productHighlight.getQuantity();
        if (qty != null)
            mETQty.setText(String.valueOf(qty));
        else
            mETQty.setText("0");

//        Boolean isProductFocus = productHighlight.isProductFocus();
//        if (isProductFocus != null && isProductFocus)
//            mIVProductType.setImageDrawable(ImageUtil.drawableProductFocus);
//        else
//            mIVProductType.setImageDrawable(ImageUtil.drawableProductHighlight);

        Double price = productHighlight.getPrice();
        if (price != null)
            mTVPrice.setText(WardahUtil.INSTANCE.getAmount(price));
        else
            mTVPrice.setText(context.getString(R.string.text_empty_value));

        setupRowView(context, viewMode);
    }
}