package com.gghouse.wardah.wardahba.webservices.request

import com.gghouse.wardah.wardahba.webservices.model.EvaluationAnswer

class EvaluationAnswerRequest(private val calendarId: Long?, private val userId: Long?, private val checklist: List<EvaluationAnswer>)
