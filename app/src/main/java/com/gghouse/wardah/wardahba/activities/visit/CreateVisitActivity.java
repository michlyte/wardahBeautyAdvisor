package com.gghouse.wardah.wardahba.activities.visit;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.event.CheckInOutActivity;
import com.gghouse.wardah.wardahba.activities.event.ChecklistBAHistoryActivity;
import com.gghouse.wardah.wardahba.activities.event.ReminderActivity;
import com.gghouse.wardah.wardahba.activities.store.StoreInformationActivity;
import com.gghouse.wardah.wardahba.adapters.BannerSliderAdapter;
import com.gghouse.wardah.wardahba.adapters.VisitCategoryAdapter;
import com.gghouse.wardah.wardahba.adapters.WardahSimpleAdapter;
import com.gghouse.wardah.wardahba.enumerations.ReminderEnum;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.ChecklistBAListener;
import com.gghouse.wardah.wardahba.models.IntentVisitCategory;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.viewmodels.CreateVisitActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.ChecklistBA;
import com.gghouse.wardah.wardahba.webservices.model.DCDropdown;
import com.gghouse.wardah.wardahba.webservices.model.DCLocationDropdown;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;
import com.gghouse.wardah.wardahba.webservices.model.VisitCategory;
import com.gghouse.wardah.wardahba.webservices.model.VisitStoreInformation;
import com.gghouse.wardah.wardahba.webservices.model.VisitView;
import com.gghouse.wardah.wardahba.webservices.request.VisitCheckOutRequest;
import com.gghouse.wardah.wardahba.webservices.request.VisitCreateRequest;
import com.gghouse.wardah.wardahba.webservices.request.VisitEditRequest;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ss.com.bannerslider.Slider;

public class CreateVisitActivity extends AppCompatActivity implements View.OnClickListener, ChecklistBAListener {

    private static final String TAG = CreateVisitActivity.class.getSimpleName();
    private static final int REMINDER_RESULT_ACTIVITY = 1;
    private static final int CHECK_IN_RESULT_ACTIVITY = 5;

    private CreateVisitActivityViewModel mModel;

    private CoordinatorLayout mCoordinatorLayout;
    private AutoCompleteTextView mDCAutoCompleteTextView;
    private ArrayAdapter<String> mDCAdapter;
    private AutoCompleteTextView mLocationAutoCompleteTextView;
    private ArrayAdapter<String> mLocationAdapter;
    private TextView mStartTimeTextView;
    private DatePickerDialog.OnDateSetListener mStartTimeOnDateSetListener;
    private TimePickerDialog.OnTimeSetListener mStartTimeOnTimeSetListener;
    private TextView mEndTimeTextView;
    private DatePickerDialog.OnDateSetListener mEndTimeOnDateSetListener;
    private TimePickerDialog.OnTimeSetListener mEndTimeOnTimeSetListener;
    private TextView mAlarmTextView;
    private TextView mAlarmTambahTextView;
    private Spinner mCategorySpinner;
    private VisitCategoryAdapter mCategoryAdapter;
    private EditText mNoteEditText;

    private View mCheckInLayout;
    private TextView mCheckInOutTextView;
    private TextView mCheckInActionTextView;
    private TextView mCheckOutActionTextView;
    private ImageView mCheckInOutRightArrowImageView;
    private LinearLayout mCheckInLinearLayout;
    private Slider mCheckInSlider;
    private TextView mCheckInTextView;
    private TextView mCheckOutTextView;
    private TextView mCheckInEmptyTextView;

    private View mStoreInformationLayout;
    private LinearLayout mStoreInformationMoreLinearLayout;
    private Button mLeftStoreInformationButton;
    private Button mRightStoreInformationButton;
    private TextView mJumlahBATextView;
    private TextView mTargetProTargetTextView;
    private TextView mOmsetKompetitorTextView;
    private View mSalesLayout;
    private TextView mSalesSummaryTextView;
    private RecyclerView mSalesRecyclerView;
    private WardahSimpleAdapter mSalesAdapter;

    private View mChecklistBALayout;
    private LinearLayout mChecklistBAMoreLinearLayout;
    private WardahSimpleAdapter mChecklistBAAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event_fc);

        mModel = ViewModelProviders.of(this).get(CreateVisitActivityViewModel.class);

        Intent i = getIntent();
        try {
            mModel.getViewModeMutableLiveData().postValue((ViewMode) i.getSerializableExtra(IntentUtil.Companion.getVIEW_MODE()));
            mModel.getVisitViewMutableLiveData().postValue((VisitView) i.getSerializableExtra(IntentUtil.Companion.getVISIT()));
            IntentVisitCategory intentVisitCategory = (IntentVisitCategory) i.getSerializableExtra(IntentUtil.Companion.getVISIT_CATEGORY());
            if (intentVisitCategory != null) {
                mModel.getVisitCategoryListMutableLiveData().postValue(intentVisitCategory.getObjects());
            } else {
                mModel.callVisitCategory(SessionUtil.getUserId());
            }
        } catch (NullPointerException npe) {
            mModel.getViewModeMutableLiveData().postValue(ViewMode.INPUT);
        }

        mCoordinatorLayout = findViewById(R.id.coordinatorLayout);

        mDCAutoCompleteTextView = findViewById(R.id.dcAutoCompleteTextView);
        mLocationAutoCompleteTextView = findViewById(R.id.locationAutoCompleteTextView);

        mStartTimeTextView = findViewById(R.id.startTimeTextView);
        mStartTimeTextView.setOnClickListener(this);
        mStartTimeOnDateSetListener = (datePicker, year, monthOfYear, dayOfMonth) -> {
            Calendar calendar = mModel.getStartTimeCalendar();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mModel.setStartTimeCalendar(calendar);

            new TimePickerDialog(this, mStartTimeOnTimeSetListener,
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                    true).show();
        };
        mStartTimeOnTimeSetListener = (timePicker, hour, minute) -> {
            Calendar calendar = mModel.getStartTimeCalendar();
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            mModel.getStartTimeMutableLiveData().postValue(calendar);
        };

        mEndTimeTextView = findViewById(R.id.endTimeTextView);
        mEndTimeTextView.setOnClickListener(this);
        mEndTimeOnDateSetListener = (datePicker, year, monthOfYear, dayOfMonth) -> {
            Calendar calendar = mModel.getEndTimeCalendar();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mModel.setEndTimeCalendar(calendar);

            new TimePickerDialog(this, mEndTimeOnTimeSetListener,
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                    true).show();
        };
        mEndTimeOnTimeSetListener = (timePicker, hour, minute) -> {
            Calendar calendar = mModel.getEndTimeCalendar();
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            mModel.getEndTimeMutableLiveData().postValue(calendar);
        };

        mAlarmTextView = findViewById(R.id.alarmTextView);
        mAlarmTambahTextView = findViewById(R.id.alarmTambahTextView);
        mAlarmTambahTextView.setOnClickListener(this);

        mCategorySpinner = findViewById(R.id.categorySpinner);

        mNoteEditText = findViewById(R.id.noteEditText);

        mCheckInLayout = findViewById(R.id.checkInLayout);
        mCheckInOutTextView = findViewById(R.id.checkInOutTextView);
        mCheckInOutTextView.setText(R.string.title_check_in_out);
        mCheckInActionTextView = findViewById(R.id.checkInActionTextView);
        mCheckInActionTextView.setOnClickListener(this);
        mCheckOutActionTextView = findViewById(R.id.checkOutActionTextView);
        mCheckOutActionTextView.setOnClickListener(this);
        mCheckInOutRightArrowImageView = findViewById(R.id.checkInOutRightArrowImageView);
        mCheckInLinearLayout = findViewById(R.id.checkInContentLinearLayout);
        mCheckInSlider = findViewById(R.id.checkInSlider);
        mCheckInTextView = findViewById(R.id.checkInTextView);
        mCheckOutTextView = findViewById(R.id.checkOutTextView);
        mCheckInEmptyTextView = findViewById(R.id.checkInEmptyTextView);

        mStoreInformationLayout = findViewById(R.id.storeInformationLayout);
        mStoreInformationMoreLinearLayout = findViewById(R.id.moreStoreInformationLinearLayout);
        mStoreInformationMoreLinearLayout.setOnClickListener(this);
        mLeftStoreInformationButton = findViewById(R.id.leftStoreInformationButton);
        mLeftStoreInformationButton.setOnClickListener(this);
        mRightStoreInformationButton = findViewById(R.id.rightStoreInformationButton);
        mRightStoreInformationButton.setOnClickListener(this);
        mJumlahBATextView = findViewById(R.id.jumlahBATextView);
        mTargetProTargetTextView = findViewById(R.id.targetSalesTextView);
        mOmsetKompetitorTextView = findViewById(R.id.competitorTurnoverTextView);
        mSalesLayout = findViewById(R.id.salesLayout);
        mSalesSummaryTextView = findViewById(R.id.categorySummaryTextView);
        mSalesRecyclerView = findViewById(R.id.categoryRecyclerView);
        mSalesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSalesAdapter = new WardahSimpleAdapter(getBaseContext(), WardahItemType.CATEGORY, new ArrayList<>());
        mSalesRecyclerView.setAdapter(mSalesAdapter);
        mSalesRecyclerView.setNestedScrollingEnabled(false);

        mChecklistBALayout = findViewById(R.id.checklistBALayout);
        mChecklistBAMoreLinearLayout = findViewById(R.id.moreChecklistBALinearLayout);
        mChecklistBAMoreLinearLayout.setOnClickListener(this);

        mModel.getViewModeMutableLiveData().observe(this, viewMode -> {
            if (viewMode != null) {
                switch (viewMode) {
                    case INPUT:
                        setTitle(R.string.title_create_event);
                        break;
                    case EDIT:
                        setTitle(R.string.title_edit_event);

                        mAlarmTambahTextView.setText(R.string.text_ubah);

                        mCheckInLayout.setVisibility(View.VISIBLE);
                        mStoreInformationLayout.setVisibility(View.VISIBLE);
                        mSalesLayout.setVisibility(View.VISIBLE);
                        mChecklistBALayout.setVisibility(View.VISIBLE);

                        // Prevent keyboard to show
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        break;
                    case VIEW:
                        setTitle(R.string.title_view_event);

                        mDCAutoCompleteTextView.setEnabled(false);
                        mLocationAutoCompleteTextView.setEnabled(false);
                        int textDisabledColor = ContextCompat.getColor(this, android.R.color.darker_gray);
                        mStartTimeTextView.setTextColor(textDisabledColor);
                        mEndTimeTextView.setTextColor(textDisabledColor);
                        mAlarmTextView.setTextColor(textDisabledColor);
                        mStartTimeTextView.setEnabled(false);
                        mEndTimeTextView.setEnabled(false);
                        mAlarmTambahTextView.setVisibility(View.GONE);
                        mCategorySpinner.setEnabled(false);
                        mNoteEditText.setEnabled(false);

                        mCheckInLayout.setVisibility(View.VISIBLE);
                        mStoreInformationLayout.setVisibility(View.VISIBLE);
                        mSalesLayout.setVisibility(View.VISIBLE);
                        mChecklistBALayout.setVisibility(View.VISIBLE);

                        // Prevent keyboard to show
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        break;
                }
            }
        });

        mModel.getVisitViewMutableLiveData().observe(this, visitView -> {
            if (visitView != null) {
                mDCAutoCompleteTextView.setText(visitView.getDc().getName());
                mModel.callDCLocationDropdown(visitView.getDc().getName(), "");
                mLocationAutoCompleteTextView.setText(visitView.getLocation().getName());
                mStartTimeTextView.setText(visitView.getStartTimeStr());
                mModel.getStartTimeCalendar().setTimeInMillis(visitView.getStartTime());
                mEndTimeTextView.setText(visitView.getEndTimeStr());
                mModel.getEndTimeCalendar().setTimeInMillis(visitView.getEndTime());
                if (visitView.getAlarmTime() != null) {
                    mModel.setReminderEnum(ReminderEnum.Companion.getReminderEnumByText(visitView.getAlarmStr()));
                    mAlarmTextView.setText(visitView.getAlarmTimeStr());
                    mAlarmTambahTextView.setText(getResources().getString(R.string.text_ubah));
                } else {
                    mModel.setReminderEnum(ReminderEnum.NO_REMINDER);
                    mAlarmTextView.setText(visitView.getAlarmTimeStr()
                    );
                }
                int selectedPosition = 0;
                List<VisitCategory> visitCategoryList = mModel.getVisitCategoryListMutableLiveData().getValue();
                if (visitCategoryList != null) {
                    for (VisitCategory visitCategory : visitCategoryList) {
                        if (visitCategory.getName().equals(visitView.getCategory())) {
                            break;
                        }
                        selectedPosition += 1;
                    }
                }
                mCategorySpinner.setSelection(selectedPosition);
                mNoteEditText.setText(visitView.getNotes());

                // Check in out
                if (visitView.getCheckInOut().getCheckInTime() == null) {
                    mCheckInLinearLayout.setVisibility(View.GONE);
                    mCheckInEmptyTextView.setVisibility(View.VISIBLE);
                    mCheckInActionTextView.setVisibility(View.VISIBLE);
                    mCheckInOutRightArrowImageView.setVisibility(View.VISIBLE);
                } else if (visitView.getCheckInOut().getCheckOutTime() == null) {
                    String checkInStr = getString(R.string.title_waktu_check_in, visitView.getCheckInOut().getCheckInTimeStr());
                    mCheckInTextView.setText(checkInStr);
                    mCheckOutTextView.setVisibility(View.GONE);
                    List<String> imageList = new ArrayList<String>() {{
                        add(ImageUtil.INSTANCE.getImageUrl(visitView.getCheckInOut().getFcImageURL()));
                    }};
                    mCheckInSlider.setAdapter(new BannerSliderAdapter(imageList));
                    mCheckInEmptyTextView.setVisibility(View.VISIBLE);
                    mCheckInActionTextView.setVisibility(View.VISIBLE);
                    mCheckInOutRightArrowImageView.setVisibility(View.VISIBLE);
                } else {
                    String checkInStr = getString(R.string.title_waktu_check_in, visitView.getCheckInOut().getCheckInTimeStr());
                    String checkOutStr = getString(R.string.title_waktu_check_out, visitView.getCheckInOut().getCheckOutTimeStr());
                    mCheckInTextView.setText(checkInStr);
                    mCheckOutTextView.setText(checkOutStr);
                    List<String> imageList = new ArrayList<String>() {{
                        add(ImageUtil.INSTANCE.getImageUrl(visitView.getCheckInOut().getFcImageURL()));
                    }};
                    mCheckInSlider.setAdapter(new BannerSliderAdapter(imageList));

                    mCheckOutTextView.setVisibility(View.VISIBLE);
                    mCheckInActionTextView.setVisibility(View.GONE);
                    mCheckOutActionTextView.setVisibility(View.GONE);
                    mCheckInOutRightArrowImageView.setVisibility(View.GONE);
                }

                // Store Information - Instantiate a ViewPager and a PagerAdapter.
                if (visitView.getStoreInformation() != null && visitView.getStoreInformation().size() > 1) {
                    VisitStoreInformation visitStoreInformation = visitView.getStoreInformation().get(0);
                    VisitStoreInformation prevVisitStoreInformation = visitView.getStoreInformation().get(1);
                    mLeftStoreInformationButton.setText(visitStoreInformation.getEvaluationPeriodStr());
                    mRightStoreInformationButton.setText(prevVisitStoreInformation.getEvaluationPeriodStr());
                    mModel.getVisitStoreInformationMutableLiveData().postValue(visitStoreInformation);
                } else {
                    mLeftStoreInformationButton.setText(DateTimeUtil.INSTANCE.getSdfMonthYear().format(new Date()));
                    mRightStoreInformationButton.setText(DateTimeUtil.INSTANCE.getSdfMonthYear().format(DateTimeUtil.INSTANCE.getPreviousMonth(1)));
                    mModel.getVisitStoreInformationMutableLiveData().postValue(null);
                }

                // Checklist BA
                if (mChecklistBAAdapter == null) {
                    RecyclerView checklistBARecyclerView = findViewById(R.id.checklistBARecyclerView);
                    checklistBARecyclerView.setLayoutManager(new LinearLayoutManager(this));
                    checklistBARecyclerView.setNestedScrollingEnabled(false);
                    mChecklistBAAdapter = new WardahSimpleAdapter(this, WardahItemType.EVENT_CHECKLIST_BA, mModel.getViewModeMutableLiveData().getValue(), visitView.getChecklist(), this);
                    checklistBARecyclerView.setAdapter(mChecklistBAAdapter);
                } else {
                    mChecklistBAAdapter.setData(visitView.getChecklist());
                    mChecklistBAAdapter.notifyDataSetChanged();
                }
            }
        });

        // DC
        mModel.getDcDropdownMutableLiveData().observe(this, dcDropdowns -> {
            if (dcDropdowns != null && dcDropdowns.size() > 0) {
                List<String> dcList = new ArrayList<>();
                for (DCDropdown dcDropdown : dcDropdowns) {
                    dcList.add(dcDropdown.getName());
                }
                mDCAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, dcList);
                mDCAutoCompleteTextView.setAdapter(mDCAdapter);

                mDCAutoCompleteTextView.setOnItemClickListener((parent, view, position, id) -> {
                    mModel.getDcMutableLiveData().postValue(mDCAdapter.getItem(position));
                });
            }
        });
        mModel.getDcMutableLiveData().observe(this, dc -> {
            if (dc != null) {
                switch (mModel.getViewModeMutableLiveData().getValue()) {
                    case INPUT:
                    case EDIT:
                        mLocationAutoCompleteTextView.setEnabled(true);
                        break;
                    case VIEW:
                        mLocationAutoCompleteTextView.setEnabled(false);
                }

                mModel.callDCLocationDropdown(dc, "");
            }
        });
        mModel.getDcLocationDropdownMutableLiveData().observe(this, dcLocationDropdowns -> {
            if (dcLocationDropdowns != null && dcLocationDropdowns.size() > 0) {
                List<String> dcLocationList = new ArrayList<>();
                for (DCLocationDropdown dcLocationDropdown : dcLocationDropdowns) {
                    dcLocationList.add(dcLocationDropdown.getName());
                }
                mLocationAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, dcLocationList);
                mLocationAutoCompleteTextView.setAdapter(mLocationAdapter);
            }
        });

        // Time
        mModel.getStartTimeMutableLiveData().observe(this, startTime -> {
            if (startTime != null) {
                mModel.setStartTimeCalendar(startTime);
                mStartTimeTextView.setText(DateTimeUtil.INSTANCE.getSdfEventStartEndTime().format(startTime.getTime()));
            }
        });
        mModel.getEndTimeMutableLiveData().observe(this, endTime -> {
            if (endTime != null) {
                mModel.setEndTimeCalendar(endTime);
                mEndTimeTextView.setText(DateTimeUtil.INSTANCE.getSdfEventStartEndTime().format(endTime.getTime()));
            }
        });

        mModel.getVisitStoreInformationMutableLiveData().observe(this, visitStoreInformation -> {
            if (visitStoreInformation != null) {
                mJumlahBATextView.setText(getString(R.string.form_jumlah_ba, String.valueOf(visitStoreInformation.getTotalBA())));
//                mTargetProTargetTextView.setText(getString(R.string.form_target_pro_rate, String.valueOf(storeInformation.getTargetSales())));
                mOmsetKompetitorTextView.setText(getString(R.string.form_omset_kompetitor, String.valueOf(visitStoreInformation.getCompetitorTurnover())));

                List<ProductType> productTypes = visitStoreInformation.getProductTypes();
                try {
                    Double salesByCategoryAmount = 0.00;
                    int salesByCateogryPcs = 0;
                    for (ProductType productType : productTypes) {
                        salesByCategoryAmount += productType.getAmount();
                        salesByCateogryPcs += productType.getQuantity();
                    }
                    mSalesSummaryTextView.setText(WardahUtil.INSTANCE.getStoreSalesAmountAndPcsContent(salesByCategoryAmount, salesByCateogryPcs));
                    mSalesAdapter.setData(productTypes);
                    mSalesAdapter.notifyDataSetChanged();
                    mSalesLayout.setVisibility(View.VISIBLE);
                } catch (NullPointerException npe) {
                    mSalesLayout.setVisibility(View.GONE);
                }
            } else {
                mJumlahBATextView.setText(getString(R.string.form_jumlah_ba, "-"));
                mTargetProTargetTextView.setText(getString(R.string.form_target_pro_rate, "-"));
                mOmsetKompetitorTextView.setText(getString(R.string.form_omset_kompetitor, "-"));
                mSalesLayout.setVisibility(View.GONE);
            }
        });
        mModel.getStoreInformationMutableLiveData().observe(this, storeInformation -> {
            if (storeInformation != null) {
                Intent iStoreInformation = new Intent(this, StoreInformationActivity.class);
                iStoreInformation.putExtra(IntentUtil.Companion.getSTORE_INFORMATION(), storeInformation);
                startActivity(iStoreInformation);
            }
        });

        mModel.getVisitCategoryListMutableLiveData().observe(this, visitCategories -> {
            if (visitCategories != null && visitCategories.size() > 0) {
                mCategoryAdapter = new VisitCategoryAdapter(this, R.layout.spinner_item, visitCategories);
                mCategorySpinner.setAdapter(mCategoryAdapter);
            }
        });

        mModel.getIsSubmitSucceedMutableLiveData().observe(this, isSuccess -> {
            if (isSuccess != null && isSuccess) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        // Check In
        mModel.getCheckInRadiusMutableLiveData().observe(this, checkInRadius -> {
            if (checkInRadius != null) {
                Intent iCheckIn = new Intent(getBaseContext(), CheckInOutActivity.class);
                iCheckIn.putExtra(IntentUtil.Companion.getCHECK_IN_RADIUS(), checkInRadius);
                startActivityForResult(iCheckIn, CHECK_IN_RESULT_ACTIVITY);
            }
        });
        // Check Out
        mModel.getIsCheckedInOrOutMutableLiveData().observe(this, isCheckedInOrOut -> {
            if (isCheckedInOrOut != null && isCheckedInOrOut) {
                VisitView visitView = mModel.getVisitViewMutableLiveData().getValue();
                if (visitView != null) {
                    mModel.callVisitView(visitView.getCalendarId());
                }
            }
        });

        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            switch (mModel.getViewModeMutableLiveData().getValue()) {
                case INPUT:
                    getMenuInflater().inflate(R.menu.menu_input, menu);
                    break;
                case EDIT:
                    getMenuInflater().inflate(R.menu.menu_edit, menu);
                    break;
                case VIEW:
                    break;
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                attemptSubmit(ViewMode.INPUT);
                break;
            case R.id.action_edit:
                attemptSubmit(ViewMode.EDIT);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.startTimeTextView:
                Calendar calendar = mModel.getStartTimeMutableLiveData().getValue();
                if (calendar == null) {
                    calendar = mModel.getStartTimeCalendar();
                }
                new DatePickerDialog(this, mStartTimeOnDateSetListener,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.endTimeTextView:
                calendar = mModel.getEndTimeMutableLiveData().getValue();
                if (calendar == null) {
                    calendar = mModel.getEndTimeCalendar();
                }
                new DatePickerDialog(this, mEndTimeOnDateSetListener,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.alarmTambahTextView:
                Intent iReminder = new Intent(this, ReminderActivity.class);
                iReminder.putExtra(IntentUtil.Companion.getREMINDER(), mModel.getReminderEnum());
                startActivityForResult(iReminder, REMINDER_RESULT_ACTIVITY);
                break;
            case R.id.checkInActionTextView:
                VisitView visitView = mModel.getVisitViewMutableLiveData().getValue();
                if (visitView != null) {
                    mModel.callCheckInRadius(SessionUtil.getUserId(), visitView.getCalendarId());
                }
                break;
            case R.id.checkOutActionTextView:
                new MaterialDialog.Builder(this)
                        .title(R.string.title_attempt_check_out)
                        .content(R.string.message_attempt_check_out)
                        .positiveText(R.string.action_ok)
                        .positiveColorRes(R.color.colorPrimary)
                        .negativeText(R.string.action_cancel)
                        .negativeColorRes(R.color.colorAccent)
                        .onPositive((dialog, which) -> {
                            VisitView innerVisitView = mModel.getVisitViewMutableLiveData().getValue();
                            if (innerVisitView != null) {
                                VisitCheckOutRequest visitCheckOutRequest = new VisitCheckOutRequest(innerVisitView.getCalendarId());
                                mModel.callCheckOut(visitCheckOutRequest);
                            }
                        })
                        .show();
                break;
            case R.id.moreStoreInformationLinearLayout:
                mModel.callStoreInformation(1L);
                break;
            case R.id.moreChecklistBALinearLayout:
                visitView = mModel.getVisitViewMutableLiveData().getValue();
                if (visitView != null) {
                    Intent iChecklistBAHistory = new Intent(getBaseContext(), ChecklistBAHistoryActivity.class);
                    iChecklistBAHistory.putExtra(IntentUtil.Companion.getCALENDAR_ID(), visitView.getCalendarId());
                    iChecklistBAHistory.putExtra(IntentUtil.Companion.getLOCATION_ID(), visitView.getLocation().getId());
                    startActivity(iChecklistBAHistory);
                }
                break;
            case R.id.leftStoreInformationButton:
                WardahUtil.INSTANCE.setTabButton(getBaseContext(), mLeftStoreInformationButton, true);
                WardahUtil.INSTANCE.setTabButton(getBaseContext(), mRightStoreInformationButton, false);
//                mModel.getVisitStoreInformationMutableLiveData().postValue(mModel.getVisitViewMutableLiveData().getValue().getStoreInformation().get(1));
                break;
            case R.id.rightStoreInformationButton:
                WardahUtil.INSTANCE.setTabButton(getBaseContext(), mLeftStoreInformationButton, false);
                WardahUtil.INSTANCE.setTabButton(getBaseContext(), mRightStoreInformationButton, true);
//                mModel.getVisitStoreInformationMutableLiveData().postValue(mModel.getVisitViewMutableLiveData().getValue().getStoreInformation().get(1));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REMINDER_RESULT_ACTIVITY && resultCode == Activity.RESULT_OK) {
            ReminderEnum reminderEnum = (ReminderEnum) data.getSerializableExtra(IntentUtil.Companion.getREMINDER());
            mAlarmTextView.setText(reminderEnum.getText());
            mAlarmTambahTextView.setText(getResources().getString(R.string.text_ubah));
            mModel.setReminderEnum(reminderEnum);
            Snackbar.make(mCoordinatorLayout, reminderEnum.getText(), Snackbar.LENGTH_LONG).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(ChecklistBA checklistBA) {
        Intent iBAInformation = new Intent(getBaseContext(), BaInformationActivity.class);
        iBAInformation.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
        startActivity(iBAInformation);
    }

    @Override
    public void onEvaluationButtonClicked(ChecklistBA checklistBA) {
        Intent iEvaluation = new Intent(this, EvaluationBAActivity.class);
        iEvaluation.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.INPUT);
        startActivity(iEvaluation);
    }

    private void attemptSubmit(ViewMode viewMode) {
        mDCAutoCompleteTextView.setError(null);
        mLocationAutoCompleteTextView.setError(null);
        mStartTimeTextView.setError(null);
        mEndTimeTextView.setError(null);

        boolean cancel = false;
        View focusView = null;

        String dc = mDCAutoCompleteTextView.getText().toString();
        String location = mLocationAutoCompleteTextView.getText().toString();
        String startTimeStr = mStartTimeTextView.getText().toString();
        String endTimeStr = mEndTimeTextView.getText().toString();
        String alarmTimeStr = mAlarmTextView.getText().toString();
        String notes = mNoteEditText.getText().toString();

        if (TextUtils.isEmpty(dc)) {
            mDCAutoCompleteTextView.setError(getString(R.string.error_field_required));
            focusView = mDCAutoCompleteTextView;
            cancel = true;
        } else if (getDCDropdown(dc) == null) {
            mDCAutoCompleteTextView.setError(getString(R.string.error_dc_invalid));
            focusView = mDCAutoCompleteTextView;
            cancel = true;
        } else if (TextUtils.isEmpty(location)) {
            mLocationAutoCompleteTextView.setError(getString(R.string.error_field_required));
            focusView = mLocationAutoCompleteTextView;
            cancel = true;
        } else if (getDCLocationDropdown(location) == null) {
            mLocationAutoCompleteTextView.setError(getString(R.string.error_location_invalid));
            focusView = mLocationAutoCompleteTextView;
            cancel = true;
        } else if (TextUtils.isEmpty(startTimeStr)) {
            mStartTimeTextView.setError(getString(R.string.error_location_invalid));
            focusView = mStartTimeTextView;
            cancel = true;
        } else if (TextUtils.isEmpty(endTimeStr)) {
            mEndTimeTextView.setError(getString(R.string.error_location_invalid));
            focusView = mEndTimeTextView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            switch (viewMode) {
                case INPUT:
                    VisitCreateRequest visitCreateRequest = new VisitCreateRequest(SessionUtil.getUserId(),
                            getDCLocationDropdown(location),
                            getDCDropdown(dc),
                            mCategoryAdapter.getData(mCategorySpinner.getSelectedItemPosition()).getName(),
                            mModel.getStartTimeCalendar().getTimeInMillis(),
                            mModel.getEndTimeCalendar().getTimeInMillis(),
                            ReminderEnum.Companion.getReminder(mModel.getStartTimeCalendar().getTimeInMillis(), mModel.getReminderEnum()),
                            alarmTimeStr,
                            notes);
                    mModel.submitVisit(visitCreateRequest);
                    break;
                case EDIT:
                    VisitEditRequest visitEditRequest = new VisitEditRequest(SessionUtil.getUserId(),
                            getDCLocationDropdown(location),
                            getDCDropdown(dc),
                            mCategoryAdapter.getData(mCategorySpinner.getSelectedItemPosition()).getName(),
                            mModel.getStartTimeCalendar().getTimeInMillis(),
                            mModel.getEndTimeCalendar().getTimeInMillis(),
                            ReminderEnum.Companion.getReminder(mModel.getStartTimeCalendar().getTimeInMillis(), mModel.getReminderEnum()),
                            alarmTimeStr,
                            notes,
                            mModel.getVisitViewMutableLiveData().getValue().getCalendarId());
                    mModel.editVisit(visitEditRequest);
                    break;
            }
        }
    }

    private DCDropdown getDCDropdown(String name) {
        List<DCDropdown> dcDropdownList = mModel.getDcDropdownMutableLiveData().getValue();
        if (dcDropdownList != null) {
            for (DCDropdown dcDropdown : dcDropdownList) {
                if (name.equals(dcDropdown.getName())) {
                    return dcDropdown;
                }
            }
        }

        return null;
    }

    private DCLocationDropdown getDCLocationDropdown(String name) {
        List<DCLocationDropdown> dcLocationDropdownList = mModel.getDcLocationDropdownMutableLiveData().getValue();
        if (dcLocationDropdownList != null) {
            for (DCLocationDropdown dcLocationDropdown : dcLocationDropdownList) {
                if (name.equals(dcLocationDropdown.getName())) {
                    return dcLocationDropdown;
                }
            }
        }

        return null;
    }
}
