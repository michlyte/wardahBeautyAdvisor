package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.ChecklistEvent

class ChecklistEventResponse : GenericResponse() {
    var data: ChecklistEvent? = null
}
