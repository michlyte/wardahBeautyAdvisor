package com.gghouse.wardah.wardahba.dummy

import com.gghouse.wardah.wardahba.models.HomepageFC
import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import com.gghouse.wardah.wardahba.webservices.model.CheckInEvent
import com.gghouse.wardah.wardahba.webservices.model.Event
import com.gghouse.wardah.wardahba.webservices.model.EventLocation
import com.gghouse.wardah.wardahba.webservices.model.Visit
import java.util.*

object VisitDummy {

    val kunjunganTerdekat = Visit(0L, "ABC Store", Date().time, Date().time, "Bandung", Date().time, "Performance", "12", 8000000.00, 4000000.00, Date().time, Date().time, "6/6 BA Terevaluasi")
    val kunjunganTerakhir = Visit(0L, "BCD Store", DateTimeUtil.getPreviousDay(1).time, DateTimeUtil.getPreviousDay(1).time, "Bandung", DateTimeUtil.getPreviousDay(1).time, "Performance", "12", 8000000.00, 4000000.00, DateTimeUtil.getPreviousDay(1).time, DateTimeUtil.getPreviousDay(1).time, "6/6 BA Terevaluasi")

    val eventTerdekat = Event("EDIT", 8L, "Event Meet n' Greet Wardah",
            "Performance", Date().time, Date().time, "notes",
            EventLocation(11L, "Jl. Asia Afrika", "-6.921894", "107.615599"),
            CheckInEvent(Date().time, "A", "img/bp-image-1547960263329.jpg", "img/booth-image-1547960263381.jpg", false),
            ChecklistDummy.checklistList,
            AttendantDummy.ATTENDANT_EVENT_LIST)
    val eventTerakhir = Event("EDIT", 8L, "Event Meet n' Greet Wardah",
            "Performance", Date().time, Date().time, "notes",
            EventLocation(11L, "Jl. Asia Afrika", "-6.921894", "107.615599"),
            CheckInEvent(Date().time, "A", "img/bp-image-1547960263329.jpg", "img/booth-image-1547960263381.jpg", false),
            ChecklistDummy.checklistList,
            AttendantDummy.ATTENDANT_EVENT_LIST)

    val homepageFC = HomepageFC(kunjunganTerakhir, kunjunganTerdekat)

}