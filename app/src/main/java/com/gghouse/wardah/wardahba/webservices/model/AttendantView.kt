package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class AttendantView : Serializable {
    val name: String
    val mobileNumber: String
    val email: String
    val address: String
    val attendantId: Long
    val viewType: String

    constructor(name: String, mobileNumber: String, email: String, address: String, attendantId: Long, viewType: String) {
        this.name = name
        this.mobileNumber = mobileNumber
        this.email = email
        this.address = address
        this.attendantId = attendantId
        this.viewType = viewType
    }
}