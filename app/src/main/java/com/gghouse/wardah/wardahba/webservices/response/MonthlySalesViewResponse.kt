package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.MonthlySalesView

class MonthlySalesViewResponse : GenericResponse() {
    var data: MonthlySalesView? = null
}
