package com.gghouse.wardah.wardahba.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.home.HomeBAActivity;
import com.gghouse.wardah.wardahba.activities.home.HomeBPActivity;
import com.gghouse.wardah.wardahba.activities.home.HomeBPLActivity;
import com.gghouse.wardah.wardahba.activities.home.HomeFCActivity;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.dummy.UserDummy;
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum;
import com.gghouse.wardah.wardahba.utils.LogUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.SystemUtil;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.utils.ValidationUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.User;
import com.gghouse.wardah.wardahba.webservices.request.LoginRequest;
import com.gghouse.wardah.wardahba.webservices.response.LoginResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelcomeActivity extends AppCompatActivity {

    public static final String TAG = WelcomeActivity.class.getSimpleName();

    private final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 77;

    private EditText mETUsername;
    private EditText mETPassword;

    private MaterialDialog mMaterialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(null)
                .content(R.string.title_logging_in)
                .cancelable(false)
                .progress(true, 0);
        mMaterialDialog = builder.build();

        requestPermission();

        ImageView imageView = findViewById(R.id.iv_image);
        switch (Config.Companion.getMode()) {
            case DUMMY_DEVELOPMENT:
            case DEVELOPMENT:
                imageView.setOnLongClickListener(v -> {
                    String curIP = SessionUtil.loadIPAddress();
                    new MaterialDialog.Builder(v.getContext())
                            .title(R.string.title_ip_address)
                            .content(null)
                            .inputType(InputType.TYPE_CLASS_TEXT)
                            .input(null, curIP, (dialog, input) -> SessionUtil.saveIPAddress(input.toString()))
                            .positiveColorRes(R.color.colorPrimary)
                            .positiveText(R.string.action_ok)
                            .negativeColorRes(R.color.colorAccent)
                            .negativeText(R.string.action_cancel)
                            .cancelable(false)
                            .show();
                    return true;
                });
                break;
            case PRODUCTION:
                break;
        }
        mETUsername = findViewById(R.id.username);
        mETPassword = findViewById(R.id.password);
        mETPassword.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        });
        Button signIn = findViewById(R.id.sign_in);
        signIn.setOnClickListener(view -> {
            switch (Config.Companion.getMode()) {
                case DUMMY_DEVELOPMENT:
                    showLoginAsDialog();
                    break;
                case DEVELOPMENT:
                case PRODUCTION:
                    attemptLogin();
                    break;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void attemptLogin() {
        // Reset errors.
        mETUsername.setError(null);
        mETPassword.setError(null);

        // Store values at the time of the login attempt.
        String username = mETUsername.getText().toString();
        String password = mETPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            mETUsername.setError(getString(R.string.error_field_required));
            focusView = mETUsername;
            cancel = true;
        }
        // Check for a valid password, if the user entered one.
        else if (TextUtils.isEmpty(password)) {
            mETPassword.setError(getString(R.string.error_field_required));
            focusView = mETPassword;
            cancel = true;
        } else if (!ValidationUtil.INSTANCE.isPasswordValid(password)) {
            mETPassword.setError(getString(R.string.error_password_too_short));
            focusView = mETPassword;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            callLoginApi(username, password);
        }
    }

    private void callLoginApi(String username, String password) {
        mMaterialDialog.show();

        Call<LoginResponse> callUserlogin = ApiClient.INSTANCE.getClient().apiLogin(new LoginRequest(
                username, password, SystemUtil.getImei(this), FirebaseInstanceId.getInstance().getToken()));
        callUserlogin.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LogUtil.INSTANCE.log(LogUtil.INSTANCE.getON_RESPONSE());
                mMaterialDialog.dismiss();

                if (response.isSuccessful()) {
                    LoginResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            actionLogin(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_login_invaled);
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                ToastUtil.INSTANCE.toastMessage(TAG, t.getMessage());
                mMaterialDialog.dismiss();
            }
        });
    }

    private void actionLogin(User user) {
        UserTypeEnum userTypeEnum = UserTypeEnum.Companion.getUserTypeEnum(user.getPosition());
        Intent intent;
        if (userTypeEnum != null) {
            SessionUtil.loggingIn(user);

            switch (userTypeEnum) {
                case BEAUTY_ADVISOR:
                    intent = new Intent(getBaseContext(), HomeBAActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case BEAUTY_PROMOTOR:
                    intent = new Intent(getBaseContext(), HomeBPActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case BEAUTY_PROMOTER_LEADER:
                    intent = new Intent(getBaseContext(), HomeBPLActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case FIELD_CONTROLLER:
                    intent = new Intent(getBaseContext(), HomeFCActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                default:
                    break;
            }
        } else {
            ToastUtil.INSTANCE.toastMessage(R.string.error_user_type_is_not_supported);
        }
    }

    private void requestPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_PHONE_STATE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private void showLoginAsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Login as")
                .setItems(R.array.login_as, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            actionLogin(UserDummy.INSTANCE.getBAUser());
                            break;
                        case 1:
                            actionLogin(UserDummy.INSTANCE.getBPUser());
                            break;
                        case 2:
                            actionLogin(UserDummy.INSTANCE.getBPLUser());
                            break;
                        case 3:
                            actionLogin(UserDummy.INSTANCE.getFCUser());
                            break;
                    }
                });
        builder.create();
        builder.show();
    }
}