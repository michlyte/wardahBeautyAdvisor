package com.gghouse.wardah.wardahba.viewmodels;

import android.os.Handler;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.WardahApp;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.dummy.AttendantDummy;
import com.gghouse.wardah.wardahba.dummy.CustomerDummy;
import com.gghouse.wardah.wardahba.dummy.SalesDummy;
import com.gghouse.wardah.wardahba.dummy.StoreDummy;
import com.gghouse.wardah.wardahba.dummy.TestDummy;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.AttendantView;
import com.gghouse.wardah.wardahba.webservices.model.DailySalesLatestDate;
import com.gghouse.wardah.wardahba.webservices.model.DailySalesView;
import com.gghouse.wardah.wardahba.webservices.model.MonthlyLatestMonth;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySalesView;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;
import com.gghouse.wardah.wardahba.webservices.model.SalesEditProducts;
import com.gghouse.wardah.wardahba.webservices.model.SalesInputProducts;
import com.gghouse.wardah.wardahba.webservices.model.SalesLatestDate;
import com.gghouse.wardah.wardahba.webservices.model.SalesTarget;
import com.gghouse.wardah.wardahba.webservices.model.TestLatestDate;
import com.gghouse.wardah.wardahba.webservices.model.TestQuestion;
import com.gghouse.wardah.wardahba.webservices.response.AttendantEventResponse;
import com.gghouse.wardah.wardahba.webservices.response.AttendantViewResponse;
import com.gghouse.wardah.wardahba.webservices.response.CustomerListResponse;
import com.gghouse.wardah.wardahba.webservices.response.DailySalesLatestDateResponse;
import com.gghouse.wardah.wardahba.webservices.response.DailySalesResponse;
import com.gghouse.wardah.wardahba.webservices.response.DailySalesViewResponse;
import com.gghouse.wardah.wardahba.webservices.response.HistoryTestResponse;
import com.gghouse.wardah.wardahba.webservices.response.MonthlyLatestMonthResponse;
import com.gghouse.wardah.wardahba.webservices.response.MonthlySalesCreateResponse;
import com.gghouse.wardah.wardahba.webservices.response.MonthlySalesResponse;
import com.gghouse.wardah.wardahba.webservices.response.MonthlySalesViewResponse;
import com.gghouse.wardah.wardahba.webservices.response.QuestionerLatestDateResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesEditProductResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesInputProductResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesLatestDateResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesListResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesTargetHistoryResponse;
import com.gghouse.wardah.wardahba.webservices.response.SalesTargetViewResponse;
import com.gghouse.wardah.wardahba.webservices.response.TestLatestDateResponse;
import com.gghouse.wardah.wardahba.webservices.response.TestQuestionsResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WardahSimpleFragmentViewModel extends ViewModel {
    private static final String TAG = WardahSimpleFragmentViewModel.class.getSimpleName();

    private MutableLiveData<ViewMode> mViewModeMutableLiveData;
    private MutableLiveData<List<?>> mDataSetMutableLiveData;

    // Sales
    private MutableLiveData<SalesInputProducts> salesInputProductsMutableLiveData;
    private MutableLiveData<SalesEditProducts> salesEditProductsMutableLiveData;

    // Daily Sales
    private MutableLiveData<DailySalesView> dailySalesViewMutableLiveData;

    // Monthly Sales
    private MutableLiveData<List<ProductType>> monthlyProductTypeListMutableLiveData;
    private MutableLiveData<MonthlySalesView> monthlySalesViewMutableLiveData;

    // Test
    private MutableLiveData<TestLatestDate> testLatestDateMutableLiveData;
    private MutableLiveData<TestQuestion> testQuestionListMutableLiveData;

    // Attendant
    private MutableLiveData<AttendantView> attendantViewMutableLiveData;

    // Sales Target
    private MutableLiveData<SalesTarget> salesTargetMutableLiveData;

    // Questionnaire
    private MutableLiveData<Long> questionerLatestDateMutableLiveData;

    private Long latestDate;
    private Integer month;
    private Integer year;

    // Attendant
    private Long calendarId;

    public MutableLiveData<List<?>> getDataSetMutableLiveData() {
        if (mDataSetMutableLiveData == null) {
            mDataSetMutableLiveData = new MutableLiveData<>();
        }
        return mDataSetMutableLiveData;
    }

    public MutableLiveData<ViewMode> getViewModeMutableLiveData() {
        if (mViewModeMutableLiveData == null) {
            mViewModeMutableLiveData = new MutableLiveData<>();
        }
        return mViewModeMutableLiveData;
    }

    public MutableLiveData<SalesInputProducts> getSalesInputProductsMutableLiveData() {
        if (salesInputProductsMutableLiveData == null) {
            salesInputProductsMutableLiveData = new MutableLiveData<>();
        }
        return salesInputProductsMutableLiveData;
    }

    public MutableLiveData<SalesEditProducts> getSalesEditProductsMutableLiveData() {
        if (salesEditProductsMutableLiveData == null) {
            salesEditProductsMutableLiveData = new MutableLiveData<>();
        }
        return salesEditProductsMutableLiveData;
    }

    public MutableLiveData<DailySalesView> getDailySalesViewMutableLiveData() {
        if (dailySalesViewMutableLiveData == null) {
            dailySalesViewMutableLiveData = new MutableLiveData<>();
        }
        return dailySalesViewMutableLiveData;
    }

    public MutableLiveData<List<ProductType>> getMonthlyProductTypeListMutableLiveData() {
        if (monthlyProductTypeListMutableLiveData == null) {
            monthlyProductTypeListMutableLiveData = new MutableLiveData<>();
        }
        return monthlyProductTypeListMutableLiveData;
    }

    public MutableLiveData<MonthlySalesView> getMonthlySalesViewMutableLiveData() {
        if (monthlySalesViewMutableLiveData == null) {
            monthlySalesViewMutableLiveData = new MutableLiveData<>();
        }
        return monthlySalesViewMutableLiveData;
    }

    public MutableLiveData<TestLatestDate> getTestLatestDateMutableLiveData() {
        if (testLatestDateMutableLiveData == null) {
            testLatestDateMutableLiveData = new MutableLiveData<>();
        }
        return testLatestDateMutableLiveData;
    }

    public MutableLiveData<TestQuestion> getTestQuestionListMutableLiveData() {
        if (testQuestionListMutableLiveData == null) {
            testQuestionListMutableLiveData = new MutableLiveData<>();
        }
        return testQuestionListMutableLiveData;
    }

    public MutableLiveData<AttendantView> getAttendantViewMutableLiveData() {
        if (attendantViewMutableLiveData == null) {
            attendantViewMutableLiveData = new MutableLiveData<>();
        }
        return attendantViewMutableLiveData;
    }

    public MutableLiveData<SalesTarget> getSalesTargetMutableLiveData() {
        if (salesTargetMutableLiveData == null) {
            salesTargetMutableLiveData = new MutableLiveData<>();
        }
        return salesTargetMutableLiveData;
    }

    public MutableLiveData<Long> getQuestionerLatestDateMutableLiveData() {
        if (questionerLatestDateMutableLiveData == null) {
            questionerLatestDateMutableLiveData = new MutableLiveData<>();
        }
        return questionerLatestDateMutableLiveData;
    }

    public Long getLatestDate() {
        return latestDate;
    }

    public Long getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(Long calendarId) {
        this.calendarId = calendarId;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void getData(WardahItemType type) {
        if (SessionUtil.validateLoginSession()) {
            Long userId = SessionUtil.getUserId();

            switch (Config.Companion.getMode()) {
                case DUMMY_DEVELOPMENT:
                    callDummyData(type);
                    break;
                case DEVELOPMENT:
                case PRODUCTION:
                    callTodayData(userId, type);
                    callLatestData(userId, type);
                    break;
            }
        }
    }

    private void callDummyData(WardahItemType type) {
        new Handler().postDelayed(() -> {
            getViewModeMutableLiveData().setValue(ViewMode.INPUT);
            switch (type) {
                case TEST:
                    getDataSetMutableLiveData().postValue(TestDummy.INSTANCE.getTestList());
                    break;
                case STORE_DAILY_SALES:
                    getDataSetMutableLiveData().postValue(StoreDummy.storeSalesList);
                    break;
                case STORE_MONTHLY_SALES:
                    getDataSetMutableLiveData().postValue(StoreDummy.storeSalesMonthlyList);
                    break;
                case SALES:
                    getDataSetMutableLiveData().postValue(SalesDummy.salesList);
                    break;
                case CUSTOMER:
                    getDataSetMutableLiveData().postValue(CustomerDummy.INSTANCE.getCUSTOMER_SIMPLE_LIST());
                    break;
                case ATTENDANT:
                    getDataSetMutableLiveData().postValue(AttendantDummy.INSTANCE.getATTENDANT_SIMPLE_LIST());
                    break;
                case INPUT_TARGET:
//                    getDataSetMutableLiveData().postValue(SalesDummy.salesTargetSimpleList);
                    break;
            }
        }, Config.Companion.getDELAY_LOAD_MORE());
    }

    private void callTodayData(long userId, WardahItemType type) {
        switch (type) {
            case TEST:
                Call<TestLatestDateResponse> callTestLatestDate = ApiClient.INSTANCE.getClient().apiTestLatestDate(userId);
                callTestLatestDate.enqueue(new Callback<TestLatestDateResponse>() {
                    @Override
                    public void onResponse(Call<TestLatestDateResponse> call, Response<TestLatestDateResponse> response) {
                        if (response.isSuccessful()) {
                            TestLatestDateResponse testLatestDateResponse = response.body();
                            TestLatestDate testLatestDate = testLatestDateResponse.getData();
                            switch (testLatestDateResponse.getCode()) {
                                case Config.CODE_200:
                                    latestDate = testLatestDate.getTestDate();

                                    getTestLatestDateMutableLiveData().setValue(testLatestDate);
                                    break;
                                default:
                                    getTestLatestDateMutableLiveData().setValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, testLatestDateResponse.getStatus());
                                    break;
                            }
                        } else {
                            getTestLatestDateMutableLiveData().setValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<TestLatestDateResponse> call, Throwable t) {
                        getViewModeMutableLiveData().setValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case SALES:
                Call<SalesLatestDateResponse> callSalesLatestDate = ApiClient.INSTANCE.getClient().apiSalesLatestDate(userId);
                callSalesLatestDate.enqueue(new Callback<SalesLatestDateResponse>() {
                    @Override
                    public void onResponse(Call<SalesLatestDateResponse> call, Response<SalesLatestDateResponse> response) {
                        if (response.isSuccessful()) {
                            SalesLatestDateResponse salesLatestDateResponse = response.body();
                            SalesLatestDate salesLatestDate = salesLatestDateResponse.getData();
                            switch (salesLatestDateResponse.getCode()) {
                                case Config.CODE_200:
                                    latestDate = salesLatestDate.getInputDate();

                                    if (salesLatestDate.getInputDate() == null) {
                                        getViewModeMutableLiveData().setValue(ViewMode.VIEW);
                                    } else {
                                        getViewModeMutableLiveData().setValue(ViewMode.INPUT);
                                    }
                                    break;
                                default:
                                    getViewModeMutableLiveData().setValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, salesLatestDateResponse.getStatus());
                                    break;
                            }
                        } else {
                            getViewModeMutableLiveData().setValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesLatestDateResponse> call, Throwable t) {
                        getViewModeMutableLiveData().setValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_DAILY_SALES:
                Call<DailySalesLatestDateResponse> callDailySalesLatestDate = ApiClient.INSTANCE.getClient().apiDailySalesLatestDate(SessionUtil.getUserLocationId());
                callDailySalesLatestDate.enqueue(new Callback<DailySalesLatestDateResponse>() {
                    @Override
                    public void onResponse(Call<DailySalesLatestDateResponse> call, Response<DailySalesLatestDateResponse> response) {
                        if (response.isSuccessful()) {
                            DailySalesLatestDateResponse dailySalesLatestDateResponse = response.body();
                            DailySalesLatestDate dailySalesLatestDate = dailySalesLatestDateResponse.getData();
                            switch (dailySalesLatestDateResponse.getCode()) {
                                case Config.CODE_200:
                                    latestDate = dailySalesLatestDate.getInputDate();

                                    if (dailySalesLatestDate.getInputDate() == null) {
                                        getViewModeMutableLiveData().setValue(ViewMode.VIEW);
                                    } else {
                                        getViewModeMutableLiveData().setValue(ViewMode.INPUT);
                                    }
                                    break;
                                default:
                                    getViewModeMutableLiveData().setValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, dailySalesLatestDateResponse.getStatus());
                                    break;
                            }
                        } else {
                            getViewModeMutableLiveData().setValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<DailySalesLatestDateResponse> call, Throwable t) {
                        getViewModeMutableLiveData().setValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_MONTHLY_SALES:
                Call<MonthlyLatestMonthResponse> callMonthlyLatestMonth = ApiClient.INSTANCE.getClient().apiMonthlySalesMonth(SessionUtil.getUserLocationId());
                callMonthlyLatestMonth.enqueue(new Callback<MonthlyLatestMonthResponse>() {
                    @Override
                    public void onResponse(Call<MonthlyLatestMonthResponse> call, Response<MonthlyLatestMonthResponse> response) {
                        if (response.isSuccessful()) {
                            MonthlyLatestMonthResponse monthlyLatestMonthResponse = response.body();
                            MonthlyLatestMonth monthlyLatestMonth = monthlyLatestMonthResponse.getData();
                            switch (monthlyLatestMonthResponse.getCode()) {
                                case Config.CODE_200:
                                    month = monthlyLatestMonth.getMonth();
                                    year = monthlyLatestMonth.getYear();

                                    if (monthlyLatestMonth.getMonth() == null || monthlyLatestMonth.getYear() == null) {
                                        getViewModeMutableLiveData().setValue(ViewMode.VIEW);
                                    } else {
                                        getViewModeMutableLiveData().setValue(ViewMode.INPUT);
                                    }
                                    break;
                                default:
                                    getViewModeMutableLiveData().setValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, monthlyLatestMonthResponse.getStatus());
                                    break;
                            }
                        } else {
                            getViewModeMutableLiveData().setValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<MonthlyLatestMonthResponse> call, Throwable t) {
                        getViewModeMutableLiveData().setValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case CUSTOMER:
                getViewModeMutableLiveData().postValue(ViewMode.INPUT);
                break;
            case ATTENDANT:
                getViewModeMutableLiveData().postValue(ViewMode.INPUT);
                break;
            case INPUT_TARGET:
                getViewModeMutableLiveData().postValue(ViewMode.INPUT);
                break;
        }
    }

    private void callLatestData(long userId, WardahItemType type) {
        switch (type) {
            case TEST:
                Call<HistoryTestResponse> callTestList = ApiClient.INSTANCE.getClient().apiTest(userId, 0, 5, null, null);
                callTestList.enqueue(new Callback<HistoryTestResponse>() {
                    @Override
                    public void onResponse(Call<HistoryTestResponse> call, Response<HistoryTestResponse> response) {
                        if (response.isSuccessful()) {
                            HistoryTestResponse historyTestResponse = response.body();
                            switch (historyTestResponse.getCode()) {
                                case Config.CODE_200:
                                    getDataSetMutableLiveData().postValue(historyTestResponse.getData());
                                    break;
                                default:
                                    getDataSetMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, historyTestResponse.getStatus());
                                    break;
                            }
                        } else {
                            getDataSetMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<HistoryTestResponse> call, Throwable t) {
                        getDataSetMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case SALES:
                Call<SalesListResponse> callSales = ApiClient.INSTANCE.getClient().apiSales(userId, 0, 5, null, null);
                callSales.enqueue(new Callback<SalesListResponse>() {
                    @Override
                    public void onResponse(Call<SalesListResponse> call, Response<SalesListResponse> response) {
                        if (response.isSuccessful()) {
                            SalesListResponse salesListResponse = response.body();
                            switch (salesListResponse.getCode()) {
                                case Config.CODE_200:
                                    getDataSetMutableLiveData().postValue(salesListResponse.getData());
                                    break;
                                default:
                                    getDataSetMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, salesListResponse.getStatus());
                                    break;
                            }
                        } else {
                            getDataSetMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesListResponse> call, Throwable t) {
                        getDataSetMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_DAILY_SALES:
                Call<DailySalesResponse> callDailySales = ApiClient.INSTANCE.getClient().apiDailySales(
                        SessionUtil.getUserLocationId(), 0, 5, "created_time,DESC", null, null);
                callDailySales.enqueue(new Callback<DailySalesResponse>() {
                    @Override
                    public void onResponse(Call<DailySalesResponse> call, Response<DailySalesResponse> response) {
                        if (response.isSuccessful()) {
                            DailySalesResponse dailySalesResponse = response.body();
                            switch (dailySalesResponse.getCode()) {
                                case Config.CODE_200:
                                    getDataSetMutableLiveData().postValue(dailySalesResponse.getData());
                                    break;
                                default:
                                    getDataSetMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, dailySalesResponse.getStatus());
                                    break;
                            }
                        } else {
                            getDataSetMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<DailySalesResponse> call, Throwable t) {
                        getDataSetMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case STORE_MONTHLY_SALES:
                Call<MonthlySalesResponse> callMonthlySales = ApiClient.INSTANCE.getClient().apiMonthlySales(
                        SessionUtil.getUserLocationId(), 0, 5, "created_time,DESC", null, null);
                callMonthlySales.enqueue(new Callback<MonthlySalesResponse>() {
                    @Override
                    public void onResponse(Call<MonthlySalesResponse> call, Response<MonthlySalesResponse> response) {
                        if (response.isSuccessful()) {
                            MonthlySalesResponse monthlySalesResponse = response.body();
                            switch (monthlySalesResponse.getCode()) {
                                case Config.CODE_200:
                                    getDataSetMutableLiveData().postValue(monthlySalesResponse.getData());
                                    break;
                                default:
                                    getDataSetMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, monthlySalesResponse.getStatus());
                                    break;
                            }
                        } else {
                            getDataSetMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<MonthlySalesResponse> call, Throwable t) {
                        getDataSetMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case CUSTOMER:
                Call<CustomerListResponse> callCustomer = ApiClient.INSTANCE.getClient().apiCustomer(SessionUtil.getUserId(), 0, 5, null, null);
                callCustomer.enqueue(new Callback<CustomerListResponse>() {
                    @Override
                    public void onResponse(Call<CustomerListResponse> call, Response<CustomerListResponse> response) {
                        if (response.isSuccessful()) {
                            CustomerListResponse customerListResponse = response.body();
                            switch (customerListResponse.getCode()) {
                                case Config.CODE_200:
                                    getDataSetMutableLiveData().postValue(customerListResponse.getData());
                                    break;
                                default:
                                    getDataSetMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, customerListResponse.getStatus());
                                    break;
                            }
                        } else {
                            getDataSetMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerListResponse> call, Throwable t) {
                        getDataSetMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case ATTENDANT:
                Call<AttendantEventResponse> callAttendantEvent = ApiClient.INSTANCE.getClient().apiAttendantEvent(calendarId, 0, 3);
                callAttendantEvent.enqueue(new Callback<AttendantEventResponse>() {
                    @Override
                    public void onResponse(Call<AttendantEventResponse> call, Response<AttendantEventResponse> response) {
                        if (response.isSuccessful()) {
                            AttendantEventResponse attendantEventResponse = response.body();
                            switch (attendantEventResponse.getCode()) {
                                case Config.CODE_200:
                                    getDataSetMutableLiveData().postValue(attendantEventResponse.getData());
                                    break;
                                default:
                                    getDataSetMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, attendantEventResponse.getStatus());
                                    break;
                            }
                        } else {
                            getDataSetMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<AttendantEventResponse> call, Throwable t) {
                        getDataSetMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
            case INPUT_TARGET:
                Call<SalesTargetHistoryResponse> callSalesTargetHistory = ApiClient.INSTANCE.getClient().apiSalesTargetHistory(SessionUtil.getUserId(), 0, 3);
                callSalesTargetHistory.enqueue(new Callback<SalesTargetHistoryResponse>() {
                    @Override
                    public void onResponse(Call<SalesTargetHistoryResponse> call, Response<SalesTargetHistoryResponse> response) {
                        if (response.isSuccessful()) {
                            SalesTargetHistoryResponse salesTargetHistoryResponse = response.body();
                            switch (salesTargetHistoryResponse.getCode()) {
                                case Config.CODE_200:
                                    getDataSetMutableLiveData().postValue(salesTargetHistoryResponse.getData());
                                    break;
                                default:
                                    getDataSetMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, salesTargetHistoryResponse.getStatus());
                                    break;
                            }
                        } else {
                            getDataSetMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<SalesTargetHistoryResponse> call, Throwable t) {
                        getDataSetMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
        }
    }

    public void callQuestionData(Long userId) {
        switch (Config.Companion.getMode()) {
            case DUMMY_DEVELOPMENT:
                getTestQuestionListMutableLiveData().postValue(TestDummy.INSTANCE.getQuestionList());
                break;
            case DEVELOPMENT:
            case PRODUCTION:
                Call<TestQuestionsResponse> callTestList = ApiClient.INSTANCE.getClient().apiTestQuestions(userId);
                callTestList.enqueue(new Callback<TestQuestionsResponse>() {
                    @Override
                    public void onResponse(Call<TestQuestionsResponse> call, Response<TestQuestionsResponse> response) {
                        if (response.isSuccessful()) {
                            TestQuestionsResponse rps = response.body();
                            switch (rps.getCode()) {
                                case Config.CODE_200:
                                    if (rps.getData().getQuestions().size() > 0) {
                                        getTestQuestionListMutableLiveData().postValue(rps.getData());
                                    } else {
                                        ToastUtil.INSTANCE.toastMessage(TAG, WardahApp.getInstance().getString(R.string.message_test_no_more));
                                    }
                                    break;
                                case Config.CODE_211:
                                    getTestQuestionListMutableLiveData().postValue(new TestQuestion(new Date().getTime(), new ArrayList<>()));
                                    break;
                                default:
                                    getTestQuestionListMutableLiveData().postValue(null);
                                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                                    break;
                            }
                        } else {
                            getTestQuestionListMutableLiveData().postValue(null);
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                        }
                    }

                    @Override
                    public void onFailure(Call<TestQuestionsResponse> call, Throwable t) {
                        getTestQuestionListMutableLiveData().postValue(null);
                        ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
                    }
                });
                break;
        }
    }

    public void callSalesInputProducts(long locationId) {
        Call<SalesInputProductResponse> callSalesInputProducts = ApiClient.INSTANCE.getClient().apiSalesInputProduct(locationId);
        callSalesInputProducts.enqueue(new Callback<SalesInputProductResponse>() {
            @Override
            public void onResponse(Call<SalesInputProductResponse> call, Response<SalesInputProductResponse> response) {
                if (response.isSuccessful()) {
                    SalesInputProductResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getSalesInputProductsMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<SalesInputProductResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callSalesEditProducts(long salesId) {
        Call<SalesEditProductResponse> callSalesEditProducts = ApiClient.INSTANCE.getClient().apiSalesEditProduct(salesId);
        callSalesEditProducts.enqueue(new Callback<SalesEditProductResponse>() {
            @Override
            public void onResponse(Call<SalesEditProductResponse> call, Response<SalesEditProductResponse> response) {
                if (response.isSuccessful()) {
                    SalesEditProductResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getSalesEditProductsMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<SalesEditProductResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callDailySalesView(long dailyReportId) {
        Call<DailySalesViewResponse> callDailySalesEdit = ApiClient.INSTANCE.getClient().apiDailySalesView(dailyReportId);
        callDailySalesEdit.enqueue(new Callback<DailySalesViewResponse>() {
            @Override
            public void onResponse(Call<DailySalesViewResponse> call, Response<DailySalesViewResponse> response) {
                if (response.isSuccessful()) {
                    DailySalesViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            DailySalesView dailySalesView = rps.getData();
                            dailySalesView.setDailyReportId(dailyReportId);
                            getDailySalesViewMutableLiveData().postValue(dailySalesView);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<DailySalesViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callMonthlySalesCreate(long locationId) {
        Call<MonthlySalesCreateResponse> callMonthlySalesCreate = ApiClient.INSTANCE.getClient().apiMonthlySalesCreate(locationId);
        callMonthlySalesCreate.enqueue(new Callback<MonthlySalesCreateResponse>() {
            @Override
            public void onResponse(Call<MonthlySalesCreateResponse> call, Response<MonthlySalesCreateResponse> response) {
                if (response.isSuccessful()) {
                    MonthlySalesCreateResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getMonthlyProductTypeListMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<MonthlySalesCreateResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callMonthlySalesView(long monthlyReportId) {
        Call<MonthlySalesViewResponse> callMonthySalesView = ApiClient.INSTANCE.getClient().apiMonthlySalesView(monthlyReportId);
        callMonthySalesView.enqueue(new Callback<MonthlySalesViewResponse>() {
            @Override
            public void onResponse(Call<MonthlySalesViewResponse> call, Response<MonthlySalesViewResponse> response) {
                if (response.isSuccessful()) {
                    MonthlySalesViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            MonthlySalesView monthlySalesView = rps.getData();
                            monthlySalesView.setMonthlyReportId(monthlyReportId);
                            getMonthlySalesViewMutableLiveData().postValue(monthlySalesView);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<MonthlySalesViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callAttendantView(long attendantId) {
        Call<AttendantViewResponse> callAttendantView = ApiClient.INSTANCE.getClient().apiAttendantView(attendantId);
        callAttendantView.enqueue(new Callback<AttendantViewResponse>() {
            @Override
            public void onResponse(Call<AttendantViewResponse> call, Response<AttendantViewResponse> response) {
                if (response.isSuccessful()) {
                    AttendantViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getAttendantViewMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<AttendantViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    // TODO: Must remove editable parameter
    public void callSalesTargetView(long salesTargetId, boolean editable) {
        Call<SalesTargetViewResponse> callSalesTargetView = ApiClient.INSTANCE.getClient().apiSalesTargetView(salesTargetId);
        callSalesTargetView.enqueue(new Callback<SalesTargetViewResponse>() {
            @Override
            public void onResponse(Call<SalesTargetViewResponse> call, Response<SalesTargetViewResponse> response) {
                if (response.isSuccessful()) {
                    SalesTargetViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            SalesTarget salesTarget = rps.getData();
                            salesTarget.setEditable(editable);
                            getSalesTargetMutableLiveData().postValue(salesTarget);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<SalesTargetViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callQuestionerLatestDate(long userId) {
        if (SessionUtil.validateLoginSession()) {
            Call<QuestionerLatestDateResponse> callQuestionerLatestDate = ApiClient.INSTANCE.getClient().apiQuestionerLatestDate(userId);
            callQuestionerLatestDate.enqueue(new Callback<QuestionerLatestDateResponse>() {
                @Override
                public void onResponse(Call<QuestionerLatestDateResponse> call, Response<QuestionerLatestDateResponse> response) {
                    if (response.isSuccessful()) {
                        QuestionerLatestDateResponse questionerLatestDateResponse = response.body();
                        switch (questionerLatestDateResponse.getCode()) {
                            case Config.CODE_200:
                                questionerLatestDateMutableLiveData.postValue(questionerLatestDateResponse.getData().getQuestionnaireDate());
                                break;
                            default:
                                questionerLatestDateMutableLiveData.postValue(null);
                                ToastUtil.INSTANCE.toastMessage(TAG, questionerLatestDateResponse.getStatus());
                                break;
                        }
                    } else {
                        questionerLatestDateMutableLiveData.postValue(null);
                        ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                    }
                }

                @Override
                public void onFailure(Call<QuestionerLatestDateResponse> call, Throwable t) {
                    questionerLatestDateMutableLiveData.postValue(null);
                    ToastUtil.INSTANCE.toastMessage(TAG, t.getMessage());
                }
            });
        }
    }
}
