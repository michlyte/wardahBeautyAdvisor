package com.gghouse.wardah.wardahba.utils

/**
 * Created by michaelhalim on 5/28/17.
 */

interface IntentUtil {
    companion object {
        val NOTIF = "NOTIF"
        val DATA = "DATA"
        val DATE = "DATE"
        val LOCK = "LOCK"
        val EDIT = "EDIT"
        val SALES = "SALES"
        val STORE_DAILY_SALES = "STORE_DAILY_SALES"
        val PRODUCT_HIGHLIGHT = "PRODUCT_HIGHLIGHT"
        val PRODUCT_FOCUS = "PRODUCT_FOCUS"
        val FREQUENT = "FREQUENT"
        val CUSTOMER = "CUSTOMER"
        val EVENT = "EVENT"
        val CHECKLIST = "CHECKLIST"
        val EMPTY_VIEW = "EMPTY_VIEW"
        val VIEW_MODE = "VIEW_MODE"
        val TYPE = "TYPE"
        val INPUT_TARGET = "INPUT_TARGET"
        val STORE_INFORMATION = "STORE_INFORMATION"
        val VISIT = "VISIT"
        val PRODUCT_TYPE = "PRODUCT_TYPE"
        val ATTENDANT = "ATTENDANT"
        val ATTENDANT_BUTTON = "ATTENDANT_BUTTON"
        val CALENDAR_ID = "CALENDAR_ID"
        val EVENT_CATEGORY = "EVENT_CATEGORY"
        val VISIT_CATEGORY = "VISIT_CATEGORY"
        val LOCATION_ID = "LOCATION_ID"
        val USER_ID = "USER_ID"
        val EVALUTION_QUESTIONS = "EVALUTION_QUESTIONS"
        val CHECK_IN_RADIUS = "CHECK_IN_RADIUS"
        val STORE_ID = "STORE_ID"
        val REMINDER = "REMINDER"
    }
}
