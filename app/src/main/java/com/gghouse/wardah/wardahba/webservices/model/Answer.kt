package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

/**
 * Created by michaelhalim on 5/6/17.
 */

class Answer(var id: Long?, var content: String?) : Serializable {
    var questionId: Long? = null
}
