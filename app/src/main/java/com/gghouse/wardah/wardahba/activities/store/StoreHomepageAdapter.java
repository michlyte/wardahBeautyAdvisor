package com.gghouse.wardah.wardahba.activities.store;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.enumerations.HomepageRecyclerViewItem;
import com.gghouse.wardah.wardahba.models.StoreHomepage2DotsRowItem;
import com.gghouse.wardah.wardahba.models.StoreHomepage3DotsRowItem;
import com.gghouse.wardah.wardahba.models.StoreHomepageNormalItem;
import com.gghouse.wardah.wardahba.viewholders.StoreHomepage2DotsRowItemViewHolder;
import com.gghouse.wardah.wardahba.viewholders.StoreHomepage3DotsRowItemViewHolder;
import com.gghouse.wardah.wardahba.viewholders.StoreHomepageNormalRowItemViewHolder;

import java.util.List;

public class StoreHomepageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_NORMAL = 0;
    private final int VIEW_TYPE_2_DOTS = 1;
    private final int VIEW_TYPE_3_DOTS = 2;

    private List<HomepageRecyclerViewItem> mDataset;

    public StoreHomepageAdapter(List<HomepageRecyclerViewItem> mDataset) {
        this.mDataset = mDataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_store_horizontal, parent, false);
                return new StoreHomepageNormalRowItemViewHolder(v);
            case VIEW_TYPE_3_DOTS:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_store_horizontal_3_dots, parent, false);
                return new StoreHomepage3DotsRowItemViewHolder(v);
            case VIEW_TYPE_2_DOTS:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_store_horizontal_2_dots, parent, false);
                return new StoreHomepage2DotsRowItemViewHolder(v);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HomepageRecyclerViewItem homepageRecyclerViewItem = (HomepageRecyclerViewItem) mDataset.get(position);
        switch (homepageRecyclerViewItem.getType()) {
            case NORMAL:
                StoreHomepageNormalRowItemViewHolder storeHomepageNormalRowItemViewHolder = (StoreHomepageNormalRowItemViewHolder) holder;
                storeHomepageNormalRowItemViewHolder.setData((StoreHomepageNormalItem) homepageRecyclerViewItem);
                break;
            case THREE_DOTS:
                StoreHomepage3DotsRowItemViewHolder threeDotsHolder = (StoreHomepage3DotsRowItemViewHolder) holder;
                threeDotsHolder.setData((StoreHomepage3DotsRowItem) homepageRecyclerViewItem);
                break;
            case TWO_DOTS:
                StoreHomepage2DotsRowItemViewHolder twoDotsHolder = (StoreHomepage2DotsRowItemViewHolder) holder;
                twoDotsHolder.setData((StoreHomepage2DotsRowItem) homepageRecyclerViewItem);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mDataset == null ? 0 : mDataset.size();
    }

    @Override
    public int getItemViewType(int position) {
//        switch (position) {
//            case 0:
//                return VIEW_TYPE_3_DOTS;
//            case 1:
//                return VIEW_TYPE_2_DOTS;
//        }
//        return super.getItemViewType(position);
        return VIEW_TYPE_NORMAL;
    }

    public List<HomepageRecyclerViewItem> getDataset() {
        return mDataset;
    }

    public void setDataset(List<HomepageRecyclerViewItem> mDataset) {
        this.mDataset = mDataset;
    }
}
