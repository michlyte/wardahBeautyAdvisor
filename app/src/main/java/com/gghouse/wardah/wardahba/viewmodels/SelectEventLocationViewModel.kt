package com.gghouse.wardah.wardahba.viewmodels

import android.location.Address
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.request.StoreGPSRequest
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SelectEventLocationViewModel : ViewModel() {
    val address: MutableLiveData<Address> by lazy { MutableLiveData<Address>() }
    val isSubmitSuccess: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    var storeId: Long = 0

    fun submitStoreGPS(storeGPSRequest: StoreGPSRequest) {
        val callSubmitStoreGPS = ApiClient.client.apiStoreGPS(storeGPSRequest)
        callSubmitStoreGPS.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSuccess.postValue(true)
                        else -> {
                            isSubmitSuccess.postValue(false)
                            ToastUtil.toastMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSuccess.postValue(false)
                    ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSuccess.postValue(false)
                ToastUtil.toastMessage(TAG, t.message.toString())
            }
        })
    }

    companion object {

        private val TAG = SelectEventLocationViewModel::class.java.simpleName
    }
}
