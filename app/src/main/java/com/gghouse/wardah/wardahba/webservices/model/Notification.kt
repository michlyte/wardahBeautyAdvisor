package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

/**
 * Created by michael on 3/13/2017.
 */

class Notification(var id: Long?, var description: String?, var type: String?, // Dummy variables
                   var drawable: Int?, var periodStart: Long?) : Serializable {
    var periodEnd: Long? = null
    var fileLocation: FileLocation? = null
    var status: String? = null
    var notificationTime: Long? = null
}
