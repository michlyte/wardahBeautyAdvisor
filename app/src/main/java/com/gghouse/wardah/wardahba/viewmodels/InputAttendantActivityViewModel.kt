package com.gghouse.wardah.wardahba.viewmodels

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.ModeEnum
import com.gghouse.wardah.wardahba.enumerations.ViewMode
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.AttendantView
import com.gghouse.wardah.wardahba.webservices.request.AttendantCreateRequest
import com.gghouse.wardah.wardahba.webservices.request.AttendantEditRequest
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InputAttendantActivityViewModel : ViewModel() {

    val attendantMutableLiveData: MutableLiveData<AttendantView> by lazy {
        MutableLiveData<AttendantView>()
    }
    val viewModeMutableLiveData: MutableLiveData<ViewMode> by lazy {
        MutableLiveData<ViewMode>()
    }
    val isSubmitSucceedMutableLiveData: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    var calendarId: Long? = null

    fun submitAttendant(attendantCreateRequest: AttendantCreateRequest) {
        when (Config.mode) {
            ModeEnum.DUMMY_DEVELOPMENT -> Handler().postDelayed({ isSubmitSucceedMutableLiveData.postValue(true) }, Config.DELAY_LOAD_MORE.toLong())
            ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> {
                val callAttendantCreate = ApiClient.client.apiAttendantEventCreate(attendantCreateRequest)
                callAttendantCreate.enqueue(object : Callback<GenericResponse> {
                    override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                        if (response.isSuccessful) {
                            val genericResponse = response.body()
                            when (genericResponse!!.code) {
                                Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                                else -> {
                                    isSubmitSucceedMutableLiveData.postValue(false)
                                    ToastUtil.toastMessage(TAG, genericResponse.status!!)
                                }
                            }
                        } else {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                        }
                    }

                    override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                        isSubmitSucceedMutableLiveData.postValue(false)
                        ToastUtil.toastMessage(TAG, t.message.toString())
                    }
                })
            }
        }
    }

    fun editAttendant(attendantEditRequest: AttendantEditRequest) {
        when (Config.mode) {
            ModeEnum.DUMMY_DEVELOPMENT -> Handler().postDelayed({ isSubmitSucceedMutableLiveData.postValue(true) }, Config.DELAY_LOAD_MORE.toLong())
            ModeEnum.DEVELOPMENT, ModeEnum.PRODUCTION -> {
                val callCustomerEdit = ApiClient.client.apiAttendantEdit(attendantEditRequest)
                callCustomerEdit.enqueue(object : Callback<GenericResponse> {
                    override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                        if (response.isSuccessful) {
                            val genericResponse = response.body()
                            when (genericResponse!!.code) {
                                Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                                else -> {
                                    isSubmitSucceedMutableLiveData.postValue(false)
                                    ToastUtil.toastMessage(TAG, genericResponse.status!!)
                                }
                            }
                        } else {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                        }
                    }

                    override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                        isSubmitSucceedMutableLiveData.postValue(false)
                        ToastUtil.toastMessage(TAG, t.message.toString())
                    }
                })
            }
        }
    }

    companion object {
        private val TAG = InputAttendantActivityViewModel::class.java.simpleName
    }
}
