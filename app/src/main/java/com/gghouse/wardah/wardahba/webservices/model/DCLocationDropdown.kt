package com.gghouse.wardah.wardahba.webservices.model

class DCLocationDropdown {
    val id: Long
    val name: String
    val address: String
    val latitude: String
    val longitude: String

    constructor(id: Long, name: String, address: String, latitude: String, longitude: String) {
        this.id = id
        this.name = name
        this.address = address
        this.latitude = latitude
        this.longitude = longitude
    }
}