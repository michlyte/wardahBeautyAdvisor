package com.gghouse.wardah.wardahba.activities.home;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class ImagePreviewActivity extends AppCompatActivity {

    private SubsamplingScaleImageView mIVImage;

    private final Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            mIVImage.setImage(ImageSource.bitmap(bitmap));
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        mIVImage = findViewById(R.id.iv_image);
        mIVImage.setMinimumDpi(20);
        Intent intent = getIntent();
        if (intent != null) {
            String url = intent.getStringExtra(IntentUtil.Companion.getDATA());
            switch (Config.Companion.getMode()) {
                case DUMMY_DEVELOPMENT:
                    Picasso.get()
                            .load(url)
                            .placeholder(R.drawable.progress_animation)
                            .error(R.drawable.pic_image_not_found)
                            .into(target);
                    break;
                default:
                    Picasso.get()
                            .load(ImageUtil.INSTANCE.getImageUrl(url))
                            .placeholder(R.drawable.progress_animation)
                            .error(R.drawable.pic_image_not_found)
                            .into(target);
                    break;
            }
        } else {
            mIVImage.setImage(ImageSource.resource(R.drawable.pic_image_not_found));
        }

        findViewById(R.id.b_close).setOnClickListener(v -> finish());
    }
}
