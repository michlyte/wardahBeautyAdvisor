package com.gghouse.wardah.wardahba.activities.event;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProviders;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.WardahApp;
import com.gghouse.wardah.wardahba.enumerations.GPSStatus;
import com.gghouse.wardah.wardahba.utils.DateTimeUtil;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.LocationUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.viewmodels.CheckInActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.CheckInLocation;
import com.gghouse.wardah.wardahba.webservices.model.CheckInRadius;
import com.gghouse.wardah.wardahba.webservices.request.CheckInFCRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CheckInOutActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private static final String TAG = CheckInOutActivity.class.getSimpleName();

    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;

    private CheckInActivityViewModel mModel;
    private ProgressBar mProgressBar;
    private TextView mAddPhotoTextView;
    private ImageView mPhotoImageView;
    private String mPhotoPhotoPath;
    private TextView mGPSStatusTextView;

    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    // Temporary variable
    private PhotoType mPhotoType;

    private GoogleMap mMap;
    private TextView mLocationTextView;
    private MarkerOptions mMarkerOptions;
    private Geocoder mGeocoder;
    private boolean mLocationPermissionGranted;
    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    // Keys for storing activity state.
    private static final String KEY_LOCATION = "location";
    private static final int DEFAULT_ZOOM = 15;
    // A default location (Cihampelas Walk) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-6.893400380132707, 107.60556701570749);

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
        }
        setContentView(R.layout.activity_check_in_out);
        mModel = ViewModelProviders.of(this).get(CheckInActivityViewModel.class);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mProgressBar = findViewById(R.id.progressBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        if (i != null) {
            mModel.setCheckInRadius((CheckInRadius) i.getSerializableExtra(IntentUtil.Companion.getCHECK_IN_RADIUS()));
        } else {
            mModel.setCheckInRadius(new CheckInRadius(200, 200, null));
        }

        mGPSStatusTextView = findViewById(R.id.gpsStatusTextView);
        mPhotoImageView = findViewById(R.id.fotoBersamaImageView);
        mPhotoImageView.setOnClickListener(this);
        Picasso.get().load(R.drawable.pic_image_not_found).into(mPhotoImageView);
        mAddPhotoTextView = findViewById(R.id.ambilFotoBersamaTextView);
        mAddPhotoTextView.setOnClickListener(this);
        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        // Geocoder
        mGeocoder = new Geocoder(this, DateTimeUtil.INSTANCE.getLocale());
        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ImageView locationImageView = findViewById(R.id.locationImageView);
        Picasso.get().load(R.mipmap.ic_location).into(locationImageView);
        mLocationTextView = findViewById(R.id.locationTextView);

        mModel.getBpMutableLiveData().observe(this, fcImagePhotoPath -> {
            if (fcImagePhotoPath != null) {
                ImageUtil.INSTANCE.galleryAddPic(getBaseContext(), fcImagePhotoPath);
                ImageUtil.INSTANCE.setPic(this, mPhotoImageView, fcImagePhotoPath);
                mAddPhotoTextView.setText(R.string.action_retake_photo);
            } else {
                Picasso.get().load(R.drawable.pic_image_not_found).into(mPhotoImageView);
            }
        });

        mModel.getAddressMutableLiveData().observe(this, address -> {
            progressBar(false);

            if (address != null && address.getAddressLine(0) != null) {

                if (mModel.getCheckInRadius().getLocation() != null) {
                    LatLng eventLatLng = new LatLng(Double.parseDouble(mModel.getCheckInRadius().getLocation().getLatitude()),
                            Double.parseDouble(mModel.getCheckInRadius().getLocation().getLongitude()));
                    LatLng userLatLng = new LatLng(address.getLatitude(), address.getLongitude());

                    if (LocationUtil.INSTANCE.getDistranceInMeters(eventLatLng, userLatLng) < mModel.getCheckInRadius().getRadius()) {
                        mModel.getGpsStatusMutableLiveData().postValue(GPSStatus.A);
                    } else {
                        mModel.getGpsStatusMutableLiveData().postValue(GPSStatus.O);
                    }
                } else {
                    mModel.getGpsStatusMutableLiveData().postValue(GPSStatus.E);
                }

                String addr = address.getAddressLine(0);
                mLocationTextView.setText(addr);

                LatLng curLatLng = new LatLng(address.getLatitude(), address.getLongitude());
                mMarkerOptions = new MarkerOptions().position(curLatLng);

                mMap.clear();
                mMap.addMarker(mMarkerOptions);
            } else {
                mModel.getGpsStatusMutableLiveData().postValue(GPSStatus.F);
            }
        });

        mModel.getGpsStatusMutableLiveData().observe(this, gpsStatus -> {
            if (gpsStatus != null) {
                String titleStr = getString(R.string.title_gps_status, getString(gpsStatus.getResId()));
                mGPSStatusTextView.setText(titleStr);
            }
        });

        mModel.isSubmitSucceedMutableLiveData().observe(this, isSuccess -> {
            progressBar(false);

            if (isSuccess != null && isSuccess) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_check_in, menu);
        menu.findItem(R.id.action_save).setVisible(mModel.getAddressMutableLiveData().getValue() != null);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_save:
                attemptSubmit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void attemptSubmit() {
        String bpImage = mModel.getBpMutableLiveData().getValue();

        if (bpImage == null) {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_error)
                    .content(R.string.error_check_in_image_required)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .show();
        } else {
            String gpsStatus = mModel.getGpsStatusMutableLiveData().getValue() != null ?
                    mModel.getGpsStatusMutableLiveData().getValue().name() : GPSStatus.F.name();
            Address address = mModel.getAddressMutableLiveData().getValue();
            CheckInLocation checkInLocation = new CheckInLocation(
                    address.getAddressLine(0), String.valueOf(address.getLatitude()),
                    String.valueOf(address.getLongitude()), gpsStatus);
            CheckInFCRequest checkInFCRequest = new CheckInFCRequest(SessionUtil.getUserId(), mModel.getCheckInRadius().getCalendarId(),
                    checkInLocation, ImageUtil.INSTANCE.getImageInBase64String(bpImage));
            progressBar(true);
            mModel.checkInFC(checkInFCRequest);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ambilFotoBersamaTextView:
                mPhotoType = PhotoType.FC;
                getTakePhotoPermission(mPhotoType);
                break;
            case R.id.fotoBersamaImageView:
                if (mPhotoPhotoPath != null && !mPhotoPhotoPath.isEmpty())
                    zoomImageFromThumb(mPhotoImageView, mPhotoPhotoPath);
                else {
                    mPhotoType = PhotoType.FC;
                    getTakePhotoPermission(mPhotoType);
                }
                break;
        }
    }

    private void zoomImageFromThumb(final View thumbView, String photoPath) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) findViewById(
                R.id.expanded_image);
        Bitmap bitmap = ImageUtil.INSTANCE.getBitmap(expandedImageView, photoPath);
        expandedImageView.setImageBitmap(bitmap);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(view -> {
            if (mCurrentAnimator != null) {
                mCurrentAnimator.cancel();
            }

            // Animate the four positioning/sizing properties in parallel,
            // back to their original values.
            AnimatorSet set1 = new AnimatorSet();
            set1.play(ObjectAnimator
                    .ofFloat(expandedImageView, View.X, startBounds.left))
                    .with(ObjectAnimator
                            .ofFloat(expandedImageView,
                                    View.Y, startBounds.top))
                    .with(ObjectAnimator
                            .ofFloat(expandedImageView,
                                    View.SCALE_X, startScaleFinal))
                    .with(ObjectAnimator
                            .ofFloat(expandedImageView,
                                    View.SCALE_Y, startScaleFinal));
            set1.setDuration(mShortAnimationDuration);
            set1.setInterpolator(new DecelerateInterpolator());
            set1.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    thumbView.setAlpha(1f);
                    expandedImageView.setVisibility(View.GONE);
                    mCurrentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    thumbView.setAlpha(1f);
                    expandedImageView.setVisibility(View.GONE);
                    mCurrentAnimator = null;
                }
            });
            set1.start();
            mCurrentAnimator = set1;
        });
    }

    private void dispatchTakePictureIntent(PhotoType photoType) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(photoType);
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(TAG, ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        WardahApp.getInstance().getPackageName() + ".fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                switch (photoType) {
                    case FC:
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                        break;
                }

            }
        }
    }

    private File createImageFile(PhotoType photoType) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        switch (photoType) {
            case FC:
                mPhotoPhotoPath = image.getAbsolutePath();
                break;
        }
        return image;
    }

    private void getTakePhotoPermission(PhotoType photoType) {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            dispatchTakePictureIntent(photoType);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case REQUEST_TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
                    // Thumbnail
//                    Bundle extras = data.getExtras();
//                    Bitmap imageBitmap = (Bitmap) extras.get("data");

                    mModel.getBpMutableLiveData().postValue(mPhotoPhotoPath);
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getLocationPermission();
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            getDeviceLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                } else {
                    mLastKnownLocation = null;
                }
                getDeviceLocation();
                break;
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent(mPhotoType);
                }
                break;
        }
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        progressBar(true);

        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Log.d(TAG, "onComplete");
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            double lat = (mLastKnownLocation == null) ? -6.9220405 : mLastKnownLocation.getLatitude();
                            double lng = (mLastKnownLocation == null) ? 107.616701 : mLastKnownLocation.getLongitude();

//                            LatLng latLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                            LatLng latLng = new LatLng(lat, lng);
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
                            try {
                                Address address = getAddressFromLatLng(latLng);
                                // Jika lokasi tidak ditemukan, gunakan lokasi default
                                if (address == null) {
                                    address = getAddressFromLatLng(mDefaultLocation);
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                                }

                                mModel.getAddressMutableLiveData().postValue(address);
                            } catch (IOException ioe) {
                                Log.e(TAG, ioe.getMessage());

                                mModel.getAddressMutableLiveData().postValue(null);
                            }
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            try {
                                mModel.getAddressMutableLiveData().postValue(getAddressFromLatLng(mDefaultLocation));
                            } catch (IOException ioe) {
                                Log.e(TAG, ioe.getMessage());

                                mModel.getAddressMutableLiveData().postValue(null);
                            }
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private Address getAddressFromLatLng(LatLng latLng) throws IOException {
        List<Address> addressList = mGeocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

        for (Address addr : addressList) {
            return addr;
        }

        return null;
    }

    private void progressBar(Boolean visible) {
        mModel.setCheckInLoading(visible);
        mProgressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
        invalidateOptionsMenu();

        mPhotoImageView.setOnClickListener(visible ? null : this);
        mAddPhotoTextView.setOnClickListener(visible ? null : this);
    }

    private enum PhotoType {
        FC
    }
}
