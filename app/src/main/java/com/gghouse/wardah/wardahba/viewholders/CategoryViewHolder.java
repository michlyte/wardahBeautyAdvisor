package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.webservices.model.ProductType;
import com.squareup.picasso.Picasso;

public class CategoryViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    private final ImageView mIconImageView;
    private final TextView mTitleTextView;
    private final TextView mPcsTextView;
    private final TextView mAmountTextView;
    public ProductType mItem;

    public CategoryViewHolder(View view) {
        super(view);

        this.view = view;
        mIconImageView = view.findViewById(R.id.iconImageView);
        mPcsTextView = view.findViewById(R.id.pcsTextView);
        mAmountTextView = view.findViewById(R.id.amountTextView);
        mTitleTextView = view.findViewById(R.id.titleEditText);
    }

    public void setData(ProductType productType) {
        mItem = productType;

        String pcs = WardahUtil.INSTANCE.getPcs(mItem.getQuantity());
        String amount = WardahUtil.INSTANCE.getAmount(mItem.getAmount());

        if (mItem.getImageUrl() != null && !mItem.getImageUrl().isEmpty()) {
            Picasso.get()
                    .load(ImageUtil.INSTANCE.getImageUrl(mItem.getImageUrl()))
                    .fit()
                    .centerCrop()
                    .into(mIconImageView);
        } else {
            Picasso.get()
                    .load(R.drawable.pic_image_not_found)
                    .fit()
                    .centerCrop()
                    .into(mIconImageView);
        }

        mPcsTextView.setText(pcs);
        mAmountTextView.setText(amount);
        mTitleTextView.setText(mItem.getName());
    }
}