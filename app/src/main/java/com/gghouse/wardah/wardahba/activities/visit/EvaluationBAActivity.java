package com.gghouse.wardah.wardahba.activities.visit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.adapters.EvaluationAdapter;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.models.EvaluationQuestionInner;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.viewholders.EvaluationBAActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.EvaluationAnswer;
import com.gghouse.wardah.wardahba.webservices.model.EvaluationChecklist;
import com.gghouse.wardah.wardahba.webservices.model.EvaluationQuestion;
import com.gghouse.wardah.wardahba.webservices.request.EvaluationAnswerRequest;

import java.util.ArrayList;
import java.util.List;

public class EvaluationBAActivity extends AppCompatActivity {

    private EvaluationBAActivityViewModel mModel;
    private static final String PERFORMANCE = "Performance";
    private static final String KNOWLEDGE = "Knowledge";
    private static final String REPORT = "Report";

    private LinearLayout mPerformanceLinearLayout;
    private RecyclerView mPerformanceRecyclerView;
    private EvaluationAdapter mPerformanceAdapter;

    private LinearLayout mReportLinearLayout;
    private RecyclerView mReportRecyclerView;
    private EvaluationAdapter mReportAdapter;

    private LinearLayout mKnowledgeLinearLayout;
    private RecyclerView mKnowledgeRecyclerView;
    private EvaluationAdapter mKnowledgeAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation_ba);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mModel = ViewModelProviders.of(this).get(EvaluationBAActivityViewModel.class);

        Intent intent = getIntent();

        try {
            mModel.getViewModeMutableLiveData().postValue((ViewMode) intent.getSerializableExtra(IntentUtil.Companion.getVIEW_MODE()));
            EvaluationQuestionInner evaluationQuestionInner = (EvaluationQuestionInner) intent.getSerializableExtra(IntentUtil.Companion.getEVALUTION_QUESTIONS());
            if (evaluationQuestionInner != null) {
                mModel.getEvaluationQuestionsMutableLiveData().postValue(evaluationQuestionInner);
            }
        } catch (NullPointerException npe) {
            mModel.getViewModeMutableLiveData().postValue(ViewMode.VIEW);
            npe.printStackTrace();
        }

        mPerformanceLinearLayout = findViewById(R.id.performanceLinearLayout);
        mPerformanceRecyclerView = findViewById(R.id.performanceRecyclerView);
        mPerformanceRecyclerView.setHasFixedSize(true);
        mPerformanceRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager performanceLinearLayoutManager = new LinearLayoutManager(this);
        mPerformanceRecyclerView.setLayoutManager(performanceLinearLayoutManager);

        mReportLinearLayout = findViewById(R.id.reportLinearLayout);
        mReportRecyclerView = findViewById(R.id.reportRecyclerView);
        mReportRecyclerView.setHasFixedSize(true);
        mReportRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager reportLinearLayoutManager = new LinearLayoutManager(this);
        mReportRecyclerView.setLayoutManager(reportLinearLayoutManager);

        mKnowledgeLinearLayout = findViewById(R.id.knowledgeLinearLayout);
        mKnowledgeRecyclerView = findViewById(R.id.knowledgeRecyclerView);
        mKnowledgeRecyclerView.setHasFixedSize(true);
        mKnowledgeRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager knowledgeLinearLayoutManager = new LinearLayoutManager(this);
        mKnowledgeRecyclerView.setLayoutManager(knowledgeLinearLayoutManager);

        mModel.getEvaluationQuestionsMutableLiveData().observe(this, evaluationQuestions -> {
            if (evaluationQuestions != null) {

                List<EvaluationChecklist> performanceChecklist = new ArrayList<>();
                List<EvaluationChecklist> reportChecklist = new ArrayList<>();
                List<EvaluationChecklist> knowledgeChecklist = new ArrayList<>();

                for (EvaluationQuestion evaluationQuestion : evaluationQuestions.getData()) {
                    switch (evaluationQuestion.getName()) {
                        case PERFORMANCE:
                            performanceChecklist = evaluationQuestion.getChecklist();
                            break;
                        case REPORT:
                            reportChecklist = evaluationQuestion.getChecklist();
                            break;
                        case KNOWLEDGE:
                            knowledgeChecklist = evaluationQuestion.getChecklist();
                            break;
                    }
                }

                if (performanceChecklist.size() > 0) {
                    mPerformanceAdapter = new EvaluationAdapter(performanceChecklist);
                    mPerformanceRecyclerView.setAdapter(mPerformanceAdapter);
                } else {
                    mPerformanceLinearLayout.setVisibility(View.GONE);
                }

                if (reportChecklist.size() > 0) {
                    mReportAdapter = new EvaluationAdapter(reportChecklist);
                    mReportRecyclerView.setAdapter(mReportAdapter);
                } else {
                    mReportLinearLayout.setVisibility(View.GONE);
                }

                if (knowledgeChecklist.size() > 0) {
                    mKnowledgeAdapter = new EvaluationAdapter(knowledgeChecklist);
                    mKnowledgeRecyclerView.setAdapter(mKnowledgeAdapter);
                } else {
                    mKnowledgeLinearLayout.setVisibility(View.GONE);
                }
            }
        });

        mModel.getIsSubmitSucceedMutableLiveData().observe(this, isSuccess -> {
            if (isSuccess != null && isSuccess) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            switch (mModel.getViewModeMutableLiveData().getValue()) {
                case INPUT:
                    getMenuInflater().inflate(R.menu.menu_input, menu);
                    break;
                case EDIT:
                    getMenuInflater().inflate(R.menu.menu_edit, menu);
                    break;
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                attemptCancel();
                return true;
            case R.id.action_save:
                attemptSubmit();
                return true;
            case R.id.action_edit:
                break;
        }
        return true;
    }

    private void attemptSubmit() {
        if (validate()) {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_submit_evaluation_ba)
                    .content(R.string.message_submit_evaluation_ba)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .negativeText(R.string.action_cancel)
                    .negativeColorRes(R.color.colorAccent)
                    .onPositive((dialog, which) -> {
                        List<EvaluationAnswer> evaluationAnswers = new ArrayList<>();
                        for (EvaluationChecklist evaluationChecklist : mPerformanceAdapter.getData()) {
                            evaluationAnswers.add(new EvaluationAnswer(evaluationChecklist.getId(), evaluationChecklist.getRating()));
                        }
                        for (EvaluationChecklist evaluationChecklist : mReportAdapter.getData()) {
                            evaluationAnswers.add(new EvaluationAnswer(evaluationChecklist.getId(), evaluationChecklist.getRating()));
                        }
                        for (EvaluationChecklist evaluationChecklist : mKnowledgeAdapter.getData()) {
                            evaluationAnswers.add(new EvaluationAnswer(evaluationChecklist.getId(), evaluationChecklist.getRating()));
                        }

                        EvaluationQuestionInner evaluationQuestionInner = mModel.getEvaluationQuestionsMutableLiveData().getValue();
                        EvaluationAnswerRequest evaluationAnswerRequest = new EvaluationAnswerRequest(evaluationQuestionInner.getCalendarId(), evaluationQuestionInner.getUserId(), evaluationAnswers);
                        mModel.submitEvaluation(evaluationAnswerRequest);
                    })
                    .show();
        } else {
            new MaterialDialog.Builder(this)
                    .title(R.string.title_validation_error)
                    .content(R.string.message_submit_validation_error)
                    .positiveText(R.string.action_ok)
                    .positiveColorRes(R.color.colorPrimary)
                    .show();
        }
    }

    private void attemptCancel() {
        finish();
    }

    private boolean validate() {
        for (EvaluationChecklist evaluationChecklist : mPerformanceAdapter.getData()) {
            if (evaluationChecklist.getRating() == null || evaluationChecklist.getRating() <= 0) {
                return false;
            }
        }

        for (EvaluationChecklist evaluationChecklist : mReportAdapter.getData()) {
            if (evaluationChecklist.getRating() == null || evaluationChecklist.getRating() <= 0) {
                return false;
            }
        }

        for (EvaluationChecklist evaluationChecklist : mKnowledgeAdapter.getData()) {
            if (evaluationChecklist.getRating() == null || evaluationChecklist.getRating() <= 0) {
                return false;
            }
        }

        return true;
    }
}
