package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahVisit
import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import java.io.Serializable

class LatestVisit : WardahVisit, Serializable {
    val category: String
    val alarmTime: Long
    val notes: String
    val location: VisitLocation
    val checkInOutButtonShown: String
    val checkInOutbuttonEnable: Boolean
    val evaluationCompleted: Int
    val totalBA: Int
    val dcName: String
    val fcImageURL: String?
    val storeId: Long

    constructor(startTime: Long, endTime: Long, category: String, alarmTime: Long, notes: String, location: VisitLocation, checkInOutButtonShown: String, checkInOutbuttonEnable: Boolean, evaluationCompleted: Int, totalBA: Int, dcName: String, fcImageURL: String?, storeId: Long) : super(startTime, endTime) {
        this.category = category
        this.alarmTime = alarmTime
        this.notes = notes
        this.location = location
        this.checkInOutButtonShown = checkInOutButtonShown
        this.checkInOutbuttonEnable = checkInOutbuttonEnable
        this.evaluationCompleted = evaluationCompleted
        this.totalBA = totalBA
        this.dcName = dcName
        this.fcImageURL = fcImageURL
        this.storeId = storeId
    }

    val dateStr: String
        get() = DateTimeUtil.sdfDateMonthYear.format(startTime)

    val timeStr: String
        get() = DateTimeUtil.sdfEventTime.format(startTime) + " - " + DateTimeUtil.sdfEventTime.format(endTime)

    val alarmStr: String
        get() = DateTimeUtil.sdfEventStartEndTime.format(alarmTime)

//    val targetSalesStr: String
//        get() = WardahUtil.getAmount(targetSales)
//
//    val totalSalesStr: String
//        get() = WardahUtil.getAmount(totalSales)
//
//    val checkInStr: String
//        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkIn)
//
//    val checkOutStr: String
//        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkOut)
}