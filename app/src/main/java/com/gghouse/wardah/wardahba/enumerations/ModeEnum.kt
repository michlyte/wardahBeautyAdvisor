package com.gghouse.wardah.wardahba.enumerations

enum class ModeEnum {
    PRODUCTION,
    DEVELOPMENT,
    DUMMY_DEVELOPMENT
}
