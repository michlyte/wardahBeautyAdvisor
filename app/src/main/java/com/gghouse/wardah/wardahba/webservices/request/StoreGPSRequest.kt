package com.gghouse.wardah.wardahba.webservices.request

class StoreGPSRequest {
    val storeId: Long
    val latitude: Double
    val longitude: Double

    constructor(storeId: Long, latitude: Double, longitude: Double) {
        this.storeId = storeId
        this.latitude = latitude
        this.longitude = longitude
    }
}