package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.TestLatestDate

class TestLatestDateResponse : GenericResponse() {
    var data: TestLatestDate? = null
}
