package com.gghouse.wardah.wardahba.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.webservices.model.EventCategory;

import java.util.List;

public class EventCategoryAdapter extends ArrayAdapter<EventCategory> {
    private List<EventCategory> mValues;

    public EventCategoryAdapter(Context context, int resource, List<EventCategory> objects) {
        super(context, resource, objects);
        mValues = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return rowView(convertView, position);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowView(convertView, position);
    }

    public EventCategory getData(int position) {
        return mValues.get(position);
    }

    private View rowView(View convertView, int position) {
        EventCategory rowItem = getItem(position);

        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {

            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.spinner_item, null, false);

            holder.txtTitle = rowView.findViewById(R.id.titleEditText);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.txtTitle.setText(rowItem.getName());

        return rowView;
    }

    private class ViewHolder {
        TextView txtTitle;
    }
}
