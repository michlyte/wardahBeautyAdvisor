package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.Questioner

class QuestionerQuestions {
    var questionnaireDate: Long? = null
    var questionnaires: List<Questioner>? = null
}
