package com.gghouse.wardah.wardahba.webservices.model

class NearestEvent {
    val calendarId: Long
    val title: String
    val category: String
    val startTime: Long
    val endTime: Long
    val alarmTime: Long
    val notes: String
    val location: EventLocation
    val locationAddress: String
    val totalChecklist: Int
    val checklistCompleted: Int
    val totalAttendant: Int
    val checkInButtonEnable: Boolean
    val attendantButtonEnable: Boolean

    constructor(calendarId: Long, title: String, category: String, startTime: Long, endTime: Long, alarmTime: Long, notes: String, location: EventLocation, locationAddress: String, totalChecklist: Int, checklistCompleted: Int, totalAttendant: Int, checkInButtonEnable: Boolean, attendantButtonEnable: Boolean) {
        this.calendarId = calendarId
        this.title = title
        this.category = category
        this.startTime = startTime
        this.endTime = endTime
        this.alarmTime = alarmTime
        this.notes = notes
        this.location = location
        this.locationAddress = locationAddress
        this.totalChecklist = totalChecklist
        this.checklistCompleted = checklistCompleted
        this.totalAttendant = totalAttendant
        this.checkInButtonEnable = checkInButtonEnable
        this.attendantButtonEnable = attendantButtonEnable
    }
}