package com.gghouse.wardah.wardahba.webservices.model

class BAHomeDaily {
    val salesDate: Long
    val sales: BAHomeSales
    val highlight: BAHomeSales
    val focus: BAHomeSales
    val buyer: Integer
    val visitor: Integer
    val buyingPower: BuyingPower

    constructor(salesDate: Long, sales: BAHomeSales, highlight: BAHomeSales, focus: BAHomeSales, buyer: Integer, visitor: Integer, buyingPower: BuyingPower) {
        this.salesDate = salesDate
        this.sales = sales
        this.highlight = highlight
        this.focus = focus
        this.buyer = buyer
        this.visitor = visitor
        this.buyingPower = buyingPower
    }
}