package com.gghouse.wardah.wardahba.interfaces

import com.gghouse.wardah.wardahba.webservices.model.Checklist

interface ChecklistListener {
    fun onCheck(checklist: Checklist, isChecked: Boolean)
}
