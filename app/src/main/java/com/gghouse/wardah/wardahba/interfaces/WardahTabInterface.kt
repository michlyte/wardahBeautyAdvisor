package com.gghouse.wardah.wardahba.interfaces

import com.gghouse.wardah.wardahba.enumerations.WsMode

interface WardahTabInterface {
    fun ws(wsMode: WsMode)
}
