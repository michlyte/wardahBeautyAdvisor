package com.gghouse.wardah.wardahba.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.enumerations.ReminderEnum;
import com.gghouse.wardah.wardahba.enumerations.UserTypeEnum;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.model.AttendantButton;
import com.gghouse.wardah.wardahba.webservices.model.ChecklistEvent;
import com.gghouse.wardah.wardahba.webservices.model.Event;
import com.gghouse.wardah.wardahba.webservices.model.EventCategory;
import com.gghouse.wardah.wardahba.webservices.model.EventLocation;
import com.gghouse.wardah.wardahba.webservices.request.EventCreateRequest;
import com.gghouse.wardah.wardahba.webservices.request.EventEditRequest;
import com.gghouse.wardah.wardahba.webservices.response.AttendantButtonResponse;
import com.gghouse.wardah.wardahba.webservices.response.ChecklistEventResponse;
import com.gghouse.wardah.wardahba.webservices.response.EventViewResponse;
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateEventActivityViewModel extends ViewModel {
    private static final String TAG = CreateEventActivityViewModel.class.getSimpleName();

    private MutableLiveData<ViewMode> viewModeMutableLiveData;
    private MutableLiveData<Event> eventMutableLiveData;
    private MutableLiveData<ChecklistEvent> checklistEventMutableLiveData;
    private MutableLiveData<Boolean> isSubmitSucceedMutableLiveData;
    private MutableLiveData<EventLocation> locationEventMutableLiveData;
    private MutableLiveData<List<EventCategory>> eventCategoryListMutableLiveData;
    private MutableLiveData<Long> startTimeMutableLiveData;
    private MutableLiveData<AttendantButton> attendantEventMutableLiveData;
    private UserTypeEnum userType;

    public Calendar startTimeCalendar = Calendar.getInstance();
    public Calendar endTimeCalendar = Calendar.getInstance();
    private ReminderEnum reminderEnum = ReminderEnum.NO_REMINDER;

    public MutableLiveData<ViewMode> getViewModeMutableLiveData() {
        if (viewModeMutableLiveData == null) {
            viewModeMutableLiveData = new MutableLiveData<>();
        }
        return viewModeMutableLiveData;
    }

    public MutableLiveData<Event> getEventMutableLiveData() {
        if (eventMutableLiveData == null) {
            eventMutableLiveData = new MutableLiveData<>();
        }
        return eventMutableLiveData;
    }

    public MutableLiveData<ChecklistEvent> getChecklistEventMutableLiveData() {
        if (checklistEventMutableLiveData == null) {
            checklistEventMutableLiveData = new MutableLiveData<>();
        }
        return checklistEventMutableLiveData;
    }

    public MutableLiveData<Boolean> getIsSubmitSucceedMutableLiveData() {
        if (isSubmitSucceedMutableLiveData == null) {
            isSubmitSucceedMutableLiveData = new MutableLiveData<>();
        }
        return isSubmitSucceedMutableLiveData;
    }

    public MutableLiveData<EventLocation> getLocationEventMutableLiveData() {
        if (locationEventMutableLiveData == null) {
            locationEventMutableLiveData = new MutableLiveData<>();
        }
        return locationEventMutableLiveData;
    }

    public MutableLiveData<List<EventCategory>> getEventCategoryListMutableLiveData() {
        if (eventCategoryListMutableLiveData == null) {
            eventCategoryListMutableLiveData = new MutableLiveData<>();
        }
        return eventCategoryListMutableLiveData;
    }

    public MutableLiveData<Long> getStartTimeMutableLiveData() {
        if (startTimeMutableLiveData == null) {
            startTimeMutableLiveData = new MutableLiveData<>();
        }
        return startTimeMutableLiveData;
    }

    public MutableLiveData<AttendantButton> getAttendantEventMutableLiveData() {
        if (attendantEventMutableLiveData == null) {
            attendantEventMutableLiveData = new MutableLiveData<>();
        }
        return attendantEventMutableLiveData;
    }

    public UserTypeEnum getUserType() {
        return userType;
    }

    public void setUserType(UserTypeEnum userType) {
        this.userType = userType;
    }

    public ReminderEnum getReminderEnum() {
        return reminderEnum;
    }

    public void setReminderEnum(ReminderEnum reminderEnum) {
        this.reminderEnum = reminderEnum;
    }

    public void submitEvent(EventCreateRequest eventCreateRequest) {
        Call<GenericResponse> callEventCreate = ApiClient.INSTANCE.getClient().apiEventCreate(eventCreateRequest);
        callEventCreate.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    GenericResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getIsSubmitSucceedMutableLiveData().postValue(true);
                            break;
                        default:
                            getIsSubmitSucceedMutableLiveData().postValue(false);
                            ToastUtil.INSTANCE.toastMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    getIsSubmitSucceedMutableLiveData().postValue(false);
                    ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                getIsSubmitSucceedMutableLiveData().postValue(false);
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void editEvent(EventEditRequest eventEditRequest) {
        Call<GenericResponse> callEventCreate = ApiClient.INSTANCE.getClient().apiEventEdit(eventEditRequest);
        callEventCreate.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    GenericResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getIsSubmitSucceedMutableLiveData().postValue(true);
                            break;
                        default:
                            getIsSubmitSucceedMutableLiveData().postValue(false);
                            ToastUtil.INSTANCE.toastMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    getIsSubmitSucceedMutableLiveData().postValue(false);
                    ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                getIsSubmitSucceedMutableLiveData().postValue(false);
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callEventView(long calendarId, long userId) {
        Call<EventViewResponse> callEventView = ApiClient.INSTANCE.getClient().apiEventView(calendarId, userId);
        callEventView.enqueue(new Callback<EventViewResponse>() {
            @Override
            public void onResponse(Call<EventViewResponse> call, Response<EventViewResponse> response) {
                if (response.isSuccessful()) {
                    EventViewResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getEventMutableLiveData().postValue(rps.getData());
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<EventViewResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callChecklistEventData(long calendarId) {
        Call<ChecklistEventResponse> callChecklistEvent = ApiClient.INSTANCE.getClient().apiChecklistEvent(calendarId);
        callChecklistEvent.enqueue(new Callback<ChecklistEventResponse>() {
            @Override
            public void onResponse(Call<ChecklistEventResponse> call, Response<ChecklistEventResponse> response) {
                if (response.isSuccessful()) {
                    ChecklistEventResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            ChecklistEvent checklistEvent = rps.getData();
                            checklistEvent.setCalendarId(calendarId);
                            getChecklistEventMutableLiveData().postValue(checklistEvent);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<ChecklistEventResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }

    public void callAttendantEventData(long calendarId) {
        Call<AttendantButtonResponse> callAttendantButton = ApiClient.INSTANCE.getClient().apiAttendantButton(calendarId);
        callAttendantButton.enqueue(new Callback<AttendantButtonResponse>() {
            @Override
            public void onResponse(Call<AttendantButtonResponse> call, Response<AttendantButtonResponse> response) {
                if (response.isSuccessful()) {
                    AttendantButtonResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            AttendantButton attendantButton = rps.getData();
                            attendantButton.setCalendarId(calendarId);
                            getAttendantEventMutableLiveData().postValue(attendantButton);
                            break;
                        default:
                            ToastUtil.INSTANCE.retrofitFailMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    ToastUtil.INSTANCE.retrofitFailMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<AttendantButtonResponse> call, Throwable t) {
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }
}
