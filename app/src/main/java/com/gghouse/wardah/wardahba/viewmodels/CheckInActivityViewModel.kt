package com.gghouse.wardah.wardahba.viewmodels

import android.location.Address
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.common.Config
import com.gghouse.wardah.wardahba.enumerations.GPSStatus
import com.gghouse.wardah.wardahba.utils.ToastUtil
import com.gghouse.wardah.wardahba.webservices.ApiClient
import com.gghouse.wardah.wardahba.webservices.model.CheckInRadius
import com.gghouse.wardah.wardahba.webservices.request.CheckInFCRequest
import com.gghouse.wardah.wardahba.webservices.request.CheckInRequest
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CheckInActivityViewModel : ViewModel() {
    val bpMutableLiveData: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
    val boothMutableLiveData: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
    val addressMutableLiveData: MutableLiveData<Address> by lazy {
        MutableLiveData<Address>()
    }
    val gpsStatusMutableLiveData: MutableLiveData<GPSStatus> by lazy {
        MutableLiveData<GPSStatus>()
    }
    val isSubmitSucceedMutableLiveData: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    var checkInRadius: CheckInRadius? = null
    var checkInLoading: Boolean = false

    fun getData() {
        bpMutableLiveData.postValue(null)
        boothMutableLiveData.postValue(null)
    }

    fun checkIn(checkInRequest: CheckInRequest) {
        val callCheckIn = ApiClient.client.apiCheckIn(checkInRequest)
        callCheckIn.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                        else -> {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.toastMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSucceedMutableLiveData.postValue(false)
                    ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSucceedMutableLiveData.postValue(false)
                ToastUtil.toastMessage(TAG, t.message.toString())
            }
        })
    }

    fun checkInFC(checkInFCRequest: CheckInFCRequest) {
        val callCheckInFC = ApiClient.client.apiFCCheckIn(checkInFCRequest)
        callCheckInFC.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(call: Call<GenericResponse>, response: Response<GenericResponse>) {
                if (response.isSuccessful) {
                    val rps = response.body()
                    when (rps!!.code) {
                        Config.CODE_200 -> isSubmitSucceedMutableLiveData.postValue(true)
                        else -> {
                            isSubmitSucceedMutableLiveData.postValue(false)
                            ToastUtil.toastMessage(TAG, rps.status!!)
                        }
                    }
                } else {
                    isSubmitSucceedMutableLiveData.postValue(false)
                    ToastUtil.toastMessage(TAG, R.string.message_retrofit_error)
                }
            }

            override fun onFailure(call: Call<GenericResponse>, t: Throwable) {
                isSubmitSucceedMutableLiveData.postValue(false)
                ToastUtil.toastMessage(TAG, t.message.toString())
            }
        })
    }

    companion object {
        private val TAG = CheckInActivityViewModel::class.java.simpleName
    }
}
