package com.gghouse.wardah.wardahba.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.models.HomePageListItem;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;

import java.util.List;

public class DailySalesPersonalAdapter extends BaseAdapter {
    private Context mContext;
    private List<HomePageListItem> mList;

    public DailySalesPersonalAdapter(Context c, List<HomePageListItem> homePageListItems) {
        mContext = c;
        mList = homePageListItems;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_homepage_daily_sales, parent, false);

            ImageView mIconImageView = (ImageView) view.findViewById(R.id.ivDailySalesPersonalIcon);
            TextView mAmountTextView = (TextView) view.findViewById(R.id.tvDailySalesPersonalAmount);
            TextView mPcsTextView = (TextView) view.findViewById(R.id.tvDailySalesPersonalPcs);
            HomePageListItem homePageListItem = mList.get(position);
            switch (homePageListItem.getSalesCategory()) {
                case SALES:
                    mIconImageView.setImageDrawable(ImageUtil.INSTANCE.getHomepageSales());
                    break;
                case PRODUCT_FOCUS:
                    mIconImageView.setImageDrawable(ImageUtil.INSTANCE.getHomepageProductFocus());
                    break;
                case PRODUCT_HIGHLIGHT:
                    mIconImageView.setImageDrawable(ImageUtil.INSTANCE.getHomepageProductHighlight());
                    break;
            }
            mAmountTextView.setText(WardahUtil.INSTANCE.getAmount(homePageListItem.getAmount()));
            mPcsTextView.setText(WardahUtil.INSTANCE.getPcs(homePageListItem.getPcs()));
        } else {
            view = convertView;
        }
        return view;
    }
}