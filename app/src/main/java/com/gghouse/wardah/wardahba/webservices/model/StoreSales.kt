package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem

import java.io.Serializable

open class StoreSales(var createdTime: Long?, var id: Long?, var salesAmount: Double?, var userId: Long?, var isEditable: Boolean, var pcs: Int?, var numOfVisitor: Int?, var numOfBuyer: Int?, var buyingPowerList: List<BuyingPower>?) : WardahHistoryRecyclerViewItem(), Serializable
