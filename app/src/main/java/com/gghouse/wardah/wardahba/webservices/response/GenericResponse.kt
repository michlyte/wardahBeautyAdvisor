package com.gghouse.wardah.wardahba.webservices.response

/**
 * Created by michael on 3/13/2017.
 */

open class GenericResponse {
    var status: String? = null
    var code: Int? = null

    constructor() {

    }

    constructor(status: String, code: Int?) {
        this.status = status
        this.code = code
    }
}
