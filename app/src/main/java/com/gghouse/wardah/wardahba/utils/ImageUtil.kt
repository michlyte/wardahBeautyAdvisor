package com.gghouse.wardah.wardahba.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.util.Base64
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.amulyakhare.textdrawable.TextDrawable
import com.gghouse.wardah.wardahba.R
import com.gghouse.wardah.wardahba.WardahApp
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*


/**
 * Created by michaelhalim on 5/6/17.
 */

object ImageUtil {
    var questionnaire = R.drawable.pic_questionnaire
    var test = R.drawable.pic_test
    var sales = R.drawable.pic_sales
    var customer = R.drawable.pic_customer
    var dailyStore = R.drawable.pic_daily_store
    var monthlyStore = R.drawable.pic_monthly_store

    var bannerImage: List<String> = object : ArrayList<String>() {
        init {
            add("https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg")
        }
    }

    var bannerImages: List<String> = object : ArrayList<String>() {
        init {
            add("https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg")
            add("https://assets.materialup.com/uploads/20ded50d-cc85-4e72-9ce3-452671cf7a6d/preview.jpg")
        }
    }

    // Circle Gmail-like in Sales History
    var drawableProductHighlight = TextDrawable.builder()
            .beginConfig()
            .fontSize(28)
            .bold()
            .endConfig()
            .buildRoundRect("H", Color.RED, R.dimen.product_highlight_icon_size)
    var drawableProductFocus = TextDrawable.builder()
            .beginConfig()
            .fontSize(28)
            .bold()
            .endConfig()
            .buildRoundRect("F", Color.BLUE, R.dimen.product_focus_icon_size)

    // Cicle Gmail-like in HomePage
    var homepageSales = TextDrawable.builder()
            .beginConfig()
            .fontSize(40)
            .bold()
            .endConfig()
            .buildRoundRect("S", ContextCompat.getColor(WardahApp.getInstance().appContext, R.color.colorPrimary), R.dimen.homepage_gmail_like_icon_size)
    var homepageProductHighlight = TextDrawable.builder()
            .beginConfig()
            .fontSize(40)
            .bold()
            .endConfig()
            .buildRoundRect("H", Color.RED, R.dimen.homepage_gmail_like_icon_size)
    var homepageProductFocus = TextDrawable.builder()
            .beginConfig()
            .fontSize(40)
            .bold()
            .endConfig()
            .buildRoundRect("F", Color.BLUE, R.dimen.homepage_gmail_like_icon_size)

    // Cicle Gmail-like in HomePage
    var buyingPower1 = TextDrawable.builder()
            .beginConfig()
            .fontSize(40)
            .bold()
            .endConfig()
            .buildRoundRect(WardahApp.getInstance().getString(R.string.title_buying_power_1), ContextCompat.getColor(WardahApp.getInstance().appContext, R.color.colorPrimary), R.dimen.homepage_gmail_like_icon_size)

    var buyingPower2 = TextDrawable.builder()
            .beginConfig()
            .fontSize(40)
            .bold()
            .endConfig()
            .buildRoundRect(WardahApp.getInstance().getString(R.string.title_buying_power_2), ContextCompat.getColor(WardahApp.getInstance().appContext, R.color.colorPrimary), R.dimen.homepage_gmail_like_icon_size)

    var buyingPower3 = TextDrawable.builder()
            .beginConfig()
            .fontSize(40)
            .bold()
            .endConfig()
            .buildRoundRect(WardahApp.getInstance().getString(R.string.title_buying_power_3), ContextCompat.getColor(WardahApp.getInstance().appContext, R.color.colorPrimary), R.dimen.homepage_gmail_like_icon_size)

    var buyingPower4 = TextDrawable.builder()
            .beginConfig()
            .fontSize(40)
            .bold()
            .endConfig()
            .buildRoundRect(WardahApp.getInstance().getString(R.string.title_buying_power_4), ContextCompat.getColor(WardahApp.getInstance().appContext, R.color.colorPrimary), R.dimen.homepage_gmail_like_icon_size)

    var buyingPower5 = TextDrawable.builder()
            .beginConfig()
            .fontSize(40)
            .bold()
            .endConfig()
            .buildRoundRect(WardahApp.getInstance().getString(R.string.title_buying_power_5), ContextCompat.getColor(WardahApp.getInstance().appContext, R.color.colorPrimary), R.dimen.homepage_gmail_like_icon_size)

    fun getBitmap(imageView: ImageView, currentPhotoPath: String): Bitmap {
        // Get the dimensions of the View
        val targetW = imageView.width
        val targetH = imageView.height

        // Get the dimensions of the bitmap
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight

        // Determine how much to scale down the image
        val scaleFactor = Math.min(photoW / targetW, photoH / targetH)

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true

        return BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
    }

    fun setPic(activity: Activity, imageView: ImageView, currentPhotoPath: String) {
        // Get the dimensions of the View
        var targetW = imageView.width
        var targetH = imageView.height

        // Get the dimensions of the bitmap
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight

        // Determine how much to scale down the image
        if (targetW == 0) {
            targetW = ScreenUtil.getWidth(activity).toInt()
        }
        if (targetH == 0) {
            targetH = ScreenUtil.getHeight(activity).toInt()
        }
        val scaleFactor = Math.min(photoW / targetW, photoH / targetH)

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true

        val bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
        imageView.setImageBitmap(bitmap)
    }

    fun galleryAddPic(context: Context, currentPhotoPath: String) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(currentPhotoPath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        context.sendBroadcast(mediaScanIntent)
    }

    fun getImageInBase64String(currentPhotoPath: String): String {
        val bitmap = BitmapFactory.decodeFile(currentPhotoPath)
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos)
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)
    }

    fun getImageUrl(imageUrl: String?): String {
        return SessionUtil.loadIPAddress() + "/" + imageUrl
    }
}
