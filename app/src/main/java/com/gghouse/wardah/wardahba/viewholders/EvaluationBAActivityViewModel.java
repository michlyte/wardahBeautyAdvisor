package com.gghouse.wardah.wardahba.viewholders;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.common.Config;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.models.EvaluationQuestionInner;
import com.gghouse.wardah.wardahba.utils.ToastUtil;
import com.gghouse.wardah.wardahba.webservices.ApiClient;
import com.gghouse.wardah.wardahba.webservices.request.EvaluationAnswerRequest;
import com.gghouse.wardah.wardahba.webservices.response.GenericResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EvaluationBAActivityViewModel extends ViewModel {
    private static final String TAG = EvaluationBAActivityViewModel.class.getSimpleName();

    private MutableLiveData<ViewMode> viewModeMutableLiveData;
    private MutableLiveData<EvaluationQuestionInner> evaluationQuestionsMutableLiveData;
    private MutableLiveData<Boolean> isSubmitSucceedMutableLiveData;

    public MutableLiveData<ViewMode> getViewModeMutableLiveData() {
        if (viewModeMutableLiveData == null) {
            viewModeMutableLiveData = new MutableLiveData<>();
        }
        return viewModeMutableLiveData;
    }

    public MutableLiveData<EvaluationQuestionInner> getEvaluationQuestionsMutableLiveData() {
        if (evaluationQuestionsMutableLiveData == null) {
            evaluationQuestionsMutableLiveData = new MutableLiveData<>();
        }
        return evaluationQuestionsMutableLiveData;
    }

    public MutableLiveData<Boolean> getIsSubmitSucceedMutableLiveData() {
        if (isSubmitSucceedMutableLiveData == null) {
            isSubmitSucceedMutableLiveData = new MutableLiveData<>();
        }
        return isSubmitSucceedMutableLiveData;
    }

    public void submitEvaluation(EvaluationAnswerRequest evaluationAnswerRequest) {
        Call<GenericResponse> callEvaluationSubmit = ApiClient.INSTANCE.getClient().apiEvaluationSubmit(evaluationAnswerRequest);
        callEvaluationSubmit.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    GenericResponse rps = response.body();
                    switch (rps.getCode()) {
                        case Config.CODE_200:
                            getIsSubmitSucceedMutableLiveData().postValue(true);
                            break;
                        default:
                            getIsSubmitSucceedMutableLiveData().postValue(false);
                            ToastUtil.INSTANCE.toastMessage(TAG, rps.getStatus());
                            break;
                    }
                } else {
                    getIsSubmitSucceedMutableLiveData().postValue(false);
                    ToastUtil.INSTANCE.toastMessage(TAG, R.string.message_retrofit_error);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                getIsSubmitSucceedMutableLiveData().postValue(false);
                ToastUtil.INSTANCE.retrofitFailMessage(TAG, t.getMessage());
            }
        });
    }
}
