package com.gghouse.wardah.wardahba.interfaces

import com.gghouse.wardah.wardahba.webservices.model.EventCalendar

interface EventBPListener : EventListener {
    fun onEventClicked(event: EventCalendar)

    fun onChecklistClicked(event: EventCalendar)

    fun onCheckInClicked(event: EventCalendar)

    fun onAttendantClicked(event: EventCalendar)
}
