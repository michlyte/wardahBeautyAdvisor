package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.DailySales
import com.gghouse.wardah.wardahba.webservices.model.Pagination

class DailySalesResponse : GenericResponse() {
    var data: List<DailySales>? = null
    var pagination: Pagination? = null
}
