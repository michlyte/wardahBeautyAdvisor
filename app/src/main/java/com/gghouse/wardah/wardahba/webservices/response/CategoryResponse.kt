package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.Pagination
import com.gghouse.wardah.wardahba.webservices.model.ProductType

class CategoryResponse : GenericResponse() {
    var data: List<ProductType>? = null
    var pagination: Pagination? = null
}
