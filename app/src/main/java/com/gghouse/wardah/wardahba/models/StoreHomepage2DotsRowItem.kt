package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.enumerations.HomepageRecyclerViewItem
import com.gghouse.wardah.wardahba.enumerations.HomepageRecyclerViewItemType

class StoreHomepage2DotsRowItem : HomepageRecyclerViewItem(HomepageRecyclerViewItemType.TWO_DOTS) {
    var leftTitle: String? = null
    var leftValue: String? = null
    var leftDrawableResId: Int = 0
    var rightTitle: String? = null
    var rightValue: String? = null
    var rightDrawableResId: Int = 0
}
