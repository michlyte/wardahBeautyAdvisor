package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.AttendantButton

class AttendantButtonResponse : GenericResponse() {
    var data: AttendantButton? = null
}
