package com.gghouse.wardah.wardahba.models

class ProductHighlightHistoryHeader(var totalIDR: Double?, var totalPcs: Int?, var curMonthIDR: Double?, var curMonthPcs: Int?) : WardahHistoryRecyclerViewItem()
