package com.gghouse.wardah.wardahba.webservices.model

import java.io.Serializable

class BuyingPower : Serializable {
    var less50: Integer?
    var less100: Integer?
    var less150: Integer?
    var less300: Integer?
    var more300: Integer?

    constructor(less50: Integer, less100: Integer, less150: Integer, less300: Integer, more300: Integer) {
        this.less50 = less50
        this.less100 = less100
        this.less150 = less150
        this.less300 = less300
        this.more300 = more300
    }

    val buyingPowerFirstLineStr: String
        get() = "<50K:" + less50.toString() + " | >100K:" + less100.toString() + " | <150K:" + less150.toString()

    val buyingPowerSecondLineStr: String
        get() = "<300K:" + less300.toString() + " | >300K:" + more300.toString()
}