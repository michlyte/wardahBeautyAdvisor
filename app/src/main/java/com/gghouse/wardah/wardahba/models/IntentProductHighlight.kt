package com.gghouse.wardah.wardahba.models

import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight

import java.io.Serializable

class IntentProductHighlight(var objects: List<ProductHighlight>?) : Serializable
