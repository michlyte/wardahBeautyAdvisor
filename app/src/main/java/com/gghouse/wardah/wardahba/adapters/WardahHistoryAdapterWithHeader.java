package com.gghouse.wardah.wardahba.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.WardahApp;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.enumerations.WardahItemType;
import com.gghouse.wardah.wardahba.interfaces.WardahListener;
import com.gghouse.wardah.wardahba.models.WardahHistoryRecyclerViewItem;
import com.gghouse.wardah.wardahba.viewholders.LoadingViewHolder;
import com.gghouse.wardah.wardahba.viewholders.ProductHighlightHistoryHeaderViewHolder;
import com.gghouse.wardah.wardahba.viewholders.ProductHighlightViewHolder;
import com.gghouse.wardah.wardahba.viewholders.SalesHistoryHeaderViewHolder;
import com.gghouse.wardah.wardahba.viewholders.SalesViewHolder;
import com.gghouse.wardah.wardahba.viewholders.StoreDailySalesViewHolder;
import com.gghouse.wardah.wardahba.viewholders.StoreMonthlySalesViewHolder;
import com.gghouse.wardah.wardahba.viewholders.StoreSalesHistoryHeaderViewHolder;
import com.gghouse.wardah.wardahba.viewholders.TestHistoryHeaderViewHolder;
import com.gghouse.wardah.wardahba.viewholders.TestViewHolder;
import com.gghouse.wardah.wardahba.webservices.model.DailySales;
import com.gghouse.wardah.wardahba.webservices.model.MonthlySales;
import com.gghouse.wardah.wardahba.webservices.model.ProductHighlight;
import com.gghouse.wardah.wardahba.webservices.model.Sales;
import com.gghouse.wardah.wardahba.webservices.model.SalesRecap;
import com.gghouse.wardah.wardahba.webservices.model.StoreSalesRecap;
import com.gghouse.wardah.wardahba.webservices.model.Test;
import com.gghouse.wardah.wardahba.webservices.model.TestRecap;

import java.util.List;

public class WardahHistoryAdapterWithHeader extends WardahLoadMoreAdapter {
    private static final String TAG = WardahHistoryAdapterWithHeader.class.getSimpleName();

    private final int VIEW_TYPE_HEADER = 0;
    private final int VIEW_TYPE_ITEM = 1;
    private final int VIEW_TYPE_LOADING = 2;

    private final WardahItemType mType;
    private final Context mContext;
    private final WardahListener mListener;
    private List<WardahHistoryRecyclerViewItem> mValues;

    public WardahHistoryAdapterWithHeader(WardahItemType type,
                                          Context context, RecyclerView recyclerView, List<WardahHistoryRecyclerViewItem> items,
                                          WardahListener wardahListener) {
        super(context, recyclerView);
        mType = type;
        mContext = context;
        mValues = items;
        mListener = wardahListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case VIEW_TYPE_ITEM:
                // Checking item type
                switch (mType) {
                    case SALES:
                    case SALES_FC:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_sales, parent, false);
                        return new SalesViewHolder(view);
                    case TEST:
                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_test, parent, false);
                        return new TestViewHolder(view);
                    case PRODUCT_HIGHLIGHT:
                    case PRODUCT_FOCUS:
                    case STORE_DAILY_PRODUCT_HIGHLIGHT:
                    case STORE_DAILY_PRODUCT_FOCUS:
                    case STORE_MONTHLY_PRODUCT_HIGHLIGHT:
                    case STORE_MONTHLY_PRODUCT_FOCUS:
                    case PRODUCT_HIGHLIGHT_FC:
                    case PRODUCT_FOCUS_FC:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_product_highlight, parent, false);
                        return new ProductHighlightViewHolder(view);
                    case STORE_DAILY_SALES:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_store_sales_harian, parent, false);
                        return new StoreDailySalesViewHolder(view);
                    case STORE_MONTHLY_SALES:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_store_sales_bulanan, parent, false);
                        return new StoreMonthlySalesViewHolder(view);
                    case BA_INFORMATION:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_product_highlight, parent, false);
                        return new ProductHighlightViewHolder(view);
                }
            case VIEW_TYPE_LOADING:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_loading_item, parent, false);
                return new LoadingViewHolder(view);
            case VIEW_TYPE_HEADER:
                // Checking item type
                switch (mType) {
                    case SALES:
                    case SALES_FC:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_sales_history_header_p2, parent, false);
                        return new SalesHistoryHeaderViewHolder(view);
                    case TEST:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_test_history_header, parent, false);
                        return new TestHistoryHeaderViewHolder(view);
                    case PRODUCT_HIGHLIGHT:
                    case PRODUCT_FOCUS:
                    case STORE_DAILY_PRODUCT_HIGHLIGHT:
                    case STORE_DAILY_PRODUCT_FOCUS:
                    case STORE_MONTHLY_PRODUCT_HIGHLIGHT:
                    case STORE_MONTHLY_PRODUCT_FOCUS:
                    case PRODUCT_HIGHLIGHT_FC:
                    case PRODUCT_FOCUS_FC:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_sales_history_header_p2, parent, false);
                        return new ProductHighlightHistoryHeaderViewHolder(view);
                    case STORE_DAILY_SALES:
                    case STORE_MONTHLY_SALES:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_store_sales_history_header, parent, false);
                        return new StoreSalesHistoryHeaderViewHolder(view);
                    case BA_INFORMATION:
                        view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.view_ba_information_overview, parent, false);
                        return new SalesHistoryHeaderViewHolder(view);
                }
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_ITEM:
                // Checking item type
                switch (mType) {
                    case SALES:
                    case SALES_FC:
                        SalesViewHolder salesViewHolder = (SalesViewHolder) holder;
                        salesViewHolder.setData(mContext, (Sales) mValues.get(position), mListener);
                        break;
                    case TEST:
                        final TestViewHolder testViewHolder = (TestViewHolder) holder;
                        testViewHolder.setData(mContext, (Test) mValues.get(position));
                        break;
                    case PRODUCT_HIGHLIGHT:
                    case PRODUCT_FOCUS:
                    case STORE_DAILY_PRODUCT_HIGHLIGHT:
                    case STORE_DAILY_PRODUCT_FOCUS:
                    case STORE_MONTHLY_PRODUCT_HIGHLIGHT:
                    case STORE_MONTHLY_PRODUCT_FOCUS:
                    case PRODUCT_HIGHLIGHT_FC:
                    case PRODUCT_FOCUS_FC:
                        ProductHighlightViewHolder productHighlightViewHolder = (ProductHighlightViewHolder) holder;
                        productHighlightViewHolder.setData(mContext, (ProductHighlight) mValues.get(position), ViewMode.VIEW);
                        break;
                    case STORE_DAILY_SALES:
                        StoreDailySalesViewHolder storeDailySalesViewHolder = (StoreDailySalesViewHolder) holder;
                        storeDailySalesViewHolder.setData(mContext, (DailySales) mValues.get(position), mListener);
                        break;
                    case STORE_MONTHLY_SALES:
                        StoreMonthlySalesViewHolder storeMonthlySalesViewHolder = (StoreMonthlySalesViewHolder) holder;
                        storeMonthlySalesViewHolder.setData(mContext, (MonthlySales) mValues.get(position), mListener);
                        break;
                }
                break;
            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
                break;
            case VIEW_TYPE_HEADER:
                // Checking item type
                switch (mType) {
                    case SALES:
                        SalesHistoryHeaderViewHolder salesHistoryHeaderViewHolder = (SalesHistoryHeaderViewHolder) holder;
                        salesHistoryHeaderViewHolder.setData((SalesRecap) mValues.get(position));
                        break;
                    case TEST:
                        final TestHistoryHeaderViewHolder testHistoryHeaderViewHolder = (TestHistoryHeaderViewHolder) holder;
                        if (mValues.get(position) instanceof TestRecap) {
                            testHistoryHeaderViewHolder.setData((TestRecap) mValues.get(position));
                        } else {
                            Log.e(TAG, WardahApp.getInstance().getGson().toJson(mValues.get(position)) + " cannot be casted to TestRecap.");
                        }
                        break;
                    case PRODUCT_HIGHLIGHT:
                    case PRODUCT_FOCUS:
                    case STORE_DAILY_PRODUCT_HIGHLIGHT:
                    case STORE_DAILY_PRODUCT_FOCUS:
                    case STORE_MONTHLY_PRODUCT_HIGHLIGHT:
                    case STORE_MONTHLY_PRODUCT_FOCUS:
                        ProductHighlightHistoryHeaderViewHolder productHighlightHistoryHeaderViewHolder = (ProductHighlightHistoryHeaderViewHolder) holder;
                        productHighlightHistoryHeaderViewHolder.setData((SalesRecap) mValues.get(position));
                        break;
                    case STORE_DAILY_SALES:
                    case STORE_MONTHLY_SALES:
                        StoreSalesHistoryHeaderViewHolder storeSalesHistoryHeaderViewHolder = (StoreSalesHistoryHeaderViewHolder) holder;
                        storeSalesHistoryHeaderViewHolder.setData((StoreSalesRecap) mValues.get(position));
                        break;
                }
                break;
        }
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return VIEW_TYPE_HEADER;
        } else if (mValues.get(position) == null) {
            return VIEW_TYPE_LOADING;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mValues == null ? 0 : mValues.size();
    }

    public void add(WardahHistoryRecyclerViewItem wardahHistoryRecyclerViewItem) {
        mValues.add(wardahHistoryRecyclerViewItem);
    }

    public void addAll(List<? extends WardahHistoryRecyclerViewItem> wardahHistoryRecyclerViewItemList) {
        mValues.addAll(wardahHistoryRecyclerViewItemList);
    }

    public void remove(int i) {
        mValues.remove(i);
    }

    public void setData(List<WardahHistoryRecyclerViewItem> dataSet) {
        this.mValues = dataSet;
    }

    public List<WardahHistoryRecyclerViewItem> getData() {
        return mValues;
    }

    public void clear() {
        mValues.clear();
    }
}
