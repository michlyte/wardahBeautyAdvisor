package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.SalesTarget

class SalesTargetHistoryResponse : GenericResponse() {
    var data: List<SalesTarget>? = null
}
