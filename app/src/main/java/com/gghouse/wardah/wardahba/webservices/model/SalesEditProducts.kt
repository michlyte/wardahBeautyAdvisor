package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.webservices.request.SalesRequest
import java.io.Serializable

class SalesEditProducts(val salesId: Long, val salesDate: Long, val sales: SalesRequest, val highlight: List<ProductHighlight>, val focus: List<ProductHighlight>, val editable: Boolean) : Serializable