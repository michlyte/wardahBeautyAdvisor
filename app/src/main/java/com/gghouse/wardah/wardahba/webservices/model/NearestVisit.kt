package com.gghouse.wardah.wardahba.webservices.model

import com.gghouse.wardah.wardahba.models.WardahVisit
import com.gghouse.wardah.wardahba.utils.DateTimeUtil
import java.io.Serializable

class NearestVisit : WardahVisit, Serializable {
    val location: VisitLocation
    val dc: String
    val category: String
    val fcImageURL: String?
    val alarmTime: Long
    val totalBA: Int
    val evaluationCompleted: Int
    val checkInOutButtonShown: String
    val checkInOutbuttonEnable: Boolean
    val checkInTime: Long
    val targetSales: Double
    val totalSales: Double
    val storeId: Long

    constructor(startTime: Long, endTime: Long, location: VisitLocation, dc: String, category: String, fcImageURL: String?, alarmTime: Long, totalBA: Int, evaluationCompleted: Int, checkInOutButtonShown: String, checkInOutbuttonEnable: Boolean, checkInTime: Long, targetSales: Double, totalSales: Double, storeId: Long) : super(startTime, endTime) {
        this.location = location
        this.dc = dc
        this.category = category
        this.fcImageURL = fcImageURL
        this.alarmTime = alarmTime
        this.totalBA = totalBA
        this.evaluationCompleted = evaluationCompleted
        this.checkInOutButtonShown = checkInOutButtonShown
        this.checkInOutbuttonEnable = checkInOutbuttonEnable
        this.checkInTime = checkInTime
        this.targetSales = targetSales
        this.totalSales = totalSales
        this.storeId = storeId
    }

    val dateStr: String
        get() = DateTimeUtil.sdfDateMonthYear.format(startTime)

    val timeStr: String
        get() = DateTimeUtil.sdfEventTime.format(startTime) + " - " + DateTimeUtil.sdfEventTime.format(endTime)

    val alarmStr: String
        get() = DateTimeUtil.sdfEventStartEndTime.format(alarmTime)

//    val targetSalesStr: String
//        get() = WardahUtil.getAmount(targetSales)
//
//    val totalSalesStr: String
//        get() = WardahUtil.getAmount(totalSales)
//
    val checkInStr: String
        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkInTime)
//
//    val checkOutStr: String
//        get() = DateTimeUtil.sdfEventCheckInOutTime.format(checkOut)
}