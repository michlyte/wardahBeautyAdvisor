package com.gghouse.wardah.wardahba.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.utils.SystemUtil;
import com.gghouse.wardah.wardahba.webservices.model.SalesRecap;

public class SalesHistoryHeaderViewHolder extends RecyclerView.ViewHolder {

    public final View view;
    public final TextView mTotalIDR;
    public final TextView mTotalPcs;
    public final TextView mCurMonthIDR;
    public final TextView mCurMonthPcs;
    public SalesRecap mItem;

    public SalesHistoryHeaderViewHolder(View view) {
        super(view);

        this.view = view;
        mTotalIDR = view.findViewById(R.id.tvTotalIDR);
        mTotalPcs = view.findViewById(R.id.tvTotalPcs);
        mCurMonthIDR = view.findViewById(R.id.tvCurMonthIDR);
        mCurMonthPcs = view.findViewById(R.id.tvCurMonthPcs);
    }

    public void setData(SalesRecap salesRecap) {
        mItem = salesRecap;

        String totalIDR = SystemUtil.withSuffix(mItem.getAmount() != null ? mItem.getAmount() : 0.00, 0);
        String totalPcs = String.valueOf(mItem.getQuantity() != null ? mItem.getQuantity() : 0);
        String curMonthIDR = SystemUtil.withSuffix(mItem.getThisMonthAmount() != null ? mItem.getThisMonthAmount() : 0.00, 0);
        String curMonthPcs = String.valueOf(mItem.getThisMonthQuantity() != null ? mItem.getThisMonthQuantity() : 0);

        mTotalIDR.setText(totalIDR);
        mTotalPcs.setText(totalPcs);
        mCurMonthIDR.setText(curMonthIDR);
        mCurMonthPcs.setText(curMonthPcs);
    }
}