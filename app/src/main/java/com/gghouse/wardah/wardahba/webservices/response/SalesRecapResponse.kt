package com.gghouse.wardah.wardahba.webservices.response

import com.gghouse.wardah.wardahba.webservices.model.SalesRecap

class SalesRecapResponse {
    var data: SalesRecap? = null
}
