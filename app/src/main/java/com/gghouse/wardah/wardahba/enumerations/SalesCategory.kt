package com.gghouse.wardah.wardahba.enumerations

enum class SalesCategory {
    SALES,
    PRODUCT_HIGHLIGHT,
    PRODUCT_FOCUS
}
