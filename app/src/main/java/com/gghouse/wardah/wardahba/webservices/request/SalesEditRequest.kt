package com.gghouse.wardah.wardahba.webservices.request


class SalesEditRequest(val salesId: Long, val userId: Long, val sales: SalesRequest, val highlight: List<ProductHighlightRequest>, val focus: List<ProductHighlightRequest>, val salesDate: Long)