package com.gghouse.wardah.wardahba.activities.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gghouse.wardah.wardahba.R;
import com.gghouse.wardah.wardahba.activities.event.CalendarMonthlyViewActivity;
import com.gghouse.wardah.wardahba.activities.event.CheckInActivity;
import com.gghouse.wardah.wardahba.activities.event.ChecklistBAHistoryActivity;
import com.gghouse.wardah.wardahba.activities.profile.ProfileActivity;
import com.gghouse.wardah.wardahba.activities.sales.SalesTargetActivity;
import com.gghouse.wardah.wardahba.activities.store.StoreInformationActivity;
import com.gghouse.wardah.wardahba.activities.visit.CreateVisitActivity;
import com.gghouse.wardah.wardahba.adapters.BannerSliderAdapter;
import com.gghouse.wardah.wardahba.adapters.HomepageMenuItemAdapter;
import com.gghouse.wardah.wardahba.enumerations.HomePageMenuEnum;
import com.gghouse.wardah.wardahba.enumerations.ViewMode;
import com.gghouse.wardah.wardahba.utils.ImageUtil;
import com.gghouse.wardah.wardahba.utils.IntentUtil;
import com.gghouse.wardah.wardahba.utils.SessionUtil;
import com.gghouse.wardah.wardahba.utils.WardahUtil;
import com.gghouse.wardah.wardahba.viewmodels.HomeFCActivityViewModel;
import com.gghouse.wardah.wardahba.webservices.model.FCHome;
import com.gghouse.wardah.wardahba.webservices.model.LatestVisit;
import com.gghouse.wardah.wardahba.webservices.model.NearestVisit;

import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.Slider;

public class HomeFCActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private static final String TAG = HomeFCActivity.class.getSimpleName();
    private static final int CHECK_IN_RESULT_ACTIVITY = 5;

    private HomeFCActivityViewModel mModel;

    private SwipeRefreshLayout mSwipeRefresh;

    private TextView mUsername;
    private GridView mMenuGridView;
    private HomepageMenuItemAdapter mMenuAdapter;

    /**
     * Nearest Visit
     */
    private View mNearestVisitCardView;
    private TextView mNearestVisitTitleTextView;
    private TextView mNearestVisitDateTextView;
    private TextView mNearestVisitTimeTextView;
    private TextView mNearestVisitLocationTextView;
    private TextView mNearestVisitAlarmTextView;
    private TextView mNearestVisitCategoryTextView;
    private TextView mNearestVisitTotalBATextView;
    private TextView mNearestVisitTargetSalesTextView;
    private TextView mNearestVisitTotalSalesTextView;
    private TextView mNearestVisitCheckInTextView;
    private TextView mNearestVisitCheckOutTextView;
    private TextView mNearestVisitEvaluationTextView;
    private LinearLayout mNearestVisitButtonLinearLayout;
    private Button mNearestVisitCheckOutButton;
    private Button mNearestVisitInformasiTokoButton;
    private Button mNearestVisitChecklistBAButton;

    /**
     * Kunjungan Terakhir
     */
    private Slider mLatestVisitBannerSlider;
    private BannerSliderAdapter mSliderAdapter;
    private CardView mLatestVisitCardView;
    private TextView mLatestVisitTitleTextView;
    private TextView mLatestVisitDateTextView;
    private TextView mLatestVisitTimeTextView;
    private TextView mLatestVisitLocationTextView;
    private TextView mLatestVisitAlarmTextView;
    private TextView mLatestVisitCategoryTextView;
    private TextView mLatestVisitTotalBATextView;
    private TextView mLatestVisitTargetSalesTextView;
    private TextView mLatestVisitTotalSalesTextView;
    private TextView mLatestVisitCheckInTextView;
    private TextView mLatestVisitCheckOutTextView;
    private TextView mLatestVisitEvaluationTextView;
    private LinearLayout mLatestVisitButtonLinearLayout;
    private Button mLatestVisitCheckOutButton;
    private Button mLatestVisitInformasiTokoButton;
    private Button mLatestVisitChecklistBAButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_fc);

        mSwipeRefresh = findViewById(R.id.swipe_container);
        mSwipeRefresh.setOnRefreshListener(this);

        mMenuGridView = findViewById(R.id.gridview);
        mMenuAdapter = new HomepageMenuItemAdapter(new ArrayList<>());
        mMenuGridView.setAdapter(mMenuAdapter);

        // Homepage menu section
        mUsername = findViewById(R.id.etHeaderUsername);
        TextView lihatProfile = findViewById(R.id.etProfile);
        lihatProfile.setOnClickListener(this);

        bindNearestVisitViews();
        bindLastVisitedViews();

        mModel = ViewModelProviders.of(this).get(HomeFCActivityViewModel.class);

        mModel.getUserMutableLiveData().observe(this, user -> {
            if (user != null) {
                mUsername.setText(user.getFullname());
            } else {
                mUsername.setText(R.string.placeholder_user);
            }

            mModel.getMenuMutableLiveData().postValue(new ArrayList<Integer>() {{
                add(HomePageMenuEnum.VISIT.getResourceId());
                add(HomePageMenuEnum.INPUT_TARGET.getResourceId());
            }});
        });
        mModel.getFcHomeMutableLiveData().observe(this, fcHome -> {
            mSwipeRefresh.setRefreshing(false);

            if (fcHome != null) {
                setFCHomepageData(fcHome);
            }
        });
        mModel.getMenuMutableLiveData().observe(this, menuList -> {
            if (menuList != null) {
                mMenuAdapter.setData(menuList);
                mMenuAdapter.notifyDataSetChanged();

                mMenuGridView.setOnItemClickListener((parent, v, position, id) -> {
                    HomePageMenuEnum homePageMenuEnum = HomePageMenuEnum.Companion.getHomePageMenuEnum(menuList.get(position));
                    switch (homePageMenuEnum) {
                        case VISIT:
                            Intent iVisit = new Intent(getBaseContext(), CalendarMonthlyViewActivity.class);
                            startActivity(iVisit);
                            break;
                        case INPUT_TARGET:
                            Intent iInputTarget = new Intent(getBaseContext(), SalesTargetActivity.class);
                            startActivity(iInputTarget);
                            break;
                    }
                });
            }
        });
        mModel.getVisitViewMutableLiveData().observe(this, visitView -> {
            if (visitView != null) {
                Intent iEditEventTerakhir = new Intent(this, CreateVisitActivity.class);
                iEditEventTerakhir.putExtra(IntentUtil.Companion.getVIEW_MODE(), ViewMode.valueOf(visitView.getViewType()));
                iEditEventTerakhir.putExtra(IntentUtil.Companion.getVISIT(), visitView);
                startActivityForResult(iEditEventTerakhir, CalendarMonthlyViewActivity.EDIT_EVENT_FC_RESULT_ACTIVITY);
            }
        });
        mModel.getCheckInRadiusBPMutableLiveData().observe(this, checkInRadius -> {
            if (checkInRadius != null) {
                Intent iCheckIn = new Intent(getBaseContext(), CheckInActivity.class);
                iCheckIn.putExtra(IntentUtil.Companion.getCHECK_IN_RADIUS(), checkInRadius);
                startActivityForResult(iCheckIn, CHECK_IN_RESULT_ACTIVITY);
            }
        });
        mModel.getStoreInformationMutableLiveData().observe(this, storeInformation -> {
            if (storeInformation != null) {
                Intent iStoreInformation = new Intent(this, StoreInformationActivity.class);
                iStoreInformation.putExtra(IntentUtil.Companion.getSTORE_INFORMATION(), storeInformation);
                startActivity(iStoreInformation);
            }
        });
        mModel.getProfileMutableLiveData().observe(this, profile -> {
            if (profile != null) {
                Intent iProfile = new Intent(this, ProfileActivity.class);
                iProfile.putExtra(IntentUtil.Companion.getDATA(), profile);
                startActivity(iProfile);
            }
        });

        mModel.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_homepage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notification:
                Intent iNotification = new Intent(this, NotificationActivity.class);
                startActivity(iNotification);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        mModel.getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CHECK_IN_RESULT_ACTIVITY && resultCode == Activity.RESULT_OK) {
            mModel.getData();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etProfile:
                mModel.callProfileData();
                break;
                // Check In Out
            case R.id.nearestVisitCheckInOutButton:
                FCHome fcHome = mModel.getFcHomeMutableLiveData().getValue();
                if (fcHome != null) {
                    NearestVisit nearestVisit = fcHome.getNearestVisit();
                    if (nearestVisit != null) {
                        mModel.callCheckInRadius(SessionUtil.getUserTypeEnum(), SessionUtil.getUserId(), nearestVisit.getCalendarId());
                    }
                }
                break;
            case R.id.latestVisitedCheckInOutButton:
                fcHome = mModel.getFcHomeMutableLiveData().getValue();
                if (fcHome != null) {
                    LatestVisit latestVisit = fcHome.getLatestVisit();
                    if (latestVisit != null) {
                        mModel.callCheckInRadius(SessionUtil.getUserTypeEnum(), SessionUtil.getUserId(), latestVisit.getCalendarId());
                    }
                }
                break;
                // Store Information
            case R.id.latestVisitedStoreInformationButton:
                fcHome = mModel.getFcHomeMutableLiveData().getValue();
                if (fcHome != null) {
                    LatestVisit latestVisit = fcHome.getLatestVisit();
                    if (latestVisit != null) {
                        mModel.callStoreInformation(latestVisit.getStoreId());
                    }
                }
                break;
            case R.id.nearestVisitStoreInformationButton:
                fcHome = mModel.getFcHomeMutableLiveData().getValue();
                if (fcHome != null) {
                    NearestVisit nearestVisit = fcHome.getNearestVisit();
                    if (nearestVisit != null) {
                        mModel.callStoreInformation(nearestVisit.getStoreId());
                    }
                }
                break;
                // BA Checklist
            case R.id.nearestVisitChecklistBAButton:
                fcHome = mModel.getFcHomeMutableLiveData().getValue();
                if (fcHome != null) {
                    NearestVisit nearestVisit = fcHome.getNearestVisit();
                    if (nearestVisit != null) {
                        Intent iChecklistBAHistory = new Intent(getBaseContext(), ChecklistBAHistoryActivity.class);
                        iChecklistBAHistory.putExtra(IntentUtil.Companion.getCALENDAR_ID(), nearestVisit.getCalendarId());
                        iChecklistBAHistory.putExtra(IntentUtil.Companion.getLOCATION_ID(), nearestVisit.getLocation().getId());
                        startActivity(iChecklistBAHistory);
                    }
                }
                break;
            case R.id.latestVisitedChecklistBAButton:
                fcHome = mModel.getFcHomeMutableLiveData().getValue();
                if (fcHome != null) {
                    LatestVisit latestVisit = fcHome.getLatestVisit();
                    if (latestVisit != null) {
                        Intent iChecklistBAHistory = new Intent(getBaseContext(), ChecklistBAHistoryActivity.class);
                        iChecklistBAHistory.putExtra(IntentUtil.Companion.getCALENDAR_ID(), latestVisit.getCalendarId());
                        iChecklistBAHistory.putExtra(IntentUtil.Companion.getLOCATION_ID(), latestVisit.getLocation().getId());
                        startActivity(iChecklistBAHistory);
                    }
                }
                break;
                // Layout
            case R.id.nearestVisitLayout:
                if (mModel.getFcHomeMutableLiveData().getValue() != null) {
                    NearestVisit nearestVisit = mModel.getFcHomeMutableLiveData().getValue().getNearestVisit();
                    if (nearestVisit != null) {
                        mModel.callVisitView(nearestVisit.getCalendarId());
                    }
                }
                break;
            case R.id.lastVisitedLayout:
                if (mModel.getFcHomeMutableLiveData().getValue() != null) {
                    LatestVisit latestVisit = mModel.getFcHomeMutableLiveData().getValue().getLatestVisit();
                    if (latestVisit != null) {
                        mModel.callVisitView(latestVisit.getCalendarId());
                    }
                }
                break;
        }
    }

    private void bindNearestVisitViews() {
        mNearestVisitCardView = findViewById(R.id.nearestVisitLayout);
        mNearestVisitCardView.setOnClickListener(this);
        mNearestVisitTitleTextView = findViewById(R.id.nearestVisitTitleTextView);
        mNearestVisitDateTextView = findViewById(R.id.nearestVisitDateTextView);
        mNearestVisitTimeTextView = findViewById(R.id.nearestVisitTimeTextView);
        mNearestVisitLocationTextView = findViewById(R.id.nearestVisitDcTextView);
        mNearestVisitAlarmTextView = findViewById(R.id.nearestVisitAlarmTextView);
        mNearestVisitCategoryTextView = findViewById(R.id.nearestVisitCategoryTextView);
        mNearestVisitTotalBATextView = findViewById(R.id.nearestVisitTotalBATextView);
        mNearestVisitTargetSalesTextView = findViewById(R.id.nearestVisitTargetSalesTextView);
        mNearestVisitTotalSalesTextView = findViewById(R.id.nearestVisitTotalSalesTextView);
        mNearestVisitCheckInTextView = findViewById(R.id.nearestVisitCheckInTextView);
        mNearestVisitCheckOutTextView = findViewById(R.id.nearestVisitCheckOutTextView);
        mNearestVisitEvaluationTextView = findViewById(R.id.nearestVisitEvaluationTextView);
        mNearestVisitButtonLinearLayout = findViewById(R.id.nearestVisitButtonLinearLayout);

        mNearestVisitCheckOutButton = findViewById(R.id.nearestVisitCheckInOutButton);
        mNearestVisitCheckOutButton.setOnClickListener(this);
        mNearestVisitInformasiTokoButton = findViewById(R.id.nearestVisitStoreInformationButton);
        mNearestVisitInformasiTokoButton.setOnClickListener(this);
        mNearestVisitChecklistBAButton = findViewById(R.id.nearestVisitChecklistBAButton);
        mNearestVisitChecklistBAButton.setOnClickListener(this);
    }

    private void bindLastVisitedViews() {
        mLatestVisitBannerSlider = findViewById(R.id.latestVisitedBannerSlider);
        mSliderAdapter = new BannerSliderAdapter(ImageUtil.INSTANCE.getBannerImage());
        mLatestVisitBannerSlider.setAdapter(mSliderAdapter);
        mLatestVisitBannerSlider.setOnSlideClickListener(position -> {
            Intent iImagePreview = new Intent(this, ImagePreviewActivity.class);
            iImagePreview.putExtra(IntentUtil.Companion.getDATA(), mSliderAdapter.getUrl(position));
            startActivity(iImagePreview);
        });

        mLatestVisitCardView = findViewById(R.id.lastVisitedLayout);
        mLatestVisitCardView.setOnClickListener(this);
        mLatestVisitTitleTextView = findViewById(R.id.latestVisitedTitleTextView);
        mLatestVisitDateTextView = findViewById(R.id.latestVisitedDateTextView);
        mLatestVisitTimeTextView = findViewById(R.id.latestVisitedTimeTextView);
        mLatestVisitLocationTextView = findViewById(R.id.latestVisitedDcTextView);
        mLatestVisitAlarmTextView = findViewById(R.id.latestVisitedAlarmTextView);
        mLatestVisitCategoryTextView = findViewById(R.id.latestVisitedCategoryTextView);
        mLatestVisitTotalBATextView = findViewById(R.id.latestVisitedTotalBATextView);
        mLatestVisitTargetSalesTextView = findViewById(R.id.latestVisitedTargetSalesTextView);
        mLatestVisitTotalSalesTextView = findViewById(R.id.latestVisitedTotalSalesTextView);
        mLatestVisitCheckInTextView = findViewById(R.id.latestVisitedCheckInTextView);
        mLatestVisitCheckOutTextView = findViewById(R.id.latestVisitedCheckOutTextView);
        mLatestVisitEvaluationTextView = findViewById(R.id.latestVisitedEvaluationTextView);
        mLatestVisitButtonLinearLayout = findViewById(R.id.latestVisitedButtonLinearLayout);

        mLatestVisitCheckOutButton = findViewById(R.id.latestVisitedCheckInOutButton);
        mLatestVisitCheckOutButton.setOnClickListener(this);
        mLatestVisitInformasiTokoButton = findViewById(R.id.latestVisitedStoreInformationButton);
        mLatestVisitInformasiTokoButton.setOnClickListener(this);
        mLatestVisitChecklistBAButton = findViewById(R.id.latestVisitedChecklistBAButton);
        mLatestVisitChecklistBAButton.setOnClickListener(this);
    }

    private void setFCHomepageData(FCHome fcHome) {
        NearestVisit nearestVisit = fcHome.getNearestVisit();
        if (nearestVisit != null) {
            findViewById(R.id.nearestVisitLayout).setVisibility(View.VISIBLE);

            if (nearestVisit.getLocation() != null) {
                mNearestVisitTitleTextView.setText(nearestVisit.getLocation().getName());
            }
            mNearestVisitDateTextView.setText(nearestVisit.getDateStr());
            mNearestVisitTimeTextView.setText(nearestVisit.getTimeStr());
            mNearestVisitAlarmTextView.setText(nearestVisit.getAlarmStr());
            mNearestVisitLocationTextView.setText(nearestVisit.getDc());
            mNearestVisitCategoryTextView.setText(nearestVisit.getCategory());
            mNearestVisitCheckInTextView.setText(nearestVisit.getCheckInStr());
            mNearestVisitTotalBATextView.setText(String.valueOf(nearestVisit.getTotalBA()));
            mNearestVisitEvaluationTextView.setText(String.valueOf(nearestVisit.getEvaluationCompleted()));

            mNearestVisitCheckOutButton.setEnabled(nearestVisit.getCheckInOutbuttonEnable());
            mNearestVisitCheckOutButton.setText(nearestVisit.getCheckInOutButtonShown());
            mLatestVisitInformasiTokoButton.setEnabled(true);
            mLatestVisitChecklistBAButton.setEnabled(true);
            WardahUtil.INSTANCE.setEventButtonStyle(this, mLatestVisitCheckOutButton, nearestVisit.getCheckInOutbuttonEnable());
            WardahUtil.INSTANCE.setEventButtonStyle(this, mLatestVisitInformasiTokoButton, true);
            WardahUtil.INSTANCE.setEventButtonStyle(this, mLatestVisitChecklistBAButton, true);
        }

        LatestVisit latestVisit = fcHome.getLatestVisit();
        if (latestVisit != null) {
            findViewById(R.id.lastVisitedLayout).setVisibility(View.VISIBLE);

            if (latestVisit.getFcImageURL() == null) {
                mLatestVisitBannerSlider.setVisibility(View.GONE);
            } else {
                List<String> latestEventImages = new ArrayList<String>() {{
                    add(ImageUtil.INSTANCE.getImageUrl(latestVisit.getFcImageURL()));
                }};
                mSliderAdapter.setData(latestEventImages);
                mLatestVisitBannerSlider.setAdapter(mSliderAdapter);
                mLatestVisitBannerSlider.setVisibility(View.VISIBLE);
            }
            mLatestVisitTitleTextView.setText(latestVisit.getLocation().getName());
            mLatestVisitDateTextView.setText(latestVisit.getDateStr());
            mLatestVisitTimeTextView.setText(latestVisit.getTimeStr());
            mLatestVisitAlarmTextView.setText(latestVisit.getAlarmStr());
            mLatestVisitLocationTextView.setText(latestVisit.getDcName());
            mLatestVisitCategoryTextView.setText(latestVisit.getCategory());
            mLatestVisitTotalBATextView.setText(String.valueOf(latestVisit.getTotalBA()));
//            mLatestVisitTargetSalesTextView.setText(lastVisited.getTargetSalesStr());
//            mLatestVisitTotalSalesTextView.setText(lastVisited.getTotalSalesStr());
//            mLatestVisitCheckInTextView.setText(lastVisited.getCheckInStr());
//            mLatestVisitCheckOutTextView.setText(lastVisited.getCheckOutStr());
            mLatestVisitEvaluationTextView.setText(String.valueOf(latestVisit.getEvaluationCompleted()));

            mLatestVisitCheckOutButton.setEnabled(latestVisit.getCheckInOutbuttonEnable());
            mLatestVisitCheckOutButton.setText(latestVisit.getCheckInOutButtonShown());
            mLatestVisitInformasiTokoButton.setEnabled(true);
            mLatestVisitChecklistBAButton.setEnabled(true);

            WardahUtil.INSTANCE.setEventButtonStyle(this, mLatestVisitCheckOutButton, latestVisit.getCheckInOutbuttonEnable());
            WardahUtil.INSTANCE.setEventButtonStyle(this, mLatestVisitInformasiTokoButton, true);
            WardahUtil.INSTANCE.setEventButtonStyle(this, mLatestVisitChecklistBAButton, true);

        }
    }
}
